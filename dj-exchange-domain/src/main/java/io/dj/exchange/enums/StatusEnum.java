package io.dj.exchange.enums;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 19.
 * Description
 */
public enum StatusEnum {

    PENDING, COMPLETED, CANCEL, PROGRESS, ANSWERED, USE

}
