package io.dj.exchange.domain.primary;

import io.dj.exchange.enums.StatusEnum;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "support_qna")
@Data
@IdClass(QnaPK.class)
public class SupportQna implements Serializable{

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Id
    @Column(name = "user_id")
    private Long userId;

    @Transient
    private List<SupportQna> supportQna;

    @Column(name = "title")
    private String title;

    @Column(name = "contents")
    private String contents;

    @Column(name = "reg_dt")
    private LocalDateTime regDt;

    @Column(name = "mod_dt")
    private LocalDateTime modDt;

    @Column(name = "show_yn")
    private String showYn;

    @Id
    @Column(name = "parent_id")
    private Long parentId;

    @ManyToOne
    @JoinColumn(name="userId", referencedColumnName = "id", insertable = false, updatable = false)
    private User user;

    @Column(name="category")
    private String category;

    @Column(name="status")
    private String status;

    @Column(name = "read_yn")
    private String readYn;

    @Transient
    private int unreadCnt;

    @Transient
    private String regDtTxt;
    @Transient
    private String modDtTxt;

    public void setRegDtTxt(String regDtTxt) {
        this.regDtTxt = regDtTxt;
    }

    public void setModDtTxt(String modDtTxt) {
        this.modDtTxt = modDtTxt;
    }
}
