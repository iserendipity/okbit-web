package io.dj.exchange.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 7. 14.
 * Description
 */
@Data
public class ResCoinAssets {

    private String label;
    private Double data;
    private String color;

    public ResCoinAssets(){

    }

    public ResCoinAssets(String label, Double data, String color){
        this.label = label;
        this.data = data;
        this.color = color;
    }

}
