#!/bin/sh -xe

echo "compile ....."
./gradlew clean build -x test

cd /Users/jeongwoo/IdeaProjects/okbit-web/dj-exchange-admin/build/libs

ssh -i /Users/jeongwoo/keyholder/okbit-web.pem ubuntu@52.79.174.83 "sudo cp /home/ubuntu/exchange-admin.war /home/ubuntu/exchange-admin.war.bak"
scp -i /Users/jeongwoo/keyholder/okbit-web.pem exchange-admin.war ubuntu@52.79.174.83:/home/ubuntu
ssh -i /Users/jeongwoo/keyholder/okbit-web.pem ubuntu@52.79.174.83 "sudo nohup /data/services/startadmin.sh > /dev/null 2>&1 &"


