package io.dj.exchange.controller.api;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimaps;
import io.dj.exchange.S3Wrapper;
import io.dj.exchange.annotation.TransactionalEx;
import io.dj.exchange.domain.dto.*;
import io.dj.exchange.domain.primary.*;
import io.dj.exchange.domain.readonly.BankCode;
import io.dj.exchange.domain.readonly.MarketHistoryOrders;
import io.dj.exchange.domain.readonly.WalletTransaction;
import io.dj.exchange.enums.*;
import io.dj.exchange.exception.OkbitException;
import io.dj.exchange.dto.ResCoinAssets;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.repository.hibernate.primary.*;
import io.dj.exchange.repository.hibernate.readonly.BankCodeRepository;
import io.dj.exchange.repository.hibernate.readonly.MarketOrdersRepository;
import io.dj.exchange.repository.hibernate.readonly.WalletTransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 24.
 * Description
 */
@Slf4j
@RestController
@RequestMapping(value = "/api")
public class ApiController {

    private final SupportQnaRepository supportQnaRepository;
    private final ManualTransactionsRepository confirmRepository;
    private final AdminLevelRepository adminLevelRepository;
    private final WalletRepository walletRepository;
    private final WrongDepositRepository wrongDepositRepository;
    private final UserRepository userRepository;
    private final ApiProvider apiProvider;
    private final CoinRepository coinRepository;
    private final UserSettingRepository userSettingRepository;
    private final ApiProvider adminProvider;
    private final BankCodeRepository bankCodeRepository;
    private final OrderRepository orderRepository;
    private final MarketOrdersRepository marketOrdersRepository;
    private final WalletTransactionRepository walletTransactionRepository;

    @Autowired
    private S3Wrapper s3Wrapper;

//    @Autowired
//    private MailService mailService;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    public ApiController(SupportQnaRepository supportQnaRepository,
                         ManualTransactionsRepository confirmRepository,
                         AdminLevelRepository adminLevelRepository,
                         WalletRepository walletRepository,
                         WrongDepositRepository wrongDepositRepository,
                         UserRepository userRepository,
                         ApiProvider apiProvider,
                         CoinRepository coinRepository,
                         UserSettingRepository userSettingRepository,
                         ApiProvider adminProvider,
                         BankCodeRepository bankCodeRepository,
                         OrderRepository orderRepository,
                         MarketOrdersRepository marketOrdersRepository,
                         WalletTransactionRepository walletTransactionRepository)
    {
        this.supportQnaRepository = supportQnaRepository;
        this.confirmRepository = confirmRepository;
        this.adminLevelRepository = adminLevelRepository;
        this.walletRepository = walletRepository;
        this.wrongDepositRepository = wrongDepositRepository;
        this.userRepository = userRepository;
        this.apiProvider = apiProvider;
        this.coinRepository = coinRepository;
        this.userSettingRepository = userSettingRepository;
        this.adminProvider = adminProvider;
        this.bankCodeRepository = bankCodeRepository;
        this.orderRepository = orderRepository;
        this.marketOrdersRepository = marketOrdersRepository;
        this.walletTransactionRepository = walletTransactionRepository;
    }

    @RequestMapping(value = "/health")
    public String health(){
        return "200";
    }

    @PostMapping(value = "/console/questions", produces = "application/json")
    public PagedResources<SupportQna> getQuestions(Pageable p, PagedResourcesAssembler assembler){

        Page<SupportQna> result = supportQnaRepository.findAllByShowYn("Y",p);

        result.getContent().stream().forEach(data ->{

            data.setRegDtTxt(data.getRegDt().format(formatter));
            //data.setModDtTxt(data.getModDt().format(formatter));

        });


        return assembler.toResource(result);
    }

    @PostMapping(value = "/console/reqWithdraws", produces = "application/json")
    public PagedResources<ManualTransactions> reqWithdraws(Pageable p, PagedResourcesAssembler assembler){

        Page<ManualTransactions> result = confirmRepository.findAllByIsConfirmationAndStatusAndCategoryAndCoinNameOrderByRegDtDesc("N","PENDING", "send", "KRW", p);

        result.getContent().stream().forEach(data -> {
            data.setRegDtTxt(data.getRegDt().format(formatter));
            Coin coin = coinRepository.findOneByName("KRW");
            data.setAmount(data.getAmount().subtract(coin.getWithdrawalTxFee()));

        });

        return assembler.toResource(result);
    }

    @PostMapping(value = "/console/reqCoinWithdraws", produces = "application/json")
    public PagedResources<ManualTransactions> reqWithdrawsCoins(Pageable p, PagedResourcesAssembler assembler){

        Page<ManualTransactions> result = confirmRepository.findAllByIsConfirmationAndStatusAndCategoryAndCoinNameNotOrderByRegDtDesc("N","PENDING", "send", "KRW", p);

        result.getContent().stream().forEach(data -> {
            data.setRegDtTxt(data.getRegDt().format(formatter));

        });

        return assembler.toResource(result);
    }

    @PostMapping(value = "/console/reqDeposit", produces = "application/json")
    public PagedResources<ManualTransactions> reqDeposit(Pageable p, PagedResourcesAssembler assembler){

        Page<ManualTransactions> result = confirmRepository.findAllByIsConfirmationAndStatusAndCategoryAndCoinNameOrderByRegDtDesc("N","PENDING", "receive", "KRW", p);

        result.getContent().stream().forEach(data -> {
            data.setRegDtTxt(data.getRegDt().format(formatter));
            if(data.getDepositDt() != null) {
                data.setDepositDtTxt(data.getDepositDt().format(formatter));
            }

            //wallet의 deposit dvcd로 유저아이디 조회
            //유저아이디 유저 정보조회
            //유저정보의 adminlevel의 realname 가져옴
            Wallet wallet = walletRepository.findOneByCoinNameAndDepositDvcd("KRW",data.getUserNm()).get();
            User user = userRepository.findOne(wallet.getUserId());
            AdminLevel level = user.getAdminLevel();

            if(level != null && level.getLevel() != null){
                if(level.getLevel().equals(UserLevels.LEV2.name()) || level.getLevel().equals(UserLevels.LEV3.name())) {
                    data.setRealName(level.getRealName());
                    //data.setLevel();
                }
            }

            data.setLevel(user.getUserSetting().getLevel());

        });

        return assembler.toResource(result);
    }
    @PostMapping(value = "/console/getWrongDeposit", produces = "application/json")
    public PagedResources<WrongDeposit> getWrongDeposit(Pageable p, PagedResourcesAssembler assembler){

        Page<WrongDeposit> result = wrongDepositRepository.findAllByStatusOrderByIdAsc("PENDING", p);

        result.getContent().stream().forEach(data -> {
            if(data.getDepositDt() != null) {
                data.setDepositDtTxt(data.getDepositDt().format(formatter));
            }
        });

        return assembler.toResource(result);
    }
    @PostMapping(value = "/console/getReqVerify", produces = "application/json")
    public PagedResources<AdminLevel> getReqVerify(Pageable p, PagedResourcesAssembler assembler){

        Page<AdminLevel> result = adminLevelRepository.findAllByBankbookYnOrderByBankbookRegDtAsc("P", p);
        //Page<ManualTransactions> result = confirmRepository.findAllByIsConfirmationAndStatusAndCategoryOrderByRegDtDesc("N","PENDING", "receive", p);

        result.getContent().stream().forEach(data -> {
            data.setEmail(userRepository.findOne(data.getUserId()).getEmail());
            if(data.getBankbookRegDt() != null) {
                data.setBankbookRegDtTxt(data.getBankbookRegDt().format(formatter));
            }

        });

        return assembler.toResource(result);
    }

    @PostMapping(value = "/console/getReqIdVerify", produces = "application/json")
    public PagedResources<AdminLevel> getReqIdVerify(Pageable p, PagedResourcesAssembler assembler){

        Page<AdminLevel> result = adminLevelRepository.findAllByIdcardYnOrderByIdcardRegDtAsc("P", p);
        //Page<ManualTransactions> result = confirmRepository.findAllByIsConfirmationAndStatusAndCategoryOrderByRegDtDesc("N","PENDING", "receive", p);

        result.getContent().stream().forEach(data -> {
            data.setEmail(userRepository.findOne(data.getUserId()).getEmail());
            if(data.getIdcardRegDt() != null) {
                data.setIdcardRegDtTxt(data.getIdcardRegDt().format(formatter));
            }

        });

        return assembler.toResource(result);
    }

    @PostMapping(value = "/console/bankbooks")
    public Response<String> bankbooks(@RequestParam long userId) throws OkbitException, IOException{

        AdminLevel result = adminLevelRepository.findOneByUserId(userId).orElseThrow(() -> new OkbitException("NO_BANK_BOOK_APPROVED"));

        if(!"Y".equals(result.getBankbookYn())){

            throw new OkbitException("NO_BANK_BOOK_APPROVED");

        }

        String encodedArr = Base64.getEncoder().encodeToString(s3Wrapper.downloadImgByteArr(result.getBankbook()));

        return Response.<String>builder().code(CodeEnum.SUCCESS).data(encodedArr).build();

    }

    @PostMapping(value = "/console/idCard")
    public Response<String> idCard(@RequestParam long userId) throws OkbitException, IOException{

        AdminLevel result = adminLevelRepository.findOneByUserId(userId).orElseThrow(() -> new OkbitException("NO_ID_CARD_APPROVED"));

        if(!"Y".equals(result.getIdcardYn())){

            throw new OkbitException("NO_ID_CARD_APPROVED");

        }

        return Response.<String>builder().code(CodeEnum.SUCCESS).data(Base64.getEncoder().encodeToString(s3Wrapper.downloadImgByteArr(result.getIdcard()))).build();

    }

    @PostMapping(value = "/console/getBankbook")
    public Response<String> getBankbook(@RequestParam long userId) throws OkbitException, IOException{

        AdminLevel result = adminLevelRepository.findOneByUserIdAndBankbookYn(userId, "P").orElseThrow(() -> new OkbitException("NO_BANK_BOOK_APPROVED"));
        String encoded = Base64.getEncoder().encodeToString(s3Wrapper.downloadImgByteArr(result.getBankbook()));
        return Response.<String>builder().code(CodeEnum.SUCCESS).data(encoded).build();

    }

    @PostMapping(value = "/console/approveWrongDeposit")
    @TransactionalEx
    public Response<String> approveWrongDeposit(@Valid @RequestBody AdminDTO.approveWrongDepositRequest req) throws OkbitException{

        User user = userRepository.findOneByEmail(req.getEmail()).orElseThrow(() -> new OkbitException("이메일 주소를 확인하세요."));

        Wallet wallet = walletRepository.findOneByUserIdAndCoinName(user.getId(), "KRW").orElseThrow(()-> new OkbitException("해당 이메일 주소는 원화지갑을 가지고 있지 않습니다."));

        WrongDeposit wrongDeposit = wrongDepositRepository.findOne(req.getId());

//        ManualTransactions mt = new ManualTransactions();
//        mt.setIsConfirmation("N");
//        mt.setUserId(user.getId());
//        mt.setStatus("PENDING");
//        mt.setRegDt(LocalDateTime.now());
//        mt.setCoinName("KRW");
//        mt.setCategory("receive");
//        mt.setBankNm(wrongDeposit.getBankNm());
//        mt.setUserNm(wrongDeposit.getUserNm());
//        mt.setAmount(wrongDeposit.getAmount());
//        mt.setTxId(RandomStringUtils.randomAlphanumeric(64));
//
//        confirmRepository.save(mt);

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.put(ACCEPT, singletonList(APPLICATION_JSON_VALUE));
        headers.put(CONTENT_TYPE, singletonList(APPLICATION_JSON_VALUE));
        headers.put("apiKey", singletonList(user.getUserSetting().getApiKey()));

        adminProvider.reqReceive(headers, "KRW", wrongDeposit.getAmount(), wallet.getDepositDvcd(), wrongDeposit.getBankNm(), "11", wrongDeposit.getDepositDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), "11", "11");

        wrongDeposit.setStatus("COMPLETED");

        wrongDepositRepository.save(wrongDeposit);

        return Response.<String>builder().code(CodeEnum.SUCCESS).build();

    }

    @TransactionalEx
    public void updateUserLevel(AdminLevel adminLevel){

        UserSetting userSetting = userSettingRepository.findOneByUserId(adminLevel.getUserId());

        if(adminLevel.getIdcardYn() != null && "Y".equals(adminLevel.getIdcardYn()) && adminLevel.getBankbookYn() != null && "Y".equals(adminLevel.getBankbookYn())){

            userSetting.setLevel(UserLevels.LEV2.name());
            adminLevel.setLevelModifyDt(LocalDateTime.now());
            adminLevel.setLevel(UserLevels.LEV2.name());

        }

    }


    @PostMapping(value = "/console/approveBank")
    @TransactionalEx
    public Response<String> approveBank(@Valid @RequestBody AdminDTO.approveBankRequest req) throws OkbitException{

        AdminLevel result = adminLevelRepository.findOneByUserId(req.getUserId()).orElseThrow(() -> new OkbitException("UNKNOWN_ERROR"));

        if(!req.getRealNm().equals(result.getRealName())){
            throw new OkbitException("인증받은 실명이 아닙니다.");
        }

        BankCode bankCode = bankCodeRepository.findOne(req.getWithdrawBankCode());

        result.setWithdrawBankAccount(req.getWithdrawBankAccount());
        result.setWithdrawBankNm(bankCode.getBankName());
        result.setRealName(req.getRealNm());
        result.setBankbookYn("Y");
        result.setWithdrawBankCode(req.getWithdrawBankCode());

        updateUserLevel(result);

        //adminLevelRepository.save(result);
        //AdminLevel result = adminLevelRepository.findOneByUserIdAndBankbookYn(userId, "N").orElseThrow(() -> new OkbitException("NO_BANK_BOOK_APPROVED"));

        return Response.<String>builder().code(CodeEnum.SUCCESS).build();

    }

    @PostMapping(value = "/console/approveId")
    @TransactionalEx
    public Response<String> approveId(@RequestParam long userId, @RequestParam String realName) throws OkbitException{

        AdminLevel result = adminLevelRepository.findOneByUserId(userId).orElseThrow(() -> new OkbitException("UNKNOWN_ERROR"));
        if(!realName.equals(result.getRealName())){
            throw new OkbitException("인증받은 실명이 아닙니다.");
        }
        result.setIdcardYn("Y");
        //adminLevelRepository.save(result);
        //AdminLevel result = adminLevelRepository.findOneByUserIdAndBankbookYn(userId, "N").orElseThrow(() -> new OkbitException("NO_BANK_BOOK_APPROVED"));

        updateUserLevel(result);

        return Response.<String>builder().code(CodeEnum.SUCCESS).build();

    }

    @PostMapping(value = "/console/getIdcard")
    public Response<String> getIdCard(@RequestParam long userId) throws OkbitException, IOException{

        AdminLevel result = adminLevelRepository.findOneByUserIdAndIdcardYn(userId, "P").orElseThrow(() -> new OkbitException("NO_ID_CARD_APPROVED"));

        return Response.<String>builder().code(CodeEnum.SUCCESS).data(Base64.getEncoder().encodeToString(s3Wrapper.downloadImgByteArr(result.getIdcard()))).build();

    }

    @PostMapping(value = "/console/approveTransaction")
    public Response<String> approveTransaction(@RequestParam String id) throws OkbitException{

        ManualTransactions t = confirmRepository.findOneById(id);
        User user = userRepository.findOne(t.getUserId());
//
//        t.setIsConfirmation("Y");
//        confirmRepository.save(t);

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.put(ACCEPT, singletonList(APPLICATION_JSON_VALUE));
        headers.put(CONTENT_TYPE, singletonList(APPLICATION_JSON_VALUE));
        headers.put("apiKey", singletonList(user.getUserSetting().getApiKey()));

        adminProvider.confirmManualTransactions(headers, id, CoinEnum.KRW, StatusEnum.COMPLETED);

        return Response.<String>builder().code(CodeEnum.SUCCESS).build();

    }

    @PostMapping(value = "/console/approveSendTransaction")
    public Response<String> approveSendTransaction(@RequestParam String id) throws OkbitException{

        ManualTransactions t = confirmRepository.findOneById(id);
//
        if(t.getCoinName().equals("KRW")) {

            User user = userRepository.findOne(t.getUserId());

            MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.put(ACCEPT, singletonList(APPLICATION_JSON_VALUE));
            headers.put(CONTENT_TYPE, singletonList(APPLICATION_JSON_VALUE));
            headers.put("apiKey", singletonList(user.getUserSetting().getApiKey()));

            adminProvider.confirmManualTransactionsSend(headers, id, t.getCoinName(), StatusEnum.COMPLETED);
        }else{

            t.setIsConfirmation("Y");
            //t.setStatus(StatusEnum.COMPLETED.name());
            confirmRepository.save(t);

        }

        return Response.<String>builder().code(CodeEnum.SUCCESS).build();

    }

    @TransactionalEx
    @PostMapping(value = "/console/reasonTransaction")
    public Response<String> reasonTransaction(@RequestParam("id") String id, @RequestParam("reason") String reason) throws OkbitException{

        ManualTransactions t = confirmRepository.findOneById(id);

        t.setReason(reason);
        t.setStatus(StatusEnum.CANCEL.name());

        confirmRepository.save(t);

        User user = userRepository.findOne(t.getUserId());

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.put(ACCEPT, singletonList(APPLICATION_JSON_VALUE));
        headers.put(CONTENT_TYPE, singletonList(APPLICATION_JSON_VALUE));
        headers.put("apiKey", singletonList(user.getUserSetting().getApiKey()));

        adminProvider.cancelSendKrw(headers, id, "KRW");

        return Response.<String>builder().code(CodeEnum.SUCCESS).build();

    }

    @TransactionalEx
    @PostMapping(value = "/console/reasonDepTransaction")
    public Response<String> reasonDepTransaction(@RequestParam("id") String id, @RequestParam("reason") String reason) throws OkbitException{

        ManualTransactions t = confirmRepository.findOneById(id);

        t.setReason(reason);
        t.setStatus(StatusEnum.CANCEL.name());

        confirmRepository.save(t);

        return Response.<String>builder().code(CodeEnum.SUCCESS).build();

    }

    @TransactionalEx
    @PostMapping(value = "/console/sendExcel")
    public Response<String> sendExcel(@RequestBody ExcelDTO.uploadRequest req) throws OkbitException{

        if(req != null && req.getSheet1() != null && req.getSheet1().size() > 0){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss");

            List<ExcelDTO.Cells> reqList = req.getSheet1();

            ListIterator<ExcelDTO.Cells> it = reqList.listIterator();

            while(it.hasNext()){
                int firstIndex = reqList.indexOf(it.next());
                int lastIndex = reqList.lastIndexOf(reqList.get(firstIndex));

                if(firstIndex != lastIndex){
                    throw new OkbitException((lastIndex+1)+"번째 줄에 중복된 입금건이 있습니다. 엑셀파일을 확인하세요.");
                }
            }

            boolean duplicate = reqList.stream()
                    .distinct()
                    .count() != reqList.size();

            if(duplicate){

                throw new OkbitException("중복된 입금건이 있습니다. 엑셀파일을 확인하세요.");

            }

            //지갑에 있는 depositDvcd -> manualTransaction
            //없는 거면 -> wrong_deposit
            int i =0;
            for(ExcelDTO.Cells cell : req.getSheet1()){

                //db에 krw, category, userNm, regDt, amount
                i++;

                if (confirmRepository.findAllByCoinNameAndCategoryAndUserNmAndDepositDtAndAmountAndBankNm("KRW", "receive", cell.getDepositDvcd(), LocalDateTime.parse(cell.getDate(), formatter), new BigDecimal(cell.getAmount().replaceAll(",", "")), cell.getBank()).isPresent()) {
                    throw new OkbitException("엑셀의 "+i + "번째 줄에 기존에 입금처리된 동일한 요청건이 있습니다. 엑셀파일을 확인하세요.");
                }

                Optional<Wallet> wallets = walletRepository.findOneByCoinNameAndDepositDvcd("KRW", cell.getDepositDvcd());

                if(wallets.isPresent() && wallets.get() != null){

                    User user = userRepository.findOne(wallets.get().getUserId());

                    ManualTransactions mt = new ManualTransactions();
                    mt.setIsConfirmation("N");
                    mt.setUserId(wallets.get().getUserId());
                    mt.setStatus("PENDING");
                    mt.setRegDt(LocalDateTime.now());
                    mt.setDepositDt(LocalDateTime.parse(cell.getDate(), formatter));
                    mt.setCoinName("KRW");
                    mt.setCategory("receive");
                    mt.setBankNm(cell.getBank());
                    mt.setUserNm(cell.getDepositDvcd());
                    mt.setAmount(new BigDecimal(cell.getAmount().replaceAll(",", "")));
                    mt.setId(RandomStringUtils.randomAlphanumeric(64));

                    BankCode bankCode = bankCodeRepository.findOneByBankNameContaining(cell.getBank().replaceAll("\\(", "").replaceAll("\\)", ""));

                    //log.info("------------ : {}", mt);
                    MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
                    headers.put(ACCEPT, singletonList(APPLICATION_JSON_VALUE));
                    headers.put(CONTENT_TYPE, singletonList(APPLICATION_JSON_VALUE));
                    headers.put("apiKey", singletonList(user.getUserSetting().getApiKey()));

                    adminProvider.reqReceive(headers, mt.getCoinName(), mt.getAmount(), mt.getUserNm(), mt.getBankNm(), bankCode.getCode(), mt.getDepositDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), "UNKNOWN_ACCOUNT", "UNKNOWN_ADDRESS");

                    //log.info("-----------result");

                    //confirmRepository.save(mt);

                }else{

                    //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss");

                    WrongDeposit wd = new WrongDeposit();
                    wd.setBankNm(cell.getBank());
                    wd.setDepositDt(LocalDateTime.parse(cell.getDate(), formatter));
                    wd.setRegDt(LocalDateTime.now());
                    wd.setUserNm(cell.getDepositDvcd());
                    wd.setAmount(new BigDecimal(cell.getAmount().replaceAll(",", "")));
                    wd.setStatus("PENDING");

                    wrongDepositRepository.save(wd);

                }

            }

        }else{
            throw new OkbitException("NOT_THIS_EXCEL");
        }

        return Response.<String>builder().code(CodeEnum.SUCCESS).build();

    }
    @TransactionalEx
    @PostMapping(value = "/console/replyQna")
    public Response<String> reply(@ModelAttribute("user") User user, @RequestBody @Valid AdminDTO.replyRequest req) throws OkbitException{

//        SupportQna qna = supportQnaRepository.findOneById(req.getId()).orElseThrow(()->new OkbitException("WRONG_ACTION"));
//        qna.setShowYn("N");
//        qna.setStatus(StatusEnum.ANSWERED.name());

        //log.info("qna parent id : {}", req.getParentId());
        if(req.getParentId() != 0L) {
            List<SupportQna> list = supportQnaRepository.findAllByParentIdOrderByRegDtDesc(req.getParentId());
            list.stream().forEach(supportQna -> {
                supportQna.setShowYn("N");
                supportQna.setStatus(StatusEnum.ANSWERED.name());
            });
        }

        SupportQna qna = supportQnaRepository.findOneById(req.getParentId()).orElseThrow(() -> new OkbitException("문제가 발생했습니다."));
        qna.setShowYn("N");
        qna.setStatus(StatusEnum.ANSWERED.name());

        //log.info("-----------qna : {}", qna);

        supportQnaRepository.save(qna);

        SupportQna answer = new SupportQna();
        answer.setRegDt(LocalDateTime.now());
        answer.setContents(req.getContents());
        answer.setParentId(req.getParentId());
        answer.setShowYn("N");
        answer.setStatus(StatusEnum.ANSWERED.name());
        answer.setTitle(req.getTitle());
        answer.setUserId(user.getId());

//        AdminDTO.replyResponse res = new AdminDTO.replyResponse();
//        res.setRepContents(req.getContents());
//        res.setContents(qna.getContents());
//        res.setRepTitle(req.getTitle());
//        res.setTitle(qna.getTitle());
//        res.setEmail(req.getEmail());

        //mailService.sendEmail(res);
        //supportQnaRepository.save(qna);
        supportQnaRepository.save(answer);

        return Response.<String>builder().code(CodeEnum.SUCCESS).build();

    }

    @RequestMapping(value="/console/balance", method=RequestMethod.POST)
    public Response<List<Wallet>> balanceAll() throws Exception{

        FeignResult wallets = apiProvider.getWalletAll();

        FeignResultData resultData = wallets.getResult();

        if(resultData.getWallets() != null){

            Collections.sort(resultData.getWallets(), new Comparator<Wallet>() {
                @Override
                public int compare(Wallet o1, Wallet o2) {
                    return o1.getCoin().getPriority() < o2.getCoin().getPriority() ? -1 : (o1.getCoin().getPriority() > o2.getCoin().getPriority()) ? 1 : 0;
                }
            });

            for(int i=0;i<resultData.getWallets().size(); i++){

                resultData.getWallets().get(i).setTotalBalance(resultData.getWallets().get(i).getAvailableBalance().add(resultData.getWallets().get(i).getUsingBalance()));
                resultData.getWallets().get(i).initTotalBalance();
                resultData.getWallets().get(i).initAvailableBalance();
                resultData.getWallets().get(i).initUsingBalance();
                resultData.getWallets().get(i).initRealBalance();
            }

        }

        return Response.<List<Wallet>>builder()
                .code(CodeEnum.SUCCESS)
                .data(resultData.getWallets())
                .build();

    }

    @RequestMapping(value="/console/coin", method=RequestMethod.POST)
    public Response<List<Coin>> coin() throws Exception{

        List<Coin> result = coinRepository.findAllByOrderByPriorityAsc();

        return Response.<List<Coin>>builder()
                .code(CodeEnum.SUCCESS)
                .data(result)
                .build();

    }

    @PostMapping(value = "/console/coinAll")
    public Response<List<Integer>> coinAll(){

        List<Coin> result = coinRepository.findAllByNameNotOrderByPriorityAsc("KRW");

        List<Integer> countList = new ArrayList<>();

        countList.add(result.size());


        return Response.<List<Integer>>builder()
                .code(CodeEnum.SUCCESS)
                .data(countList)
                .build();

    }

    @PostMapping(value = "/console/todaySignUp")
    public Response<List<Long>> todaySignUp(){

        List<TodayUsers> usersList = userRepository.getTodayUsers(LocalDateTime.now().minusDays(7));
        //log.info("-------------------- : {}", usersList);

        List<Long> countList = new ArrayList<>(usersList.size());

        for(TodayUsers user : usersList){

            countList.add(user.getUsers());

        }
        if(!getBooleanToday(usersList)){
            countList.add(0l);
        }

        return Response.<List<Long>>builder()
                .code(CodeEnum.SUCCESS)
                .data(countList)
                .build();

    }

    @PostMapping(value = "/console/todayBank")
    public Response<List<Long>> todayBank(){

        List<TodayUsers> usersList = adminLevelRepository.getTodayBank(LocalDateTime.now().minusDays(7));

        //List<TodayUsers> usersList = userRepository.getTodayUsers(LocalDateTime.now().minusDays(7));
        //log.info("-------------------- : {}", usersList);

        List<Long> countList = new ArrayList<>(usersList.size());

        for(TodayUsers user : usersList){

            countList.add(user.getUsers());

        }
        if(!getBooleanToday(usersList)){
            countList.add(0l);
        }

        return Response.<List<Long>>builder()
                .code(CodeEnum.SUCCESS)
                .data(countList)
                .build();

    }

    @PostMapping(value = "/console/todayOrder")
    public Response<List<Long>> todayOrder(){

        //List<TodayUsers> usersList = adminLevelRepository.getTodayId(LocalDateTime.now().minusDays(7));

        List<TodayUsers> usersList = orderRepository.getOrderCount(LocalDateTime.now().minusDays(7));

        //List<TodayUsers> usersList = userRepository.getTodayUsers(LocalDateTime.now().minusDays(7));
        //log.info("-------------------- : {}", usersList);

        List<Long> countList = new ArrayList<>(usersList.size());

        for(TodayUsers user : usersList){

            countList.add(user.getUsers());

        }
        if(!getBooleanToday(usersList)){
            countList.add(0l);
        }
        return Response.<List<Long>>builder()
                .code(CodeEnum.SUCCESS)
                .data(countList)
                .build();

    }

    @PostMapping(value = "/console/todayId")
    public Response<List<Long>> todayId(){

        List<TodayUsers> usersList = adminLevelRepository.getTodayId(LocalDateTime.now().minusDays(7));

        //List<TodayUsers> usersList = userRepository.getTodayUsers(LocalDateTime.now().minusDays(7));
        //log.info("-------------------- : {}", usersList);

        List<Long> countList = new ArrayList<>(usersList.size());

        for(TodayUsers user : usersList){

            countList.add(user.getUsers());

        }
        if(!getBooleanToday(usersList)){
            countList.add(0l);
        }
        return Response.<List<Long>>builder()
                .code(CodeEnum.SUCCESS)
                .data(countList)
                .build();

    }

    @PostMapping(value = "/console/todayAll")
    public Response<List<Integer>> todayAll(){

        List<User> usersList = userRepository.findAll();

        //List<TodayUsers> usersList = userRepository.getTodayUsers(LocalDateTime.now().minusDays(7));
        //log.info("-------------------- : {}", usersList);

        List<Integer> countList = new ArrayList<>(usersList.size());

        countList.add(usersList.size());

        return Response.<List<Integer>>builder()
                .code(CodeEnum.SUCCESS)
                .data(countList)
                .build();

    }

    @PostMapping(value = "/console/todayTrade")
    public Response<List<Long>> todayTrade(){

        List<TodayUsers> usersList = marketOrdersRepository.getTodayTrade(LocalDateTime.now().minusDays(7));

        //List<TodayUsers> usersList = userRepository.getTodayUsers(LocalDateTime.now().minusDays(7));
        //log.info("-------------------- : {}", usersList);

        List<Long> countList = new ArrayList<>(usersList.size());

        for(TodayUsers user : usersList){

            countList.add(user.getUsers());

        }

        if(!getBooleanToday(usersList)){
            countList.add(0l);
        }

        //if(booleanToday(list))

        return Response.<List<Long>>builder()
                .code(CodeEnum.SUCCESS)
                .data(countList)
                .build();

    }

    public boolean getBooleanToday(List<TodayUsers> list){

        boolean result = false;

        ImmutableListMultimap group = Multimaps.index(list, sumHistory -> sumHistory.getRegDt());

        if(group.containsKey(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))){

            result = true;

        }
        return result;
    }

//    public long getTodayCnt(List<TodayUsers> list){
//
//        long today = 0l;
//
//        ImmutableListMultimap group = Multimaps.index(list, sumHistory -> sumHistory.getRegDt());
//
//        if(!group.containsKey(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))){
//
//
//
//        }
//
//
//        return today;
//
//    }

    @PostMapping(value = "/console/coinAssets")
    public Response<List<ResCoinAssets>> coinAssets(@Valid @RequestBody AdminDTO.ResCoinAssets coin){

        List<Wallet> walletList = walletRepository.findAllByCoinNameAndUserIdNot(coin.getCoinName(), 1L);

        List<Wallet> userWallets = walletList.stream().filter(wallet -> wallet.getUserId() != 82).filter(wallet -> wallet.getUserId() != 86).collect(Collectors.toList());

        log.info("------------ userWallets : {}", userWallets);

        double sendCount = 0d;
        double receiveCount = 0d;

        double sendAmount = 0d;
        double receiveAmount = 0d;

        double sellAmount = 0d;
        double buyAmount = 0d;

        List<WalletTransaction> transactionList = walletTransactionRepository.findAllByCoinNameAndGroupByDate(coin.getCoinName(), LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        if(transactionList != null && transactionList.size() > 0){

            List<WalletTransaction> sendList = transactionList.parallelStream().filter(walletTransaction -> walletTransaction.getCategory().equals("send")).filter(walletTransaction -> walletTransaction.getUserId() != 1l).collect(Collectors.toList());

            if(sendList != null && sendList.size()>0){

                sendCount = sendList.size();

                sendAmount = sendList.parallelStream().mapToDouble(walletTransaction -> walletTransaction.getAmount().doubleValue()).sum();

            }


            List<WalletTransaction> receiveList = transactionList.parallelStream().filter(walletTransaction -> walletTransaction.getCategory().equals("receive")).filter(walletTransaction -> walletTransaction.getUserId() != 1l).collect(Collectors.toList());

            if(receiveList != null && receiveList.size()>0){

                receiveCount = receiveList.size();

                receiveAmount = receiveList.parallelStream().mapToDouble(walletTransaction -> walletTransaction.getAmount().doubleValue()).sum();

            }

        }

        List<MarketHistoryOrders> marketHistoryOrdersList = marketOrdersRepository.findAllByFromCoinNameOrToCoinNameAndCompletedDt(coin.getCoinName(), LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        if(marketHistoryOrdersList != null && marketHistoryOrdersList.size() > 0){

            List<MarketHistoryOrders> sellList = marketHistoryOrdersList.parallelStream().filter(marketHistoryOrders -> marketHistoryOrders.getOrderType().equals("SELL")).collect(Collectors.toList());

            if(sellList != null && sellList.size()>0){

                sellAmount = sellList.size();

                sellAmount = sellList.parallelStream().mapToDouble(marketHistoryOrders -> marketHistoryOrders.getAmount().doubleValue()).sum();

            }

            List<MarketHistoryOrders> buyList = marketHistoryOrdersList.parallelStream().filter(marketHistoryOrders -> marketHistoryOrders.getOrderType().equals("BUY")).collect(Collectors.toList());

            if(buyList != null && buyList.size()>0){

                buyAmount = sellList.size();

                buyAmount = sellList.parallelStream().mapToDouble(marketHistoryOrders -> marketHistoryOrders.getAmount().doubleValue()).sum();

            }


        }


        //available
        //using
        //user

        //{data: 1, color: 'rgba(255,255,255,0.2)'},
//        {data: 2, color: 'rgba(255,255,255,0.8)'},
//        {data: 3, color: 'rgba(255,255,255,0.5)'},
//        {data: 4, color: 'rgba(255,255,255,0.1)'},
//        {data: 4, color: 'rgba(255,255,255,0.9)'},

        double userCnt = userWallets.size();
        double available = userWallets.stream().mapToDouble(o -> o.getAvailableBalance().doubleValue()).sum();
        double using = userWallets.stream().mapToDouble(o -> o.getUsingBalance().doubleValue()).sum();


        double total = available + using;

        double adminWalletCoinAmount = 0d;

        FeignResult wallets = apiProvider.getWalletAll();

        FeignResultData resultData = wallets.getResult();

        if(resultData.getWallets() != null){

            List<Wallet> walletsResult = resultData.getWallets();

            for(Wallet w : walletsResult){

                if(w.getCoin().getName().equals(coin.getCoinName())){

                    adminWalletCoinAmount = w.getRealBalance().doubleValue();
                    break;

                }

            }

        }

        double benefitAmount = adminWalletCoinAmount - total;

        ResCoinAssets benefitAsset = new ResCoinAssets();
        benefitAsset.setLabel("가용 코인");
        benefitAsset.setColor("rgba(255,255,255,0.9)");
        benefitAsset.setData(benefitAmount);

        ResCoinAssets userCntAsset = new ResCoinAssets();
        userCntAsset.setLabel("지갑");
        userCntAsset.setData(userCnt);
        userCntAsset.setColor("rgba(255,255,255,0.2)");

        ResCoinAssets availAsset = new ResCoinAssets();
        availAsset.setLabel("유저코인");
        availAsset.setData(available);
        availAsset.setColor("rgba(255,255,255,0.8)");

        ResCoinAssets usingAsset = new ResCoinAssets();
        usingAsset.setLabel("사용중 코인");
        usingAsset.setData(using);
        usingAsset.setColor("rgba(255,255,255,0.5)");

        ResCoinAssets sendAsset = new ResCoinAssets();
        sendAsset.setLabel("출금액");
        sendAsset.setData(sendAmount);
        sendAsset.setColor("rgba(255,255,255,0.1)");

        ResCoinAssets receiveAsset = new ResCoinAssets();
        receiveAsset.setLabel("입금액");
        receiveAsset.setData(receiveAmount);
        receiveAsset.setColor("rgba(255,255,255,0.3)");

        ResCoinAssets sellAsset = new ResCoinAssets("판매 볼륨", sellAmount, "rgba(255,255,255,0.4");
        ResCoinAssets buyAsset = new ResCoinAssets("구매 볼륨", buyAmount, "rgba(255,255,255,0.7");


        List<ResCoinAssets> resultList = new ArrayList<>();
        resultList.add(userCntAsset);
        resultList.add(availAsset);
        resultList.add(usingAsset);
        resultList.add(receiveAsset);
        resultList.add(sendAsset);
        resultList.add(sellAsset);
        resultList.add(buyAsset);

        return Response.<List<ResCoinAssets>>builder()
                .code(CodeEnum.SUCCESS)
                .data(resultList)
                .build();

    }

}
