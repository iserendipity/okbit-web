package io.dj.exchange.repository.cache;

import io.dj.exchange.domain.cache.RedisChart;
import org.springframework.data.repository.CrudRepository;

public interface HistoryRepository extends CrudRepository<RedisChart, String> {

    RedisChart findOneByTicker(String ticker);

}
