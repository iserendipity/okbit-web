package io.dj.exchange.domain.readonly;

import io.dj.exchange.domain.primary.Coin;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

/**
 * Created by jeongwoo on 2017. 7. 19..
 */
@Data
public class WalletTransactionPK implements Serializable {

    @Id
    private String id;

    @Id
    private Long userId;

   @Id
   @Column(name="coin_name")
    private String coinName;

}
