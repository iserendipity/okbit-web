package io.dj.exchange.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.dj.exchange.util.LocalDateTimeConverter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by kun7788 on 2016. 8. 1..
 */
@Configuration
@EnableWebMvc
public class MvcConfiguration extends WebMvcConfigurerAdapter {

//    @Value("${spring.mvc.locale}")
//    Locale locale = null;
//    @Value("${spring.messages.basename}")
//    String messagesBasename = null;
//    @Value("${spring.messages.encoding}")
//    String messagesEncoding = null;
//    @Value("${spring.messages.cache-seconds}")
//    int messagesCacheSeconds;

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        GsonHttpMessageConverter gsonHttpMessageConverter = new GsonHttpMessageConverter();
        gsonHttpMessageConverter.setGson(createGson());
        converters.add(gsonHttpMessageConverter);
    }

    private Gson createGson() {
        return new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeConverter())
                .setPrettyPrinting()
                .create();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public FormattingConversionServiceFactoryBean formattingConversionServiceFactory(){
        return new FormattingConversionServiceFactoryBean();
    }

//    @Bean
//    public LocaleResolver localeResolver() {
//        SessionLocaleResolver slr = new SessionLocaleResolver();
//        slr.setDefaultLocale(locale);
//        return slr;
//    }
//    @Bean
//    public LocaleChangeInterceptor localeChangeInterceptor() {
//        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
//        lci.setParamName("lang");
//        return lci;
//    }
//
//    //message source
//    @Bean
//    public ReloadableResourceBundleMessageSource messageSource(){
//        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
//        messageSource.setBasename(messagesBasename);                //"classpath:/messages/message"
//        messageSource.setDefaultEncoding(messagesEncoding);
//        messageSource.setCacheSeconds(messagesCacheSeconds);
//        return messageSource;
//    }
//
//    @Bean(name = "messageSourceAccessor")
//    public MessageSourceAccessor getMessageSourceAccessor(){
//        ReloadableResourceBundleMessageSource m = messageSource();
//        return new MessageSourceAccessor(m);
//    }

}
