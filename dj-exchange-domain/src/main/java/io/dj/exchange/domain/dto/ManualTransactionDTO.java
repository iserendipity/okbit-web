package io.dj.exchange.domain.dto;

import javafx.beans.DefaultProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.Nullable;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.math.BigDecimal;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 19.
 * Description
 */
public class ManualTransactionDTO {

    @Data
    @EqualsAndHashCode(callSuper=false)
    public class RequestKrw extends OtpRequest {
        @NotNull
        @DecimalMin("1000")
        BigDecimal amount;

        @NotNull
        String coinName;

        @NotNull
        String bankCode;

        String bankNm;

        @NotNull
        String userNm;

        @NotNull
        String account;

    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    public class RequestCoin extends OtpRequest {
        @NotNull
        @DecimalMin("0.0001")
        BigDecimal amount;

        @NotNull
        String coinName;

        @NotNull
        String fromAddress;

        String fromTag;//Monero

    }

}
