/**
 * Created by jeongwoo on 2017. 4. 21..
 */
function SupportApi () {
    this.call = function(uri, callback, resultProcFunc, params) {
        $.ajax({
            url: "/api/support" + uri
            , type: "POST"
            , dataType: 'json'
            , contentType:"application/json; charset=UTF-8"
            , data: JSON.stringify(params)
            , success: function (data) {
                resultProcFunc(data);
                return callback(data);
            }
            , error:function(e){
                //showAlert("ERROR : " + JSON.parse(e.responseText).message);
            }
        });
        return true;
    };

    this.callPage = function(uri, callback, resultProcFunc, params) {
        $.ajax({
            url: "/api/support" + uri
            , type: "POST"
            , dataType: 'json'
            //, contentType:"application/json; charset=UTF-8"
            , data: params
            , success: function (data) {
                resultProcFunc(data);
                return callback(data);
            }
            , error:function(e){
                //showAlert("ERROR : " + JSON.parse(e.responseText).message);
            }
        });
        return true;
    };

    this.write = function(callback, params){
        return this.call("/write", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {

            }
        }, params);
    };

    this.getQuestions = function(callback, params){
        return this.callPage("/questions", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {

            }
        }, params);
    };
    this.getNotices = function(callback, params){
        return this.callPage("/notices", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {

            }
        }, params);
    };

}