<script id="myTradeTemplate" type="text/x-jquery-tmpl">
    <tr class="text-center">
        <td class="col-xs-3">${r"${dt}"}</td>
        <td class="col-xs-3">${r"${orderType}"}</td>
        <td class="col-xs-3">${r"${priceTxt}"}</td>
        <td class="col-xs-3">${r"${amountTxt}"}</td>
    </tr>
</script>
<table class="table table-condensed table-hover table-fixed">
    <thead style="height:0px;">
    <tr>
        <th class="col-xs-3">
        </th>
        <th class="col-xs-3">
        </th>
        <th class="col-xs-3">
        </th>
        <th class="col-xs-3">
        </th>
    </tr>

    </thead>
    <tbody id="myTradeBody" style="height:366px; overflow:scroll">
        <tr class="text-center">
            <td colspan="4" class="text-center">
                <@spring.message "EMPTY_DATA"/>
            </td>
        </tr>
    </tbody>
</table>