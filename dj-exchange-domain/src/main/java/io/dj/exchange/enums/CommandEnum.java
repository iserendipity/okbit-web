package io.dj.exchange.enums;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 7.
 * Description
 */

/**
 * Created by kun7788 on 2017. 5. 12..
 */
public enum CommandEnum {
    TRADE, ORDER, MAIL
}