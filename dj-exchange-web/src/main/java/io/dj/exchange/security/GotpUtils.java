package io.dj.exchange.security;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import io.dj.exchange.domain.dto.OtpRequest;
import io.dj.exchange.domain.dto.OtpSettingDTO;
import io.dj.exchange.domain.primary.OtpSetting;
import io.dj.exchange.domain.primary.User;
import io.dj.exchange.exception.OkbitException;
import io.dj.exchange.repository.hibernate.primary.OtpRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 19.
 * Description
 */
@Component
@Slf4j
public class GotpUtils {

    @Autowired
    private GoogleAuthenticator a1;

    @Autowired
    private OtpRepository o1;

    private static GoogleAuthenticator authenticator;
    private static OtpRepository otpRepository;

    @Autowired
    public void setAuthenticator(GoogleAuthenticator a1){
        GotpUtils.authenticator = a1;
    }

    @Autowired
    public void setOtpRepository(OtpRepository o1){
        GotpUtils.otpRepository = o1;
    }

    public static boolean otpCheckWithdraw(User user, OtpRequest request) throws Exception{

        OtpSetting setting = otpRepository.findOneByUserId(user.getId());

        //log.info("---------음 : {}", request.getOtpCode());

        if("Y".equals(user.getUserSetting().getOtpActive())){

            if (setting.getUseWithdraw() != null && setting.getUseWithdraw().equals("Y")) {//기존 세팅에 otp 사용중이면

                Integer verificationCode = request.getOtpCode();
                if (verificationCode != null) {

                    //log.info("--------- : {}", authenticator.authorize(user.getUserSetting().getOtpHash(), verificationCode));
                    if (!authenticator.authorize(user.getUserSetting().getOtpHash(), verificationCode)) {
                        //if (!otpProvider.isOtpValid(retrieveUser.getUser(), verificationCode)) {
                        throw new OkbitException("INVALID_VERIFICATION_CODE");
                    }
                } else {
                    throw new OkbitException("OTP_CODE_MANDATORY");
                }

            }

        }
        return true;
    }

//    public static boolean otpCheckWithdrawCoin(User user, OtpRequest request) throws Exception{
//
//        OtpSetting setting = otpRepository.findOneByUserId(user.getId());
//
//        log.info("---------음 : {}", request.getOtpCode());
//
//        if("Y".equals(user.getUserSetting().getOtpActive())){
//
//            if (setting.getUseWithdraw() != null && setting.getUseWithdraw().equals("Y")) {//기존 세팅에 otp 사용중이면
//
//                Integer verificationCode = request.getOtpCode();
//                if (verificationCode != null) {
//
//                    log.info("--------- : {}", authenticator.authorize(user.getUserSetting().getOtpHash(), verificationCode));
//                    if (!authenticator.authorize(user.getUserSetting().getOtpHash(), verificationCode)) {
//                        //if (!otpProvider.isOtpValid(retrieveUser.getUser(), verificationCode)) {
//                        throw new OkbitException("INVALID_VERIFICATION_CODE");
//                    }
//                } else {
//                    throw new OkbitException("OTP_CODE_MANDATORY");
//                }
//
//            }
//
//        }
//        return true;
//    }


    public static boolean otpCheckLogin(User user, UsernamePasswordAuthenticationToken authentication) {

            if(user.getOtpSetting() != null && user.getOtpSetting().getUseLogin() != null && user.getOtpSetting().getUseLogin().equals("Y")) {

                Integer verificationCode = ((GotpWebAuthenticationDetails) authentication.getDetails()).getTotpKey();
                if (verificationCode != null) {
                    if (!authenticator.authorize(user.getUserSetting().getOtpHash(), verificationCode)) {
                    //if (!otpProvider.isOtpValid(retrieveUser.getUser(), verificationCode)) {
                        throw new BadCredentialsException("INVALID_VERIFICATION_CODE");
                    }
                } else {
                    throw new BadCredentialsException("OTP_CODE_MANDATORY");
                }
            }

            return true;
    }

    public static boolean otpCheckActivate(User user, OtpSettingDTO.OtpSettingRequest request) {

        Integer verificationCode = request.getOtpCode();
        if (verificationCode != null) {
            if (!authenticator.authorize(user.getUserSetting().getOtpHash(), verificationCode)) {
                throw new BadCredentialsException("INVALID_VERIFICATION_CODE");
            }
        } else {
            throw new BadCredentialsException("OTP_CODE_MANDATORY");
        }

        return true;
    }
}
