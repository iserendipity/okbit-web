package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.ManualTransactions;
import io.dj.exchange.domain.primary.ManualTransactionsPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Created by jeongwoo on 2017. 5. 9..
 */
public interface ManualTransactionsRepository extends JpaRepository<ManualTransactions, ManualTransactionsPK> {

    Page<ManualTransactions> findAllByIsConfirmationAndStatus(String isConfirmation, String status, Pageable p);

    Page<ManualTransactions> findAllByIsConfirmationAndStatusAndCategoryOrderByRegDtDesc(String n, String pending, String send, Pageable p);

    ManualTransactions findOneByTxId(String txId);

    Optional<ManualTransactions> findTopByUserIdAndCategoryAndCoinNameOrderByRegDtAsc(Long userId, String receive, String krw);

    Page<ManualTransactions> findAllByIsConfirmationAndStatusAndCategoryAndCoinNameOrderByRegDtDesc(String n, String pending, String receive, String krw, Pageable p);

    Optional<ManualTransactions> findAllByCoinNameAndCategoryAndUserNmAndDepositDtAndAmountAndBankNm(String coinName, String category, String userNm, LocalDateTime depositDt, BigDecimal amount, String bankNm);

    ManualTransactions findOneById(String id);

    Page<ManualTransactions> findAllByIsConfirmationAndStatusAndCategoryAndCoinNameNotOrderByRegDtDesc(String n, String pending, String send, String name, Pageable p);
}
