package io.dj.exchange.domain.cache;

import io.dj.exchange.domain.chart.History;
import lombok.Data;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 12.
 * Description
 */
@Data
@RedisHash(value = "Chart:v1")
public class RedisChart implements Serializable{

    @Id
    private Long id;

    @Indexed
    private String ticker;

    private List<History> histories;



}
