package io.dj.exchange.domain.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 7.
 * Description
 */
@Data
@Slf4j
public class Volume {

    BigDecimal volume24h;
    BigDecimal volume7d;
    BigDecimal volume30d;

    BigDecimal volume;

    String volume24hTxt;
    String volume7dTxt;
    String volume30dTxt;

    public Volume() {
    }

    public Volume(BigDecimal volume) {
        this.volume = volume;
    }

    public void setVolume24h(BigDecimal volume24h) {
        this.volume24h = volume24h;
        DecimalFormat df = new DecimalFormat("#,##0");
        this.volume24hTxt = df.format(volume24h)+"KRW";
    }

    public void setVolume7d(BigDecimal volume7d) {
        this.volume7d = volume7d;
        DecimalFormat df = new DecimalFormat("#,##0");
        this.volume7dTxt = df.format(volume7d)+"KRW";
    }

    public void setVolume30d(BigDecimal volume30d) {
        this.volume30d = volume30d;
        DecimalFormat df = new DecimalFormat("#,##0");
        //log.info("--------------30d : {}", volume30d);
        this.volume30dTxt = df.format(volume30d)+"KRW";
//        this.volume30dTxt = df.format(volume30d)+"W";
    }
}
