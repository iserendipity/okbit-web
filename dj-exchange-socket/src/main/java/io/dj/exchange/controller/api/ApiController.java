package io.dj.exchange.controller.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 24.
 * Description
 */
@Slf4j
@RestController
@RequestMapping(value = "/api")
public class ApiController {

    @RequestMapping(value = "/health")
    public String health(){
        return "200";
    }

}
