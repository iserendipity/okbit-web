package io.dj.exchange.exception;

import io.dj.exchange.domain.dto.FeignResult;
import io.dj.exchange.enums.ErrorCode;
import io.dj.exchange.util.LogUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OkbitAlertException extends Exception {

    public OkbitAlertException(String desc) {
        super(desc);
        LogUtil.info(LogUtil.ANSI_RED, desc);
    }

    public OkbitAlertException(ErrorCode code) {
        super(code.name());
        LogUtil.info(LogUtil.ANSI_RED, code.name());
    }

    public OkbitAlertException(FeignResult result){

        super(result.getCode());
        LogUtil.info(LogUtil.ANSI_RED, result.getCode());


    }
}
