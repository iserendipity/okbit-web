<div id="krwModal" class="modal fade"> <!-- Style for just preview -->
    <div class="modal-dialog" style="margin-top:180px;">
        <div class="modal-content">
            <div class="modal-header">
            <#--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">-->
            <#--×-->
            <#--</button>-->
                <h4 class="modal-title"><@spring.message "TITLE_WITHDRAW_INFO_KRW"/></h4>
            </div>
            <div class="modal-body">
            <@spring.message "KRW_NOTICE1"/><br>
            <@spring.message "KRW_NOTICE2"/><br>
            <@spring.message "KRW_NOTICE3"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm" data-dismiss="modal"><@spring.message "BTN_AGREE"/></button>
            </div>
        </div>
    </div>
</div>