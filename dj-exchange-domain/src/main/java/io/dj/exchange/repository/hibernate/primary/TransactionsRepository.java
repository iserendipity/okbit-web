package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.Transactions;
import io.dj.exchange.domain.primary.TransactionsPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by jeongwoo on 2017. 5. 9..
 */
public interface TransactionsRepository extends JpaRepository<Transactions, TransactionsPK> {

    Page<Transactions> findAllByUserId(Long userId, Pageable pageable);

    Page<Transactions> findAllByCategoryAndUserId(String category, Long userId, Pageable p);

    Page<Transactions> findAllByCategoryAndUserIdAndCoinName(String category, Long userId, String coinName, Pageable p);

    Page<Transactions> findAllByCategoryAndUserIdOrderByDtDesc(String category, Long id, Pageable p);

    List<Transactions> findAllByCategoryAndUserIdAndCoinNameAndDtBetweenOrderByDtAsc(String send, Long id, String coinName, LocalDateTime now, LocalDateTime toDay);

    Optional<Transactions> findTopByCategoryAndUserIdAndCoinNameOrderByDtAsc(String receive, Long id, String coinName);
}
