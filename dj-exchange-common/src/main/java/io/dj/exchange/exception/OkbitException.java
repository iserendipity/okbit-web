package io.dj.exchange.exception;

import io.dj.exchange.domain.dto.FeignResult;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.util.LogUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.apache.bcel.classfile.Code;

import java.util.Arrays;

@Slf4j
public class OkbitException extends Exception {

    public OkbitException(String desc) {
        super(desc);
        LogUtil.info(LogUtil.ANSI_RED, desc);
    }

    public OkbitException(FeignResult result){

//        new CodeEnum(result.getCode(), result.getDesc())

        //super("");

        //super(CodeEnum.getName(result.getCode()));

        super(CodeEnum.getName(result.getCode()));

//        log.info("------1 : {}", result.getCode());
//
//        log.info("------2 : {}", CodeEnum.getName("4008"));
//
//        log.info("------3 : {}", CodeEnum.getName(result.getCode()));
//
//
////        for(CodeEnum e : CodeEnum.values()){
////
////            //e.init();
////
////        }
//
//        //new OkbitException(CodeEnum.getName(result.getCode()));
//
////
////
        LogUtil.info(LogUtil.ANSI_RED, CodeEnum.getName(result.getCode()));


    }
}
