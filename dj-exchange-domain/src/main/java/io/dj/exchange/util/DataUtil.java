package io.dj.exchange.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.time.LocalDateTime;

/**
 * Created by kun7788 on 2017. 1. 29..
 */
public class DataUtil {

    public static String rtrim(String str){
        int len = str.length();
        while ((0 < len) && (str.charAt(len-1) <= '0'))
        {
            len--;
        }
        return str.substring(0, len);
    }

    public static String decimal(BigDecimal val) {
        String[] _val = val.toPlainString().split("\\.");
        if (_val.length == 2) {
            return _val[0] + "." + ("".equals(DataUtil.rtrim(_val[1])) ? "0" : DataUtil.rtrim(_val[1]));
        }
           return val.toPlainString();
    }

    public static String decimal(BigDecimal val, String format){

        DecimalFormat df = new DecimalFormat(format);
        return df.format(val);

    }

    @Converter(autoApply = true)
    public static class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Timestamp> {

        @Override
        public Timestamp convertToDatabaseColumn(LocalDateTime locDateTime) {
            return (locDateTime == null ? null : Timestamp.valueOf(locDateTime));
        }

        @Override
        public LocalDateTime convertToEntityAttribute(Timestamp sqlTimestamp) {
            return (sqlTimestamp == null ? null : sqlTimestamp.toLocalDateTime());
        }
    }
}
