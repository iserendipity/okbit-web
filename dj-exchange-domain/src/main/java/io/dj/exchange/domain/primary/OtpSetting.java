package io.dj.exchange.domain.primary;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 18.
 * Description
 */
@Data
@Entity
@Table(name = "otp_setting")
public class OtpSetting implements Serializable{

    @Id
    @Column(name="user_id")
    Long userId;

    @Column(name="use_login")
    private String useLogin;

    @Column(name="use_withdraw")
    private String useWithdraw;

    @Column(name="use_setting")
    private String useSetting;

}
