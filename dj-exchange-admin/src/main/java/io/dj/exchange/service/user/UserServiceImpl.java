package io.dj.exchange.service.user;


import io.dj.exchange.domain.primary.User;
import io.dj.exchange.enums.Role;
import io.dj.exchange.repository.hibernate.primary.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by jeongwoo on 2017. 4. 4..
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> getUserById(long id) {
        LOGGER.info("Getting user={}", id);
        return Optional.ofNullable(userRepository.findOne(id));
    }

    @Override
    public Optional<User> getUserByLoginId(String loginId) {
        Optional<User> user = userRepository.findOneByEmailAndActiveAndRole(loginId, "Y", Role.ADMIN);
        return user;
    }

    @Override
    public Collection<User> getAllUsers() {
        LOGGER.info("Getting all users");
        return userRepository.findAll(new Sort("email"));
    }
}
