package io.dj.exchange.exception.decoder;

import com.google.gson.Gson;
import feign.Response;
import feign.codec.ErrorDecoder;
import io.dj.exchange.domain.dto.FeignResult;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.exception.OkbitException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.IOException;

import static feign.FeignException.errorStatus;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 9.
 * Description
 */
@Slf4j
public class FeignExceptionDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String methodKey, Response response) {

        if (response.status() >= 500 && response.status() <= 599) {

            byte[] responseBody;
            try {
                responseBody = IOUtils.toByteArray(response.body().asInputStream());
            } catch (IOException e) {
                throw new RuntimeException("Failed to process response body.", e);
            }

            Gson gson = new Gson();
            FeignResult result = gson.fromJson(new String(responseBody), FeignResult.class);
            result.setDesc(result.getMsg());
            //result.setCode(CodeEnum.valueOf(result.getCode()))
            return new OkbitException(result);

        }else {
            return errorStatus(methodKey, response);
        }
    }


}
