package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.OtpSetting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OtpRepository extends JpaRepository<OtpSetting, Long> {

    OtpSetting findOneByUserId(Long id);
}
