package io.dj.exchange.domain.primary;

import lombok.Data;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 10.
 * Description
 */
@Data
@Entity
@Table(name = "wallet_transaction")
@IdClass(value = TransactionsPK.class)
public class Transactions implements Serializable{

    @Id
    @Column(name = "id")
    private String id;

    @Id
    @Column(name = "tx_id")
    private String txId;

    @Id
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "coin_name")
    private String coinName;

    @Column(name = "category")
    private String category;

    @Column(name = "address")
    private String address;

    @Column(name = "dt")
    public LocalDateTime dt;

    @Column(name = "status")
    private String status;

    @Column(name = "confirmation")
    private int confirmation;

    @Column(name = "reason")
    private String reason;

//    @Column(name = "childTxId")
//    private String childTxId;

    @Column(name = "amount")
//    @NumberFormat(style = NumberFormat.Style.CURRENCY)
    private BigDecimal amount;

    @Id
    @OneToOne
    @JoinColumn(name="coin_name", updatable = false, insertable = false)
    private Coin coin;

    @OneToOne
    @JoinColumn(name="coin_name", updatable = false, insertable = false)
    private BlockExplorer blockExplorer;

    @Transient
    private String dtTxt;

    public void initDtTxt(){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        this.dtTxt =  formatter.format(this.dt);

    }


}
