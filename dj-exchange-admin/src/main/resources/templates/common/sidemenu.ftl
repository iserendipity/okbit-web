<li class="active">
    <a class="sa-side-home" href="/console">
        <span class="menu-item">Home</span>
    </a>
</li>
<#--<li>-->
    <#--<a class="sa-side-support" href="/coinInfo">-->
        <#--<span class="menu-item">Coin Info</span>-->
    <#--</a>-->
<#--</li>-->
<#if user.userSetting.level =='LEV3' >
<li>
    <a class="sa-side-deposit" href="/deposit">
        <span class="menu-item">Deposit</span>
    </a>
</li>
<#elseif user.userSetting.level =='LEV1'>
<li>
    <a class="sa-side-deposit" href="/deposit">
        <span class="menu-item">Deposit</span>
    </a>
</li>
</#if>
<#if user.userSetting.level =='LEV2'>
<li>
    <a class="sa-side-withdraw" href="/withdraw">
        <span class="menu-item">Withdraw</span>
    </a>
</li>
<#elseif user.userSetting.level =='LEV3'>
<li>
    <a class="sa-side-withdraw" href="/withdraw">
        <span class="menu-item">Withdraw</span>
    </a>
</li>
</#if>
<li>
    <a class="sa-side-security" href="/verify">
        <span class="menu-item">Verify</span>
    </a>
</li>
<li>
    <a class="sa-side-signout" href="/signOut">
        <span class="menu-item">Sign Out</span>
    </a>
</li>
<#--<li>-->
    <#--<a class="sa-side-chart" href="charts.html">-->
        <#--<span class="menu-item">Charts</span>-->
    <#--</a>-->
<#--</li>-->
<#--<li>-->
    <#--<a class="sa-side-folder" href="file-manager.html">-->
        <#--<span class="menu-item">File Manager</span>-->
    <#--</a>-->
<#--</li>-->
<#--<li>-->
    <#--<a class="sa-side-calendar" href="calendar.html">-->
        <#--<span class="menu-item">Calendar</span>-->
    <#--</a>-->
<#--&lt;#&ndash;</li>&ndash;&gt;-->
<#--<li class="dropdown">-->
    <#--<a class="sa-side-page" href="">-->
        <#--<span class="menu-item">Change Theme</span>-->
    <#--</a>-->
    <#--<ul class="list-unstyled menu-item">-->
        <#--<li><a href=""><img src="/img/body/blue.jpg"></a></li>-->
        <#--<li><a href=""><img src="/img/body/chrome.jpg"></a></li>-->
        <#--<li><a href=""><img src="/img/body/greenish.jpg"></a></li>-->
        <#--<li><a href=""><img src="/img/body/violate.jpg"></a></li>-->
    <#--</ul>-->
<#--</li>-->