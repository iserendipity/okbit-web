package io.dj.exchange.config;


import io.dj.exchange.filter.AjaxSessionTimeoutFilter;
import io.dj.exchange.filter.CsrfHeaderFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

/**
 * Created by kun7788 on 2016. 10. 28..
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/","/static/**", "/img/**", "/css/**", "/fonts/**", "/js/**", "/websock/**", "/report").permitAll()
                .antMatchers("/api/**").fullyAuthenticated()
                .anyRequest().fullyAuthenticated()
                .and()
                .formLogin()
                .loginPage("/")
                .failureUrl("/?error=invalid")
                .loginProcessingUrl("/loginProcess").permitAll()
                .defaultSuccessUrl("/console")
                .usernameParameter("loginId")
                .passwordParameter("password")
                .and()
                .logout()
                .logoutUrl("/signOut").permitAll()
                .deleteCookies("remember-me")
                .deleteCookies("JSESSIONID").permitAll()
                .logoutSuccessUrl("/").permitAll()

                //.and().sessionFixation().migrateSession()
                //.headers().frameOptions().disable().and()
                .and().addFilterAfter(new AjaxSessionTimeoutFilter(), ExceptionTranslationFilter.class)
                .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
                .csrf().csrfTokenRepository(csrfTokenRepository()).disable();
                //.sessionManagement().maximumSessions(1).expiredUrl("/?error=expire").maxSessionsPreventsLogin(true);
                //.antMatchers("/doSignup", "/findPassword", "/test/**", "/api/common/**", "/ad/**","/static/**", "/img/**", "/css/**", "/fonts/**", "/js/**", "/less/**", "/sass/**", "/vendor/**", "/").permitAll()
                //.antMatchers("/api/public/**", "/console/websock/**", "/api/dummy/**", "/api/common/**", "/ad/**", "/test/**", "/info", "/confirmEmail", "/signup", "/doSignup", "/static/**", "/clock_assets/**", "/assets/**", "/", "/public/**", "/doc/**", "/img/**", "/css/**", "/fonts/**", "/js/**", "/less/**", "/sass/**", "/vendor/**", "/font-awesome/**").permitAll()
//                .antMatchers("/console").authenticated()
//                .antMatchers("/admin").hasAuthority("ADMIN")
//                .anyRequest().fullyAuthenticated()
//                .and()
//                .formLogin()
//                .loginPage("/")
//                .failureUrl("/?error=Email_or_Password_is_invalid")
//                .loginProcessingUrl("/loginProcess")
//                .defaultSuccessUrl("/console")
//                .usernameParameter("loginId")
//                .passwordParameter("password")
//                .permitAll()
//                .and()
//                .logout()
//                .logoutUrl("/signout")
//                .deleteCookies("remember-me")
//                .logoutSuccessUrl("/")
//                .permitAll()
//                .and()
//                .sessionManagement()
//                .maximumSessions(1)
//                .expiredUrl("/?error=Session_expired")
//                .and()
//                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).sessionFixation().newSession()
//                .invalidSessionUrl("/?error=Invalid_session")
//                .and()
//                .rememberMe().rememberMeCookieName("remember-me")
//                .rememberMeParameter("remember")
//                .tokenValiditySeconds(2149200)
//                .and()
//                .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
//                .csrf().csrfTokenRepository(csrfTokenRepository()).disable();
    }

    private CsrfTokenRepository csrfTokenRepository() {
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName("X-XSRF-TOKEN");
        return repository;
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }


}
