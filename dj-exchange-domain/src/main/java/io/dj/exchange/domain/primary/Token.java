package io.dj.exchange.domain.primary;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 19.
 * Description
 */
@Entity
@Table(name = "token")
@Data
public class Token implements Serializable{
    @Id
    @GeneratedValue
    private Long id;
    private String code, scope, access_token, fintech_use_num, token_type, expires_in, user_seq_no;
}
