package io.dj.exchange.domain.primary;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 22.
 * Description
 */
@Entity
@Table(name = "user_setting")
@Data
public class UserSetting implements Serializable{

    @Id
    @Column(name = "user_id", nullable = false, unique = true)
    private Long userId;

    @Column(name = "api_key")
    private String apiKey;

    @Column(name = "level", nullable = false)
    private String level;

    @Column(name = "otp_active", nullable = false)
    private String otpActive;

    @Column(name = "otp_hash", nullable = false)
    private String otpHash;

    @Column(name = "reg_dt")
    private LocalDateTime regDt;

    @Column(name = "theme")
    private String theme;

}
