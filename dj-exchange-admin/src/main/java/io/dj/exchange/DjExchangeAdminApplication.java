package io.dj.exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DjExchangeAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(DjExchangeAdminApplication.class, args);
	}
}
