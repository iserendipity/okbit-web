package io.dj.exchange.annotation;

import org.springframework.transaction.annotation.Transactional;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 29.
 * Description
 */
@Transactional(rollbackFor = Throwable.class)
public @interface TransactionalEx {
}
