package io.dj.exchange.context;

import com.netflix.hystrix.strategy.concurrency.HystrixRequestVariableDefault;
import io.dj.exchange.domain.primary.User;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 10.
 * Description
 */
public class UserHystrixRequestContext {

    private static final HystrixRequestVariableDefault<User> userContextVariable = new HystrixRequestVariableDefault<>();

    private UserHystrixRequestContext() {}

    public static HystrixRequestVariableDefault<User> getInstance() {
        return userContextVariable;
    }
}