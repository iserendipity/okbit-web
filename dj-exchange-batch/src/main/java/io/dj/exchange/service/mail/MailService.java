package io.dj.exchange.service.mail;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.dj.exchange.domain.primary.EmailConfirm;
import io.dj.exchange.enums.MailType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 24.
 * Description
 */
@Component
@Slf4j
public class MailService {

    @Autowired
    JavaMailSender javaMailSender;

    //freemarker configuration
    @Autowired
    Configuration configuration;

    @Value("${mail.redirect.url}")
    String url;

    public boolean sendEmail(EmailConfirm confirmObj) {
        MimeMessage message = javaMailSender.createMimeMessage();
        try {
            //log.info("------------- sending mail to "+confirmObj.getEmail());
            Template t;

            confirmObj.setUrl(url);

            if(confirmObj.getType() != null && confirmObj.getType().equals(MailType.SIGNUP)) {

                t = configuration.getTemplate("mail.ftl");
                message.setSubject("Okbit sign up Confirmation.");

            }else{

                t = configuration.getTemplate("forgetPass.ftl");
                message.setSubject("Okbit password reset process mail.");

            }

            String readyParsedTemplate = FreeMarkerTemplateUtils
                    .processTemplateIntoString(t, confirmObj);

            message.setFrom(new InternetAddress("support@ok-bit.com"));
            message.addRecipient(MimeMessage.RecipientType.TO, confirmObj.getEmail() != null ? new InternetAddress(confirmObj.getEmail()) : new InternetAddress("support@ok-bit.com"));
            message.setText(readyParsedTemplate, "UTF-8", "html");

            javaMailSender.send(message);

            //log.info("------------- Done Sending Mail "+confirmObj.getEmail());

            return true;

        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        } catch (MailException e) {
            e.printStackTrace();
            return false;

        } catch (IOException e) {
            e.printStackTrace();
            return false;

        }// try - catch
        catch (TemplateException e) {
            e.printStackTrace();
            return false;
        }
    }

}
