<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
    </head>
<#if (user.userSetting.theme)??>
<body id="skin-blur-${user.userSetting.theme}">
<#else>
<body id="skin-blur-blue">
</#if>

        <header id="header" class="media">
            <#include "common/top.ftl">
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            
            <!-- Sidebar -->
            <aside id="sidebar">

                <!-- Side Menu -->
                <ul class="list-unstyled side-menu">
                    <#include "common/sidemenu.ftl">
                </ul>

            </aside>
        
            <!-- Content -->
            <section id="content" class="container">

                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li style="font-size:14px;"><a href="/media/guide.pdf" target="_blank">GUIDE BOOK</a></li>
                </ol>
                
                <h4 class="page-title">SUPPORT</h4>

                <!-- Shortcuts -->

                <#include "common/shortcut.ftl">

                <hr class="whiter" />

                <!-- Main Widgets -->
                <div class="block-area">
                    <div class="row" >
                        <#--<div class="col-md-4">-->
                            <#--<div class="tile collapse">-->
                                <#--<h2 class="tile-title">FAQ-->
                                <#--</h2>-->
                                <#--<div class="overflow">-->
                                    <#--<div id="myQuestion" class="col-md-12">-->
                                    <#--<#include "content/myQuestion.ftl">-->
                                    <#--</div>-->
                                <#--</div>-->
                                <#--<div class="panel-footer">-->
                                <#--&lt;#&ndash;<a href="" class="nb pull-right" style="font-size:8px;">Full History</a>&ndash;&gt;-->
                                    <#--<div class="clearfix"></div>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->
                        <div class="col-md-3">
                            <div class="tile">
                                <div class="tile-title">
                                    Support Center Information
                                </div>
                                <div class="overflow">

                                    <div class="col-md-12 p-b-10">
                                        <h5 class="block-title"><@spring.message "SUPPORT_EMAIL"/></h5>
                                        <br>
                                        <a href="mailto:support@ok-bit.com">support@ok-bit.com</a>
                                    </div>
                                    <hr class="whiter">
                                    <div class="col-md-12 p-b-10">
                                        <h5 class="block-title"><@spring.message "SUPPORT_CALL"/></h5>
                                        <br>
                                        <a href="tel:1588-9520">1588-9520</a><br><@spring.message "SUPPORT_CALL_EX"/>
                                    </div>
                                    <hr class="whiter">
                                    <div class="col-md-12 p-b-10">
                                        <h5 class="block-title"><@spring.message "SUPPORT_TIME_LIMIT"/></h5>
                                        <br>
                                        <p><@spring.message "SUPPORT_WORK_DAY"/> 10:00 ~ 17:00 / <@spring.message "SUPPORT_LUNCH_TIME"/> 12:00 ~ 13:00(UTC+09)</p>
                                    </div>
                                    <hr class="whiter">
                                    <div class="col-md-12 p-b-10">
                                        <h5 class="block-title"><@spring.message "SUPPORT_QNA"/></h5>
                                        <br>
                                        <p><@spring.message "SUPPORT_QNA_ANSWER"/></p>
                                    </div>
                                </div>

                            </div>
                            <div class="tile">
                                <div class="tile-title">
                                    GUIDES and TERMS
                                </div>
                                <div class="overflow">
                                <#--<div class="col-md-12 p-b-10">-->
                                <#--<h5 class="block-title">이용가이드</h5>-->
                                <#--<br>-->
                                <#--<a href="/media/guide.pdf" target="_blank" class="btn btn-alt">이용가이드 보기</a>-->

                                <#--</div>-->
                                <#--<hr class="whiter">-->
                                    <div class="col-md-12 p-b-10">
                                        <h5 class="block-title"><@spring.message "SUPPORT_TERMS"/></h5> <br>
                                        <a href="/media/terms.pdf" target="_blank" class="btn btn-alt"><@spring.message "BTN_TERMS"/></a>

                                    </div>
                                    <hr class="whiter">
                                    <div class="col-md-12 p-b-10">
                                        <h5 class="block-title"><@spring.message "SUPPORT_PRIVACY"/></h5>
                                        <br>
                                        <a href="/media/privacy.pdf" target="_blank" class="btn btn-alt"><@spring.message "BTN_PRIVACY"/></a>

                                    </div>
                                <#--<hr class="whiter">-->
                                <#--<div class="col-md-12 p-b-10">-->
                                <#--<h5 class="block-title">문의하기</h5>-->
                                <#--<br>-->
                                <#--<button type="button" class="btn btn-alt">문의하기</button>-->

                                <#--</div>-->
                                <#--<hr class="whiter">-->
                                <#--<div class="col-md-12 p-b-10">-->
                                <#--<h5 class="block-title">입금확인</h5>-->
                                <#--<br>-->
                                <#--<button type="button" class="btn btn-alt">입금확인요청</button>-->

                                <#--</div>-->
                                </div>
                            </div>
                        </div>
                        <#--<div class="col-md-4">-->
                            <#--<#include "rightInfo/questionForm.ftl">-->
                        <#--</div>-->
                        <#include "rightInfo/questionForm.ftl">
                        <div class="col-md-7">
                            <div class="row">
                                <script id="noticeTemplate" type="text/x-jquery-tmpl">
                                        <div class="media">
                                            <a class="media-body" href="javascript:noticeDetail('${r"${id}"}');">
                                                <#--<div class="pull-left list-title">-->
                                                    <#--<span class="t-overflow f-bold">${r"${dtTxt}"}</span>-->
                                                <#--</div>-->
                                                <div class="pull-right list-date">${r"${dtTxt}"}</div>
                                                <div class="media-body">
                                                    <span class="t-overflow">${r"${title}"}</span>
                                                </div>
                                            </a>
                                        </div>
                                    </script>
                                <div class="tile">
                                    <div class="tile-title">
                                        Notice
                                    </div>
                                    <div class="overflow">
                                        <div class="message-list list-container" id="noticeWrapper">

                                            <div class="media text-center p-10">
                                            <@spring.message "EMPTY_DATA"/>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="panel-footer text-center">
                                        <ul id="noticePage" class="pagination pagination-sm">
                                            <li class="active"><a href="javascript:getNotices('1');">1</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="tile">
                                    <div class="tile-title">
                                        Q&A
                                    </div>
                                    <div class="tile-config dropdown">
                                        <a data-toggle="modal" href="#compose-message" class="tile-menu" style="width:auto !important; background-image:none !important; padding:5px;"><@spring.message "ADD_QUESTION"/></a>
                                        <#--<ul class="dropdown-menu pull-right text-right">-->
                                            <#--<li>-->
                                                <#--<a data-toggle="modal" href="#compose-message" title="" data-original-title="Add">-->
                                                <#--ADD</a>-->
                                            <#--</li>-->
                                        <#--</ul>-->
                                    </div>
                                    <script id="questionTemplate" type="text/x-jquery-tmpl">
                                        <div class="media">
                                            <a class="media-body" href="javascript:getDetail('${r"${id}"}');">
                                                <div class="pull-left list-title hidden-xs">
                                                    <span class="t-overflow f-bold">${r"${status}"}
                                                    </span>
                                                </div>
                                                <div class="pull-right list-date">${r"${regDtTxt}"}</div>
                                                <div class="media-body">
                                                    <span class="t-overflow">${r"${title}"}
                                                    {{if (status =='ANSWERED')}}
                                                        <label class="label label-info label-xs">${r"${unreadCnt}"}</label>
                                                    {{/if}}
                                                    </span>
                                                </div>
                                            </a>
                                        </div>
    <#--<tr>-->
        <#--<td class="col-xs-2">${r"${dtTxt}"}-->
        <#--</td>-->
        <#--<td class="col-xs-1">${r"${status}"}-->
        <#--</td>-->
        <#--<td class="col-xs-1">${r"${coin.name}"}-->
        <#--</td>-->
        <#--<td class="col-xs-2">${r"${amount}"}-->
        <#--</td>-->
        <#--<td class="col-xs-6"><a target="_blank" href="${r"${blockExplorer.explorerUrl}"}${r"${txId}"}">${r"${txId}"}</a>-->
        <#--</td>-->
    <#--</tr>-->
                                    </script>
                                    <div class="overflow">
                                        <div class="message-list list-container" id="questionWrapper">
                                            <#--<header class="listview-header media">-->
                                                <#--<input class="input-sm col-md-4 pull-right message-search" type="text" placeholder="Search....">-->
                                                <#--<div class="clearfix"></div>-->
                                            <#--</header>-->

                                                <div class="media text-center p-10">
                                                    <@spring.message "EMPTY_DATA"/>
                                                </div>

                                        </div>
                                    </div>
                                    <div class="panel-footer text-center">
                                        <ul id="questionPage" class="pagination pagination-sm">
                                            <li class="active"><a href="javascript:getMyQuestions('1');">1</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- COIN Info -->
                            <div id="coinInfoWrap" class="col-md-12">
                            <#include "rightInfo/infoCoin.ftl">
                            </div>

                            <!-- Order Form -->
                            <div id="orderFormWrap" class="col-md-12">
                                <div class="tile">
                                <#include "rightInfo/infoOrderForm.ftl">
                                </div>
                            </div>

                            <!-- Balance -->
                            <div id="balanceWrap" class="col-md-12">
                            <#include "rightInfo/infoBalance.ftl">
                            </div>

                            <!-- My Orders -->
                            <div id="myOrderWrap" class="col-md-12">
                                <div class="tile">
                                    <h2 class="tile-title"><@spring.message "TITLE_MYORDERS"/></h2>
                                    <div class="tile-config">
                                        <a data-toggle="collapse" href="#myOrder" class="tile-menu"></a>
                                    </div>
                                    <div id="myOrder" class="overflow collapse in">
                                    <#include "rightInfo/infoMyOrder.ftl">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </section>

            <!-- Older IE Message -->
            <!--[if lt IE 9]>
                <div class="ie-block">
                    <h1 class="Ops">Ooops!</h1>
                    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser in order to access the maximum functionality of this website. </p>
                    <ul class="browsers">
                        <li>
                            <a href="https://www.google.com/intl/en/chrome/browser/">
                                <img src="/img/browsers/chrome.png" alt="">
                                <div>Google Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.mozilla.org/en-US/firefox/new/">
                                <img src="/img/browsers/firefox.png" alt="">
                                <div>Mozilla Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com/computer/windows">
                                <img src="/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://safari.en.softonic.com/">
                                <img src="/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-10/worldwide-languages">
                                <img src="/img/browsers/ie.png" alt="">
                                <div>Internet Explorer(New)</div>
                            </a>
                        </li>
                    </ul>
                    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
                </div>   
            <![endif]-->
        </section>
        
        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->
        <script src="/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
        <script src="/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Charts -->
        <script src="/js/charts/jquery.flot.js"></script> <!-- Flot Main -->
        <script src="/js/charts/jquery.flot.time.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen -->

        <script src="/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts -->
        <script src="/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts -->
        <script src="/js/charts.js"></script> <!-- All the above chart related functions -->

        <!-- Map -->
        <script src="/js/maps/jvectormap.min.js"></script> <!-- jVectorMap main library -->
        <script src="/js/maps/usa.js"></script> <!-- USA Map for jVectorMap -->

        <!-- cookie -->
        <script src="/js/jquery.cookie.js"></script>

        <!--  Form Related -->
        <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->
        <#--<script src="/js/select.min.js"></script>-->

        <!-- UX -->
        <script src="/js/scroll.min.js"></script> <!-- Custom Scrollbar -->

        <!-- Other -->
        <script src="/js/calendar.min.js"></script> <!-- Calendar -->
        <script src="/js/feeds.min.js"></script> <!-- News Feeds -->

        <script src="js/editor.min.js"></script> <!-- WYSIWYG Editor -->
        <#--<script src="js/markdown.min.js"></script> <!-- Markdown Editor &ndash;&gt;-->

        <!--  Form Related -->
        <script src="js/validation/validate.min.js"></script> <!-- jQuery Form Validation Library -->
        <script src="js/validation/validationEngine.min.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->

        <!-- All JS functions -->
        <script src="/js/functions.js"></script>

        <!-- trade functions -->
        <script src="/js/api/trade_api.js"></script>
        <script src="/js/jquery.loading.min.js"></script> <!-- loading -->
        <!-- Jqutemplateslate -->
        <script src="/js/jquery.tmpl.min.js"></script>

        <!-- common functions -->
        <script src="/js/api/common_api.js"></script>
        <script src="/js/common/common.js"></script>

        <script src="/js/api/support_api.js"></script>
        <script src="/js/support/support.js"></script>

        <!-- custom utils -->
        <script src="/js/api/util.js"></script>

        <#include "common/validation_locale.ftl"/>

        <script type="text/javascript">

            $('document').ready(function(){
               commonInit();
               init();

               $('form').on('submit',function(){
                  return confirm(submitConfirm);
               });

                $('.message-editor').summernote({
                    toolbar: false,
                    height: 200,
                    resizable: false
                });


            });



        </script>
        <form id="form_chk" name="form_chk" method="post">
            <input type="hidden" name="id" >

        </form>
    </body>
</html>
