package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.BatchOffset;
import io.dj.exchange.enums.BatchKey;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by jeongwoo on 2017. 4. 26..
 */
public interface BatchOffsetRepository extends JpaRepository<BatchOffset, BatchKey> {

    Optional<BatchOffset> findOneByOffsetName(BatchKey lastIdResolutionD);
}
