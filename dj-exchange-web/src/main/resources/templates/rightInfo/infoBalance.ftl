<script id="balanceTemplate" type="text/x-jquery-tmpl">
    <tr>
        <td rowspan="2" style="width:40px" class="visible-lg balanceIcon">
            <img src="/img/shortcuts/${r"${coin.name}"}.png" alt="${r"${coin.name}"}">
        </td>
        <td rowspan="2" style="width:30%">
            ${r"${coin.name}"}
        </td>
        <td>
            <@spring.message "TITLE_ALL"/> : ${r"${totalBalanceTxt}"} ${r"${coin.unit}"}
        </td>

    </tr>
    <tr>
        <td>
            <@spring.message "TITLE_AVAILABLE"/> : ${r"${availableBalanceTxt}"} ${r"${coin.unit}"}
        </td>
    </tr>
    <#--<tr>-->
        <#--<td>-->
            <#--Using : ${r"${usingBalance}"}-->
        <#--</td>-->
    <#--</tr>-->
</script>
<div id="balance" class="tile">
    <h2 class="tile-title"><@spring.message "TITLE_BALANCE"/></h2>
    <div class="tile-config dropdown">
        <a data-toggle="dropdown" href="" class="tile-menu"></a>
        <ul class="dropdown-menu pull-right text-right">
            <li><a href="">Refresh</a></li>
        </ul>
    </div>
    <div class="overflow">
        <table id="balanceTab" class="table table-responsive">
            <tbody id="balanceBody">
            <tr>
                <td>
                    <@spring.message "NOT_EXIST_WALLET" />
                </td>
            </tr>

            </tbody>
        </table>

    </div>
</div>