<div class="tile">
    <div class="tile-title">
        Withdrawl Security Setting
    </div>
    <div class="overflow">
        <form role="form">
            <div class="row-fluid">
                <div class="col-xs-12">
                    <h4 class="block-title">Use Email</h4>
                </div>
                <div class="col-md-9 col-lg-10 m-b-10">
                    If a withdrawal is requested from a new IP address you will receive an email asking you to check and verify the withdrawal. The 'untrusting' period for IP changes is 24 hours. If the withdrawal is made more than 24 hours after the IP address change, this extra email check is not triggered.
                </div>
                <div class="col-md-3 col-lg-2">
                    <div class="make-switch switch-mini pull-right m-b-10">
                        <input type="checkbox">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr class="whiter">
            <div class="row-fluid">
                <div class="col-xs-12">
                    <h4 class="block-title">GOOGLE OTP</h4>
                </div>
                <div class="col-md-9 col-lg-10 m-b-10">
                    If a withdrawal is requested from a new IP address you will receive an email asking you to check and verify the withdrawal. The 'untrusting' period for IP changes is 24 hours. If the withdrawal is made more than 24 hours after the IP address change, this extra email check is not triggered.
                </div>
                <div class="col-md-3 col-lg-2">
                    <div class="make-switch switch-mini pull-right m-b-10">
                        <input type="checkbox">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
</div>