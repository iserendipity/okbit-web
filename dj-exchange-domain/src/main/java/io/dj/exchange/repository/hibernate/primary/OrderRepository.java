package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.dto.OrdersCount;
import io.dj.exchange.domain.dto.OrdersDTO;
import io.dj.exchange.domain.dto.TodayUsers;
import io.dj.exchange.domain.primary.Orders;
import io.dj.exchange.enums.OrderStatus;
import io.dj.exchange.enums.OrderType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderRepository extends JpaRepository<Orders, Long> {

    List<Orders> findAllByOrderType(OrderType order_type);

    @Query(value = "SELECT count(p) as count, p.amount, p.price FROM Orders p WHERE (p.fromCoinName = :fromCoinName or p.toCoinName = :toCoinName) AND p.orderType = :orderType AND p.status = :orderStatus group by p.price, p.amount order by p.price asc "
            , countQuery = "SELECT count(p) FROM Orders p WHERE (p.fromCoinName = :fromCoinName or p.toCoinName = :toCoinName) AND p.orderType = :orderType AND p.status = :orderStatus group by p.price, p.amount order by p.price asc"
            , nativeQuery = false)
    Page<OrdersDTO.ResponseOrderList> findAllByOrderTypeAndFromCoinNameAndToCoinNameAndStatus(@Param("orderType") OrderType order_type, @Param("fromCoinName") String fromCoinName, @Param("toCoinName") String toCoinName, @Param("orderStatus") OrderStatus orderStatus, Pageable p);

    @Query(value = "SELECT new io.dj.exchange.domain.dto.OrdersCount(count(p.id), sum(amountRemaining), p.price, (sum(amountRemaining) * p.price), 'BITCOIN') FROM Orders p WHERE p.fromCoinName = :fromCoinName AND p.toCoinName = :toCoinName AND p.orderType = :orderType AND p.status = :orderStatus group by p.price order by p.price desc")
    Page<OrdersCount> getBuy(@Param("orderType") OrderType order_type, @Param("fromCoinName") String fromCoinName, @Param("toCoinName") String toCoinName, @Param("orderStatus") OrderStatus orderStatus, Pageable p);

    @Query(value = "SELECT new io.dj.exchange.domain.dto.OrdersCount(count(p.id), sum(amountRemaining), p.price, (sum(amountRemaining) * p.price), 'BITCOIN') FROM Orders p WHERE p.fromCoinName = :fromCoinName AND p.toCoinName = :toCoinName AND p.orderType = :orderType AND p.status = :orderStatus group by p.price order by p.price asc")
    Page<OrdersCount> getSell(@Param("orderType") OrderType order_type, @Param("fromCoinName") String fromCoinName, @Param("toCoinName") String toCoinName, @Param("orderStatus") OrderStatus orderStatus, Pageable p);

    @Query(value = "SELECT new io.dj.exchange.domain.dto.OrdersCount(count(p.id), sum(amountRemaining), p.price, (sum(amountRemaining) * p.price), 'BITCOIN') FROM Orders p WHERE p.fromCoinName = :fromCoinName AND p.toCoinName = :toCoinName AND p.orderType = :orderType AND p.status = :orderStatus group by p.price order by p.price desc")
    List<OrdersCount> getBuy(@Param("orderType") OrderType order_type, @Param("fromCoinName") String fromCoinName, @Param("toCoinName") String toCoinName, @Param("orderStatus") OrderStatus orderStatus);

    @Query(value = "SELECT new io.dj.exchange.domain.dto.OrdersCount(count(p.id), sum(amountRemaining), p.price, (sum(amountRemaining) * p.price), 'BITCOIN') FROM Orders p WHERE p.fromCoinName = :fromCoinName AND p.toCoinName = :toCoinName AND p.orderType = :orderType AND p.status = :orderStatus group by p.price order by p.price asc")
    List<OrdersCount> getSell(@Param("orderType") OrderType order_type, @Param("fromCoinName") String fromCoinName, @Param("toCoinName") String toCoinName, @Param("orderStatus") OrderStatus orderStatus);

    @Query(value="select p from Orders p where (p.fromCoinName = :fromCoinName or p.toCoinName = :toCoinName) And p.status = :status And p.userId = :userId")
    Page<Orders> findAllByUserIdAndStatusAndFromCoinNameOrToCoinName(@Param("userId") Long id, @Param("status") OrderStatus placed, @Param("fromCoinName") String name1, @Param("toCoinName") String name2, Pageable p);

    @Query(value="select p from Orders p where (p.fromCoinName = :fromCoinName or p.toCoinName = :toCoinName) And p.status = :status And p.userId = :userId")
    List<Orders> findAllByUserIdAndStatusAndFromCoinNameOrToCoinName(@Param("userId") Long id, @Param("status") OrderStatus placed, @Param("fromCoinName") String name1, @Param("toCoinName") String name2);

    Orders findOneById(Long orderId);

    List<Orders> findAllByOrderTypeAndToCoinNameAndStatusOrderByPriceDesc(@Param("orderType") OrderType orderType, @Param("fromCoinName") String coinName, @Param("status") OrderStatus status);

    List<Orders> findAllByOrderTypeAndFromCoinNameAndStatusOrderByPriceAsc(@Param("orderType") OrderType orderType, @Param("toCoinName") String coinName, @Param("status") OrderStatus status);

    @Query("select new io.dj.exchange.domain.dto.TodayUsers(function('count', p.id), function('date_format', p.regDt, '%Y-%m-%d')) from Orders p where p.regDt >= :dt group by function('date_format', p.regDt, '%Y-%m-%d') order by p.regDt asc")
    List<TodayUsers> getOrderCount(@Param("dt") LocalDateTime dateTime);
}
