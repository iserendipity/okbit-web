#!/bin/sh -xe

echo "compile ....."
./gradlew clean build -x test

cd /Users/jeongwoo/IdeaProjects/okbit-web/dj-exchange-web/build/libs

ssh -i /Users/jeongwoo/keyholder/okbit-web.pem ubuntu@52.79.114.5 "sudo cp /home/ubuntu/exchange-web.war /home/ubuntu/exchange-web.war.bak"
ssh -i /Users/jeongwoo/keyholder/okbit-web.pem ubuntu@13.124.146.176 "sudo cp /home/ubuntu/exchange-web.war /home/ubuntu/exchange-web.war.bak"
ssh -i /Users/jeongwoo/keyholder/okbit-web.pem ubuntu@13.124.117.59 "sudo cp /home/ubuntu/exchange-web.war /home/ubuntu/exchange-web.war.bak"

scp -i /Users/jeongwoo/keyholder/okbit-web.pem exchange-web.war ubuntu@52.79.114.5:/home/ubuntu
scp -i /Users/jeongwoo/keyholder/okbit-web.pem exchange-web.war ubuntu@13.124.146.176:/home/ubuntu
scp -i /Users/jeongwoo/keyholder/okbit-web.pem exchange-web.war ubuntu@13.124.117.59:/home/ubuntu

ssh -i /Users/jeongwoo/keyholder/okbit-web.pem ubuntu@52.79.114.5 "sudo nohup /data/services/start.sh > /dev/null 2>&1 &"
ssh -i /Users/jeongwoo/keyholder/okbit-web.pem ubuntu@13.124.146.176 "sudo nohup /data/services/start.sh > /dev/null 2>&1 &"
ssh -i /Users/jeongwoo/keyholder/okbit-web.pem ubuntu@13.124.117.59 "sudo nohup /data/services/start.sh > /dev/null 2>&1 &"


