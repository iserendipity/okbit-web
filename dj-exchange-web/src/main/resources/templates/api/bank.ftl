<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "../common/head.ftl">
    </head>
    <body id="skin-blur-chrome">

        <header id="header" class="media">
            <#include "../common/top.ftl">
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            
            <!-- Sidebar -->
            <aside id="sidebar">

                <!-- Side Menu -->
                <ul class="list-unstyled side-menu">
                    <#include "../common/sidemenu.ftl">
                </ul>

            </aside>
        
            <!-- Content -->
            <section id="content" class="container">
            
                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li class="active">Home</li>
                    <#--<li><a href="#">Library</a></li>-->
                    <#--<li class="active">Data</li>-->
                </ol>
                
                <h4 class="page-title">TRADING CONSOLE</h4>
                                
                <!-- Shortcuts -->
                <div class="block-area shortcut-area">
                    <a class="shortcut tile" href="">
                        <img src="/img/shortcuts/money.png" alt="">
                        <small class="t-overflow">BIT</small>
                    </a>
                    <a class="shortcut tile" href="">
                        <img src="/img/shortcuts/twitter.png" alt="">
                        <small class="t-overflow">ETH</small>
                    </a>
                    <a class="shortcut tile" href="">
                        <img src="/img/shortcuts/calendar.png" alt="">
                        <small class="t-overflow">DASH</small>
                    </a>
                    <a class="shortcut tile" href="">
                        <img src="/img/shortcuts/stats.png" alt="">
                        <small class="t-overflow">XMR</small>
                    </a>
                    <a class="shortcut tile" href="">
                        <img src="/img/shortcuts/connections.png" alt="">
                        <small class="t-overflow">LITE</small>
                    </a>

                </div>
                
                <hr class="whiter" />

                <div class="block-area">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="row">
                                <!--  ORDERS -->
                                <div class="col-md-5">
                                    <div class="tile">
                                        <h2 class="tile-title">BANK API TEST
                                        </h2>

                                        <div class="overflow col-md-12 m-t-10">
                                            <h5 class="block-title">토큰정보</h5>

                                        </div>
                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="auth_code_login" class="control-label">로그인 인증코드</label>
                                                    <input type="text" id="auth_code_login" name="auth_code_login" class="form-control input-sm" />
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="auth_code_inquiry" class="control-label">조회 인증코드</label>
                                                    <input type="text" id="auth_code_inquiry" name="auth_code_inquiry" class="form-control input-sm" />
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="auth_code_transfer" class="control-label">이체 인증코드</label>
                                                    <input type="text" id="auth_code_transfer" name="auth_code_transfer" class="form-control input-sm"/>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="access_token" class="control-label">로그인 토큰</label>
                                                    <input type="text" id="access_token" name="access_token" class="form-control input-sm" value="<#if login_token?? >${login_token.access_token}</#if>"/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="inquiry_token" class="control-label">조회 토큰</label>
                                                    <input type="text" id="inquiry_token" name="inquiry_token" class="form-control input-sm" value="<#if inquiry_token?? >${inquiry_token.access_token}</#if>"/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="transfer_token" class="control-label">이체 토큰</label>
                                                    <input type="text" id="transfer_token" name="transfer_token" class="form-control input-sm" value="<#if transfer_token?? >${transfer_token.access_token}</#if>"/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="oob_token" class="control-label">기관이체 토큰</label>
                                                    <input type="text" id="oob_token" name="oob_token" class="form-control input-sm" value="<#if oob_token??>${oob_token.access_token}</#if>"/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="user_seq_no" class="control-label">사용자 일련번호</label>
                                                    <input type="text" id="user_seq_no" name="user_seq_no" class="form-control input-sm" value="<#if login_token?? >${login_token.user_seq_no}</#if>"/>
                                                </div>

                                            </div>
                                            <div class="col-md-12 m-t-10 m-l-10 m-b-20">
                                                <a href="/test/bankLogin" target="_blank" class="btn btn-xs btn-alt">뱅킹 로그인</a>
                                                <a href="/test/bankRegisteredAccount" target="_blank" class="btn btn-xs btn-alt" >출금계좌등록확인</a>
                                                <a href="/test/bankInquiryAccount" target="_blank" class="btn btn-xs btn-alt" >조회계좌등록확인</a>
                                                <a href="/test/bankRegisterInquiryAccount" target="_blank" class="btn btn-xs btn-alt" >조회계좌등록</a>
                                                <a href="javascript:okbitTransferToken();" class="btn btn-xs btn-alt" >기관코드 찾기</a>
                                            </div>
                                            <hr class="whiter">
                                            <div class="col-md-12 m-t-10 m-l-10">
                                                <a href="javascript:fnSearchAccount();" class="btn btn-xs btn-alt">유저 계좌 정보조회</a>
                                                <script id="accountInfoTemplate" type="text/x-jquery-tmpl">
                                                    <tr>
                                                        <td>
                                                            ${r"${account_alias}"}
                                                        </td>
                                                        <td>
                                                            ${r"${account_holder_name}"}
                                                        </td>
                                                        <td>
                                                            ${r"${bank_name}"}
                                                        </td>
                                                        <td>
                                                            ${r"${fintech_use_num}"}
                                                        </td>
                                                        <td>
                                                            ${r"${transfer_agree_yn}"}
                                                        </td>
                                                        <td><button type="button" class="btn btn-xs">이 계좌사용</button>
                                                        </td>
                                                    </tr>
                                                </script>
                                                <div class="well-lg">
                                                    <table id="accountInfoTab" class="table table-responsive">
                                                        <thead>
                                                            <tr>
                                                                <td>
                                                                    계좌 별칭
                                                                </td>
                                                                <td>
                                                                    예금주
                                                                </td>
                                                                <td>
                                                                    은행
                                                                </td>
                                                                <td>
                                                                    계좌 고유번호(핀테크번호)
                                                                </td>
                                                                <td>
                                                                    이체가능여부
                                                                </td>
                                                                <td>
                                                                    <i class="fa fa-cog"></i>
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>

                                                    </table>


                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">

                                        </div>

                                        <div class="panel-footer">
                                            <a href="" class="nb pull-right" style="font-size:8px;">Full Book</a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- TRADES -->
                                <div class="col-md-4">
                                    <div class="tile">
                                        <h2 class="tile-title">입출금내역 조회
                                        </h2>
                                        <div class="tile-config dropdown">

                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="overflow m-t-10" >

                                            <div class="form-group col-md-6">
                                                <label for="fintech_use_num" class="control-label">핀테크 이용번호</label>
                                                <input type="text" id="fintech_use_num" name="fintech_use_num" class="form-control input-sm" />
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="from_date" class="control-label col-md-12">조회기간</label>
                                                <div class="col-md-6">
                                                    <input type="text" id="from_date" name="from_date" class="form-control input-sm col-md-6" value="20160511"/>
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" id="to_date" name="to_date" class="form-control input-sm col-md-6" value="20160511"/>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="page_index" class="control-label">페이지번호</label>
                                                <input type="text" id="page_index" name="page_index" class="form-control input-sm" />
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label for="inquiry_trans_list" class="control-label">거래내역결과</label>
                                                <div id="inquiry_trans_list" name="inquiry_trans_list" class="well tile" ></div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <a href="javascript:fnSearchTransList();" class="btn btn-xs btn-alt">조회</a>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    <div class="tile">
                                        <div class="tile-title">실명조회</div>

                                        <div class="row m-t-10">

                                            <div class="col-md-12">
                                                <div class="form-group col-md-6">
                                                    <label for="bank_code_std_realname" class="control-label">은행코드</label>
                                                    <input type="text" id="bank_code_std_realname" name="bank_code_std_realname" class="form-control input-sm" />
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="account_num" class="control-label">계좌번호</label>
                                                    <input type="text" id="account_num" name="account_num" class="form-control input-sm" />
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="account_holder_info" class="control-label">예금주 생년월</label>
                                                    <input type="text" id="account_holder_info" name="account_holder_info" class="form-control input-sm"/>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="account_holder_name" class="control-label">예금주 실명</label>
                                                    <input type="text" id="account_holder_name" name="account_holder_name" class="form-control input-sm" readonly/>
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <a href="javascript:fnSearchRealName();" class="btn btn-xs btn-alt">조회</a>

                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <label class="control-label">조회결과</label>
                                                    <div id="real_name" class="well tile">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="col-md-12">
                            <div class="tile">
                                <h2 class="tile-title">출금테스트
                                </h2>

                                <div class="col-md-12 m-t-10">
                                    <h5 class="block-title">이용기관 출금</h5>

                                </div>
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group col-md-3">
                                            <label for="wd_pass_phrase" class="control-label">기관 이체용 암호문구</label>
                                            <input type="text" id="wd_pass_phrase" name="wd_pass_phrase" class="form-control input-sm" />
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="account_num" class="control-label">계좌인자내역</label>
                                            <input type="text" id="wd_print_content" name="wd_print_content" class="form-control input-sm" />
                                        </div>
                                        <div class="form-group col-md-3">
                                            <a href="javascript:fnTransferDeposit();" class="btn btn-xs btn-alt" >전송</a>

                                        </div>

                                        <div class="form-group col-md-12">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <td>거래순번
                                                        </td>
                                                        <td>은행코드
                                                        </td>
                                                        <td>계좌번호
                                                        </td>
                                                        <td>예금주
                                                        </td>
                                                        <td>입금계좌인자내역
                                                        </td>
                                                        <td>거래금액
                                                        </td>
                                                        <td>CMS번호
                                                        </td>
                                                        <td>요청일시
                                                        </td>
                                                    </tr>
                                                </thead>
                                                <tbody id="reqListBody">
                                                    <tr>
                                                        <td><input style="width:30px"  type="text" class="form-control" name="tran_no" value="1"  /></td>
                                                        <td><span class='inpbox' style="width:20px"><input type="text" class="form-control" name="bank_code_std"  /></span></td>
                                                        <td><span class="inpbox" style="width:120px"><input type="text" class="form-control"  name="fintech_use_num_transfer"  /></span></td>
                                                        <td><span class="inpbox" style="width:60px"><input type="text" class="form-control" name="account_holder_name_transfer"  /></span></td>
                                                        <td><span class="inpbox" style="width:120px"><input type="text" class="form-control"  name="print_content" /></span></td>
                                                        <td><span class="inpbox" style="width:90px"><input type="text" class="form-control"  name="tran_amt" /></span></td>
                                                        <td><span class="inpbox" style="width:70px"><input type="text" class="form-control"  name="cms_no" /></span></td>
                                                        <td><input type="text" class="form-control"  name="tran_dtime" value="20160425210015" /></span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr class="whiter">
                                <div class="col-md-12">
                                    <div id="transfer_deposit" class="well tile">


                                    </div>
                                </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </section>

            <!-- Older IE Message -->
            <!--[if lt IE 9]>
                <div class="ie-block">
                    <h1 class="Ops">Ooops!</h1>
                    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser in order to access the maximum functionality of this website. </p>
                    <ul class="browsers">
                        <li>
                            <a href="https://www.google.com/intl/en/chrome/browser/">
                                <img src="/img/browsers/chrome.png" alt="">
                                <div>Google Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.mozilla.org/en-US/firefox/new/">
                                <img src="/img/browsers/firefox.png" alt="">
                                <div>Mozilla Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com/computer/windows">
                                <img src="/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://safari.en.softonic.com/">
                                <img src="/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-10/worldwide-languages">
                                <img src="/img/browsers/ie.png" alt="">
                                <div>Internet Explorer(New)</div>
                            </a>
                        </li>
                    </ul>
                    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
                </div>   
            <![endif]-->
        </section>
        
        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->
        <script src="/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
        <script src="/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

        <!-- Jqutemplateslate -->
        <script src="/js/jquery.tmpl.min.js"></script>

        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Charts -->
        <script src="/js/charts/jquery.flot.js"></script> <!-- Flot Main -->
        <script src="/js/charts/jquery.flot.time.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen -->

        <script src="/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts -->
        <script src="/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts -->
        <script src="/js/charts.js"></script> <!-- All the above chart related functions -->

        <!-- Map -->
        <script src="/js/maps/jvectormap.min.js"></script> <!-- jVectorMap main library -->
        <script src="/js/maps/usa.js"></script> <!-- USA Map for jVectorMap -->

        <!--  Form Related -->
        <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->

        <!-- UX -->
        <script src="/js/scroll.min.js"></script> <!-- Custom Scrollbar -->

        <!-- Other -->
        <script src="/js/calendar.min.js"></script> <!-- Calendar -->
        <script src="/js/feeds.min.js"></script> <!-- News Feeds -->

        <!-- Util -->
        <script src="/js/util.js"></script>

        <!-- All JS functions -->
        <script src="/js/functions.js"></script>



        <script type="text/javascript">

            /* Authoriztion Code 얻기 */
            function fnGetAuthCode(auth_code,scope)
            {
                console.log(auth_code + ' : ' + scope);
                switch (scope){

                    case 'login' :
                        console.log(auth_code);
                        $('#auth_code_login').val(auth_code);
                        break;

                    case 'inquiry' :
                        $('#auth_code_inquiry').val(auth_code);
                        break;

                    case 'transfer' :
                        $('#auth_code_transfer').val(auth_code);
                        break;

                }
                fnSearchAccessToken(auth_code, scope);
                //okbitTransferToken();

            }

            function getAccessToken(result){
                //console.log(result);
                switch (result.scope){

                    case 'login' :

                        $('#access_token').val(result.access_token);
                        $("#user_seq_no").val(result.user_seq_no);
                        break;

                    case 'inquiry' :
                        $('#inquiry_token').val(result.access_token);
                        break;

                    case 'transfer' :
                        $('#transfer_token').val(result.access_token);
                        break;

                    case 'oob' :
                        $('#oob_token').val(result.access_token);
                        break;

                }
            }
            /*관리자 이체용 토큰 발급*/
            function okbitTransferToken(){

                $.ajax({
                    url: "/test/accessTokenClientCredential",
                    type: "POST",
                    dataType: "json",
                    success : function (data) {

                        var result = data.data;
                        console.log(result.access_token);
                        getAccessToken(result);

                    },
                    error : function (data,data2, data3) {
                        alert('error!!!');
                    }
                });

            }

            /* 사용자인증 Access Token 획득 */
            function fnSearchAccessToken(auth_code, scope)
            {
                var code = auth_code;

                $.ajax({
                    url: "/test/accessToken",
                    type: "POST",
                    cache: false,
                    data: {"code":code, "scope": scope},
                    dataType: "json",
                    success : function (data) {
                        var result = data.data;

                        getAccessToken(result);
                    },
                    error : function (data,data2, data3) {
                        alert(data);
                        alert(data2);
                        alert(data3);
                        alert('error!!!');
                    }
                });

            }
            /*사용자 정보 조회*/
            function fnSearchAccount()
            {
                var user_seq_no = $("#user_seq_no").val();
                var tran_dtime = new Date().format("yyyyMMddHHmmss");
                var  access_token = "Bearer "+$("#access_token").val();

                $.ajax({
                    url: "https://testapi.open-platform.or.kr/v1.0/account/list",
                    //url: "/tpt/test/getInquryAccountTest",
                    beforeSend : function(request){
                        request.setRequestHeader("Authorization", access_token);
                    },
                    type: "GET",
                    data: {"user_seq_no":user_seq_no,"tran_dtime":tran_dtime},
                    dataType: "json",
                    success : function (data) {
                        console.log(data);
                        console.log(data.res_list);
                        var result = data.res_list;
                        $('#accountInfoTab').html($('#accountInfoTemplate').tmpl(result));

                    },
                    error : function (data,data2, data3) {
                        console.log(data);
                        console.log(data2);
                        console.log(data3);
                        alert('error!!!');
                    }
                });
            }
            /* 거래내역조회API */
            function fnSearchTransList(inquiry_type)
            {
                var fintech_use_num = $("#fintech_use_num").val();
                var tran_dtime = new Date().format("yyyyMMddHHmmss")
                var inquiry_type = "A";
                var from_date = $("#from_date").val();
                var to_date = $("#to_date").val();
                var sort_order = "D";
                var page_index = 1;

                //alert(from_date); //return;

                var  access_token = "Bearer "+$("#inquiry_token").val();

                $.ajax({
                    //url: "/tpt/test/getOauthToken",
                    url: "https://testapi.open-platform.or.kr/v1.0/account/transaction_list",
                    beforeSend : function(request){
                        request.setRequestHeader("Authorization", access_token);
                    },
                    type: "GET",
                    contenType: "application/json",
                    data: {"fintech_use_num":fintech_use_num,"tran_dtime":tran_dtime,"inquiry_type":inquiry_type
                        ,"from_date":from_date,"to_date":to_date,"sort_order":sort_order,"page_index":page_index},
                    dataType: "json",
                    success : function (data, data2, data3) {
                        /*  alert(data);
                        alert(data2);
                        alert(data3.responseText);
                        var list = JSON.parse(data);	 */
                        $("#inquiry_trans_list").text(data3.responseText.replace(/,/gi, ",\n"));
                    },
                    error : function (data,data2, data3) {
                        alert('error!!!');
                    }
                });
            }

            /* 계좌실명조회API */
            function fnSearchRealName()
            {
                var bank_code_std = $("#bank_code_std_realname").val();
                var account_num = $("#account_num").val();
                var account_holder_info = $("#account_holder_info").val();
                var tran_dtime = new Date().format("yyyyMMddHHmmss");

                var  access_token = "Bearer "+$("#oob_token").val();

                var resData = {"bank_code_std":bank_code_std,"account_num":account_num,"account_holder_info":account_holder_info,"tran_dtime":tran_dtime};

                $.ajax({
                    url: "https://testapi.open-platform.or.kr/v1.0/inquiry/real_name",
                    beforeSend : function(request){
                        request.setRequestHeader("Authorization", access_token);
                        //request.setRequestHeader("Content-Type", "application/json");
                    },
                    type: "POST",
                    //data: {"bank_code_std":bank_code_std,"account_num":account_num,"account_holder_info":account_holder_info,"tran_dtime":tran_dtime},
                    data: JSON.stringify(resData),
                    dataType: "json",
                    success : function (data, data2, data3) {

                        console.log("data==" + data);
                        console.log("data2==" + data2);
                        console.log("data3==" + data3);
                        console.log("data3.res==" + data3.responseText);
                        $('#real_name').html(data3.responseText.replace(/,/gi, ",\n"));
                        //var list = JSON.parse(data);
                        $("#account_holder_name").val(data.account_holder_name);
                    },
                    error : function (data,data2, data3) {
                        alert('error!!!');
                    }
                });
            }

            //출금 줄 늘리기
            function fnAddRow(){

                var len = $('#reqListBody > tr').size();
                //var len = $('tr').size();
                //alert(len);

                var str = "<tr>";
                str     += "<td><input type='text' style='width:30px' class='form-control' name='tran_no' value='" + len + "'  /></td>";
                str		 +=  "<td><span class='inpbox'  style='width:40px' ><input type='text' class='form-control' name='bank_code_std'  /></span></td>";
                str     += "<td><span class='inpbox' style='width:120px' ><input type='text' class='form-control' name='fintech_use_num'  /></span></td>";
                str     += "<td><span class='inpbox' style='width:60px' ><input type='text' class='form-control' name='account_holder_name'  /></span></td>";
                str     += "<td><span class='inpbox' style='width:120px' ><input type='text' class='form-control' name='print_content'  /></span></td>";
                str     += "<td><span class='inpbox' style='width:90px' ><input type='text' class='form-control' name='tran_amt'  /></span></td>";
                str     += "<td><span class='inpbox' style='width:70px' ><input type='text' class='form-control' name='cms_no'  /></span></td>";
                str     += "<td><input type='text' class='form-control' name='tran_dtime' value='" + currentTime + "'  /></td>";
                str		 += "</tr>";

                $("#reqListBody > tr").last().after(str);

            }

            /* 입금이체조회API */
            function fnTransferDeposit()
            {
                //getInquryAccountTest
                var wd_pass_phrase = $("#wd_pass_phrase").val();
                var wd_print_content = $("#wd_print_content").val();

                var req_cnt =  $('#reqListBody > tr').size();

                var strJson = "";
                var arr = new Array();


                for(i=0; i< req_cnt ; i++){
                    var obj = new Object();
                    obj.tran_no =  $("input[name=tran_no]:eq(" + i + ")").val();
                    var type = $('#num_type').val();
                    if(type=="1"){
                        obj.fintech_use_num =  $("input[name=fintech_use_num_transfer]:eq(" + i + ")").val();
                    }else{
                        obj.bank_code_std =  $("input[name=bank_code_std]:eq(" + i + ")").val();
                        obj.account_num =  $("input[name=fintech_use_num_transfer]:eq(" + i + ")").val();
                        obj.account_holder_name = $("input[name=account_holder_name_transfer]:eq(" + i + ")").val();
                    }

                    obj.print_content =  $("input[name=print_content]:eq(" + i + ")").val();
                    obj.cms_no =  $("input[name=cms_no]:eq(" + i + ")").val();
                    obj.tran_amt =  $("input[name=tran_amt]:eq(" + i + ")").val();
                    arr.push(obj);
                }

                var resData = {"wd_pass_phrase":wd_pass_phrase,"wd_print_content":wd_print_content,"req_cnt":req_cnt, "tran_dtime":new Date().format('yyyyMMddHHmmss')
                    ,"req_list": arr};

                console.log(resData);

                var  access_token = "Bearer "+$("#oob_token").val();

                $.ajax({

                    url: "https://testapi.open-platform.or.kr/v1.0/transfer/deposit2",
                    beforeSend : function(request){
                        request.setRequestHeader("Authorization", access_token);
                    },
                    type: "POST",
                    data: JSON.stringify(resData),
                    dataType: "json",
                    success : function (data, data2, data3) {
                        /* alert(data);
                        alert(data2);
                        alert(data3.responseText);
                        var list = JSON.parse(data); */
                        $("#transfer_deposit").text(data3.responseText.replace(/,/gi, ",\n"));
                    },
                    error : function (data,data2, data3) {
                        alert('error!!!');
                    }
                });
            }
        </script>

    </body>
</html>
