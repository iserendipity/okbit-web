package io.dj.exchange.controller.api;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import io.dj.exchange.annotation.TransactionalEx;
import io.dj.exchange.domain.dto.OtpSettingDTO;
import io.dj.exchange.domain.primary.OtpSetting;
import io.dj.exchange.domain.primary.Response;
import io.dj.exchange.domain.primary.User;
import io.dj.exchange.domain.primary.UserSetting;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.exception.OkbitException;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.repository.hibernate.primary.OtpRepository;
import io.dj.exchange.repository.hibernate.primary.TransactionsRepository;
import io.dj.exchange.repository.hibernate.primary.UserSettingRepository;
import io.dj.exchange.security.GotpUtils;
import io.dj.exchange.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;


@Slf4j
@RestController
@RequestMapping("/api/profile")
public class ProfileApiController {

    @Autowired
    private ApiProvider apiProvider;

    @Autowired
    private UserSettingRepository userSettingRepository;

    @Autowired
    private FileService fileService;

    @Autowired
    private OtpRepository otpRepository;

    @Autowired
    private TransactionsRepository transactionsRepository;

    @Autowired
    private GoogleAuthenticator authenticator;

    @RequestMapping(value = "/addBank", method = RequestMethod.POST, produces="application/json", consumes="multipart/form-data")
    public Response<String> addBank(
            @ModelAttribute User user
            , @RequestParam(name = "bankbook", required = false) MultipartFile bankbook) throws Exception {

            String ret = fileService.add(
                    user.getId()
                    , "bankbook"
                    , bankbook
                    , 0 == 0 ? false : true
            );

        return Response.<String>builder()
                .desc(ret)
                .code(CodeEnum.SUCCESS.name().equals(ret) ? CodeEnum.SUCCESS : CodeEnum.FAIL)
                .build();
    }

    @RequestMapping(value = "/addId", method = RequestMethod.POST, produces="application/json", consumes="multipart/form-data")
    public Response<String> addId(
            @ModelAttribute User user
            , @RequestParam(name = "idcard", required = false) MultipartFile idcard) throws Exception {

        String ret = fileService.add(
                user.getId()
                , "idcard"
                , idcard
                , 0 == 0 ? false : true
        );

        return Response.<String>builder()
                .desc(ret)
                .code(CodeEnum.SUCCESS.name().equals(ret) ? CodeEnum.SUCCESS : CodeEnum.FAIL)
                .build();
    }

    @TransactionalEx
    @RequestMapping(value = "/activateOtp", method = RequestMethod.POST)
    public Response<OtpSetting> activateOtp(@ModelAttribute("user") User user, @Valid @RequestBody OtpSettingDTO.OtpSettingRequest request) throws Exception {

        if(GotpUtils.otpCheckActivate(user, request)){
            UserSetting userSetting = userSettingRepository.findOneByUserId(user.getId());
            userSetting.setOtpActive("Y");
            userSettingRepository.save(userSetting);
        }

        return Response.<OtpSetting>builder()
                .code(CodeEnum.SUCCESS)
                .build();
    }

    @TransactionalEx
    @RequestMapping(value = "/changeOtpSetting", method = RequestMethod.POST)
    public Response<OtpSetting> changeOtpSetting(@ModelAttribute("user") User user, @Valid @RequestBody OtpSettingDTO.OtpSettingRequest request) throws Exception {

        //log.info("-----------request : {}", request);

        OtpSetting setting = otpRepository.findOneByUserId(user.getId());

        if(setting.getUseSetting() != null && setting.getUseSetting().equals("Y")){//기존 세팅에 otp 사용중이면

            Integer verificationCode = request.getOtpCode();
            if (verificationCode != null) {
                if (!authenticator.authorize(user.getUserSetting().getOtpHash(), verificationCode)) {
                    //if (!otpProvider.isOtpValid(retrieveUser.getUser(), verificationCode)) {
                    throw new OkbitException("INVALID_VERIFICATIONCODE");
                }
            } else {
                throw new OkbitException("OTP_CODE_MANDATORY");
            }

        }

        setting.setUseWithdraw(request.getUseWithdraw());
        setting.setUseLogin(request.getUseLogin());
        setting.setUseSetting(request.getUseSetting());

        otpRepository.save(setting);

        return Response.<OtpSetting>builder()
                .code(CodeEnum.SUCCESS)
                .build();
    }

}
