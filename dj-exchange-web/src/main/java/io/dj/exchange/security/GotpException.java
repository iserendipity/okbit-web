package io.dj.exchange.security;

import org.springframework.security.authentication.AuthenticationServiceException;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 18.
 * Description
 */
public class GotpException extends AuthenticationServiceException {
    public GotpException(String msg) {
        super(msg);
    }
}
