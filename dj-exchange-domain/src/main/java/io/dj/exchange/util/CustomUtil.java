package io.dj.exchange.util;

import java.math.BigDecimal;
import java.util.stream.IntStream;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 27.
 * Description
 */
public class CustomUtil {

    public static String getRandomInt(int position){

        StringBuffer sb = new StringBuffer();
        IntStream.range(0, position).forEach(i -> {
            int n = (int)(Math.random() * 10);
            sb.append(n);
        });

        return sb.toString();

    }

    /**
     * @param decimal     부동소수
     * @param loc            자릿수 제한 위치. 2자리까지 보이면 2 , 3자리까지면 3 이런식으로 지정
     * @param mode        1 내림 , 2 반올림 , 3 올림
     * @return
     */
    public static double decimalScale(String decimal , int loc , int mode) {

        BigDecimal bd = new BigDecimal(decimal);
        BigDecimal result = null;

        if(mode == 1) {
            result = bd.setScale(loc, BigDecimal.ROUND_DOWN);       //내림
        }
        else if(mode == 2) {
            result = bd.setScale(loc, BigDecimal.ROUND_HALF_UP);   //반올림
        }
        else if(mode == 3) {
            result = bd.setScale(loc, BigDecimal.ROUND_UP);             //올림
        }else{
            result = bd.setScale(loc, BigDecimal.ROUND_UP);
        }

        return result.doubleValue();

    }

}
