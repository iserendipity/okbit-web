package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.QnaPK;
import io.dj.exchange.domain.primary.SupportQna;
import io.dj.exchange.enums.StatusEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SupportQnaRepository extends JpaRepository<SupportQna, QnaPK> {

    long countByShowYn(String showYn);

    Page<SupportQna> findAllByShowYn(String showYn, Pageable p);

    Optional<SupportQna> findOneById(long id);

    Page<SupportQna> findAllByStatusAndUserId(String status, Long userId, Pageable p);

    Page<SupportQna> findAllByStatusAndUserIdOrderByRegDtDesc(String name, Long id, Pageable p);

    Page<SupportQna> findAllByUserIdOrderByRegDtDesc(Long id, Pageable p);

    Page<SupportQna> findAllByParentIdOrderByRegDtDesc(Long id, Pageable p);

    List<SupportQna> findAllByParentIdOrderByRegDtDesc(Long id);

    Page<SupportQna> findAllByUserIdAndParentIdIsNullOrderByRegDtDesc(Long id, Pageable p);

    Page<SupportQna> findAllByUserIdAndParentIdIsNullOrderByRegDtAsc(Long id, Pageable p);

    List<SupportQna> findAllByParentIdOrderByRegDtAsc(Long id);

    Page<SupportQna> findAllByParentIdOrderByRegDtAsc(Long id, Pageable p);
}
