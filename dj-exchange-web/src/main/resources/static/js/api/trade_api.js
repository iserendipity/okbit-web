/**
 * Created by jeongwoo on 2017. 4. 21..
 */
function TradeApi () {
    this.call = function(uri, callback, resultProcFunc, params) {
        $.ajax({
            url: "/api/trade" + uri
            , type: "POST"
            , dataType: 'json'
            , contentType:"application/json; charset=UTF-8"
            , data: JSON.stringify(params)
            , success: function (data) {
                resultProcFunc(data);
                return callback(data);
            }
            , error:function(e){
                //showAlert("ERROR : " + JSON.parse(e.responseText).message);
            }
        });
        return true;
    };

    this.buyOrder = function(callback, params){
        return this.call("/buy", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //alert(result.desc);
                $('#resultIndicator').text(result.desc);
                $('#resultIndicator').collapse('show');

            }
        }, params);
    };

    this.sellOrder = function(callback, params){
        return this.call("/sell", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                $('#resultIndicator').text(result.desc);
                $('#resultIndicator').collapse('show');
            }
        }, params);
    };

    this.cancelOrder = function(callback, params){
        return this.call("/cancel", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

}