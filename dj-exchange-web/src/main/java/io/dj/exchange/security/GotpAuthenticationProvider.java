package io.dj.exchange.security;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import io.dj.exchange.domain.primary.CurrentUser;
import io.dj.exchange.provider.OtpProvider;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 18.
 * Description
 */
@Slf4j
public class GotpAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Setter
    private PasswordEncoder passwordEncoder;
    @Setter
    private UserDetailsService userDetailsService;
    @Setter
    private UserDetailsChecker loginPostUserDetailsChecker;
    @Setter
    private GoogleAuthenticator authenticator;

    @Autowired
    private OtpProvider otpProvider;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        log.info("additionalAuthenticationChecks: username={}", userDetails.getUsername());
        if (authentication.getCredentials() == null) {
            log.error("Authentication failed: no credentials provided.");
            throw new BadCredentialsException("No Credentials");
        }
        String credentialsPassword = authentication.getCredentials().toString();
        if (!passwordEncoder.matches(credentialsPassword, userDetails.getPassword())) {
            log.error("Authentication failed: password does not match stored value.");
            throw new BadCredentialsException("BadCredentials");
        }

    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        log.info("retrieveUser: {}", username);
        CurrentUser retrieveUser;

        try {
            retrieveUser = (CurrentUser) userDetailsService.loadUserByUsername(username);

            if (retrieveUser == null) {
                throw new InternalAuthenticationServiceException("UserDetails returned null.");
            }

            if("Y".equals(retrieveUser.getUser().getUserSetting().getOtpActive())){
                GotpUtils.otpCheckLogin(retrieveUser.getUser(), authentication);
            }

        } catch (UsernameNotFoundException notFoundException) {
            if (hideUserNotFoundExceptions) {
                throw new BadCredentialsException("Bad_Credentials.");
            }
            throw notFoundException;
        } catch (Exception authenticationProblem) {
            throw new InternalAuthenticationServiceException(authenticationProblem.getMessage(), authenticationProblem);
        }

        return retrieveUser;
    }


}