/**
 * Created by jeongwoo on 2017. 4. 21..
 */
function ConsoleApi () {
    this.call = function(uri, callback, resultProcFunc, params) {
        $.ajax({
            url: "/api/console" + uri
            , type: "POST"
            , dataType: 'json'
            , contentType:"application/json; charset=UTF-8"
            , data: JSON.stringify(params)
            , success: function (data) {
                resultProcFunc(data);
                return callback(data);
            }
            , error:function(e){
                //showAlert("ERROR : " + JSON.parse(e.responseText).message);
            }
        });
        return true;
    };

    this.loadBuyOrders = function(callback, params) {

        return this.call("/ordersBuy", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };
    this.loadSellOrders = function(callback, params) {

        return this.call("/ordersSell", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.loadBuyFullOrders = function(callback, params) {
        //console.log('-------');
        return this.call("/ordersFullBuy", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };
    this.loadSellFullOrders = function(callback, params) {
        //console.log('-----2--');
        return this.call("/ordersFullSell", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };


    this.trades = function(callback, params) {
        return this.call("/trades", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.fullTrades = function(callback, params) {
        return this.call("/fullTrades", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.myTrades = function(callback, params) {
        return this.call("/myTrades", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.myFullTrades = function(callback, params) {
        return this.call("/myFullTrades", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.buyOrder = function(callback, params){
        return this.call("/buy", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };
}