package io.dj.exchange.enums;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 8.
 * Description
 */
public enum BatchKey {
    ResolutionD,
    Resolution1M,
    Resolution5M,
    Resolution10M,
    Resolution15M,
    Resolution30M,
    Resolution60M
}
