package io.dj.exchange.domain.dto;

import io.dj.exchange.enums.CodeEnum;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 28.
 * Description
 */
@Data
public class FeignResult implements Serializable{

    private String code;

    private String msg, desc, debug;

    private FeignResultData result;


}
