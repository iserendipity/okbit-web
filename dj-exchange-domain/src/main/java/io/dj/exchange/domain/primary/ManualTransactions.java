package io.dj.exchange.domain.primary;

import lombok.Data;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 10.
 * Description
 */
@Data
@Entity
@Table(name = "manual_transaction")
@IdClass(value = ManualTransactionsPK.class)
public class ManualTransactions implements Serializable{

    @Id
    @Column(name = "id")
    String id;

    @Column(name = "tx_id")
    String txId;

    @Id
    @Column(name = "user_id")
    Long userId;

    @Column(name = "coin_name")
    private String coinName;

    @Column(name = "category")
    private String category;

    @Column(name = "reg_dt")
    private LocalDateTime regDt;

    @Column(name = "completed_dt")
    private LocalDateTime completedDt;

    @Column(name = "status")
    private String status;

    @Column(name = "is_confirmation")
    private String isConfirmation;

    @Column(name = "reason")
    private String reason;

    @Column(name = "deposit_dt")
    private LocalDateTime depositDt;

    @Column(name = "amount")
    @NumberFormat(style = NumberFormat.Style.CURRENCY)
    private BigDecimal amount;

    @Column(name = "account")
    private String account;

    @Column(name = "address")
    private String address;

    @Column(name = "user_nm")
    private String userNm;

    @Column(name = "bank_nm")
    private String bankNm;

    @Column(name = "bank_code")
    private String bankCode;

    @Transient
    private String regDtTxt;

    @Transient
    private String depositDtTxt;

    @Transient
    private String realName;

    @Transient
    private String level;

}
