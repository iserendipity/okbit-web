package io.dj.exchange.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 18.
 * Description
 */
@Slf4j
public class GotpAuthenticationFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) throws IOException, ServletException {
        log.debug("request: {}, response: {}, ", request, response, exception.getClass().getTypeName());
        if (exception instanceof BadCredentialsException) {
            response.sendRedirect("/?error=" + exception.getMessage());
        } else if (exception instanceof InternalAuthenticationServiceException) {
            response.sendRedirect("/?error=" + exception.getMessage());
        } else if (exception instanceof GotpException){
            response.sendRedirect("/?otp=required");
        }
    }

}
