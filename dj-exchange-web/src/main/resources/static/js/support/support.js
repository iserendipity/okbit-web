var supportApi = new SupportApi();

submitQuestion = function(){

    var params = $('#qnaForm').serializeObject();

    //console.log(params);

    $('body').loading({
        message : 'UPLOADING...',
        theme : 'dark'
        //overlay: $("#bank_loading_div")
    });

    supportApi.write(function(data){

        $('body').loading('stop');
        location.href = '/support';

    }, params);

};

getMyQuestions = function(pageNo){

    var params = new Object();
    params.page = pageNo-1;
    params.size = 20;

    supportApi.getQuestions(function(data){

        if(data.page && data.page.totalPages > 0 ) {
            setPageValues('myQuestionPage', data.page, 'getMyQuestion', pageNo);
        }

        if(data._embedded && data._embedded.supportQnas && data._embedded.supportQnas.length > 0){

            makeTemplate('questionWrapper','questionTemplate', data._embedded.supportQnas);

        }else{

            $('#questionWrapper').html('<div class="media text-center p-10">'+no_data_msg+'</div>');

        }

    }, params);

};

getNotices = function(pageNo){

    var params = new Object();
    params.page = pageNo-1;
    params.size = 5;

    supportApi.getNotices(function(data){

        if(data.page && data.page.totalPages > 0 ) {
            setPageValues('noticePage', data.page, 'getNotices', pageNo);
        }

        if(data._embedded && data._embedded.webNoticeses && data._embedded.webNoticeses.length > 0){

            makeTemplate('noticeWrapper','noticeTemplate', data._embedded.webNoticeses);

        }else{

            $('#noticeWrapper').html('<div class="media text-center p-10">'+no_data_msg+'</div>');

        }

    }, params);


};

getDetail = function(id){

    var x = document.createElement('INPUT');
    x.setAttribute('name', 'id');
    x.setAttribute('value', id);

    var form = document.createElement('form');
    form.setAttribute('action', '/qnaDetail');
    form.setAttribute('method', 'POST');
    form.appendChild(x);
    document.body.appendChild(form);
    form.submit();

};

init = function(){

  getMyQuestions('1');
  getNotices('1');

};