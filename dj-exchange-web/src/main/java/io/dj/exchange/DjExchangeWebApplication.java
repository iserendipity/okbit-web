package io.dj.exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "io.dj.exchange.repository.hibernate")
@EnableRedisRepositories(basePackages = {"io.dj.exchange.repository.cache"})
@EnableFeignClients
@EnableAutoConfiguration
public class DjExchangeWebApplication {

    public static void main(String[] args) {

        SpringApplication.run(DjExchangeWebApplication.class, args);

    }


//    @Bean
//    public ScheduledExecutorFactoryBean scheduledExecutorService () {
//        ScheduledExecutorFactoryBean bean = new ScheduledExecutorFactoryBean();
//        bean.setPoolSize(5);
//        return bean;
//    }
//
//    @Bean
//    public HttpMessageConverter<String> responseBodyConverter() {
//        return new StringHttpMessageConverter(Charset.forName("UTF-8"));
//    }
//
//    @Bean
//    public CharacterEncodingFilter characterEncodingFilter() {
//        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
//        characterEncodingFilter.setEncoding("UTF-8");
//        characterEncodingFilter.setForceEncoding(true);
//        return characterEncodingFilter;
//    }

}
