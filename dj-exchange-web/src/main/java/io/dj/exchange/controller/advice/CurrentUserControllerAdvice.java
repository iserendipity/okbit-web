package io.dj.exchange.controller.advice;

import io.dj.exchange.domain.primary.CurrentUser;
import io.dj.exchange.domain.primary.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * Created by tommy on 2016. 2. 29..
 */
@ControllerAdvice
@Slf4j
public class CurrentUserControllerAdvice {

    @ModelAttribute("user")
    public User getCurrentUser(Authentication authentication/*, @RequestBody String payload*/) {
        return (authentication == null) ? null : ((CurrentUser) authentication.getPrincipal()).getUser();
    }

//    @ModelAttribute("feignHeader")
//    public MultiValueMap<String, String> getHeaderMap(Authentication authentication) {
//
//        User currentUser = (authentication == null) ? null : ((CurrentUser) authentication.getPrincipal()).getUser();
//        MultiValueMap<String, String> headers = null;
//
//        if(currentUser!= null && !currentUser.getUserSetting().getApiKey().isEmpty())
//        {
//            headers = new LinkedMultiValueMap<>();
//            headers.put(ACCEPT, singletonList(APPLICATION_JSON_VALUE));
//            headers.put(CONTENT_TYPE, singletonList(APPLICATION_JSON_VALUE));
//            headers.put("apiKey", singletonList(currentUser.getUserSetting().getApiKey()));
//
//        }
//
//        return headers;
//    }

}
