package io.dj.exchange.domain.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 7. 11.
 * Description
 */
@Data
public class TodayUsers {

    private long users;
    private String regDt;

    public TodayUsers(long users){
        this.users = users;
    }

    public TodayUsers(long users, String regDt){
        this.users = users;
        this.regDt = regDt;
    }


}
