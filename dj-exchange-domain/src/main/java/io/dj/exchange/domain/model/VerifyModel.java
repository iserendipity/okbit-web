package io.dj.exchange.domain.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 12.
 * Description
 */
@Getter
@Setter
public class VerifyModel {

    private String msg;
    private String realName;
    private String cellphone;
    private long userId;
    private String di;

}
