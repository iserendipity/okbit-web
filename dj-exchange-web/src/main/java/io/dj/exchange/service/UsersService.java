package io.dj.exchange.service;

import com.github.mkopylec.recaptcha.validation.RecaptchaValidator;
import com.github.mkopylec.recaptcha.validation.ValidationResult;
import io.dj.exchange.annotation.TransactionalEx;
import io.dj.exchange.domain.primary.EmailConfirm;
import io.dj.exchange.domain.primary.OtpSetting;
import io.dj.exchange.domain.primary.User;
import io.dj.exchange.domain.primary.UserSetting;
import io.dj.exchange.enums.ErrorCode;
import io.dj.exchange.enums.MailType;
import io.dj.exchange.enums.Role;
import io.dj.exchange.exception.OkbitViewException;
import io.dj.exchange.provider.OtpProvider;
import io.dj.exchange.repository.hibernate.primary.ConfirmRepository;
import io.dj.exchange.repository.hibernate.primary.OtpRepository;
import io.dj.exchange.repository.hibernate.primary.UserRepository;
import io.dj.exchange.repository.hibernate.primary.UserSettingRepository;
import io.dj.exchange.service.mail.MailService;
import io.dj.exchange.util.MailUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Created by jeongwoo on 2017. 4. 4..
 */
@Slf4j
@Service
public class UsersService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserSettingRepository userSettingRepository;

    @Autowired
    ConfirmRepository confirmRepository;

    @Autowired
    OtpRepository otpRepository;

    @Autowired
    MailService mailService;

    @Autowired
    private OtpProvider otpProvider;

    @Autowired
    private RecaptchaValidator recaptchaValidator;

    public void showhideOtp(Long userId) {
        User user = userRepository.findOne(userId);
        //UserSetting userSetting = userSettingRepository.findOneByUserId(user.getId());
        if ("Y".equals(user.getUserSetting().getOtpActive())) {
            user.getUserSetting().setOtpActive("N");
        } else {
            user.getUserSetting().setOtpActive("Y");
        }
        //userSettingRepository.save(user.getUserSetting());
    }

    public User getUser(Long userId) {
        return userRepository.findOne(userId);
    }


    @TransactionalEx
    public String doSignup(String loginId
            , String password
            , HttpServletRequest request) {

        try {
            ValidationResult result = recaptchaValidator.validate(request);
            if (result.isFailure()) {
                return "redirect:/?error=Recaptcha_Error";
            }

            Optional<User> existUser = userRepository.findOneByEmail(loginId);
            if (existUser.isPresent()) {
                return "redirect:/?error=Already_User";
            }

            User user = new User();
            UserSetting userSetting = new UserSetting();

            user.setEmail(loginId);
            user.setPwd(new BCryptPasswordEncoder().encode(password));
            user.setRole(Role.USER);
            user.setActive("N");
            user.setRegDt(LocalDateTime.now());

            userRepository.save(user);

            userSetting.setOtpActive("N");
            userSetting.setOtpHash(otpProvider.genSecretKey(loginId + password));
            userSetting.setLevel("LEV1");
            userSetting.setRegDt(LocalDateTime.now());
            userSetting.setUserId(user.getId());
            userSetting.setTheme("blue");
            userSetting.setApiKey(new BigInteger(1, DigestUtils.md5Digest((user.getEmail()+user.getPwd()).getBytes())).toString(16));

            userSettingRepository.save(userSetting);

            OtpSetting otpSetting = new OtpSetting();
            otpSetting.setUseLogin("Y");
            otpSetting.setUserId(user.getId());
            otpSetting.setUseSetting("Y");
            otpSetting.setUseWithdraw("Y");

            otpRepository.save(otpSetting);

            EmailConfirm confirm = MailUtil.getMailConfirm(user.getEmail());
            confirm.setType(MailType.SIGNUP);
            confirmRepository.save(confirm);

            mailService.sendMailQue(confirm);

            //walletService.createWallets(user);
            return "redirect:/?regist=yes";
        } catch(Exception ex) {
            ex.printStackTrace();

            return "redirect:/?error=Invalid";
        }
    }

    @TransactionalEx
    public String findPassword(String loginId, HttpServletRequest request) throws OkbitViewException{

        User user = userRepository.findOneByEmail(loginId).orElseThrow(() -> new OkbitViewException("NO_USER"));

        EmailConfirm confirm = MailUtil.getMailConfirm(user.getEmail());
        confirm.setType(MailType.FORGETPASS);

        mailService.sendMailQue(confirm);

        confirm.setRegDt(LocalDateTime.now());

        confirmRepository.save(confirm);

        //해당 loginId로 메일을 보낸다.
        try {
            return "redirect:/?find=yes";
        }catch(Exception e){
            return "redirect:/?error=invalid";
        }


    }

    @TransactionalEx
    public String confirmEmail(String hash, String confirmCode) throws OkbitViewException{

        try{
            EmailConfirm confirm = confirmRepository.findOneByHashEmailAndConfirmCodeAndDelDtmIsNull(hash,confirmCode).orElseThrow(()->new OkbitViewException(ErrorCode.EMAIL_EXPIRED));

            if(hash.equals(new BigInteger(1, DigestUtils.md5Digest(confirm.getEmail().getBytes())).toString(16))){

                User user = userRepository.findOneByEmail(confirm.getEmail()).orElseThrow(()-> new OkbitViewException(ErrorCode.EMAIL_EXPIRED));

                user.setActive("Y");
                userRepository.save(user);
                confirm.setDelDtm(LocalDateTime.now());

                return "redirect:/confirm/result?confirm=ok";

            }

        }catch(Exception e){
            e.printStackTrace();

        }
        return "redirect:/confirm/result?confirm=no";

    }

    public boolean dateCheck(EmailConfirm confirm) {

        if(confirm.getRegDt().compareTo(LocalDateTime.now().minusDays(5)) == -1){

            confirm.setDelDtm(LocalDateTime.now());
            confirmRepository.save(confirm);

            return false;
        }

        return true;

    }

    public EmailConfirm getConfirmObjForPasswordReset(String hash, String confirmCode) throws OkbitViewException {

        EmailConfirm confirm = confirmRepository.findOneByHashEmailAndConfirmCodeAndDelDtmIsNull(hash, confirmCode).orElseThrow(() -> new OkbitViewException("EMAIL_EXPIRED"));

        confirm.setDelDtm(LocalDateTime.now());

        return confirm;

    }
}
