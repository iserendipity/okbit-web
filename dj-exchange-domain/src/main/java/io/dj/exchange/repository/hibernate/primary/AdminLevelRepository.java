package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.dto.TodayUsers;
import io.dj.exchange.domain.primary.AdminLevel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 12.
 * Description
 */
public interface AdminLevelRepository extends JpaRepository<AdminLevel, Long>{
    Optional<AdminLevel> findOneByUserId(Long id);

    Page<AdminLevel> findAllByIdcardYnOrBankbookYn(String n, String n1, Pageable p);

    Page<AdminLevel> findAllByIdcardYn(String n, Pageable p);

    Page<AdminLevel> findAllByBankbookYn(String n, Pageable p);

    Optional<AdminLevel> findOneByUserIdAndIdcardYn(long userId, String idCardYn);

    Optional<AdminLevel> findOneByUserIdAndBankbookYn(long userId, String bankbookYn);

    Page<AdminLevel> findAllByIdcardYnOrderByIdcardRegDtAsc(String idCardYn, Pageable p);

    Page<AdminLevel> findAllByBankbookYnOrderByBankbookRegDtAsc(String p, Pageable p1);

    Page<AdminLevel> findAllByLevelAndLevelModifyDtLessThanOrderByLevelModifyDtAsc(String lev2, LocalDateTime dateTime, Pageable p);

    Page<AdminLevel> findAllByLevelOrderByLevelModifyDtAsc(String lev2, Pageable pageRequest);

    Optional<AdminLevel> findOneByDi(String di);

    @Query("select new io.dj.exchange.domain.dto.TodayUsers(function('count', p.userId), function('date_format', p.bankbookRegDt, '%Y-%m-%d')) from AdminLevel p where p.bankbookRegDt >= :dt group by function('date_format', p.bankbookRegDt, '%Y-%m-%d') order by p.bankbookRegDt asc")
    List<TodayUsers> getTodayBank(@Param("dt") LocalDateTime dateTime);

    @Query("select new io.dj.exchange.domain.dto.TodayUsers(function('count', p.userId), function('date_format', p.idcardRegDt, '%Y-%m-%d')) from AdminLevel p where p.idcardRegDt >= :dt group by function('date_format', p.idcardRegDt, '%Y-%m-%d') order by p.idcardRegDt asc")
    List<TodayUsers> getTodayId(@Param("dt") LocalDateTime dateTime);
}
