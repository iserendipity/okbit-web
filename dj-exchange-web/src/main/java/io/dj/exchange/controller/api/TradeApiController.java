package io.dj.exchange.controller.api;

import io.dj.exchange.domain.dto.FeignResult;
import io.dj.exchange.domain.dto.FeignResultData;
import io.dj.exchange.domain.dto.OrdersCount;
import io.dj.exchange.domain.dto.OrdersDTO;
import io.dj.exchange.domain.primary.*;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.enums.CoinEnum;
import io.dj.exchange.enums.OrderStatus;
import io.dj.exchange.enums.OrderType;
import io.dj.exchange.exception.OkbitException;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.repository.hibernate.primary.OrderRepository;
import io.dj.exchange.repository.hibernate.primary.TransactionsRepository;
import io.dj.exchange.service.mail.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@RestController
@RequestMapping("/api/trade")
public class TradeApiController {

    @Autowired
    private ApiProvider apiProvider;

    @Autowired
    private MailService mailService;

//    @Autowired
//    WebsockMsgBrocker websockMsgBrocker;

    @Autowired
    private TransactionsRepository transactionsRepository;

    @Autowired
    private OrderRepository orderRepository;

    public List<OrdersCount> initOrderCount(List<OrdersCount> orderList){

        BigDecimal a = BigDecimal.valueOf(0);
        BigDecimal b;
        BigDecimal c = BigDecimal.valueOf(0);
        BigDecimal d;

        for (int n = 0; n < orderList.size(); n++) {

            b = orderList.get(n).getAmount();

            a = a.add(b);

            d = orderList.get(n).getTotalPrice();

            c = c.add(d);

            orderList.get(n).setTotalCoin(a);
            orderList.get(n).setTotalPrice(c);
            orderList.get(n).initAmount();
            orderList.get(n).initPrice();
            orderList.get(n).initTotalCoin();
            orderList.get(n).initTotalPrice();

        }

        return orderList;
    }

    @RequestMapping(value="/buy", method=RequestMethod.POST)
    public Response<FeignResult> buy(@ModelAttribute("user") User user, @Valid @RequestBody OrdersDTO.RequestBuy request) throws OkbitException {

        //krw 보유분 조회
//        FeignResult result = apiProvider.getWallet("KRW");
//        FeignResultData data = result != null ? result.getResult() : null;
//        if(data == null || (data.getWallet().getAvailableBalance().compareTo(request.getPrice().multiply(request.getAmount())) = -1){
//
//        }

        if(request.getToCoin().equals("BITCOIN") && user.getId() != 86l){

            throw new OkbitException("BTC_NOT_NOW");

        }

        FeignResult result = apiProvider.buyOrder(request.getPrice(), request.getAmount(), request.getToCoin());

        List<OrdersCount> orders = orderRepository.getBuy(OrderType.BUY, "KRW", request.getToCoin(), OrderStatus.PLACED, new PageRequest(0,20)).getContent();

        Map<String, Object> dataMap = new HashMap<>();
        List<OrdersCount> sellOrders = orderRepository.getSell(OrderType.SELL, request.getToCoin(), "KRW", OrderStatus.PLACED, new PageRequest(0,20)).getContent();
        List<OrdersCount> buyOrders = orderRepository.getBuy(OrderType.BUY, "KRW", request.getToCoin(), OrderStatus.PLACED, new PageRequest(0,20)).getContent();

        dataMap.put("coinName", request.getToCoin());
        dataMap.put("sellOrders", initOrderCount(sellOrders));
        dataMap.put("buyOrders", initOrderCount(buyOrders));

        mailService.sendTradeQue(dataMap, user.getId());

        //websockMsgBrocker.sendTo("ORDER_REFRESH", user.getId(), dataMap);

        return Response.<FeignResult>builder()
                .code(CodeEnum.SUCCESS)
                .data(result)
                .build();

    }

    @RequestMapping(value="/sell", method=RequestMethod.POST)
    public Response<FeignResult> sell(@ModelAttribute("user") User user, @Valid @RequestBody OrdersDTO.RequestSell request) throws OkbitException{

        if(request.getFromCoin().equals("BITCOIN") && user.getId() != 86l){

            throw new OkbitException("BTC_NOT_NOW");

        }


        FeignResult result = apiProvider.sellOrder(request.getPrice(), request.getAmount(), request.getFromCoin());

        List<OrdersCount> orders = orderRepository.getSell(OrderType.SELL, request.getFromCoin(), "KRW", OrderStatus.PLACED, new PageRequest(0,20)).getContent();

        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("orderType", request.getOrderType());
        dataMap.put("orderData", initOrderCount(orders));

        List<OrdersCount> sellOrders = orderRepository.getSell(OrderType.SELL, request.getFromCoin(), "KRW", OrderStatus.PLACED, new PageRequest(0,20)).getContent();
        List<OrdersCount> buyOrders = orderRepository.getBuy(OrderType.BUY, "KRW", request.getFromCoin(), OrderStatus.PLACED, new PageRequest(0,20)).getContent();

        dataMap.put("coinName", request.getFromCoin());
        dataMap.put("sellOrders", initOrderCount(sellOrders));
        dataMap.put("buyOrders", initOrderCount(buyOrders));

        mailService.sendTradeQue(dataMap, user.getId());

        //websockMsgBrocker.sendTo("ORDER_REFRESH", user.getId(), dataMap);

        return Response.<FeignResult>builder()
                .code(CodeEnum.SUCCESS)
                .data(result)
                .build();

    }

    @RequestMapping(value="/cancel", method=RequestMethod.POST)
    public Response<FeignResult> cancel(@ModelAttribute("user") User user, @Valid @RequestBody OrdersDTO.RequestToOrderCancel request) {

        FeignResult result = apiProvider.cancelOrder(request.getOrderId());

        Orders order = orderRepository.findOneById(request.getOrderId());

        Map<String, Object> dataMap = new HashMap<>();

        String coinName = order.getFromCoinName().equals("KRW") ? order.getToCoinName() : order.getFromCoinName();

        List<OrdersCount> sellOrders = orderRepository.getSell(OrderType.SELL, coinName, "KRW", OrderStatus.PLACED, new PageRequest(0,20)).getContent();
        List<OrdersCount> buyOrders = orderRepository.getBuy(OrderType.BUY, "KRW", coinName, OrderStatus.PLACED, new PageRequest(0,20)).getContent();

        dataMap.put("coinName", order.getFromCoinName().equals("KRW") ? order.getToCoinName() : order.getFromCoinName());
        dataMap.put("sellOrders", initOrderCount(sellOrders));
        dataMap.put("buyOrders", initOrderCount(buyOrders));

        mailService.sendTradeQue(dataMap, user.getId());

        //websockMsgBrocker.sendTo("ORDER_REFRESH", user.getId(), dataMap);

        return Response.<FeignResult>builder()
                .code(CodeEnum.SUCCESS)
                .data(result)
                .build();

    }

    @RequestMapping(value="/wallet", method=RequestMethod.POST)
    public Response<Wallet> wallet(@Valid @RequestBody Wallet wallet, @ModelAttribute("user") User user) {

        FeignResult result = apiProvider.getWallet(wallet.getCoinName());

        return Response.<Wallet>builder()
                .code(CodeEnum.SUCCESS)
                .data(result.getResult().getWallet())
                .build();

    }

    @RequestMapping(value="/wallets", method=RequestMethod.POST)
    public Response<List<Wallet>> wallet(@PageableDefault(sort = { "id" }, direction = Sort.Direction.DESC, size = 30) Pageable p) throws Exception{

            FeignResult result = apiProvider.getWalletAll();

            FeignResultData data = result.getResult();

            return Response.<List<Wallet>>builder()
                    .code(CodeEnum.SUCCESS)
                    //.data(apiProvider.getWalletAll(headers).getResult().getWallets())
                    .build();

    }

    @RequestMapping(value="/registWallet", method=RequestMethod.POST)
    public Response<Wallet> registWallet(@Valid @RequestBody Wallet wallet) {

        return Response.<Wallet>builder()
                .code(CodeEnum.SUCCESS)
                .data(apiProvider.registWallet(wallet.getCoinName()))
                .build();

    }

    @RequestMapping(value="/transactions", method=RequestMethod.POST)
    public Response<List<Transactions>> transactions(@Valid @RequestBody Transactions transactions, @ModelAttribute("user") User user, @PageableDefault(sort = { "dt" }, direction = Sort.Direction.DESC, size = 30) Pageable p){

        return Response.<List<Transactions>>builder()
                .code(CodeEnum.SUCCESS)
                //.data(apiProvider.getTransactions(transactions.getCategory(), p.getPageNumber(), p.getPageSize()).getResult().getTransactions())
                .data(transactionsRepository.findAllByCategoryAndUserId(transactions.getCategory(), user.getId(), p).getContent())
                //.data(transRepository.findAllByCategory(transactions.getCategory(), p).getContent())
                .build();

    }

}
