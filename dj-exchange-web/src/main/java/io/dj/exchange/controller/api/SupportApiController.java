package io.dj.exchange.controller.api;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import io.dj.exchange.domain.primary.*;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.enums.StatusEnum;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.repository.hibernate.primary.*;
import io.dj.exchange.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Slf4j
@RestController
@RequestMapping("/api/support")
public class SupportApiController {

    @Autowired
    private ApiProvider apiProvider;

    @Autowired
    private UserSettingRepository userSettingRepository;

    @Autowired
    private FileService fileService;

    @Autowired
    private OtpRepository otpRepository;

    @Autowired
    private TransactionsRepository transactionsRepository;

    @Autowired
    private GoogleAuthenticator authenticator;

    @Autowired
    private SupportQnaRepository qnaRepository;

    @Autowired
    private WebNoticesRepository webNoticesRepository;

    @RequestMapping(value="/write")
    public Response<SupportQna> writeQna(@ModelAttribute("user") User user, @RequestBody @Valid SupportQna qna){

        qna.setUserId(user.getId());
        qna.setRegDt(LocalDateTime.now());
        qna.setShowYn("Y");
        qna.setReadYn("Y");
        qna.setStatus(StatusEnum.PROGRESS.name());

        qnaRepository.save(qna);

        return Response.<SupportQna>builder()
                .code(CodeEnum.SUCCESS)
                //.data(apiProvider.getWalletAll(headers).getResult().getWallets())
                .build();
    }

    @RequestMapping(value="/questions", method= RequestMethod.POST, produces = "application/json")
    public PagedResources<SupportQna> questions(PagedResourcesAssembler assembler,
                                                     @ModelAttribute("user") User user,
                                                     Pageable p){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        Page<SupportQna> result = qnaRepository.findAllByUserIdAndParentIdIsNullOrderByRegDtAsc(user.getId(), p);

        //List<SupportQna> list = qnaRepository.findAllByParentId

        if(result.hasContent()){

            result.getContent().stream().forEach(data ->{

                data.setRegDtTxt(data.getRegDt().format(formatter));
                List<SupportQna> replyList = qnaRepository.findAllByParentIdOrderByRegDtAsc(data.getId());
                //data.setSupportQna(replyList);

                data.setUnreadCnt(replyList.parallelStream().filter(reply -> reply.getReadYn().equals("N")).collect(Collectors.toList()).size());

            });

        }

        return assembler.toResource(result);

    }

    @RequestMapping(value="/notices", method= RequestMethod.POST, produces = "application/json")
    public PagedResources<WebNotices> notices(PagedResourcesAssembler assembler,
                                                Pageable p){

        //Page<SupportQna> result = qnaRepository.findAllByUserIdAndParentIdIsNullOrderByRegDtDesc(user.getId(), p);
        Page<WebNotices> result = webNoticesRepository.findAllByStatusOrderByDtDesc("USE", p);
        //log.info("------------ : {}", p);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        result.getContent().stream().forEach(data ->{

            data.setDtTxt(data.getDt().format(formatter));

        });

        return assembler.toResource(result);

    }

}
