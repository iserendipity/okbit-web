package io.dj.exchange.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import feign.Client;
import feign.Feign;
import feign.Logger;
import feign.RequestInterceptor;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.form.FormEncoder;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.httpclient.ApacheHttpClient;
import feign.slf4j.Slf4jLogger;
import io.dj.exchange.domain.primary.CurrentUser;
import io.dj.exchange.domain.primary.User;
import io.dj.exchange.exception.decoder.FeignExceptionDecoder;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.provider.BankServiceProvider;
import io.dj.exchange.provider.BitfinexProvider;
import io.dj.exchange.util.LocalDateTimeConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@Configuration
@Slf4j
public class FeignConfig {

    @Autowired
    HttpServletRequest request;

    @Bean
    public Client client() {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();

        CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setConnectionManager(connectionManager)
                .setMaxConnPerRoute(1000)
                .setMaxConnTotal(1000)
                .build();
        return new ApacheHttpClient(httpClient);
    }

    @Bean
    public Feign.Builder feignBuilder() {
        return Feign.builder()
                .client(client());
    }

    @Bean
    public Decoder decoder() {
        return new GsonDecoder(createGson());
    }

    @Bean
    public Encoder encoder() {
        return new GsonEncoder(createGson());
    }

    private Gson createGson() {
        return new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeConverter())
                .create();
    }

    @Bean
    BankServiceProvider bankServiceProvider(){
        return Feign
                .builder()
                //.requestInterceptor(new BasicAuthRequestInterceptor("iserendipity@me.com", "rkdwjddn12!"))
                .encoder(new FormEncoder(new GsonEncoder()))
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.FULL)
                .target(BankServiceProvider.class, "https://testapi.open-platform.or.kr");
    }

//    @Bean
//    ApiProvider apiProvider(){
//        return Feign
//                .builder()
//                //.requestInterceptor(new BasicAuthRequestInterceptor("iserendipity@me.com", "rkdwjddn12!"))
//                .encoder(new FormEncoder(new GsonEncoder()))
//                .decoder(new GsonDecoder())
//                .log(new Slf4jLogger())
//                .logLevel(Logger.Level.FULL)
//                .target(ApiProvider.class, "http://localhost:8080/common/api");
//    }

    @Bean
    ApiProvider apiProvider(){
        return Feign
                .builder()
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.FULL)
                .errorDecoder(new FeignExceptionDecoder())
                .requestInterceptor(requestInterceptor())
                .target(ApiProvider.class, "https://api.ok-bit.com/api");

                //.target(ApiProvider.class, "http://okbit-api-lbs-306957068.ap-northeast-2.elb.amazonaws.com:8080/api");
                //.target(ApiProvider.class, "http://okbit-api-elb-224325200.ap-northeast-2.elb.amazonaws.com:8080/api");
                //.target(ApiProvider.class, "http://13.124.80.63:8080/api");


    }

    @Bean
    ApiProvider adminProvider(){
        return Feign
                .builder()
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.FULL)
                .errorDecoder(new FeignExceptionDecoder())
                .target(ApiProvider.class, "https://api.ok-bit.com/api");
                //.target(ApiProvider.class, "http://okbit-api-lbs-306957068.ap-northeast-2.elb.amazonaws.com:8080/api");
                //.requestInterceptor(requestInterceptor())
                //.target(ApiProvider.class, "http://okbit-api-elb-224325200.ap-northeast-2.elb.amazonaws.com:8080/api");
        //.target(ApiProvider.class, "http://13.124.80.63:8080/api");

    }


    @Bean
    BitfinexProvider bitfinexProvider(){
        return Feign
                .builder()
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.FULL)
                .errorDecoder(new FeignExceptionDecoder())
                //.requestInterceptor(requestInterceptor())
                .target(BitfinexProvider.class, "https://api.bitfinex.com/v2");
        //.target(ApiProvider.class, "http://13.124.80.63:8080/api");

    }

    @Bean
    ApiProvider testProvider(){
        return Feign
                .builder()
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.FULL)
                .errorDecoder(new FeignExceptionDecoder())
                //.requestInterceptor(requestInterceptor())
                .target(ApiProvider.class, "https://api.ok-bit.com/api");
                //.target(ApiProvider.class, "http://okbit-api-lbs-306957068.ap-northeast-2.elb.amazonaws.com:8080/api");
                //.target(ApiProvider.class, "http://okbit-api-elb-224325200.ap-northeast-2.elb.amazonaws.com:8080/api");
    }

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            User user = ((CurrentUser) authentication.getPrincipal()).getUser();
            requestTemplate.header("apiKey", user.getUserSetting().getApiKey());
            requestTemplate.header("CONTENT-TYPE", APPLICATION_JSON_VALUE);
            requestTemplate.header("X-EXCHANGE-IP", getUserIP(request));
        };
    }

    public String getUserIP(HttpServletRequest request) {
        String ip = request.getHeader("X-FORWARDED-FOR");
        log.info("TEST : X-FORWARDED-FOR : "+ip);
        if (ip == null) {
            ip = request.getHeader("Proxy-Client-IP");
            log.info("TEST : Proxy-Client-IP : "+ip);
        }
        if (ip == null) {
            ip = request.getHeader("WL-Proxy-Client-IP");
            log.info("TEST : WL-Proxy-Client-IP : "+ip);
        }
        if (ip == null) {
            ip = request.getHeader("HTTP_CLIENT_IP");
            log.info("TEST : HTTP_CLIENT_IP : "+ip);
        }
        if (ip == null) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            log.info("TEST : HTTP_X_FORWARDED_FOR : "+ip);
        }
        if (ip == null) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

//
//    @Bean
//    CoinMarkerCapProvider coinMarkerCapProvider() {
//        return Feign.builder()
//                .encoder(new GsonEncoder())
//                .decoder(new GsonDecoder())
//                .target(CoinMarkerCapProvider.class, "http://api.coinmarketcap.com");
//    }
//
//
//    @Bean
//    EthereumExplorerProvider ethereumExplorerProvider() {
//        return Feign.builder()
//                .encoder(new GsonEncoder())
//                .decoder(new GsonDecoder()).
//                .target(EthereumExplorerProvider.class,  "http://api.etherscan.io");
//    }

//    @Slf4j
//    private static class CusErrorDecoder implements ErrorDecoder{
//
//        @Override
//        public Exception decode(String methodKey, Response response) {
//            //String encoding = response.headers().
//            try {
//                InputStream is = response.body().asInputStream();
//
//                String body = IOUtils.toString(is, "UTF-8");
//
//                Gson gson = new Gson();
//                FeignResult result = gson.fromJson(body, FeignResult.class);
//
//                return result;
//
//            }catch(IOException ie){
//
//            }
//            //log.info("test ------ : {} ", response.toString());
//            //log.info(response.body().asReader().);
//        return new OkbitException(String.valueOf(response.status()));
//
//        }
//
//    }

}
