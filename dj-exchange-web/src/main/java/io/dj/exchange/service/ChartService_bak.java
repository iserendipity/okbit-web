package io.dj.exchange.service;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimaps;
import io.dj.exchange.domain.dto.ChartDTO;
import io.dj.exchange.domain.dto.SumHistory;
import io.dj.exchange.repository.hibernate.readonly.MarketOrdersRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 23.
 * Description
 */
@Service
@Slf4j
public class ChartService_bak {

    static class Person {
        public String name;
        public Integer age;
        public String job;

        public Person(String name, Integer age, String job) {
            this.name = name;
            this.age = age;
            this.job = job;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    @Autowired
    @Qualifier("redisTemplate")
    private RedisTemplate template;

    @Autowired
    private MarketOrdersRepository marketOrdersRepository;

    public ChartDTO.History getHistory(String symbol, String resolution, long from, long to) {

        String df = "%Y-%m-%d %H:%i";

        switch (resolution) {

            case "60":
                //df = "%Y-%m-%d %H:00";

                break;
            case "30":
                //df = "%Y-%m-%d %H:%i";
                break;
            case "15":
                //df = "%Y-%m-%d %H:%i";
                break;
            case "10":
                //df = "%Y-%m-%d %H:%i";
                break;
            case "5":
                //df = "%Y-%m-%d %H:%i";
                break;
            case "1":
                //df = "%Y-%m-%d %H:%i";
                break;

            default:
                df = "%Y-%m-%d %H%i";
                break;
        }

        //분까지의 날짜를 가져온다


        ValueOperations<String, ChartDTO.History> vo = template.opsForValue();
        ChartDTO.History history = vo.get(resolution);

        if(history != null){

            return history;


        }

        //List<MarketHistoryOrders> result =  marketOrdersRepository.getSumMarketHistoryOrdersFromBetweenTo(symbol, LocalDateTime.ofInstant(Instant.ofEpochSecond(from), ZoneId.systemDefault()), LocalDateTime.ofInstant(Instant.ofEpochSecond(to), ZoneId.systemDefault()));

        //List<SumHistory> result1 = marketOrdersRepository.getSumFromBetweenTo(symbol, LocalDateTime.ofInstant(Instant.ofEpochSecond(from), ZoneId.systemDefault()), LocalDateTime.ofInstant(Instant.ofEpochSecond(to), ZoneId.systemDefault()), df);
        //List<SumHistory> result = marketOrdersRepository.getSumFromBetweenTo(symbol, LocalDateTime.ofInstant(Instant.ofEpochSecond(from), ZoneId.systemDefault()), LocalDateTime.ofInstant(Instant.ofEpochSecond(to), ZoneId.systemDefault()));
        //List<SumHistory> result = marketOrdersRepository.getSumFromBetweenToWithP(symbol, LocalDateTime.ofInstant(Instant.ofEpochSecond(from), ZoneOffset.UTC), LocalDateTime.ofInstant(Instant.ofEpochSecond(to), ZoneOffset.UTC));
        //List<SumHistory> result = marketOrdersRepository.getSumFromBetweenToWithP(symbol, LocalDateTime.ofInstant(Instant.ofEpochSecond(from), ZoneOffset.UTC), LocalDateTime.ofInstant(Instant.ofEpochSecond(to), ZoneOffset.UTC), df);
        List<SumHistory> result = marketOrdersRepository.getSumHistory(symbol, LocalDateTime.ofInstant(Instant.ofEpochSecond(from), ZoneOffset.UTC), LocalDateTime.ofInstant(Instant.ofEpochSecond(to), ZoneOffset.UTC), df);
        history = new ChartDTO.History();
        if (!result.isEmpty()) {
            ImmutableListMultimap group = Multimaps.index(result, sumHistory -> sumHistory.getT());

            List<BigDecimal> hList = new ArrayList<>();
            List<BigDecimal> lList = new ArrayList<>();
            List<BigDecimal> oList = new ArrayList<>();
            List<BigDecimal> cList = new ArrayList<>();
            List<Long> tList = new ArrayList<>();
            List<Double> vList = new ArrayList<>();

            Iterator<Long> keyIterator = group.asMap().keySet().iterator();
            while (keyIterator.hasNext()) {

                Long key = keyIterator.next();
                List<SumHistory> dataRows = group.get(key);

                lList.add(dataRows.parallelStream().sorted(Comparator.comparing(SumHistory::getP)).findFirst().get().getP());
                hList.add(dataRows.parallelStream().sorted(Comparator.comparing(SumHistory::getP).reversed()).findFirst().get().getP());

                oList.add(dataRows.parallelStream().sorted(Comparator.comparing(SumHistory::getId)).findFirst().get().getP());
                cList.add(dataRows.parallelStream().sorted(Comparator.comparing(SumHistory::getId).reversed()).findFirst().get().getP());

                vList.add(dataRows.parallelStream().mapToDouble(SumHistory::getVol).sum());
                tList.add(key);
            }

            history.setL(lList);
            history.setH(hList);
            history.setO(oList);
            history.setC(cList);
            history.setV(vList);
            history.setT(tList);
            history.setS("ok");
            log.info("----------{} : ", history);

        } else {
            history.setS("no_data");
        }

        vo.set(resolution, history);

//
//        Map<String, List<>>
//
//        for (sadsad) {
//            long ticker = 60; //1hour
//            result.stream().flatMap(r -> {
//                r.getT()
//                String tickerDt = r.getT();
//            }).collect();
//
//            result.tickerDt;
//
//
//            open close;
//            ;
//            ;
//
//////////////////////////////////////////////////////////////////////
//            sdsad ==>group(result)->tickerDt -> (0), last(close);
//            Map<> result = >
//
//
//            result.ticketDt =>open:
//            0, cloase :last;
//
//
//            group.price(0 :min, last  :max)
//
//
//
//            redis => "2017-5-24 10 + KRW, BITC LISt<ET, ";  open , close, hi, min;
//
//            redis => (tickerDt, v)
//
/////////////////////////////////////////////////////////////////
//
//
//        }
//
//var apiData = {};
//        ====================================================================
//
//
//
//        fun consume();
//        {
//            while(true) {
//              data = queue.get();
//              sendWebSocket(streamData);
//            }
//        }
//
//
//
//
//        websockJS;
//
//
//        1:2:
//
//        fun receive(streamData) {
//                {}
//                apiData, streamData
//                apiData.hi = streamData.hi;
//        apiData.min = streamData.min;
//        apiData.close = streamData.close;
//} else{
//
//        }
//            chart(sadasd);
//                ticket, coin ,
//        }
//
//
//
//
//        queue OKBIT;
//
//
//
//
//
//
//
//        redis ->
//
//
//
//
//
//
//
//
//
//
//        startDt, endDt, Coin, Ticker ===>
//
//
//        String Coin;
//
//        List result = ;
//        for () {
//
//            String tickerDt = r.getT();
//
//
//            val = redis.get(rickerDt + coin);
//
//            result.add(val);
//        }

        //ChartDTO.History history = new ChartDTO.History();

//        if(result != null && result.size()>0){
//            List<Long> tList = new ArrayList<>();
//            List<Integer> vList = new ArrayList<>();
//            List<BigDecimal> lList = new ArrayList<>();
//            List<BigDecimal> hList = new ArrayList<>();
//            List<BigDecimal> oList = new ArrayList<>();
//            List<BigDecimal> cList = new ArrayList<>();
//
//            for(int i =0; i<result.size(); i++){
//
//                tList.add(result.get(i).getT());
//                vList.add(result.get(i).getV().intValue());
//                lList.add(result.get(i).getL());
//                hList.add(result.get(i).getH());
//                oList.add(result.get(0).getP());
//                cList.add(result.get(result.size()-1).getP());
//            }
//
//            history.setL(lList);
//            history.setT(tList);
//            history.setV(vList);
//            history.setH(hList);
//            history.setC(cList);
//            history.setO(oList);
//            history.setS("ok");
//
//            log.info("----------history result : {}", history);
//
//        }else{
//
//            history.setS("no_data");
//
//        }

        return history;
        //return new ChartDTO.History(from);
    }
}
