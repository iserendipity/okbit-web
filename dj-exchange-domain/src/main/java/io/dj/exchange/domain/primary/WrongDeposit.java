package io.dj.exchange.domain.primary;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 2.
 * Description
 */
@Entity
@Data
public class WrongDeposit  implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @NotNull
    @Column(name = "user_nm")
    private String userNm;

    @NotNull
    @Column(name = "bank_nm")
    private String bankNm;

    @Column(name = "reg_dt")
    private LocalDateTime regDt;

    @Column(name = "mod_dt")
    private LocalDateTime modDt;

    @Column(name = "deposit_dt")
    private LocalDateTime depositDt;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "status")
    private String status;

    @Transient
    private String depositDtTxt;

}
