package io.dj.exchange.enums;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 7.
 * Description
 */
public enum ErrorCode {
    NO_USER,
    EMAIL_EXPIRED
}
