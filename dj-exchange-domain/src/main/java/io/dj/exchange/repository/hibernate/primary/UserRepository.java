package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.dto.TodayUsers;
import io.dj.exchange.domain.primary.User;
import io.dj.exchange.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by jeongwoo on 2017. 4. 4..
 */
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findOneByEmail(String email);

    Optional<User> findOneByEmailAndActive(String email, String active);

    Optional<User> findOneByEmailAndActiveAndRole(String loginId, String y, Role admin);

    @Query("select new io.dj.exchange.domain.dto.TodayUsers(function('count', p.id), function('date_format', p.regDt, '%Y-%m-%d')) from User p where p.regDt >= :dt group by function('date_format', p.regDt, '%Y-%m-%d') order by p.regDt asc")
    List<TodayUsers> getTodayUsers(@Param("dt") LocalDateTime dateTime);
}
