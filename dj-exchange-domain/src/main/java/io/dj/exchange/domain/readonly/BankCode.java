package io.dj.exchange.domain.readonly;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 7. 6.
 * Description
 */
@Data
@Entity
public class BankCode implements Serializable{

    @Id
    @Column(name = "code")
    private String code;

    @Column(name = "bank_name")
    private String bankName;

}
