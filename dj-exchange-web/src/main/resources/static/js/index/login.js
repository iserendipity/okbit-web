function otpCheck(){

    var params = new Object();

    params.loginId = $('#box-login input[name=loginId]').val();

    if($('#box-login input[name=totp-verification-code]').val()){
        return true;
    }

    $.ajax({
        url: "/api/common/otpCheck"
        , async : false
        , type: "POST"
        , dataType: 'json'
        , contentType:"application/json; charset=UTF-8"
        , data: JSON.stringify(params)
        , success: function (data) {
            if(data.code == 'SUCCESS' && data.data == 'Y'){
                
                $('#box-login input[name=totp-verification-code]').show();

            }

        }
        , error:function(e){

        }
    });

}


paramValidation = function(obj){

    var flag = true;

    $.each(obj, function(key, value){

        if(value == '' || value == undefined || value.length <= 0){
            // alert('코인 구매량이나 가격이 입력되지 않았습니다.');
            flag = false;
            return false;

        }

        if(obj.toCoin){//buy

            switch (obj.toCoin) {

                case 'BITCOIN': //최소 0.0001개 amount, price = 1000단위

                    if(key == 'amount'){

                        if(value < 0.0001){
                            flag = false;
                            desc = obj.toCoin+'의 최소 구매단위는 0.0001 입니다.';
                            return false;

                        }else if(value.substring(value.indexOf('.')+1).length > 4){
                            flag = false;
                            desc = obj.toCoin+'의 최소 구매단위는 0.0001 입니다.';
                            return false;
                        }

                    } else if(key == 'price' && value % 1000 != 0){
                        flag = false;
                        desc = obj.toCoin+'의 구매가격 단위는 1000KRW 입니다';
                        return false;
                    }

                    break;

                default :
                    if(key =='amount') {
                        if (value < 0.01) {

                            flag = false;
                            desc = obj.toCoin + '의 최소 구매단위는 0.01 입니다';
                            return false;

                        } else if (value.substring(value.indexOf('.') + 1).length > 2) {
                            flag = false;
                            desc = obj.toCoin + '의 최소 구매단위는 0.01 입니다.';
                            return false;
                        }
                    } else if(key == 'price' && value % 500 != 0){
                        flag = false;
                        desc = obj.toCoin+'의 구매가격 단위는 500KRW 입니다';
                        return false;
                    }

                    break;

            }

        } else if(obj.fromCoin){//sell

            switch (obj.fromCoin) {

                case 'BITCOIN': //최소 0.0001개 amount, price = 1000단위

                    if(key == 'amount'){

                        if(value < 0.0001){

                            flag = false;
                            desc = obj.fromCoin+'의 최소 판매단위는 0.0001 입니다';
                            return false;

                        }else if(value.substring(value.indexOf('.')+1).length > 4){
                            flag = false;
                            desc = obj.fromCoin+'의 최소 판매단위는 0.0001입니다.';
                            return false;
                        }

                    } else if(key == 'price' && value % 1000 != 0){
                        flag = false;
                        desc = obj.fromCoin+'의 판매가격 단위는 1000KRW 입니다';
                        return false;
                    }

                    break;

                default :

                    if(key == 'amount'){

                        if(value < 0.01){

                            flag = false;
                            desc = obj.fromCoin+'의 최소 판매량은 0.01 입니다';
                            return false;

                        }else if(value.substring(value.indexOf('.')+1).length > 2){
                            flag = false;
                            desc = obj.fromCoin+'의 최소 판매단위는 0.01 입니다.';
                            return false;
                        }

                    } else if(key == 'price' && value % 500 != 0){
                        flag = false;
                        desc = obj.fromCoin+'의 판매가격 단위는 500KRW 입니다';
                        return false;
                    }

                    break;

            }

        }

    });
    return flag;
};
