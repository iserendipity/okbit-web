function ProfileApi () {
    this.call = function(uri, callback, resultProcFunc, params) {
        $.ajax({
            url: "/api/profile" + uri
            , type: "POST"
            , dataType: 'json'
            , contentType:"application/json; charset=UTF-8"
            , data: JSON.stringify(params)
            , success: function (data) {
                resultProcFunc(data);
                return callback(data);
            }
            , error:function(e){
                //showAlert("ERROR : " + JSON.parse(e.responseText).message);
            }
        });
        return true;
    };

    this.addBank = function(callback, params) {
        $.ajax({
            url: "/api/profile/addBank",
            type: "POST",
            data: params,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                return callback(data);
            },
            error: function (data) {
                return callback(data);
            }
        });
        return true;
    };

    this.addIdCard = function(callback, params) {
        $.ajax({
            url: "/api/profile/addId",
            type: "POST",
            data: params,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                return callback(data);
            },
            error: function (data) {
                return callback(data);
            }
        });
        return true;
    };

    this.changeOtpSetting = function(callback, params) {
        return this.call("/changeOtpSetting", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.activateOtp = function(callback, params) {
        return this.call("/activateOtp", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.transactions = function(callback, params) {
        return this.call("/transactions", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };
}