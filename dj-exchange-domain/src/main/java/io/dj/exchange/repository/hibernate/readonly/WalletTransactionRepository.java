package io.dj.exchange.repository.hibernate.readonly;

import io.dj.exchange.domain.dto.SumHistory;
import io.dj.exchange.domain.dto.SumMarketHistoryOrders;
import io.dj.exchange.domain.dto.TodayUsers;
import io.dj.exchange.domain.dto.Volume;
import io.dj.exchange.domain.readonly.MarketHistoryOrders;
import io.dj.exchange.domain.readonly.MarketHistoryOrdersPK;
import io.dj.exchange.domain.readonly.WalletTransaction;
import io.dj.exchange.domain.readonly.WalletTransactionPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 12.
 * Description
 */
public interface WalletTransactionRepository extends JpaRepository<WalletTransaction, WalletTransactionPK> {


    @Query("select p from WalletTransaction p where p.coinName = :coinName and function('date_format', p.dt, '%Y-%m-%d') = :dt ")
    List<WalletTransaction> findAllByCoinNameAndGroupByDate(@Param("coinName") String coinName, @Param("dt") String dt);
}
