//package io.dj.exchange.service;
//
//import com.google.common.collect.ImmutableListMultimap;
//import com.google.common.collect.Multimaps;
//import io.dj.exchange.domain.chart.History;
//import io.dj.exchange.domain.dto.SumHistory;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.core.ValueOperations;
//import org.springframework.stereotype.Component;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.Comparator;
//import java.util.Iterator;
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * Project dj-exchange-okbit.
// * Created by jeongwoo on 2017. 6. 9.
// * Description
// */
//@Slf4j
//@Component
//public class RedisProvider {
//
//    @Autowired
//    private RedisTemplate template;
//
//    public void redisSetter(List<SumHistory> result, String resolution) {
//
//        ValueOperations<String, List<History>> vo = template.opsForValue();
//
//        ImmutableListMultimap group = Multimaps.index(result, sumHistory -> sumHistory.getCoinName());
//        Iterator<String> coinNameKeyIterator = group.asMap().keySet().iterator();
//
//        while (coinNameKeyIterator.hasNext()) {
//
//            String coinName = coinNameKeyIterator.next();
//            List<SumHistory> coinNameHistory = group.get(coinName);
//
//            ImmutableListMultimap minuteGroupList = Multimaps.index(coinNameHistory, sumHistory -> sumHistory.getT());
//
//            Iterator<Long> minuteKeyIterator = minuteGroupList.asMap().keySet().iterator();
//
//            while (minuteKeyIterator.hasNext()) {
//
//                long key = minuteKeyIterator.next();
//
//                List<SumHistory> minuteHistory = minuteGroupList.get(key);
//
//                //log.info(minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution+" : {}", minuteHistory);
//
//                BigDecimal c = minuteHistory.get(minuteHistory.size()-1).getP();
//                BigDecimal l = minuteHistory.parallelStream().sorted(Comparator.comparing(SumHistory::getP)).findFirst().get().getP();
//                BigDecimal h = minuteHistory.parallelStream().sorted(Comparator.comparing(SumHistory::getP).reversed()).findFirst().get().getP();
////                BigDecimal h = minuteHistory.parallelStream().max(Comparator.comparing(SumHistory::getP)).get().getP();
////                BigDecimal l = minuteHistory.parallelStream().min(Comparator.comparing(SumHistory::getP)).get().getP();
//                //BigDecimal l = minuteHistory.stream().sorted(Comparator.comparing(SumHistory::getP)).findFirst().get().getP();
//                BigDecimal o = minuteHistory.get(0).getP();
//                //long lastId = minuteHistory.get(minuteHistory.size()-1).getId();
//
//                long t = key;
//                double v = minuteHistory.parallelStream().mapToDouble(SumHistory::getVol).sum();
//
//                List<History> historyList = vo.get(minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution);
//
//                if(historyList != null ){
//
//                    Iterator<History> historyIterator = historyList.iterator();
//                    int idx = 0;
//                    History history = new History();
//                    while(historyIterator.hasNext()){
//
//                        History historySet = historyIterator.next();
//
//                        if(historySet.getT() == key){//새로운 데이터인데 날짜가 같다&& historySet.getId() != lastId
//
//                            if(historySet.getL().compareTo(l) != -1){
//                                history.setL(l);
//
//                            }else{
//                                history.setL(historySet.getL());
//                            }
//
//                            if(historySet.getH().compareTo(h) != 1){
//                                history.setH(h);
//
//                            }else{
//                                history.setH(historySet.getH());
//                            }
//
//                            history.setO(historySet.getO());
//                            history.setC(c);
//                            history.setV(v+historySet.getV());
//                            history.setT(key);
//
//                            historyList.set(idx, history);
//
//                        }else{
//
//                            history.setL(l);
//                            history.setT(t);
//                            history.setO(o);
//                            history.setC(c);
//                            history.setH(h);
//                            history.setV(v);
//
//                        }
//
//                        idx ++;
//
//                    }
//
//                    //히스토리리스트에서 날짜들을 가져온다.
//                    int idx2 = 0;
//                    for(History his : historyList){
//
//                        if(key <= his.getT()){
//                            break;
//                        }
//
//                        idx2++;
//                    }
//
//                    historyList.add(idx2, history);
//
//                }else{
//
//                    historyList = new ArrayList<>();
//
//                    History history = new History();
//                    history.setL(l);
//                    history.setT(t);
//                    history.setO(o);
//                    history.setC(c);
//                    history.setH(h);
//                    history.setV(v);
//
//                    historyList.add(history);
//
//                }
//
//                historyList.parallelStream().sorted(Comparator.comparing(History::getT)).collect(Collectors.toList());
//                log.info(minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution + " : {}", historyList);
//                vo.set(minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution, historyList);
//
//            }
//
//        }
//
//    }
//
//
//}
