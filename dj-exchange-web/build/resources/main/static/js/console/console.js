/**
 * Created by jeongwoo on 2017. 4. 21..
 */
//jsonData를 받아서 테이블을 구성한다.
//required jquery.tmpl
function loadOrderTable(tbodyId, templateId, jsonData){

    $('#'+tbodyId).html($('#'+templateId).tmpl(jsonData));

}
function trParser(tr){

    var $rows = [];

        $(tr).each(function(index) {
            var $cells = $(this).find("td");
            $rows[index] = {};
            $cells.each(function (cellIndex) {
                $rows[index]['col'+cellIndex] = $(this).html();
            });
        });
        //console.log($rows);
        return $rows;
    }

function getBuyTab(data){

    if(data.length > 0 ) {
        makeTemplate('buyOrders', 'buyTemplate', data);
        //scrollStop();
        $('#buyOrders > tr').each(function(i,_tr){
            //console.log(_td);
            $(_tr).on('click', function(e){

                var $tr = trParser(_tr);

                // $('#amount').val($tr[0]['col2']);

                if(nowCoin == 'BITCOIN') {
                    $('#amount').val(new Number($tr[0]['col2']).toFixed(4));
                }else{
                    $('#amount').val(new Number($tr[0]['col2']).toFixed(2));
                }

                $('#price').val($tr[0]['col4']);

                //setAmountPrice()
            });
        });

    }else{
        $('#buyOrders').html('<tr><td colspan="5">'+no_data_msg+'</td></tr>');
    }

}

function getSellTab(data){

    if(data && data.length > 0) {
        makeTemplate('sellOrders', 'sellTemplate', data);

        $('#sellOrders > tr').each(function(i,_tr){
            //console.log(_td);
            $(_tr).on('click', function(e){
                //console.log(e);
                var $tr = trParser(_tr);

                $('#price').val($tr[0]['col0']);
                if(nowCoin == 'BITCOIN') {
                    $('#amount').val(new Number($tr[0]['col2']).toFixed(4));
                }else{
                    $('#amount').val(new Number($tr[0]['col2']).toFixed(2));
                }

                //setAmountPrice()
            });
        });

    }else{
        $('#sellOrders').html('<tr><td colspan="5">'+no_data_msg+'</td></tr>');
    }
}

function loadOrdersFull(coinName){
    var param = new Object();

    param.fromCoin = 'KRW';
    param.orderType = 'BUY';

    if(coinName){
        param.toCoin = coinName;
    }else{
        param.toCoin = nowCoin;
    }

    var buyOrdersFull = consoleApi.loadBuyFullOrders(function(data) {
        if(data.data && data.data.length > 0) {
            makeTemplate('buyOrdersFull', 'buyTemplate', data.data);
        }else{
            $('#buyOrdersFull').html('<tr><td colspan="5">'+no_data_msg+'</td></tr>');

        }
    }, param);

    if(coinName){
        param.fromCoin = coinName;
    }else{
        param.fromCoin = nowCoin;
    }

    param.toCoin = 'KRW';
    param.orderType = 'SELL';
    var sellOrders = consoleApi.loadSellFullOrders(function(data) {

        if(data.data && data.data.length > 0) {
            makeTemplate('sellOrdersFull', 'sellTemplate', data.data);
        }else{
            $('#sellOrdersFull').html('<tr><td colspan="5">'+no_data_msg+'</td></tr>');

        }
    }, param);

}

function loadOrders(coinName){

    var param = new Object();

    param.fromCoin = 'KRW';

    if(coinName){
        param.toCoin = coinName;
    }else{
        param.toCoin = nowCoin;
    }

    param.orderType = 'BUY';
    param.page = '1';
    param.size = '20';

    var buyOrders = consoleApi.loadBuyOrders(function(data){

        getBuyTab(data.data);

        // if(data.data.length > 0 ) {
        //     loadOrderTable('buyOrders', 'buyTemplate', data.data);
        //     //scrollStop();
        //     $('#buyOrders > tr').each(function(i,_tr){
        //         //console.log(_td);
        //         $(_tr).on('click', function(e){
        //
        //             var $tr = trParser(_tr);
        //
        //             $('#amount').val($tr[0]['col2']);
        //             $('#price').val($tr[0]['col4']);
        //
        //             //setAmountPrice()
        //         });
        //     });
        //
        // }else{
        //     $('#buyOrders').html('<tr><td colspan="5">'+no_data_msg+'</td></tr>');
        // }
    }, param);

    if(coinName){
        param.fromCoin = coinName;
    }else{
        param.fromCoin = nowCoin;
    }

    param.toCoin = 'KRW';
    param.orderType = 'SELL';
    var sellOrders = consoleApi.loadSellOrders(function(data){
        getSellTab(data.data);
         // if(data.data && data.data.length > 0) {
         //     loadOrderTable('sellOrders', 'sellTemplate', data.data);
         //
         //     $('#sellOrders > tr').each(function(i,_tr){
         //         //console.log(_td);
         //         $(_tr).on('click', function(e){
         //             //console.log(e);
         //             var $tr = trParser(_tr);
         //
         //             $('#price').val($tr[0]['col0']);
         //             $('#amount').val($tr[0]['col2']);
         //
         //             //setAmountPrice()
         //         });
         //     });
         //
         // }else{
         //     $('#sellOrders').html('<tr><td colspan="5">'+no_data_msg+'</td></tr>');
         // }
    }, param);
}
function update(coinName){

    return setInterval(function(){

            if(nowCoin){
                coinName = nowCoin;
            }

            loadMarketTrades(coinName);
            loadMyTrades(coinName);
            loadOrders(coinName);
            makeMyOrders(coinName);
        }, 3000);

}

function getMarketTrades(data){
    if(data && data.length > 0) {

        makeTemplate('tradeBody', 'tradeTemplate', data);

    }else{

        $('#tradeBody').html('<tr class="text-center"><td colspan="4">'+no_data_msg+'</td></tr>');

    }
}

function loadMarketTrades(coinName){

    var params = new Object();
    if(coinName) {
        params.coinName = coinName;
    }else{
        params.coinName = nowCoin;
    }
    params.page = '1';
    params.size = '20';

    consoleApi.trades(function(data){

        getMarketTrades(data.data);
        // //console.log(data);
        // if(data.data && data.data.length > 0) {
        //
        //     makeTemplate('tradeBody', 'tradeTemplate', data.data);
        //
        // }else{
        //
        //     $('#tradeBody').html('<tr class="text-center"><td colspan="4">'+no_data_msg+'</td></tr>');
        //
        // }

    }, params);

}
function loadMarketFullTrades(coinName){

    var params = new Object();
    if(coinName) {
        params.coinName = coinName;
    }else{
        params.coinName = nowCoin;
    }

    consoleApi.fullTrades(function(data){

        if(data.data && data.data.length > 0) {

            makeTemplate('fullTradeBody', 'tradeTemplate', data.data);

        }else{

            $('#fullTradeBody').html('<tr class="text-center"><td colspan="4">'+no_data_msg+'</td></tr>');

        }

    }, params);


}
function getMyTrades(data){
    if(data && data.length > 0) {

        makeTemplate('myTradeBody', 'myTradeTemplate', data);
    }else{

        $('#myTradeBody').html('<tr class="text-center"><td colspan="4">'+no_data_msg+'</td></tr>');

    }
}

function loadMyFullTrades(coinName){

    var params = new Object();
    if(coinName) {
        params.coinName = coinName;
    }else{
        params.coinName = nowCoin;
    }

    consoleApi.myFullTrades(function(data){


        if(data.data && data.data.length > 0) {

            makeTemplate('myTradeBodyFull', 'myTradeTemplate', data.data);
        }else{

            $('#myTradeBodyFull').html('<tr class="text-center"><td colspan="4">'+no_data_msg+'</td></tr>');

        }

    }, params);


}

function loadMyTrades(coinName){

    var params = new Object();
    if(coinName) {
        params.coinName = coinName;
    }else{
        params.coinName = nowCoin;
    }
    params.page = '1';
    params.size = '20';

    consoleApi.myTrades(function(data){

        getMyTrades(data.data);
        // if(data.data && data.data.length > 0) {
        //
        //     makeTemplate('myTradeBody', 'myTradeTemplate', data.data);
        // }else{
        //
        //     $('#myTradeBody').html('<tr class="text-center"><td colspan="4">'+no_data_msg+'</td></tr>');
        //
        // }

    }, params);


}

function scrollStop(){
    clearInterval(loadOrdersTimer);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function chageChartSymbol(coinName, interval){
    widget.setSymbol(coinName, interval);
}

function init(){
    consoleApi = new ConsoleApi();

    if($('#coinName').val()){
        coinName = $('#coinName').val();
    }

    var cookieCoin = $.cookie('nowCoin');

    var initCoin = cookieCoin ? cookieCoin : 'ETHEREUM';

    loadMarketTrades(initCoin);
    loadMyTrades(initCoin);
    loadOrders(initCoin);
    makeMyOrders(initCoin);

    now = 'console';

    //loadOrdersTimer = update('BITCOIN');
    // setInterval(function(coinName){
    //     loadOrders(coinName);
    // }, 1000);

    //loadOrders('BITCOIN');

    TradingView.onready(function()
    {
        widget = window.tvWidget = new TradingView.widget({
            //fullscreen: true,
            container_id: "tv_chart_container",
            symbol: initCoin,
            interval: '15',
            toolbar_bg: '#222',
            //height: '490',
            autosize:'off',
            timezone : 'Asia/Seoul',
            //preset : 'mobile',
            //allow_symbol_change: false,
            //container_id: "tv_chart_container",
            //datafeed: new Datafeeds.UDFCompatibleDatafeed("https://demo_feed.tradingview.com"),
            //datafeed: new Datafeeds.UDFCompatibleDatafeed("http://localhost:8080/api/chart"),
            //datafeed: new Datafeeds.UDFCompatibleDatafeed("http://172.30.1.58:8080/api/chart", 1000),
            //datafeed: new Datafeeds.UDFCompatibleDatafeed("http://172.30.1.58:8080/api/chart", 5000, 1),
            //datafeed: new Datafeeds.UDFCompatibleDatafeed(),
            datafeed: new Datafeeds.UDFCompatibleDatafeed("https://ok-bit.com/api/chart", 5000, 1),
            //datafeed: new Datafeeds.UDFCompatibleDatafeed("https://ok-bit.com/api/chart"),
            library_path: "/charting_library/",
            custom_css_url : "/charting_library/static/css/chart-dark.css",
            locale: "en",
            seconds_resolution : true,
            disabled_features: [
                "header_symbol_search",
                //'use_localstorage_for_settings',
                'header_interval_dialog_button',
                'compare_symbol',
                //'edit_buttons_in_legend',
                'legend_context_menu'
            ],
            enabled_features: ["move_logo_to_main_pane"],
            overrides: {
                //"mainSeriesProperties.style": 0,
                "symbolWatermarkProperties.color" : "#222222",//백그라운드 워터마크 색상
                "volumePaneSize": "tiny",
                "paneProperties.background": "#222222",
                "paneProperties.vertGridProperties.color": "#454545",
                "paneProperties.horzGridProperties.color": "#454545",
                "symbolWatermarkProperties.transparency": 100,
                "scalesProperties.textColor" : "#AAA",
                "mainSeriesProperties.candleStyle.upColor": "#228B22",
                "mainSeriesProperties.candleStyle.downColor": "#C20000",
                "mainSeriesProperties.candleStyle.borderColor": "#228B22",
                "mainSeriesProperties.candleStyle.borderUpColor": "#228B22",
                "mainSeriesProperties.candleStyle.borderDownColor": "#C20000",
                "mainSeriesProperties.candleStyle.wickUpColor": 'rgba( 115, 115, 117, 1)',
                "mainSeriesProperties.candleStyle.wickDownColor": 'rgba( 115, 115, 117, 1)',
                "mainSeriesProperties.candleStyle.barColorsOnPrevClose": false,
                "mainSeriesProperties.showPriceLine": true,
                "mainSeriesProperties.priceLineWidth": 1,
                "mainSeriesProperties.lockScale": false,
                "mainSeriesProperties.minTick": "default",
                "mainSeriesProperties.extendedHours": false,
        //	Hollow Candles styles
                "mainSeriesProperties.hollowCandleStyle.upColor": "#228B22",
                "mainSeriesProperties.hollowCandleStyle.downColor": "#C20000",
                "mainSeriesProperties.hollowCandleStyle.borderColor": "#228B22",
                "mainSeriesProperties.hollowCandleStyle.borderUpColor": "#228B22",
                "mainSeriesProperties.hollowCandleStyle.borderDownColor": "#C20000",
                "mainSeriesProperties.hollowCandleStyle.wickColor": "#737375",
                //	Bars styles
                "mainSeriesProperties.barStyle.upColor": "#3232FF",
                "mainSeriesProperties.barStyle.downColor": "#CD0139"
                // "paneProperties.rightAxisProperties.autoScale" :false,
                // "paneProperties.rightAxisProperties.autoScaleDisabled" :true,
                // "timeScale.rightOffset": 5
                // "paneProperties.rightAxisProperties.percentage:false",
                // "paneProperties.rightAxisProperties.percentageDisabled:false",
                // "paneProperties.rightAxisProperties.log:false",
                // "paneProperties.rightAxisProperties.logDisabled:false",
                // "paneProperties.rightAxisProperties.alignLabels:true"
            },
            //debug: true,
            time_frames: [
                { text: "3w", resolution: "D", description: "3 Weeks" },
                { text: "1w", resolution: "D", description: "1 Week" },
                { text: "1d", resolution: "15", description: "1 day", title: "1d" }
            ],
            //charts_storage_url: 'http://saveload.tradingview.com',
            // client_id: 'okbit.com',
            // user_id: 'public_user',
            favorites: {
                intervals: ["1", "5", "10", "15", "30", "60", "1D"],
                chartTypes: ["Hollow Candles"]
            }
        });
        //widget.subscribeBars(socketSend);

    });

}
