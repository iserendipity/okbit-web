package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.Coin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by jeongwoo on 2017. 4. 4..
 */
public interface CoinRepository extends JpaRepository<Coin, Long> {
    List<Coin> findAllByOrderByPriorityAsc();

    List<Coin> findNameByOrderByPriorityAsc();

    Coin findOneByName(String name);

    List<Coin> findAllByNameNotOrderByPriorityAsc(@Param("name") String name);

    List<Coin> findAllByName(@Param("name") String name);
}
