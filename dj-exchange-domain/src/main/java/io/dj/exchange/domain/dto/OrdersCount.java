package io.dj.exchange.domain.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 15.
 * Description
 */
@Data
@Slf4j
public class OrdersCount {

    private String coinName;
    private long count;
    private BigDecimal amount;
    private BigDecimal price;
    private BigDecimal totalPrice;
    private BigDecimal totalCoin;

    private String amountTxt;
    private String priceTxt;
    private String totalPriceTxt;
    private String totalCoinTxt;

    public OrdersCount(long count, BigDecimal amount, BigDecimal price, BigDecimal totalPrice){
        this.count = count;
        this.amount = amount;
        this.price = price;
        this.totalPrice = totalPrice;
    }

    public OrdersCount(long count, BigDecimal amount, BigDecimal price, BigDecimal totalPrice, String coinName){
        this.count = count;
        this.amount = amount;
        this.price = price;
        this.totalPrice = totalPrice;
        this.coinName = coinName;
    }

    public void initAmount(){

        DecimalFormat df = new DecimalFormat("#,##0.0000");
        if(this.coinName != null && this.coinName.equals("BITCOIN")) {
            df.applyPattern("#,##0.000000");
        }

        this.amountTxt = df.format(amount);
    }

    public void initPrice(){
        DecimalFormat df = new DecimalFormat("#,##0");
        this.priceTxt = df.format(price);
    }

    public void initTotalPrice(){
        DecimalFormat df = new DecimalFormat("#,##0");
        this.totalPriceTxt = df.format(totalPrice);
    }

    public void initTotalCoin(){

        DecimalFormat df = new DecimalFormat("#,##0.0000");

        if(this.coinName != null && this.coinName.equals("BITCOIN")) {
            df.applyPattern("#,##0.000000");
        }
        this.totalCoinTxt = df.format(totalCoin);
    }

}
