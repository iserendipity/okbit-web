package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface WalletRepository extends JpaRepository<Wallet, Long> {

    Optional<Wallet> findOneByCoinNameAndDepositDvcd(String coinName, String depositDvcd);

    Wallet findOneByDepositDvcd(String userNm);

    Optional<Wallet> findOneByUserIdAndCoinName(Long id, String coinName);

    List<Wallet> findAllByCoinName(String coinName);

    List<Wallet> findAllByCoinNameAndUserIdNot(String coinName, long l);
}
