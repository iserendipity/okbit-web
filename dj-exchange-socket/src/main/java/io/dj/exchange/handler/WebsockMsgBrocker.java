package io.dj.exchange.handler;

import io.dj.exchange.domain.chart.History;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by kun7788 on 2017. 1. 7..
 */
@Slf4j
@Service
public class WebsockMsgBrocker {

    @Data
    @Builder
    public static class SendToDTO {
        private String cmd;
        private Long id;
        private Object data;
        private String coin;
    }

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    public void sendAll(String cmd, Object data) {

        try {
            simpMessagingTemplate.convertAndSend("/topic/okbit", SendToDTO.builder().cmd(cmd).id(-1l).data(data).build());
        } catch (Exception ex) {

        }
    }

    public void sendTo(String cmd, Long id, Object data) {

        try {
            simpMessagingTemplate.convertAndSend("/topic/okbit", SendToDTO.builder().cmd(cmd).id(id).data(data).build());
        } catch (Exception ex) {

        }
    }

    public void sendToChart(String cmd, Long id, Object data) {

        try {
            simpMessagingTemplate.convertAndSend("/topic/okbitChart", SendToDTO.builder().cmd(cmd).id(id).data(data).build());
        } catch (Exception ex) {

        }
    }

    public void sendToChart(String cmd, long id, Object data, String coinName) {
        try {
            simpMessagingTemplate.convertAndSend("/topic/okbitChart", SendToDTO.builder().cmd(cmd).id(id).data(data).coin(coinName).build());
        } catch (Exception ex) {

        }

    }

}
