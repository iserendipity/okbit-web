package io.dj.exchange.util;

import io.dj.exchange.domain.primary.EmailConfirm;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.util.DigestUtils;

import java.math.BigInteger;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 15.
 * Description
 */
public class MailUtil {

    public static EmailConfirm getMailConfirm(String email){

        EmailConfirm confirm = new EmailConfirm();
        confirm.setEmail(email);
        confirm.setConfirmCode(RandomStringUtils.randomAlphanumeric(6));
        //confirm.setHashEmail(new BCryptPasswordEncoder().encode(loginId + confirm.getConfirmCode()));
        confirm.setHashEmail(new BigInteger(1, DigestUtils.md5Digest(email.getBytes())).toString(16));
        //confirm.setRegDt(LocalDateTime.now());

        return confirm;
    }


}
