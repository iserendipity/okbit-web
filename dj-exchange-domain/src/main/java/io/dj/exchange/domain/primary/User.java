package io.dj.exchange.domain.primary;

import io.dj.exchange.enums.Role;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by tommy on 2016. 2. 29..
 */
@Entity
@Table(name = "user")
@Data
public class User implements Serializable{

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "email", nullable = false, unique = true, length = 60)
    private String email;

    @Column(name = "pwd", nullable = false, length = 60)
    private String pwd;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name = "reg_dt", nullable = false, length = 60)
    private LocalDateTime regDt;

    @Column(name = "active", nullable = false)
    private String active;

    @OneToOne(optional=false, cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name = "user_setting",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "id")
    )
    private UserSetting userSetting;

    @OneToOne(optional=true, cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name = "admin_level",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "id")
    )
    private AdminLevel adminLevel;

    @OneToOne(optional=true, cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name = "otp_setting",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "id")
    )
    private OtpSetting otpSetting;

}
