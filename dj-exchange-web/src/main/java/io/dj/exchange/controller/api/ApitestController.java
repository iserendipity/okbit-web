package io.dj.exchange.controller.api;

import io.dj.exchange.domain.primary.Response;
import io.dj.exchange.domain.primary.Token;
import io.dj.exchange.domain.primary.User;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.provider.BankServiceProvider;
import io.dj.exchange.repository.hibernate.primary.TokenRepository;
import io.dj.exchange.repository.hibernate.primary.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 18.
 * Description
 */
@Slf4j
@RestController
public class ApitestController {

    String client_id = "l7xxe88b3120d5b546eb9d0fbb85fd8325db";
    String secret = "cbcc0a69cf124b398fe4dab6f6f40016";
    String login_token = "";
    String inquiry_token = "";
    String scope = "login";
    String apiRoot = "https://testapi.open-platform.or.kr";
    String authUrl = "/oauth/2.0/authorize";
    String regAccountUrl = "/oauth/2.0/authorize_account";
    String regAccountUrl2 = "/oauth/2.0/register_account";
    String uriAccessToken = "/oauth/2.0/token";
    String redirectUri = "http://localhost:8080/test";

    @Autowired
    private BankServiceProvider bankServiceProvider;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value="/liveCheck")
    public String liveCheck(){

        return "200";

    }

    @RequestMapping(value="/test", method= RequestMethod.GET)
    public ModelAndView test2(@ModelAttribute("param") Token token, @ModelAttribute("user") User user) {

        //Token resultToken =bankServiceProvider.accessTokenForScheduler(client_id, secret, redirectUri, token.getCode(), "authorization_code", token.getScope());
        //tokenRepository.save(resultToken);

        //bankServiceProvider.transactionList("Bearer "+resultToken.getAccess_token())


        ModelAndView mv = new ModelAndView("api/callback");

//        mv.addObject(param);

        return mv;
    }

    @RequestMapping(value="test/bank")
    public ModelAndView banktest(){

        ModelAndView mv = new ModelAndView("api/bank");

        List<Token> tokens = tokenRepository.findAll();

        for(Token t : tokens){
            log.info("------{}",t);
            switch(t.getScope()){

                case "login" : mv.addObject("login_token", t);
                    break;

                case "inquiry" : mv.addObject("inquiry_token", t);
                    break;

                case "transfer" : mv.addObject("transfer_token", t);
                    break;

                case "oob" : mv.addObject("oob_token", t);
                    break;

            }

        }

        return mv;
    }


    //로그인
    @RequestMapping(value="test/bankLogin")
    public RedirectView bankLogin(){

        RedirectView mv = new RedirectView(apiRoot+authUrl+"?client_id="+client_id+"&client_secret="+secret+"&scope=login&redirect_uri="+redirectUri+"&response_type=code");

        return mv;
    }


    //계좌등록조회 리다이렉트
    @RequestMapping(value="/test/bankRegisteredAccount", method=RequestMethod.GET)
    public RedirectView bankRegisteredAccount(){
        RedirectView mv = new RedirectView(apiRoot+regAccountUrl+"?client_id="+client_id+"&client_secret="+secret+"&scope=transfer&redirect_uri="+redirectUri+"&response_type=code");

        return mv;
    }

    //조회계좌 등록 리다이렉트
    @RequestMapping(value="/test/bankInquiryAccount", method=RequestMethod.GET)
    public RedirectView bankInquiryAccount(){
        RedirectView mv = new RedirectView(apiRoot+regAccountUrl+"?client_id="+client_id+"&client_secret="+secret+"&scope=inquiry&redirect_uri="+redirectUri+"&response_type=code");

        return mv;
    }

    //조회계좌등록 리가이렉트
    @RequestMapping(value="/test/bankRegisterInquiryAccount", method=RequestMethod.GET)
    public RedirectView bankRegisterInquiryAccount(){
        RedirectView mv = new RedirectView(apiRoot+regAccountUrl2+"?client_id="+client_id+"&client_secret="+secret+"&scope=inquiry&redirect_uri="+redirectUri+"&response_type=code");

        return mv;
    }

    //기관토큰 조회
    @RequestMapping(value="/test/accessTokenClientCredential", method= RequestMethod.POST)
    public Response<Token> accessTokenClientCredential(@ModelAttribute("user") User user, HttpServletRequest req) {

        Token token = bankServiceProvider.accessToken(client_id, secret, redirectUri, "client_credentials", "oob");

        tokenRepository.save(token);

        return Response.<Token>builder()
                .data(token)
                .code(CodeEnum.SUCCESS)
                .build();
    }
    @RequestMapping(value="/test/accessToken", method= RequestMethod.POST)
    public Response<Token> accessToken(@RequestParam Map<String, String> params, @ModelAttribute("user") User user, HttpServletRequest req) {
            //log.info("-----------{}", params);
            //log.info("---{}", params.get("code"));

            Token token = bankServiceProvider.accessToken(client_id, secret, redirectUri, params.get("code"), "authorization_code", params.get("scope"));

            tokenRepository.save(token);

        return Response.<Token>builder()
                .data(token)
                .code(CodeEnum.SUCCESS)
                .build();
    }

//    @RequestMapping(value="/test/transactionList", method= RequestMethod.GET)
//    public Response<Map<String, Object>> transactionList(@RequestParam Map<String, String> params, @ModelAttribute("user") User user, HttpServletRequest req) {
//        log.info("-----------{}", params);
//        log.info("---{}", params.get("code"));
//
//        //Map<String, Object> result =
//        return Response.<Map<String, Object>>builder()
//                .data(bankServiceProvider.transactionList(params.get("fintech_use_no"), "20170420091505", "A", "20160511","20160513","D","1"))
//                //.data(bankServiceProvider.transactionList(params.get("access_token"), params.get("fintech_use_no"), "20170420091505", "A", "20160511","20160513","D","1"))
//                //.data(bankServiceProvider.transactionList(client_id, secret, redirectUri, params.get("code"), "authorization_code", params.get("scope")))
//                .code(CodeEnum.SUCCESS)
//                .build();
//    }

//    @RequestMapping(value="/test", method= RequestMethod.GET)
//    public Response<Map<String, Object>> test2(@RequestParam Map<String, Object> param, @ModelAttribute("user") User user) {
//        log.info("-----------{}", param.toString());
//        log.info("-----------{}", user);
//
//
//        //유저 정보에 등록(auth_code, scope)
//        //userRepository.save(user);
//        /*
//
//
//
//         */
//
//        return Response.<Map<String, Object>>builder()
//                .data(param)
//                .code(CodeEnum.SUCCESS)
//                .build();
//    }


}
