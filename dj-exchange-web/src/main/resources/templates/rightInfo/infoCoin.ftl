<script id="coinInfoTemplate" type="text/x-jquery-tmpl">
    <h2 class="tile-title">${r"${unit}"}/KRW</h2>
    <div class="tile-config dropdown">
        <a data-toggle="dropdown" href="#" class="tile-menu"></a>
        <ul class="dropdown-menu pull-right text-right">
            <li><a href="#" onclick="javascript:makeCoinStatus('${r"${name}"}');">Refresh</a></li>
        </ul>
    </div>
    <div class="overflow">
        <div id="coinInfoBody" class="panel-body text-center">
            <table class="table">
                <tr>
                    <#--<td rowspan="3" class="visible-lg">-->
                        <#--<img src="/img/shortcuts/${r"${name}"}.png" alt="${r"${unit}"}">-->
                    <#--</td>-->
                    <td>
                        <span class="h4" style="font-weight:bolder; color:red">${r"${unit}"}/KRW</span>
                    </td>
                    <td>
                        <span class="h4">${r"${lastPrice}"}</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        VOL ${r"${vol}"}
                    </td>
                    <td class="pc-${r"${upDown}"}">
                        ${r"${priceDiff}"}(${r"${percent}"}%)<i class="fa fa-level-${r"${upDown}"}"></i>
                    </td>
                </tr>
                <tr>
                    <td>
                        LOW ${r"${low}"}
                    </td>
                    <td>
                        HIGH ${r"${high}"}
                    </td>
                </tr>

            </table>
            <#--<div class="row">-->
                <#--<div class="col-sm-2 col-lg-2 col-md-4"><img src="/img/shortcuts/money.png"></div>-->
                <#--<div class="col-sm-5 col-sm-offset-5 col-lg-5 col-lg-offset-5 col-md-8 col-md-offset-0"><h3 class="no-margin">1,030.9</h3></div>-->
            <#--</div>-->
            <#--<div class="row">-->
                <#--<div class="col-sm-5 col-sm-offset-2 col-lg-5 col-lg-offset-2 col-md-8 col-md-offset-4">VOL <span class="volume h5">40,495</span></div>-->
                <#--<div class="col-sm-5 col-lg-5 col-md-8 col-md-offset-4 col-lg-offset-0"><span class="point h5">62.10(<i class="fa fa-arrow-up"></i>5.68%)</span></div>-->
            <#--</div>-->
            <#--<div class="row">-->
                <#--<div class="col-sm-5 col-sm-offset-2 col-lg-5">LOW <span class="low h5">985.55</span></div>-->
                <#--<div class="col-sm-5 col-lg-5">HIGH <span class="high h5">1095.21</span></div>-->
            <#--</div>-->
        </div>
    </div>
    <#--<tr>-->
        <#--<td>${r"${orderType}"}</td>-->
        <#--<td>${r"${regDt}"}</td>-->
        <#--<td>${r"${price}"}</td>-->
        <#--<td>${r"${amount}"}</td>-->
        <#--<td><a href="#" onclick="javascript:cancel('${r"${id}"}');"><i class="fa fa-times"></i></a></td>-->
    <#--</tr>-->
</script>
<div id="coinInfo" class="tile">
    <h2 class="tile-title">BTC/KRW</h2>
    <div class="tile-config dropdown">
        <a data-toggle="dropdown" href="" class="tile-menu"></a>
        <ul class="dropdown-menu pull-right text-right">
            <li><a href="#" onclick="javascript:makeCoinStatus('BITCOIN');">Refresh</a></li>
        </ul>
    </div>
    <div class="">
        <div id="coinInfoBody" class="panel-body text-center">
            <table class="table">
                <tr>
                    <td colspan="3">Loading...</td>
                </tr>

            </table>
            <#--<div class="row">-->
                <#--<div class="col-sm-2 col-lg-2 col-md-4"><img src="/img/shortcuts/money.png"></div>-->
                <#--<div class="col-sm-5 col-sm-offset-5 col-lg-5 col-lg-offset-5 col-md-8 col-md-offset-0"><h3 class="no-margin">1,030.9</h3></div>-->
            <#--</div>-->
            <#--<div class="row">-->
                <#--<div class="col-sm-5 col-sm-offset-2 col-lg-5 col-lg-offset-2 col-md-8 col-md-offset-4">VOL <span class="volume h5">40,495</span></div>-->
                <#--<div class="col-sm-5 col-lg-5 col-md-8 col-md-offset-4 col-lg-offset-0"><span class="point h5">62.10(<i class="fa fa-arrow-up"></i>5.68%)</span></div>-->
            <#--</div>-->
            <#--<div class="row">-->
                <#--<div class="col-sm-5 col-sm-offset-2 col-lg-5">LOW <span class="low h5">985.55</span></div>-->
                <#--<div class="col-sm-5 col-lg-5">HIGH <span class="high h5">1095.21</span></div>-->
            <#--</div>-->
        </div>
    </div>
</div>