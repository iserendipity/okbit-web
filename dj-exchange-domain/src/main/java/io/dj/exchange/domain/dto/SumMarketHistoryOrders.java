package io.dj.exchange.domain.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 12.
 * Description
 */
@Data
public class SumMarketHistoryOrders {

    private BigDecimal sumAmount;

    public SumMarketHistoryOrders(BigDecimal sumAmount) {
        this.sumAmount = sumAmount;
    }
}
