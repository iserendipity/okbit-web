<li class="active">
    <a class="sa-side-home" href="/console">
        <span class="menu-item"><@spring.message "SIDE_HOME"/></span>
    </a>
</li>
<li>
    <a class="sa-side-deposit" href="/deposit">
        <span class="menu-item"><@spring.message "SIDE_DEPOSIT"/></span>
    </a>
</li>
<li>
    <a class="sa-side-withdraw" href="/withdraw">
        <span class="menu-item"><@spring.message "SIDE_WITHDRAW"/></span>
    </a>
</li>
<li>
    <a class="sa-side-security" href="/profile">
        <span class="menu-item"><@spring.message "SIDE_PROFILE"/></span>
    </a>
</li>
<li>
    <a class="sa-side-support" href="/support">
        <span class="menu-item"><@spring.message "SIDE_SUPPORT"/></span>
    </a>
</li>
<#--<li>-->
    <#--<a class="sa-side-bank" href="/test/bank">-->
        <#--<span class="menu-item">Bank</span>-->
    <#--</a>-->
<#--</li>-->
<li>
    <a class="sa-side-signout" href="/signOut">
        <span class="menu-item"><@spring.message "SIDE_SIGNOUT"/></span>
    </a>
</li>
<#--<li>-->
    <#--<a class="sa-side-chart" href="charts.html">-->
        <#--<span class="menu-item">Charts</span>-->
    <#--</a>-->
<#--</li>-->
<#--<li>-->
    <#--<a class="sa-side-folder" href="file-manager.html">-->
        <#--<span class="menu-item">File Manager</span>-->
    <#--</a>-->
<#--</li>-->
<#--<li>-->
    <#--<a class="sa-side-calendar" href="calendar.html">-->
        <#--<span class="menu-item">Calendar</span>-->
    <#--</a>-->
<#--</li>-->
<li class="dropdown">
    <a class="sa-side-page" href="#">
        <span class="menu-item"><@spring.message "SIDE_THEME"/></span>
    </a>
    <ul class="list-unstyled menu-item">
        <li><a href="#" onclick="changeTheme('blue');" onmouseover="previewTheme('blue');" onmouseout="donePreview();"><img src="/img/body/blue.jpg"></a></li>
        <li><a href="#" onclick="changeTheme('chrome');" onmouseover="previewTheme('chrome');" onmouseout="donePreview();"><img src="/img/body/chrome.jpg"></a></li>
        <li><a href="#" onclick="changeTheme('greenish');" onmouseover="previewTheme('greenish');" onmouseout="donePreview();"><img src="/img/body/greenish.jpg"></a></li>
        <li><a href="#" onclick="changeTheme('violate');" onmouseover="previewTheme('violate');" onmouseout="donePreview();"><img src="/img/body/violate.jpg"></a></li>
    </ul>
</li>