<div id="idForm" class="modal fade"> <!-- Style for just preview -->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h4 class="modal-title"><@spring.message "VERIFY_ID_CARD"/></h4>
            </div>
            <div class="modal-body text-center">
                <h5><@spring.message "VERIFY_ID_CAUTION"/></h5>
                <br>
                <form class="form-horizontal" role="form">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-preview thumbnail form-control"></div>

                        <div>
                            <span class="btn btn-file btn-alt btn-sm">
                                <span class="fileupload-new"><@spring.message "SELECT_IMAGE"/></span>
                                <span class="fileupload-exists"><@spring.message "BTN_CHANGE"/></span>
                                <input type="file" name="idcard" accept="image/*"/>
                            </span>
                            <a href="#" class="btn fileupload-exists btn-sm" data-dismiss="fileupload"><@spring.message "BTN_REMOVE"/></a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm" onclick="javascript:addIdcard();"><@spring.message "VERIFY_BTN_REGIST"/></button>
                <button type="button" class="btn btn-sm" data-dismiss="modal"><@spring.message "BTN_CLOSE"/></button>
            </div>
        </div>
    </div>
</div>