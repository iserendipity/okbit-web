//package io.dj.exchange.config;
//
//import com.fasterxml.jackson.databind.SerializationFeature;
//import org.codehaus.jackson.map.ObjectMapper;
//import org.codehaus.jackson.map.SerializationConfig;
//import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
//import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
//import org.springframework.amqp.rabbit.connection.ConnectionFactory;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
//import org.springframework.amqp.support.converter.JsonMessageConverter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.messaging.converter.MappingJackson2MessageConverter;
//import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
//
///**
// * Project dj-exchange-okbit.
// * Created by jeongwoo on 2017. 4. 26.
// * Description
// */
//@Configuration
////@EnableRabbit
//public class RabbitMQConfig implements RabbitListenerConfigurer {
//
//    public static final String QUEUE_NAME_MAIL = "mailQueue";
//
//    @Autowired
//    public ConnectionFactory connectionFactory;
//
//    @Override
//    public void configureRabbitListeners(RabbitListenerEndpointRegistrar registrar) {
//        registrar.setMessageHandlerMethodFactory(myHandlerMethodFactory());
//    }
//
//    @Bean
//    public DefaultMessageHandlerMethodFactory myHandlerMethodFactory() {
//        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
//        MappingJackson2MessageConverter mappingJackson2MessageConverter = new MappingJackson2MessageConverter();
//        mappingJackson2MessageConverter.getObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
//        factory.setMessageConverter(mappingJackson2MessageConverter);
//        return factory;
//    }
//
//    @Bean
//    public JsonMessageConverter jsonMessageConverter() {
//        JsonMessageConverter jsonMessageConverter = new JsonMessageConverter();
//        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
//        jsonMessageConverter.setJsonObjectMapper(objectMapper);
//        return jsonMessageConverter;
//    }
//
//    @Bean
//    public RabbitTemplate rabbitTemplate() {
//        RabbitTemplate template = new RabbitTemplate(connectionFactory);
//        template.setMessageConverter(jsonMessageConverter());
//        return template;
//    }
//
//    @Bean
//    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory() {
//        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
//        factory.setMessageConverter(jsonMessageConverter());
//        factory.setConnectionFactory(connectionFactory);
//        factory.setConcurrentConsumers(3);
//        factory.setMaxConcurrentConsumers(10);
//        return factory;
//    }
//
//}
