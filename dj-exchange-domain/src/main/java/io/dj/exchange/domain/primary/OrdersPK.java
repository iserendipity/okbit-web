package io.dj.exchange.domain.primary;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by kun7788 on 2016. 8. 7..
 */
@Data
public class OrdersPK implements Serializable {
    @Id
    Long id;

    @Id
    Long userId;
}
