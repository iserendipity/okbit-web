package io.dj.exchange.domain.dto.mq;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 7.
 * Description
 */

import io.dj.exchange.enums.CommandEnum;
import io.dj.exchange.enums.ScopeEnum;
import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class MessagePacket {
    @Enumerated(EnumType.STRING)
    private CommandEnum cmd;
    @Enumerated(EnumType.STRING)
    private ScopeEnum scope;
    private long userId;
    private Object data;
}