/**
 * Created by jeongwoo on 2017. 4. 27..
 */
var commonApi = new CommonApi();
var depositApi = new DepositApi();

function coinList(){

    commonApi.coins(function(data){
        if(data.data) {
            makeTemplate('coinList', 'coinTemplate', data.data);
            makeTemplate('depositWallets', 'newTemplate', data.data);
        }
    });

}

function coinWallet(coinName){

    var param = new Object();
    param.coinName = coinName;

    depositApi.wallet(function(data){

        if(data.data) {

            switch(coinName) {
                case 'MONERO': makeTemplate(coinName + 'Account', 'walletTemplateTag', data.data);
                    break;
                case 'KRW' : makeTemplate(coinName + 'Account', 'walletTemplateKRW', data.data);
                    break;
                case 'BITCOIN' : makeTemplate(coinName + 'Account', 'postponeTemplate', data.data);
                    break;
                default : makeTemplate(coinName + 'Account', 'walletTemplate', data.data);
                    break;
            }

        }

        // $('.overflow').getNiceScroll().each(function(){
        //     $(this).resize();
        // });
        /* --------------------------------------------------------
         Tooltips
         -----------------------------------------------------------*/

        if($('.tooltips')[0]) {
            $('.tooltips').tooltip();
        }

    }, param);

    if(coinName == 'KRW'){

        $('#krwModal').modal({
            'backdrop': 'static'
        });

    }

    transactions('receive', 1,coinName);

}


function coinWallets(){

    depositApi.wallets(function(data){

        if(data.data) {
            makeTemplate('depositWallets','walletTemplate',data.data);

        }

    });

}

function getAddress(coinName){

    var params = new Object();
    params.coinName = coinName;

    $('body').loading({
        message : 'LOADING...',
        theme : 'dark'
    });

    depositApi.regist(function(data){

        if(data.data){
            coinWallet(coinName);
            makeBalance();
        }

        $('body').loading('stop');

    }, params);

}
var timer;
function getDeposit(pageNo, coinName){

    transactions('receive', pageNo, coinName);

    timer = setInterval(function(){
        transactions('receive', pageNo, coinName);
        //makeBalance();
    }, 5000);


}

function transactions(category, pageNo, coinName){
    var params = new Object();
    params.page = pageNo-1;
    params.size = 20;
    params.category = category;
    params.coinName = nowCoin ? nowCoin : coinName;

    depositApi.transactions(function(data){

        //console.log(data);

        if(data.page && data.page.totalPages > 0 ) {

            setPageValues('depositPage', data.page, 'getDeposit', pageNo);
        }

        if(data._embedded && data._embedded.transactionses && data._embedded.transactionses.length > 0){

            makeTemplate('recentDepositTableBody','recentDepositTemplate', data._embedded.transactionses);

        }else{

            $('#recentDepositTableBody').html('<tr class="text-center"><td colspan="6">'+no_deposit_msg+'</td></tr>')

        }




    }, params);

}

// function searchTx(txId, coinName){
//
//     switch (coinName){
//
//         case 'BITCOIN':
//
//             //
//
//             break;
//
//
//
//
//
//     }
//
//
//
// }

function init(){

    coinList();
    getDeposit('1', 'BITCOIN');
    //coinWallets();
    //transactions('receive', 'BITCOIN');

    now = 'deposit';

    //setInterval("getDeposit('1', 'BITCOIN')", 3000);

}


