package io.dj.exchange.domain.chart;

import lombok.Data;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 22.
 * Description
 */
@Data
public class SymbolsType {
    private String name;
    private String value;
}
