package io.dj.exchange.domain.primary;

import io.dj.exchange.enums.CodeEnum;
import lombok.Builder;
import lombok.Data;

/**
 * Created by kun7788 on 16. 7. 10..
 */
@Data
@Builder
public class Response<T> {
    private CodeEnum code;
    private String desc = "succeed";
    private T data;
}
