<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
    </head>
    <body id="skin-blur-chrome" style="overflow-x:hidden">
        <#--<div class="pull-right m-t-15 m-r-15" style="width:120px">-->
            <#--<select class="select" style="display: none;">-->
                <#--<option>Divider</option>-->
                <#--<option>Toyota Avalon</option>-->
                <#--<option>Toyota Crown</option>-->
                <#--<option>Toyota FT86 </option>-->
                <#--<option data-divider="true">&nbsp;</option>-->
                <#--<option>Lexus LS600</option>-->
                <#--<option>Lexus LFA</option>-->
                <#--<option>Lexus LX570</option>-->
            <#--</select>-->
        <#--</div>-->
        <#--<div class="container-fluid visible-lg">-->
            <#--<div class="row">-->
                <#--<div class="area-ad area-ad-left">-->
                    <#--광고영역-->

                <#--</div>-->
                <#--<div class="area-ad area-ad-right">-->
                    <#--광고영역-->

                <#--</div>-->

            <#--</div>-->
        <#--</div>-->
        <section id="login">
            <header class="text-center">
                <div class="row text-center">
                    <img src="https://static.wixstatic.com/media/a36f24_ed150e6566b24afe87f02db2984e2e24~mv2.png/v1/fill/w_714,h_501,al_c,usm_0.66_1.00_0.01/a36f24_ed150e6566b24afe87f02db2984e2e24~mv2.png" style="width: 350px;">
                </div>
                <br>
                <br>

                <h3>고객의 희망과 밝은 미래를 함께 하는 최고의 거래소를 시작합니다.</h3>
            </header>
        
            <div class="clearfix"></div>
            <#--<div class="container-fluid">-->
                <#--<!-- Login &ndash;&gt;-->
                <#--<form class="box tile animated form-validation-2 active" id="box-login" method="post" action="loginProcess">-->
                    <#--<#if error?has_content>-->
                        <#--<div class="login-control alert alert-dismissable fade in" style="padding-right:35px;">-->
                            <#--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
                            <#--&lt;#&ndash;<#if error.null??>&ndash;&gt;-->
                            <#--${error}-->
                            <#--&lt;#&ndash;<#else>&ndash;&gt;-->
                                <#--&lt;#&ndash;Email or password is invalid.&ndash;&gt;-->
                            <#--&lt;#&ndash;</#if>&ndash;&gt;-->
                        <#--</div>-->
                    <#--</#if>-->
                    <#--<#if regist?has_content>-->
                        <#--<div class="login-control alert alert-dismissable fade in" style="padding-right:35px;">-->
                            <#--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
                            <#--Register success. Check your mail.-->
                        <#--</div>-->
                    <#--</#if>-->
                    <#--<#if find?has_content>-->
                        <#--<div class="login-control alert alert-dismissable fade in" style="padding-right:35px;">-->
                            <#--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
                            <#--Success Mail Sending.-->
                        <#--</div>-->
                    <#--</#if>-->
                    <#--<h2 class="m-t-0 m-b-15">-->
                        <#--Login</h2>-->
                    <#--<input type="text" name="loginId" class="login-control m-b-10 validate[required,custom[email]]" placeholder="Email Address">-->
                    <#--<input type="password" name="password" class="login-control m-b-10 validate[required]" placeholder="Password">-->
                    <#--<input type="text" name="totp-verification-code" class="login-control" placeholder="OTP" style="display:none">-->
                    <#--<div class="m-t-20">-->
                        <#--<button type="submit" class="btn btn-sm m-r-5" >Sign In</button>-->

                        <#--<small>-->
                            <#--<a class="box-switcher" data-switch="box-register" href="">Don't have an Account?</a> or-->
                            <#--<a class="box-switcher" data-switch="box-reset" href="">Forgot Password?</a>-->
                        <#--</small>-->
                    <#--</div>-->
                <#--</form>-->

                <#--<!-- Register &ndash;&gt;-->
                <#--<form class="box animated tile form-validation-3" id="box-register" method="post" action="doSignup">-->
                    <#--<h2 class="m-t-0 m-b-15">Register</h2>-->
                    <#--<input type="text" name="loginId" class="login-control m-b-10 validate[required,custom[email]]" placeholder="Email Address">-->
                    <#--<input type="password" id="password" name="password" class="login-control m-b-10 validate[required]" placeholder="Password">-->
                    <#--<input type="password" name="re_password" class="login-control m-b-20 validate[required,equals[password]]" placeholder="Confirm Password">-->
                    <#--<br>-->
                    <#--<div class="g-recaptcha text-center col-xs-12 m-b-20" data-theme="dark" data-sitekey="6Lf8GCIUAAAAALyvTLCKrAvJxpAQQt2oDDFLHbrw"></div>-->
                    <#--<br>-->
                    <#--<button class="btn btn-sm m-r-5">Register</button>-->

                    <#--<small><a class="box-switcher" data-switch="box-login" href="">Already have an Account?</a></small>-->
                <#--</form>-->

                <#--<!-- Forgot Password &ndash;&gt;-->
                <#--<form class="box animated tile form-validation-2" id="box-reset" method="post" action="findPassword">-->
                    <#--<h2 class="m-t-0 m-b-15">Reset Password</h2>-->
                    <#--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu risus. Curabitur commodo lorem fringilla enim feugiat commodo sed ac lacus.</p>-->
                    <#--<input type="text" name="loginId" class="login-control m-b-20 validate[required,custom[email]]" placeholder="Email Address">-->

                    <#--<button type="submit" class="btn btn-sm m-r-5">Reset Password</button>-->

                    <#--<small><a class="box-switcher" data-switch="box-login" href="">Already have an Account?</a></small>-->
                <#--</form>-->
            <#--</div>-->
        </section>
        <#--<div id="coinIndicator">-->
            <#--<script id="indicatorTemplate" type="text/x-jquery-tmpl">-->
                <#--<label class="indicator-block text-center" >-->
                    <#--<div class="indicator-block-header">-->

                        <#--<h5 style="display: block; color:${r"${color}"};">${r"${changePoint}"} <i class="glyphicon glyphicon-arrow-${r"${pointer}"}" ></i>(${r"${change}"}%)-->
                            <#--<br>-->
                            <#--<small>${r"${price}"}</small>-->
                        <#--</h5>-->

                    <#--</div>-->
                    <#--<div class="indicator-block-body">-->
                        <#--<h4>${r"${coinName}"}</h4>-->
                        <#--<h5 class="indicator-block-value">${r"${volume}"}</h5>-->
                    <#--</div>-->
                <#--</label>-->
            <#--</script>-->
            <#--<div class="col-lg-8 col-lg-offset-2">-->
                <#--<div class="row">-->
                    <#--<div id="coinHolder" data-toggle="buttons">-->

                    <#--</div>-->
                <#--</div>-->

            <#--</div>-->
            <#--<div class="clearfix"></div>-->
        <#--</div>-->

        <section class="block-volume">
            <div class="row">
                <div class="vol-data col-xs-12">
                <div>Now</div>
                <div class="desc">Preparing</div>
            </div>
        </section>

        <#--<section class="block-volume">-->
            <#--<div class="row">-->
                <#--<div class="col-sm-10 col-sm-offset-1">-->
                    <#--<div class="vol-data col-sm-12 col-md-4">-->
                        <#--<div>$46,870,042</div>-->
                        <#--<div class="desc">24 HOUR VOLUME</div>-->
                    <#--</div>-->
                    <#--<div class="vol-data col-sm-12 col-md-4">-->
                        <#--<div>$397,211,353</div>-->
                        <#--<div class="desc">7 DAY VOLUME</div>-->
                    <#--</div>-->
                    <#--<div class="vol-data col-sm-12 col-md-4">-->
                        <#--<div>$2,125,287,161</div>-->
                        <#--<div class="desc">30 DAY VOLUME</div>-->
                    <#--</div>-->
                <#--</div>-->
            <#--</div>-->

        <#--</section>-->
        <section>
            <div class="bs-docs-featurette text-center">
                <div id="okbitFeatures" class="container">
                    <div class="col-lg-10 col-lg-offset-1 ">
                        <div class="col-sm-12" style="margin-bottom:40px; margin-top:40px;">
                            <div id="dropcaps" class="block-area">
                                <h2 style="margin-bottom:30px; font-weight: bold;">OK-BIT Features</h2>
                                <p class="col-md-10 col-md-offset-1" style="font-size:20px!important;">Ok-bit is a full-featured spot trading platform for the major cryptocurrencies such as Bitcoin, Ethereum, Monero, Litecoin, Dash, and KRW. The platform offers leveraged margin trading through our peer-to-peer funding market.</p>
                                <br/>
                            </div>
                        </div>
                        <div class="col-sm-3 features text-center">
                            <img src="/img/index/feature_security.png" class="img-responsive" style="margin:auto">

                        </div>
                        <div class="col-sm-3 features text-center">
                            <img src="/img/index/feature_lowfee.png" class="img-responsive" style="margin:auto">

                        </div>
                        <div class="col-sm-3 features text-center">
                            <img src="/img/index/feature_easychart.png" class="img-responsive" style="margin:auto">

                        </div>
                        <div class="col-sm-3 features text-center">
                            <img src="/img/index/feature_varietycoin.png" class="img-responsive" style="margin:auto">

                        </div>

                    </div>
                </div>
            </div>

        </section>
    </body>

    <link href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <!--footer start from here-->
    <footer>
        <div class="container-fluid col-md-10 col-md-offset-1 visible-md visible-lg">
            <div class="row">
                <div class="col-md-3 col-sm-6 footerleft ">
                    <div class="logofooter"><img src = "img/logo/footer_logo.png"></div>
                    <p>(주)오케이비트 | 대표 김주현 <br><br>사업자등록번호 885-88-00694 <br>통신판매업신고 2017-서울강남0000호</p>
                    <p><i class="fa fa-map-pin"></i>경기도 부천시 송내대로 31, 202호(송내동)</p>
                    <#--<p><i class="fa fa-phone"></i> Phone (KOREA) : +82 1588-1588</p>-->
                    <p><i class="fa fa-envelope"></i> E-mail : support@ok-bit.com</p>

                </div>
                <div class="col-md-2 col-sm-6 paddingtop-bottom">
                    <h6 class="heading7">SUPPORT</h6>
                    <ul class="footer-ul">
                        <li><a href="#"> Privacy Policy</a></li>
                        <li><a href="#"> Terms & Conditions</a></li>
                        <li><a href="#"> User Guide</a></li>
                        <li><a href="#"> Ask Advertisement</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-6 paddingtop-bottom">
                    <h6 class="heading7">LANGUAGE</h6>
                    <ul class="footer-ul">
                        <li><a href="#"> English</a></li>
                        <li><a href="#"> 한국어</a></li>
                    </ul>
                </div>
                <div class="col-md-2 col-sm-6 paddingtop-bottom">
                    <h6 class="heading7">LINKS</h6>
                    <ul class="footer-ul">
                        <li>
                            <blockquote>
                                <a href="#">Coin Market Capital</a>
                            </blockquote>
                        </li>

                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 paddingtop-bottom">
                    <h6 class="heading7">LATEST POST</h6>
                    <div class="post">
                        <p>facebook crack the movie advertisment code:what it means for you <span>August 3,2015</span></p>
                        <p>facebook crack the movie advertisment code:what it means for you <span>August 3,2015</span></p>
                        <p>facebook crack the movie advertisment code:what it means for you <span>August 3,2015</span></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--footer start from here-->

    <div class="copyright">
        <div class="container-fluid col-md-10 col-md-offset-1">
            <div class="col-md-6">
                <p>© 2016 - All Rights OK-BIT</p>
            </div>
        </div>
    </div>
    <!-- Javascript Libraries -->
    <!-- jQuery -->
    <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->

    <!-- Bootstrap -->
    <script src="/js/bootstrap.min.js"></script>

    <!--  Form Related -->
    <script src="/js/validation/validate.min.js"></script> <!-- jQuery Form Validation Library -->
    <script src="/js/validation/validationEngine.min.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->
    <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->
    <script src="/js/select.min.js"></script> <!-- Custom Select -->

    <!-- Jqutemplateslate -->
    <script src="/js/jquery.tmpl.min.js"></script>

    <!-- Jquery cookie -->
    <script src="/js/jquery.cookie.js"></script>

    <!-- All JS functions -->
    <script src="/js/functions.js"></script>

    <!-- ticker -->
    <script src="/js/api/common_api.js"></script>
    <script src="/js/index/ticker.js"></script>

    <!-- recaptcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- login -->
    <script src="/js/index/login.js"></script>

    <script type="text/javascript">
        $('document').ready(function(){

           $('#box-login input[name=loginId], #box-login input[name=password]').on('focusout', function(e){

                otpCheck();

           });

        });

    </script>

</html>
