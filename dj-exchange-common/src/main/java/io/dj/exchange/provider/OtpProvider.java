package io.dj.exchange.provider;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorConfig;
import com.warrenstrange.googleauth.KeyRepresentation;
import io.dj.exchange.domain.primary.User;
import org.apache.commons.codec.binary.Base32;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 17.
 * Description
 */

    @Component
    public class OtpProvider {
        GoogleAuthenticator ga;

        @PostConstruct
        public void init() {
            GoogleAuthenticatorConfig.GoogleAuthenticatorConfigBuilder cb = new GoogleAuthenticatorConfig.GoogleAuthenticatorConfigBuilder();
            cb.setCodeDigits(6).setTimeStepSizeInMillis(TimeUnit.SECONDS.toMillis(30)).setKeyRepresentation(KeyRepresentation.BASE32);
            ga = new GoogleAuthenticator(cb.build());
        }

        public String genSecretKey(String pw) {
            Base32 codec32 = new Base32();
            String hex = DigestUtils.md5Hex(pw).substring(0, 10);
            return codec32.encodeAsString(hex.getBytes());
        }


        public Boolean isOtpValid(String otpHash, String otpCode) {
            return ga.authorize(genSecretKey(otpHash), Integer.valueOf(otpCode));
        }

        public Boolean isOtpValid(User user, String otpCode) {
            return ga.authorize(genSecretKey(user.getUserSetting().getOtpHash()), Integer.valueOf(otpCode));
        }

        public String getQRBarcodeURL(
                String otpHash) {
            String format = "https://chart.googleapis.com/chart?chs=200x200&chld=M|0&cht=qr&chl=otpauth://totp/OKBIT?secret=%s&issuer=OKBIT";
            return String.format(format, otpHash);
        }
    }


