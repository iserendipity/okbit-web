<script id="recentWithdrawTemplate" type="text/x-jquery-tmpl">
<tr>
    <td class="col-xs-2">${r"${dtTxt}"}
    </td>
    <td class="col-xs-1">${r"${status}"}
    </td>
    <td class="col-xs-1">${r"${coin.name}"}
    </td>
    <td class="col-xs-2">${r"${amount}"}
    </td>
    <td class="col-xs-6"><a target="_blank" href="${r"${blockExplorer.explorerUrl}"}${r"${txId}"}">${r"${txId}"}</a>
    </td>
</tr>
</script>
<div class="overflow" style="max-height:400px;">
<table class="table table-condensed table-hover table-fixed">
    <thead style="height:0px;">
    <tr>
        <th class="col-xs-2">
        </th>
        <th class="col-xs-1">
        </th>
        <th class="col-xs-1">
        </th>
        <th class="col-xs-2">
        </th>
        <th class="col-xs-6">
        </th>
    </tr>
    </thead>
    <tbody style="height:366px; overflow:scroll" id="recentWithdrawTableBody">
    <tr>
        <td colspan="5" class="text-center"><@spring.message "NO_RECENT_WITHDRAW"/></td>
    </tr>
    </tbody>
</table>
</div>