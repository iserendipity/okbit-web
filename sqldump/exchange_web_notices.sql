INSERT INTO exchange.web_notices (title, contents, dt, user_id, status) VALUES ('거래소 OK-BIT이 오픈합니다', '한국 최대의 거래소 OK-BIT이 오픈합니다', '2017-07-13 12:00:00', 1, 'USE');
INSERT INTO exchange.web_notices (title, contents, dt, user_id, status) VALUES ('BITCOIN segwit2x 관련 안내드립니다', '안녕하세요, (주)오케이비트입니다.<br>
<p>비트코인의 블록체인이 8월 1일경부터 나뉘어질 예정입니다.</p><p>오랜 토의 끝에 (주)오케이비트는 고객님들의 귀한 자산을 보호하기 위해 블록체인이 안정화될 때까지 비트코인의 입출금을 진행하지 않기로 결정했습니다.</p><p>향후 비트코인의 블록체인이 안정기에 접어들고 이에 대한 자사의 정책이 결정되면 다시 공지를 통해 알려드리도록 하겠습니다.</p>
<br>
감사합니다.
<br>
오케이비트 Support 팀 드림.', '2017-07-18 12:42:42', 1, 'USE');