import io.dj.exchange.DjExchangeAdminApplication;
import io.dj.exchange.dto.ReportDTO;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.provider.BankServiceProvider;
import io.dj.exchange.provider.BitfinexProvider;
import io.dj.exchange.provider.OtpProvider;
import io.dj.exchange.repository.hibernate.primary.ConfirmRepository;
import io.dj.exchange.repository.hibernate.primary.TransactionsRepository;
import io.dj.exchange.repository.hibernate.primary.UserRepository;
import io.dj.exchange.repository.hibernate.primary.UserSettingRepository;
import io.dj.exchange.repository.hibernate.readonly.MarketOrdersRepository;

import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 18.
 * Description
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DjExchangeAdminApplication.class})
@TestPropertySource(locations = "classpath:application.properties")
@WebAppConfiguration
public class Test {

    @Autowired
    MarketOrdersRepository marketOrdersRepository;

    @Autowired
    BankServiceProvider bProvider;

//    @Autowired
//    MailService mailService;

    @Autowired
    ApiProvider apiProvider;

    @Autowired
    ApiProvider testProvider;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BitfinexProvider bitfinexProvider;

    @Autowired
    TransactionsRepository transactionsRepository;

    @Autowired
    UserSettingRepository userSettingRepository;

    @Autowired
    private ConfirmRepository confirmRepository;

    String client_id = "l7xxe88b3120d5b546eb9d0fbb85fd8325db";
    String secret = "cbcc0a69cf124b398fe4dab6f6f40016";
    String login_token = "";
    String inquiry_token = "";
    String scope = "login";
    String redirectUrl = "http://localhost:8080/test";

    @Autowired
    private OtpProvider otpProvider;

//    @Autowired
//    private MailService mailService;

    @org.junit.Test
    public void sendEmail(){

        ReportDTO rep = new ReportDTO();
        rep.setEmail("iserendipity@me.com");
        rep.setCoin("BITCOIN");
        //mailService.sendReportEmail(rep);




    }


}
