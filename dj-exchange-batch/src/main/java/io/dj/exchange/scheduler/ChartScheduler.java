package io.dj.exchange.scheduler;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimaps;
import io.dj.exchange.annotation.TransactionalEx;
import io.dj.exchange.domain.primary.BatchOffset;
import io.dj.exchange.domain.cache.RedisChart;
import io.dj.exchange.domain.chart.History;
import io.dj.exchange.domain.dto.SumHistory;
import io.dj.exchange.enums.BatchKey;
import io.dj.exchange.repository.cache.HistoryRepository;
import io.dj.exchange.repository.hibernate.primary.BatchOffsetRepository;
import io.dj.exchange.repository.hibernate.primary.CoinRepository;
import io.dj.exchange.repository.hibernate.readonly.MarketOrdersRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 26.
 * Description
 */
@Component
@Slf4j
public class ChartScheduler {

    long lastIdResolutionD = 0L;
    long lastIdResolution1M = 0L;
    long lastIdResolution5M = 0L;
    long lastIdResolution10M = 0L;
    long lastIdResolution15M = 0L;
    long lastIdResolution30M = 0L;
    long lastIdResolution60M = 0L;

    private final DateTimeFormatter tickerFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH");

    private final BatchOffsetRepository batchOffsetRepository;

    private final int maxBatchCount = 100;

    private final CoinRepository coinRepository;
    private final MarketOrdersRepository marketOrdersRepository;

    private final HistoryRepository historyRepository;

//    @Autowired
//    private RedisTemplate template;

    public ChartScheduler(CoinRepository coinRepository, MarketOrdersRepository marketOrdersRepository, BatchOffsetRepository batchOffsetRepository, HistoryRepository historyRepository) {
        this.coinRepository = coinRepository;
        this.marketOrdersRepository = marketOrdersRepository;
        this.historyRepository = historyRepository;
        this.batchOffsetRepository = batchOffsetRepository;
        offsetCheck();
    }

    public void offsetCheck(BatchKey key){

        BatchOffset offset = batchOffsetRepository.findOne(key);
        if(offset != null){
            long value = offset.getOffsetValue();
            switch(offset.getOffsetName().name()){

                case "ResolutionD" :

                    this.lastIdResolutionD = value;

                    break;

                case "Resolution1M":

                    this.lastIdResolution1M = value;

                    break;

                case "Resolution5M":

                    this.lastIdResolution5M = value;

                    break;

                case "Resolution10M":

                    this.lastIdResolution10M = value;

                    break;

                case "Resolution15M":

                    this.lastIdResolution15M = value;

                    break;

                case "Resolution30M":

                    this.lastIdResolution30M = value;

                    break;

                case "Resolution60M":

                    this.lastIdResolution60M = value;

                    break;

            }
        }else{

            this.lastIdResolutionD = 0L;
            this.lastIdResolution1M = 0L;
            this.lastIdResolution5M = 0L;
            this.lastIdResolution10M = 0L;
            this.lastIdResolution15M = 0L;
            this.lastIdResolution30M = 0L;
            this.lastIdResolution60M = 0L;

        }

    }

    public void offsetCheck(){
        List<BatchOffset> offsets = batchOffsetRepository.findAll();

        for(BatchOffset offset : offsets){

            long value = offset.getOffsetValue();

            switch (offset.getOffsetName().name()){

                case "ResolutionD" :

                    this.lastIdResolutionD = value;

                    break;

                case "Resolution1M":

                    this.lastIdResolution1M = value;

                    break;

                case "Resolution5M":

                    this.lastIdResolution5M = value;

                    break;

                case "Resolution10M":

                    this.lastIdResolution10M = value;

                    break;

                case "Resolution15M":

                    this.lastIdResolution15M = value;

                    break;

                case "Resolution30M":

                    this.lastIdResolution30M = value;

                    break;

                case "Resolution60M":

                    this.lastIdResolution60M = value;

                    break;

            }

        }
    }

    public void redisSetter(List<SumHistory> result, String resolution) {

        //ValueOperations<String, List<History>> vo = template.opsForValue();

        ImmutableListMultimap group = Multimaps.index(result, sumHistory -> sumHistory.getCoinName());
        Iterator<String> coinNameKeyIterator = group.asMap().keySet().iterator();

        while (coinNameKeyIterator.hasNext()) {

            String coinName = coinNameKeyIterator.next();
            List<SumHistory> coinNameHistory = group.get(coinName);

            ImmutableListMultimap minuteGroupList = Multimaps.index(coinNameHistory, sumHistory -> sumHistory.getT());

            Iterator<Long> minuteKeyIterator = minuteGroupList.asMap().keySet().iterator();

            while (minuteKeyIterator.hasNext()) {

                long key = minuteKeyIterator.next();

                List<SumHistory> minuteHistory = minuteGroupList.get(key);

                //log.info(minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution+" : {}", minuteHistory);

                BigDecimal c = minuteHistory.get(minuteHistory.size()-1).getP();
                BigDecimal l = minuteHistory.parallelStream().sorted(Comparator.comparing(SumHistory::getP)).findFirst().get().getP();
                BigDecimal h = minuteHistory.parallelStream().sorted(Comparator.comparing(SumHistory::getP).reversed()).findFirst().get().getP();
//                BigDecimal h = minuteHistory.parallelStream().max(Comparator.comparing(SumHistory::getP)).get().getP();
//                BigDecimal l = minuteHistory.parallelStream().min(Comparator.comparing(SumHistory::getP)).get().getP();
                //BigDecimal l = minuteHistory.stream().sorted(Comparator.comparing(SumHistory::getP)).findFirst().get().getP();
                BigDecimal o = minuteHistory.get(0).getP();
                //long lastId = minuteHistory.get(minuteHistory.size()-1).getId();

                long t = key;
                double v = minuteHistory.parallelStream().mapToDouble(SumHistory::getVol).sum();

                //List<History> historyList = vo.get(minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution);
                RedisChart redisChart = historyRepository.findOneByTicker(minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution);
                //List<History> historyList = historyRepository.findOneByTicker(minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution).getHistories();
//                log.info("-------------ticker : {}", minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution);
//                log.info("-------------redisChart : {}", redisChart);
                if(redisChart != null) {
                    List<History> historyList = redisChart.getHistories();

                    //log.info("---------------------되나 : {}", historyList);

                    if(historyList != null ){

                        Iterator<History> historyIterator = historyList.iterator();
                        int idx = 0;
                        History history = new History();
                        while(historyIterator.hasNext()){

                            History historySet = historyIterator.next();

                            if(historySet.getT() == key){//새로운 데이터인데 날짜가 같다&& historySet.getId() != lastId

                                if(historySet.getL().compareTo(l) != -1){
                                    history.setL(l);

                                }else{
                                    history.setL(historySet.getL());
                                }

                                if(historySet.getH().compareTo(h) != 1){
                                    history.setH(h);

                                }else{
                                    history.setH(historySet.getH());
                                }

                                history.setO(historySet.getO());
                                history.setC(c);
                                history.setV(v+historySet.getV());
                                history.setT(key);

                                historyList.set(idx, history);

                            }else{

                                history.setL(l);
                                history.setT(t);
                                history.setO(o);
                                history.setC(c);
                                history.setH(h);
                                history.setV(v);

                            }

                            idx ++;

                        }

                        //히스토리리스트에서 날짜들을 가져온다.
                        int idx2 = 0;
                        for(History his : historyList){

                            if(key <= his.getT()){
                                break;
                            }

                            idx2++;
                        }

                        historyList.add(idx2, history);

                    }

                    historyList.parallelStream().sorted(Comparator.comparing(History::getT)).collect(Collectors.toList());
                    historyRepository.save(redisChart);
//                vo.set(minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution, historyList);

                }else{

                    List<History> historyList = new ArrayList<>();

                    History history = new History();
                    history.setL(l);
                    history.setT(t);
                    history.setO(o);
                    history.setC(c);
                    history.setH(h);
                    history.setV(v);

                    historyList.add(history);

                    RedisChart chart = new RedisChart();

                    chart.setTicker(minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution);
                    chart.setHistories(historyList);

                    historyRepository.save(chart);

                }
                log.info("batch job progressing - ticker : {}", minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution);
//                log.info("------------- : {}", historyRepository.findOneByTicker(minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution));

            }

        }

    }

    @TransactionalEx
    public void offsetSave(BatchKey key, Long lastId){
        BatchOffset offset = batchOffsetRepository.findOneByOffsetName(key).orElse(offset = new BatchOffset());
        offset.setOffsetName(key);
        offset.setOffsetValue(lastId);
        batchOffsetRepository.save(offset);
    }

    @Scheduled(fixedRate=10000)
    public void chartResolutionDCaching(){
        offsetCheck(BatchKey.ResolutionD);
        String resolution = "D";
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        List<SumHistory> result = marketOrdersRepository.getSumHistoryCoinsMin(lastIdResolutionD, new PageRequest(0, maxBatchCount));

        if(result != null && !result.isEmpty()) {

            for(int i=0; i<result.size(); i++){

                result.get(i).setTicker(result.get(i).getDt().format(tickerFormatter));

                String tickerDtFormatted = result.get(i).getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

                try {
                    result.get(i).setT((dfm.parse(tickerDtFormatted)).getTime() / 1000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            lastIdResolutionD = result.get(result.size()-1).getId();//result.parallelStream().sorted(Comparator.comparing(SumHistory::getId).reversed()).findFirst().get().getId();
            offsetSave(BatchKey.ResolutionD, lastIdResolutionD);
            redisSetter(result, resolution);
        }

    }

    @Scheduled(fixedRate=10000)
    public void chartResolution5MCaching(){
        offsetCheck(BatchKey.Resolution5M);
        String resolution = "5";
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        List<SumHistory> result = marketOrdersRepository.getSumHistoryCoinsMin(lastIdResolution30M, new PageRequest(0, maxBatchCount));

        if(result != null && !result.isEmpty()) {

            lastIdResolution5M = result.parallelStream().sorted(Comparator.comparing(SumHistory::getId).reversed()).findFirst().get().getId();

            for(SumHistory sumHistory : result) {

                sumHistory.setTicker(sumHistory.getDt().format(tickerFormatter));

                int minute = sumHistory.getDt().getMinute();

                String tickerDtFormatted = sumHistory.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:"))+StringUtils.leftPad(String.valueOf(Integer.parseInt(resolution) * (int)(Math.floor(minute / Integer.parseInt(resolution)))), 2, "0");

                try {
                    sumHistory.setT((dfm.parse(tickerDtFormatted)).getTime() / 1000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            BatchOffset offset = batchOffsetRepository.findOneByOffsetName(BatchKey.Resolution5M).orElse(offset = new BatchOffset());
//            offset.setOffsetName(BatchKey.Resolution5M);
//            offset.setOffsetValue(lastIdResolution5M);
//            batchOffsetRepository.save(offset);
            offsetSave(BatchKey.Resolution5M, lastIdResolution5M);
            redisSetter(result, resolution);

        }

    }

    @Scheduled(fixedRate=10000)
    public void chartResolution10MCaching(){
        offsetCheck(BatchKey.Resolution10M);
        String resolution = "10";
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        List<SumHistory> result = marketOrdersRepository.getSumHistoryCoinsMin(lastIdResolution30M, new PageRequest(0, maxBatchCount));

        if(result != null && !result.isEmpty()) {

            lastIdResolution10M = result.parallelStream().sorted(Comparator.comparing(SumHistory::getId).reversed()).findFirst().get().getId();

            for(SumHistory sumHistory : result) {

                sumHistory.setTicker(sumHistory.getDt().format(tickerFormatter));

                int minute = sumHistory.getDt().getMinute();

                String tickerDtFormatted = sumHistory.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:"))+StringUtils.leftPad(String.valueOf(Integer.parseInt(resolution) * (int)(Math.floor(minute / Integer.parseInt(resolution)))), 2, "0");

                try {
                    sumHistory.setT((dfm.parse(tickerDtFormatted)).getTime() / 1000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            BatchOffset offset = batchOffsetRepository.findOneByOffsetName(BatchKey.Resolution10M).orElse(offset = new BatchOffset());
//            offset.setOffsetName(BatchKey.Resolution10M);
//            offset.setOffsetValue(lastIdResolution10M);
//            batchOffsetRepository.save(offset);
            offsetSave(BatchKey.Resolution10M, lastIdResolution10M);
            redisSetter(result, resolution);

        }

    }

    @Scheduled(fixedRate=10000)
    public void chartResolution15MCaching(){
        offsetCheck(BatchKey.Resolution15M);
        String resolution = "15";
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        List<SumHistory> result = marketOrdersRepository.getSumHistoryCoinsMin(lastIdResolution15M, new PageRequest(0, maxBatchCount));

        if(result != null && !result.isEmpty()) {

            lastIdResolution15M = result.parallelStream().sorted(Comparator.comparing(SumHistory::getId).reversed()).findFirst().get().getId();

            for(SumHistory sumHistory : result) {

                sumHistory.setTicker(sumHistory.getDt().format(tickerFormatter));

                int minute = sumHistory.getDt().getMinute();

                String tickerDtFormatted = sumHistory.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:"))+StringUtils.leftPad(String.valueOf(Integer.parseInt(resolution) * (int)(Math.floor(minute / Integer.parseInt(resolution)))), 2, "0");
                //log.info(tickerDtFormatted);
                try {
                    sumHistory.setT((dfm.parse(tickerDtFormatted)).getTime() / 1000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            BatchOffset offset = batchOffsetRepository.findOneByOffsetName(BatchKey.Resolution15M).orElse(offset = new BatchOffset());
//            offset.setOffsetName(BatchKey.Resolution15M);
//            offset.setOffsetValue(lastIdResolution15M);
//            batchOffsetRepository.save(offset);
            offsetSave(BatchKey.Resolution15M, lastIdResolution15M);
            redisSetter(result, resolution);

        }

    }

    @Scheduled(fixedRate=10000)
    public void chartResolution30MCaching(){
        offsetCheck(BatchKey.Resolution30M);
        String resolution = "30";
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        List<SumHistory> result = marketOrdersRepository.getSumHistoryCoinsMin(lastIdResolution30M, new PageRequest(0, maxBatchCount));

        if(result != null && !result.isEmpty()) {

            lastIdResolution30M = result.parallelStream().sorted(Comparator.comparing(SumHistory::getId).reversed()).findFirst().get().getId();

            for(SumHistory sumHistory : result) {

                sumHistory.setTicker(sumHistory.getDt().format(tickerFormatter));

                int minute = sumHistory.getDt().getMinute();

                String tickerDtFormatted = sumHistory.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:"))+StringUtils.leftPad(String.valueOf(Integer.parseInt(resolution) * (int)(Math.floor(minute / Integer.parseInt(resolution)))), 2, "0");

                try {
                    sumHistory.setT((dfm.parse(tickerDtFormatted)).getTime() / 1000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            BatchOffset offset = batchOffsetRepository.findOneByOffsetName(BatchKey.Resolution30M).orElse(offset = new BatchOffset());
//            offset.setOffsetName(BatchKey.Resolution30M);
//            offset.setOffsetValue(lastIdResolution30M);
//            batchOffsetRepository.save(offset);
            offsetSave(BatchKey.Resolution30M, lastIdResolution30M);
            redisSetter(result, resolution);

        }

    }


    @Scheduled(fixedRate=10000)
    public void chartResolution60MCaching(){

        offsetCheck(BatchKey.Resolution60M);
        String resolution = "60";
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:00");

        List<SumHistory> result = marketOrdersRepository.getSumHistoryCoinsMin(lastIdResolution60M, new PageRequest(0, maxBatchCount));

        if(result != null && !result.isEmpty()) {

            lastIdResolution60M = result.parallelStream().sorted(Comparator.comparing(SumHistory::getId).reversed()).findFirst().get().getId();

            for(SumHistory sumHistory : result) {

                sumHistory.setTicker(sumHistory.getDt().format(tickerFormatter));

                String tickerDtFormatted = sumHistory.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:00"));

                try {
                    sumHistory.setT((dfm.parse(tickerDtFormatted)).getTime() / 1000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            BatchOffset offset = batchOffsetRepository.findOneByOffsetName(BatchKey.Resolution60M).orElse(offset = new BatchOffset());
//            offset.setOffsetName(BatchKey.Resolution60M);
//            offset.setOffsetValue(lastIdResolution60M);
//            batchOffsetRepository.save(offset);
            offsetSave(BatchKey.Resolution60M, lastIdResolution60M);
            redisSetter(result, resolution);

        }

    }

    @Scheduled(fixedRate=10000)
    public void chartResolution1MCaching() throws Exception{
        offsetCheck(BatchKey.Resolution1M);
        String resolution = "1";
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        List<SumHistory> result = marketOrdersRepository.getSumHistoryCoinsMin(lastIdResolution1M, new PageRequest(0, maxBatchCount));

        if(result != null && !result.isEmpty()) {

            lastIdResolution1M = result.parallelStream().sorted(Comparator.comparing(SumHistory::getId).reversed()).findFirst().get().getId();


            //처음의 시간을 꺼낸다.
            //마지막 시간을 꺼낸다.
            //두 시간 사이의 모든 시간을 포매터에 맞춘다.
            //모든 시간대와 result의 시간을 비교해서 같은게 있으면 그대로 넣고, 없으면 전의 전의 프라이스를 넣는다. (o, h, c , v, l)

            //LocalDateTime first = result.parallelStream().sorted(Comparator.comparing(SumHistory::getDt)).findFirst().get().getDt();
            //LocalDateTime last =result.parallelStream().sorted(Comparator.comparing(SumHistory::getDt).reversed()).findFirst().get().getDt();

//            long from = result.parallelStream().sorted(Comparator.comparing(SumHistory::getT)).findFirst().get().getT();
//            long to = result.parallelStream().sorted(Comparator.comparing(SumHistory::getT).reversed()).findFirst().get().getT();
//
//            Date fromDt = new Date(from * 1000);
//            Date toDt = new Date(to*1000);
//
//            ArrayList<String> dates = new ArrayList<>();
//            Date currentDate = fromDt;
//
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//
//            while (currentDate.compareTo(toDt) <= 0) {
//                dates.add(sdf.format(currentDate));
//                Calendar c = Calendar.getInstance();
//                c.setTime(sdf.parse(sdf.format(currentDate)));
//                c.add(Calendar.MINUTE, 1);
//                currentDate = c.getTime();
//            }
//
//            dates.stream().forEach(date -> {
//                log.info(date);
//            });

            for(SumHistory sumHistory : result) {

                sumHistory.setTicker(sumHistory.getDt().format(tickerFormatter));

                String tickerDtFormatted = sumHistory.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

                try {
                    sumHistory.setT((dfm.parse(tickerDtFormatted)).getTime() / 1000);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            BatchOffset offset = batchOffsetRepository.findOneByOffsetName(BatchKey.Resolution1M).orElse(offset = new BatchOffset());
//            offset.setOffsetName(BatchKey.Resolution1M);
//            offset.setOffsetValue(lastIdResolution1M);
//            batchOffsetRepository.save(offset);

            offsetSave(BatchKey.Resolution1M, lastIdResolution1M);

            redisSetter(result, resolution);

        }

    }

    public void chartResolution1MCachingTest() throws Exception{
        //offsetCheck(BatchKey.Resolution1M);
        String resolution = "1";
        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        List<SumHistory> result = marketOrdersRepository.getSumHistoryCoinsMin(0L, new PageRequest(0, maxBatchCount));
        log.info("---------------- 1 : {}", result.size());
        if(result != null && !result.isEmpty()) {

            lastIdResolution1M = result.parallelStream().sorted(Comparator.comparing(SumHistory::getId).reversed()).findFirst().get().getId();

            //처음의 시간을 꺼낸다.
            //마지막 시간을 꺼낸다.
            //두 시간 사이의 모든 시간을 포매터에 맞춘다.
            //모든 시간대와 result의 시간을 비교해서 같은게 있으면 그대로 넣고, 없으면 전의 전의 프라이스를 넣는다. (o, h, c , v, l)

            long from = result.parallelStream().sorted(Comparator.comparing(SumHistory::getDt)).findFirst().get().getDt().atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
            long to =result.parallelStream().sorted(Comparator.comparing(SumHistory::getDt).reversed()).findFirst().get().getDt().atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();

            Date fromDt = new Date(from);
            Date toDt = new Date(to);

            ArrayList<String> dates = new ArrayList<>();
            Date currentDate = fromDt;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            while (currentDate.compareTo(toDt) <= 0) {
                dates.add(sdf.format(currentDate));
                Calendar c = Calendar.getInstance();
                c.setTime(sdf.parse(sdf.format(currentDate)));
                c.add(Calendar.MINUTE, 1);
                currentDate = c.getTime();

//                log.info("날짜 : "+sdf.format(currentDate));

            }

            List<SumHistory> savedResult = new ArrayList<>();

            log.info("---------------- 2 : {}", dates.size());



            log.info("---------------------- 2.6 : {}", savedResult.size());
//            for(SumHistory history : result){
//
//                for(String date : dates){
//
//                    history.setTicker(history.getDt().format(tickerFormatter));
//                    String tickerDtFormatted = history.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
//
//                    if(tickerDtFormatted.equals(date)){
//
//                        history.setT((dfm.parse(tickerDtFormatted)).getTime() / 1000);
//                        savedResult.add(history);
//
//                        break;
//
//                    }else{
//
//                        //log.info("---------------3: {}", result.parallelStream().filter(sumHistory -> sumHistory.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")).equals(date)).count());
//                        if(result.parallelStream().filter(sumHistory -> sumHistory.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")).equals(date)).count() ==0 ){
//
//                            SumHistory sh = new SumHistory(history.getId(), LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")), history.getCoinName(), history.getP(), new BigDecimal(0));
//
//                            savedResult.add(sh);
//                            i++;
//
//                        }
//
//
//
//                    }
//
//                }
//
//            }
//
//            log.info("---------------4 : {}", i);
//
//            savedResult = savedResult.stream().distinct().collect(Collectors.toList());
//
//            log.info("---------------- : {}", savedResult.size());

        }

    }

//                for(SumHistory history : result){
//
//                if(dates.contains(history.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")))){
//
//                }
//
//            }
//
//
//
//            for(SumHistory history : result) {
//
//                if (dates.contains(history.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")))) {//날짜가 있음
//
//                    savedResult.add(history);
//
//                } else {
//
//
//                }
//            }
//                for(String date : dates){
//
//                    if(history.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")).equals(date)){
//                    //if(result.stream().filter(sumHistory -> sumHistory.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")).equals(date)).count() > 0 ){
//                        //날짜가 리스트에 있음
//                        savedResult.add(history);
//                        break;
//
//                    }else{
//                        //날짜가 리스트에 없음
//
//
//
//                    }
//
//                }
//            }
//
//            for(String date : dates) {
//
//                result.stream().filter(sumHistory -> sumHistory.getDt().format(tickerFormatter).equals(date)).forEach(sumHistory -> {
//                    sumHistory.setTicker(sumHistory.getDt().format(tickerFormatter));
//                    String tickerDtFormatted = sumHistory.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
//                    try {
//                        sumHistory.setT((dfm.parse(tickerDtFormatted)).getTime() / 1000);
//                        savedResult.add(sumHistory);
//
//                    } catch (ParseException pe) {
//
//                    }
//
//                });
//            }
//
//
//
//                for(SumHistory sumHistory : result) {
//
//                    sumHistory.setTicker(sumHistory.getDt().format(tickerFormatter));
//
//                    String tickerDtFormatted = sumHistory.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
//
//                    try {
//                        sumHistory.setT((dfm.parse(tickerDtFormatted)).getTime() / 1000);
//
//                        if(!savedResult.contains(sumHistory)){
//                            savedResult.add(sumHistory);
//                            continue;
//                        }
//
//                        sumHistory.setT((dfm.parse(date)).getTime() / 1000);
//                        sumHistory.setDt(LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
//                        log.info("---------여기오는게 몇번이냐");
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//
//            }
//
//            log.info("result : {}", savedResult);
//
//            BatchOffset offset = batchOffsetRepository.findOneByOffsetName(BatchKey.Resolution1M).orElse(offset = new BatchOffset());
//            offset.setOffsetName(BatchKey.Resolution1M);
//            offset.setOffsetValue(lastIdResolution1M);
//            batchOffsetRepository.save(offset);
//
//            offsetSave(BatchKey.Resolution1M, lastIdResolution1M);
//
//            redisSetter(result, resolution);

}
