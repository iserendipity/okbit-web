package io.dj.exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DjExchangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DjExchangeApplication.class, args);
	}
}
