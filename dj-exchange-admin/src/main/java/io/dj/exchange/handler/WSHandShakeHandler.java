//package io.dj.exchange.handler;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.server.ServerHttpRequest;
//import org.springframework.http.server.ServerHttpResponse;
//import org.springframework.web.socket.WebSocketHandler;
//import org.springframework.web.socket.server.HandshakeFailureException;
//import org.springframework.web.socket.server.HandshakeHandler;
//import org.springframework.web.socket.server.support.DefaultHandshakeHandler;
//
//import java.util.Map;
//
///**
// * Created by kun7788 on 2016. 12. 14..
// */
//@Slf4j
//public class WSHandShakeHandler implements HandshakeHandler {
//
//    @Override
//    public boolean doHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
//                               Map<String, Object> attributes) throws HandshakeFailureException {
//        if(request.getHeaders().containsKey("Sec-WebSocket-Extensions")) {
//            request.getHeaders().set("Sec-WebSocket-Extensions", "permessage-deflate");
//        }
//
//        DefaultHandshakeHandler handler = new DefaultHandshakeHandler();
//        return handler.doHandshake(request,response,wsHandler,attributes);
//    }
//
//}
