package io.dj.exchange.enums;

/**
 * Created by kun7788 on 2016. 12. 11..
 */
public enum CoinEnum {
    BTC, LTC, PRC, PRC_AD, ETH, KRW
}
