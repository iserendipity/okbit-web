function WithdrawApi () {
    this.call = function(uri, callback, resultProcFunc, params) {
        $.ajax({
            url: "/api/withdraw" + uri
            , type: "POST"
            , dataType: 'json'
            , contentType:"application/json; charset=UTF-8"
            , data: JSON.stringify(params)
            , success: function (data) {
                resultProcFunc(data);
                return callback(data);
            }
            , error:function(e){
                //showAlert("ERROR : " + JSON.parse(e.responseText).message);
            }
        });
        return true;
    };

    this.wallet = function(callback, params) {
        return this.call("/wallet", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.withdrawKRW = function(callback, params) {

        return this.call("/withdrawKRW", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
                location.href = '/withdraw';
            } else {
                //showAlert("failed");
                alert(result.desc);
                $('#otpCode').val('');
            }
        }, params);
    };

    this.withdrawCoin = function(callback, params) {

        return this.call("/withdrawCoin", callback, function(result) {

            if (result.code == "SUCCESS") {
                //showAlert("success");
                location.href = '/withdraw';
            } else {
                alert(result.desc);
                $('#otpCode').val('');
                //$('#otpModal').modal('hide');
                //showAlert("failed");
            }
        }, params);
    };

    this.transactions = function(callback, params) {
        return this.call("/transactions", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };
}