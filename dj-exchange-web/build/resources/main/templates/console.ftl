<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.0/animate.min.css">
            <link rel="stylesheet" href="/css/animateModal.min.css">
    </head>

<#if (user.userSetting.theme)??>
    <body id="skin-blur-${user.userSetting.theme}">
<#else>
    <body id="skin-blur-blue">
</#if>
        <header id="header" class="media">
            <#include "common/top.ftl">
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            
            <!-- Sidebar -->
            <aside id="sidebar">

                <!-- Side Menu -->
                <ul class="list-unstyled side-menu">
                    <#include "common/sidemenu.ftl">
                </ul>

            </aside>
        
            <!-- Content -->
            <section id="content" class="container">

                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li style="font-size:14px;"><a href="/media/guide.pdf" target="_blank">GUIDE BOOK</a></li>
                    <#--<li><a href="#">Library</a></li>-->
                    <#--<li class="active">Data</li>-->
                </ol>
                
                <h4 class="page-title">TRADING CONSOLE</h4>
                                
                <!-- Shortcuts -->
                <#include "common/shortcut.ftl">
                
                <hr class="whiter" />

                <#--<div class="block-area">-->
                    <#--<div class="row">-->
                        <#--<div class="col-md-2 col-xs-6">-->
                            <#--<div class="tile quick-stats">-->
                                <#--<div id="stats-line-2" class="pull-left"><canvas width="392" height="65" style="display: inline-block; width: 392px; height: 65px; vertical-align: top;"></canvas></div>-->
                                <#--<div class="data">-->
                                    <#--<h2 data-value="98">98</h2>-->
                                    <#--<small>Tickets Today</small>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->

                        <#--<div class="col-md-2 col-xs-6">-->
                            <#--<div class="tile quick-stats media">-->
                                <#--<div id="stats-line-3" class="pull-left"><canvas width="392" height="65" style="display: inline-block; width: 392px; height: 65px; vertical-align: top;"></canvas></div>-->
                                <#--<div class="media-body">-->
                                    <#--<h2 data-value="1452">1,452</h2>-->
                                    <#--<small>Shipments today</small>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->

                        <#--<div class="col-md-2 col-xs-6">-->
                            <#--<div class="tile quick-stats media">-->

                                <#--<div id="stats-line-4" class="pull-left"><canvas width="392" height="65" style="display: inline-block; width: 392px; height: 65px; vertical-align: top;"></canvas></div>-->

                                <#--<div class="media-body">-->
                                    <#--<h2 data-value="4896">4,896</h2>-->
                                    <#--<small>Sell today</small>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->

                        <#--<div class="col-md-2 col-xs-6">-->
                            <#--<div class="tile quick-stats media">-->
                                <#--<div id="stats-line" class="pull-left"><canvas width="392" height="65" style="display: inline-block; width: 392px; height: 65px; vertical-align: top;"></canvas></div>-->
                                <#--<div class="media-body">-->
                                    <#--<h2 data-value="29356">29,355</h2>-->
                                    <#--<small>Buy today</small>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->

                    <#--</div>-->

                <#--</div>-->

                <#--<hr class="whiter">-->

                <!-- Main Widgets -->
               
                <div class="block-area">
                    <div class="row">
                        <div class="col-md-9 col-lg-10">
                            <!-- Main Chart -->
                            <#include "content/mainChart.ftl">

                            <div class="row">
                                <!--  ORDERS -->
                                <div class="col-md-12">
                                    <div class="tile">
                                        <h2 class="tile-title"><@spring.message "TITLE_ORDERS"/>
                                        </h2>
                                        <div id="orders" class="no-padding clearfix">
                                            <!-- Market tab -->
                                            <div class="fixed-thead-wrap col-xs-12 text-center" style="overflow-x:scrolll; width:100%">
                                                <div class="col-xs-6 m-b-5"><span class="h4"><@spring.message "TITLE_BUY"/></span></div>
                                                <div class="col-xs-6 m-b-5"><span class="h4"><@spring.message "TITLE_SELL"/></span></div>
                                                <#--<hr class="whiter">-->
                                                <div class="row-fluid">
                                                    <div style="width:10%" class="col-xs-1"><@spring.message "TITLE_COUNT"/></div>
                                                    <div style="width:10%" class="col-xs-2"><@spring.message "TITLE_TOT_PRICE"/></div>
                                                    <div style="width:10%" class="col-xs-1"><@spring.message "TITLE_TOT_COIN"/></div>
                                                    <div style="width:10%" class="col-xs-1"><@spring.message "TITLE_AMOUNT"/></div>
                                                    <div style="width:10%; color:green; font-size:14px !important; font-weight:bolder; " class="col-xs-1"><@spring.message "TITLE_PRICE"/></div>
                                                    <div style="width:10%; color:red; font-weight:bolder; font-size:14px !important;" class="col-xs-1"><@spring.message "TITLE_PRICE"/></div>
                                                    <div style="width:10%" class="col-xs-1"><@spring.message "TITLE_AMOUNT"/></div>
                                                    <div style="width:10%" class="col-xs-1"><@spring.message "TITLE_TOT_COIN"/></div>
                                                    <div style="width:10%" class="col-xs-2"><@spring.message "TITLE_TOT_PRICE"/></div>
                                                    <div style="width:10%" class="col-xs-1"><@spring.message "TITLE_COUNT"/></div>
                                                </div>
                                            </div>
                                            <div id="order" style="width:100%">
                                                <#include "content/mainOrders.ftl">
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <a href="#animatedModal" id="fullOrder" class="nb pull-right" style="font-size:8px;">Full Book</a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <!-- My Orders -->
                                    <div class="tile">
                                        <h2 class="tile-title"><@spring.message "TITLE_MYORDERS"/></h2>
                                        <div class="tile-config">
                                            <a data-toggle="collapse" href="#myOrder" class="tile-menu"></a>
                                        </div>
                                        <div id="myOrder" class="collapse clearfix">
                                            <!-- Market tab -->

                                            <div class="fixed-thead-wrap col-xs-12 text-center">
                                                <div class="fixed-thead col-xs-2"><@spring.message "TITLE_TYPE"/>
                                                </div>
                                                <div class="fixed-thead col-xs-4"><@spring.message "TITLE_DATE"/>
                                                </div>
                                                <div class="fixed-thead col-xs-2"><@spring.message "TITLE_PRICE"/>
                                                </div>
                                                <div class="fixed-thead col-xs-3"><@spring.message "TITLE_AMOUNT"/>
                                                </div>
                                                <div class="fixed-thead col-xs-1"><i class="fa fa-cog"></i>
                                                </div>
                                            </div>
                                            <div class="scroll-table overflow col-xs-12">
                                                <#include "content/infoMyOrderConsole.ftl">
                                            </div>

                                        </div>
                                        <div class="panel-footer">
                                            <a href="#myOrderModal" id="myOrderFullBtn" class="nb pull-right" style="font-size:8px;">Full Book</a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>

                                <!-- TRADES -->
                                <div class="col-md-4">
                                    <div class="tab-container tile">
                                        <h2 class="tile-title"><@spring.message "TITLE_MARKETTRADES"/>
                                        </h2>
                                        <div class="tile-config">
                                            <a data-toggle="collapse" href="#trades" class="tile-menu"></a>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="trades" class="collapse clearfix">
                                            <!-- Market tab -->
                                            <div class="fixed-thead-wrap col-xs-12 text-center">
                                                <div class="fixed-thead col-xs-3"><@spring.message "TITLE_DATE"/></div>
                                                <div class="fixed-thead col-xs-3"><@spring.message "TITLE_TYPE"/></div>
                                                <div class="fixed-thead col-xs-3"><@spring.message "TITLE_PRICE"/></div>
                                                <div class="fixed-thead col-xs-3"><@spring.message "TITLE_AMOUNT"/></div>
                                            </div>
                                            <div id="trade" class="scroll-table overflow col-xs-12">
                                            <#include "content/mainTrades.ftl">
                                            </div>

                                        </div>
                                        <div class="panel-footer">
                                            <a href="#fullMarketTradeModal" id="fullTradeBtn" class="nb pull-right" style="font-size:8px;">Full History</a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>

                                <!-- Mine TRADES -->
                                <div class="col-md-4">
                                    <div class="tile">
                                        <h2 class="tile-title"><@spring.message "TITLE_MYTRADES"/>
                                        </h2>
                                        <div class="tile-config">
                                            <a data-toggle="collapse" href="#tradesMine" class="tile-menu"></a>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="tradesMine" class="collapse clearfix">
                                            <!-- my tab -->
                                            <div class="" id="mine">
                                                <div class="fixed-thead-wrap col-xs-12 text-center">
                                                    <div class="fixed-thead col-xs-3"><@spring.message "TITLE_DATE"/></div>
                                                    <div class="fixed-thead col-xs-3"><@spring.message "TITLE_TYPE"/></div>
                                                    <div class="fixed-thead col-xs-3"><@spring.message "TITLE_PRICE"/></div>
                                                    <div class="fixed-thead col-xs-3"><@spring.message "TITLE_AMOUNT"/></div>
                                                </div>
                                                <div id="trade" class="scroll-table overflow col-xs-12">
                                                <#include "content/myTrades.ftl">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <a href="#fullMyTradeModal" id="myTradeBtn" class="nb pull-right" style="font-size:8px;">Full History</a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="col-md-3 col-lg-2">
                            <!-- COIN Info -->
                            <div id="coinInfoWrap" class="col-md-12">
                            <#include "rightInfo/infoCoin.ftl">
                            </div>

                            <!-- Order Form -->
                            <div id="orderFormWrap" class="col-md-12">
                                <div class="tile">
                                <#include "rightInfo/infoOrderForm.ftl">
                                </div>
                            </div>

                            <!-- Balance -->
                            <div id="balanceWrap" class="col-md-12">
                            <#include "rightInfo/infoBalance.ftl">
                            </div>



                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <#include "content/fullOrder.ftl">
                <#include "content/fullMyOrder.ftl">
                <#include "content/fullMyTrade.ftl">
                <#include "content/fullTrade.ftl">
            </section>

            <!-- Older IE Message -->
            <!--[if lt IE 9]>
                <div class="ie-block">
                    <h1 class="Ops">Ooops!</h1>
                    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser in order to access the maximum functionality of this website. </p>
                    <ul class="browsers">
                        <li>
                            <a href="https://www.google.com/intl/en/chrome/browser/">
                                <img src="/img/browsers/chrome.png" alt="">
                                <div>Google Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.mozilla.org/en-US/firefox/new/">
                                <img src="/img/browsers/firefox.png" alt="">
                                <div>Mozilla Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com/computer/windows">
                                <img src="/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://safari.en.softonic.com/">
                                <img src="/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-10/worldwide-languages">
                                <img src="/img/browsers/ie.png" alt="">
                                <div>Internet Explorer(New)</div>
                            </a>
                        </li>
                    </ul>
                    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
                </div>   
            <![endif]-->
        </section>

        <script type="text/javascript">

            var no_data_msg = '<@spring.message "EMPTY_DATA"/>';

        </script>
        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->
        <script src="/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
        <script src="/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Charts -->
        <script src="/js/charts/jquery.flot.js"></script> <!-- Flot Main -->
        <script src="/js/charts/jquery.flot.time.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen -->

        <script src="/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts -->
        <script src="/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts -->
        <script src="/js/charts.js"></script> <!-- All the above chart related functions -->

        <!-- Map -->
        <script src="/js/maps/jvectormap.min.js"></script> <!-- jVectorMap main library -->
        <script src="/js/maps/usa.js"></script> <!-- USA Map for jVectorMap -->

        <!--  Form Related -->
        <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->

        <!-- UX -->
        <script src="/js/scroll.min.js"></script> <!-- Custom Scrollbar -->

        <!-- Jqutemplateslate -->
        <script src="/js/jquery.tmpl.min.js"></script>

        <!-- cookie -->
        <script src="/js/jquery.cookie.js"></script>

        <!-- Other -->
        <script src="/js/calendar.min.js"></script> <!-- Calendar -->
        <script src="/js/feeds.min.js"></script> <!-- News Feeds -->

        <!-- All JS functions -->
        <script src="/js/functions.js"></script>

        <!-- animatedModal -->
        <script src="/js/animatedModal.min.js"></script>

        <!-- Jqutemplateslate -->
        <script src="/js/jquery.tmpl.min.js"></script>

        <script src="/js/jquery.loading.min.js"></script>

        <!-- trading view -->
        <script type="text/javascript" src="/charting_library/charting_library.min.js"></script>
        <script type="text/javascript" src="/charting_library/datafeed/udf/datafeed.js"></script>
<#--<script type="text/javascript" src="/charting_library/datafeed/udf/datafeed_ori.js"></script>-->

        <!-- trade functions -->
        <script src="/js/api/trade_api.js"></script>

        <!-- common functions -->
        <script src="/js/api/common_api.js"></script>
        <script src="/js/common/common.js"></script>
        <!-- console functions -->
        <script src="/js/api/console_api.js"></script>
        <script src="/js/console/console.js"></script>

        <!-- custom utils -->
        <script src="/js/api/util.js"></script>

        <script src="https://cdn.jsdelivr.net/sockjs/1/sockjs.min.js"></script>
        <script src="/js/stomp.min.js"></script>

        <!-- socket -->
        <script src="/js/console/socket.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){

                $("#fullOrder").animatedModal({
                    animatedIn:'zoomIn',
                    animatedOut:'bounceOut',
                    color:'#444',
                    beforeOpen: function() {

                        loadOrdersFull();

                    },
                    afterClose: function() {
                        //$(".thumb").hide();

                    }
                });

                $("#myOrderFullBtn").animatedModal({
                    modalTarget:'myOrderModal',
                    animatedIn:'zoomIn',
                    animatedOut:'bounceOut',
                    color:'#444',
                    beforeOpen: function() {

                        makeMyOrdersFull();

                    },
                    afterClose: function() {
                        //$(".thumb").hide();

                    }
                });

                $("#myTradeBtn").animatedModal({
                    modalTarget:'fullMyTradeModal',
                    animatedIn:'zoomIn',
                    animatedOut:'bounceOut',
                    color:'#444',
                    beforeOpen: function() {

                        loadMyFullTrades();

                    },
                    afterClose: function() {
                        //$(".thumb").hide();

                    }
                });

                $("#fullTradeBtn").animatedModal({
                    modalTarget:'fullMarketTradeModal',
                    animatedIn:'zoomIn',
                    animatedOut:'bounceOut',
                    color:'#444',
                    beforeOpen: function() {

                        loadMarketFullTrades();

                    },
                    afterClose: function() {
                        //$(".thumb").hide();

                    }
                });

                commonInit();

                init();


                //$('#animatedModal').animatedModal();
                //setTimeout(scrollStop, 3000);

            });

        </script>
        <#include "common/validation_locale.ftl"/>
    </body>
</html>
