<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

<#if (data.confirm) == 'ok' >
<script type="text/javascript">
    alert('<@spring.message "MAIL_CONFIRMED"/>');
    location.href = '/';
    //
    //self.close();

</script>
<#else>
<script type="text/javascript">

    alert('<@spring.message "MAIL_ERROR"/>');
    location.href = '/';

</script>

</#if>
</body>

</html>