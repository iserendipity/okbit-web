package io.dj.exchange.controller.api;

import io.dj.exchange.annotation.TransactionalEx;
import io.dj.exchange.domain.dto.FeignResult;
import io.dj.exchange.domain.dto.FeignResultData;
import io.dj.exchange.domain.dto.ManualTransactionDTO;
import io.dj.exchange.domain.primary.*;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.exception.OkbitException;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.repository.hibernate.primary.TransactionsRepository;
import io.dj.exchange.repository.hibernate.primary.WalletRepository;
import io.dj.exchange.service.withdraw.WithdrawService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;


@Slf4j
@RestController
@RequestMapping("/api/withdraw")
public class WithdrawApiController {

    @Autowired
    private ApiProvider apiProvider;

    @Autowired
    private TransactionsRepository transactionsRepository;

    @Autowired
    private WithdrawService withdrawService;

    @Autowired
    private WalletRepository walletRepository;

    @RequestMapping(value="/wallet", method=RequestMethod.POST)
    public Response<Wallet> wallet(@Valid @RequestBody Wallet wallet, @ModelAttribute("user") User user) {

        log.info("data : {}", apiProvider.getWallet(wallet.getCoinName()));

        FeignResult result = apiProvider.getWallet(wallet.getCoinName());

        return Response.<Wallet>builder()
                .code(CodeEnum.SUCCESS)
                .data(result.getResult().getWallet())
                .build();

    }

    @RequestMapping(value="/wallets", method=RequestMethod.POST)
    public Response<List<Wallet>> wallet(@PageableDefault(sort = { "id" }, direction = Sort.Direction.DESC, size = 30) Pageable p) throws Exception{

            FeignResult result = apiProvider.getWalletAll();

            FeignResultData data = result.getResult();

            return Response.<List<Wallet>>builder()
                    .code(CodeEnum.SUCCESS)
                    //.data(apiProvider.getWalletAll(headers).getResult().getWallets())
                    .build();

    }

    @RequestMapping(value="/registWallet", method=RequestMethod.POST)
    public Response<Wallet> registWallet(@Valid @RequestBody Wallet wallet) throws OkbitException{

        Wallet result = apiProvider.registWallet(wallet.getCoinName());

        if(result.getCoinName().equals("KRW")){
            result.setAvailableBalance(result.getAvailableBalance().add(new BigDecimal(1000)));
        }

        walletRepository.save(result);

        return Response.<Wallet>builder()
                .code(CodeEnum.SUCCESS)
                .data(result)
                .build();

    }
    @RequestMapping(value="/withdrawKRW", method=RequestMethod.POST)
    public Response<ManualTransactions> withdrawKRW(@ModelAttribute("user") User user, @Valid @RequestBody ManualTransactionDTO.RequestKrw request) throws Exception{

        withdrawService.withdrawKrw(user, request);

        return Response.<ManualTransactions>builder()
                .code(CodeEnum.SUCCESS)
                //.data(apiProvider.registWallet(wallet.getCoinName()))
                .build();

    }

    @RequestMapping(value="/withdrawCoin", method=RequestMethod.POST)
    public Response<Transactions> withdrawCoin(@ModelAttribute("user") User user, @Valid @RequestBody ManualTransactionDTO.RequestCoin request) throws Exception{

        return Response.<Transactions>builder()
                .code(CodeEnum.SUCCESS)
                .data(withdrawService.withdrawCoin(user, request).getResult().getTransaction())
                //.data(apiProvider.registWallet(wallet.getCoinName()))
                .build();

    }

    @RequestMapping(value="/transactions", method=RequestMethod.POST)
    public Response<List<Transactions>> transactions(@Valid @RequestBody Transactions transactions, @ModelAttribute("user") User user, @PageableDefault(sort = { "dt" }, direction = Sort.Direction.DESC, size = 30) Pageable p){

        return Response.<List<Transactions>>builder()
                .code(CodeEnum.SUCCESS)
                //.data(apiProvider.getTransactions(transactions.getCategory(), p.getPageNumber(), p.getPageSize()).getResult().getTransactions())
                .data(transactionsRepository.findAllByCategoryAndUserId(transactions.getCategory(), user.getId(), p).getContent())
                //.data(transRepository.findAllByCategory(transactions.getCategory(), p).getContent())
                .build();

    }

}
