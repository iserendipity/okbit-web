<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
        <#--<style type="text/css">-->


            <#--.countdown-timer-wrapper {-->
                <#--border-radius: 5px;-->
                <#--background-color: rgba(0, 0, 0, 0.6);-->
                <#--margin-bottom: 20px;-->
                <#--max-width: 300px;-->
                <#--margin: 50px auto;-->
            <#--}-->

            <#--.countdown-timer-wrapper h5 {-->
                <#--font-size: 14px;-->
                <#--letter-spacing: 0.5px;-->
                <#--text-align: center;-->
                <#--padding-top: 10px;-->
                <#--text-shadow: none;-->
            <#--}-->

            <#--.countdown-timer-wrapper .timer {-->
                <#--padding: 10px;-->
                <#--text-align: center;-->
                <#--padding-top: 15px;-->
            <#--}-->

            <#--.countdown-timer-wrapper .timer .timer-wrapper {-->
                <#--display: inline-block;-->
                <#--width: 60px;-->
                <#--height: 50px;-->
            <#--}-->

            <#--.countdown-timer-wrapper .timer .timer-wrapper .time {-->
                <#--font-size: 28px;-->
                <#--font-weight: bold;-->
                <#--color: #ffffff;-->
            <#--}-->

            <#--.countdown-timer-wrapper .timer .timer-wrapper .text {-->
                <#--font-size: 12px;-->
                <#--color: rgba(255, 255, 255, 0.6);-->
            <#--}-->

        <#--</style>-->


    </head>
    <body id="skin-blur-blue" style="overflow-x:hidden">
        <#--<div class="container-fluid visible-lg">-->
            <#--<div class="row">-->
                <#--<div class="area-ad area-ad-left">-->
                    <#--광고 모집중-->

                <#--</div>-->
                <#--<div class="area-ad area-ad-right">-->
                    <#--광고 모집중-->

                <#--</div>-->

            <#--</div>-->
        <#--</div>-->
        <section id="login">
            <header class="text-center">
                <div class="row text-center">
                    <img src="/img/logo/logo_index.png" style="width: 350px;">
                </div>
                <br>
                <br>

                <h3><@spring.message "EXCHANGE_PR"/></h3>
            </header>
            <#--<div class="countdown-timer-wrapper">-->
                <#--<div class="timer" id="countdown"></div>-->
            <#--</div>-->
            <div class="clearfix"></div>
            <div class="container-fluid">
                <!-- Login -->
                <form class="box tile animated form-validation-2 active" id="box-login" method="post" action="loginProcess">
                    <#if error?has_content>
                        <#if error != 'SESSION_EXPIRED'>
                            <#if error != 'Invalid_session'>
                            <div class="alert alert-dismissable alert-danger fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <#--<#if error.null??>-->
                                <#if error == 'Invalid_session'>
                                <#--<@spring.message "INVALID_SESSION"/>-->
                                <#elseif error == 'Invalid'>
                                    <@spring.message "INVALID_ACTION"/>
                                <#elseif error == 'Bad credentials'>
                                    <@spring.message "NO_USER"/>
                                <#elseif error == 'Bad_credential'>
                                    <@spring.message "NO_USER"/>
                                <#elseif error == 'SESSION_EXPIRED'>
                                    <#--<@spring.message "INVALID_SESSION"/>-->
                                <#elseif error == 'Already_User'>
                                    <@spring.message "ALREADY_USER"/>
                                <#elseif error == 'Recaptcha_Error'>
                                    <@spring.message "RECAPTCHA_ERROR"/>
                                <#else>
                                ${error}
                                </#if>
                            </div>
                            </#if>
                        </#if>
                    </#if>
                    <#if regist?has_content>
                        <div class="alert alert-dismissable alert-info fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <@spring.message "REGISTER_SUCCESS"/>
                        </div>
                    </#if>
                    <#if find?has_content>
                        <div class="alert alert-dismissable alert-info fade in" style="padding-right:35px;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <@spring.message "FIND_PASSWORD_SUCCESS"/>
                        </div>
                    </#if>
                    <h2 class="m-t-0 m-b-15">
                        Login</h2>
                    <input type="text" name="loginId" class="login-control m-b-10 validate[required,custom[email]]" placeholder="Email Address" style="font-size: 20px;">
                    <input type="password" name="password" class="login-control m-b-10 validate[custom[password]]" placeholder="Password">
                    <input type="text" name="totp-verification-code" class="login-control" placeholder="OTP" style="display:none">
                    <div class="m-t-20">
                        <button type="submit" class="btn btn-lg m-r-5" ><@spring.message "INDEX_LOGIN"/></button>

                        <h5>
                            <a class="box-switcher" data-switch="box-register" href=""><@spring.message "ACCOUNT_REGISTER"/></a> or
                            <a class="box-switcher" data-switch="box-reset" href=""><@spring.message "FORGET_PASSWORD"/></a>
                        </h5>
                    </div>
                </form>

                <!-- Register -->
                <form class="box animated tile form-validation-3" id="box-register" method="post" action="doSignup">
                    <h2 class="m-t-0 m-b-15">Register</h2>
                    <input type="text" name="loginId" class="login-control m-b-10 validate[required,custom[email]]" placeholder="Email Address">
                    <input type="password" id="password" name="password" class="login-control m-b-10 validate[custom[password]]" placeholder="Password">
                    <input type="password" name="re_password" class="login-control m-b-20 validate[equals[password]]" placeholder="Confirm Password">
                    <div class="form-group">
                    <label>
                        <input type="checkbox" name="termsAgree" class="login-control m-b-20 validate[required]">
                            <@spring.message "AGREE_TERMS"/>(<a href="/media/terms.pdf" target="_blank"><@spring.message "VIEW_TERMS"/></a>)
                        </label>
                    </div>
                    <div class="form-group">
                    <label>
                        <input type="checkbox" name="privacyAgree" class="login-control m-b-20 validate[required]">
                        <@spring.message "AGREE_PRIVACY"/>
                        (<a href="/media/privacy.pdf" target="_blank"><@spring.message "VIEW_PRIVACY"/></a>)
                    </label>
                    </div>
                    <br>
                    <div class="g-recaptcha text-center col-xs-12 m-b-20" data-theme="dark" data-sitekey="6Lf8GCIUAAAAALyvTLCKrAvJxpAQQt2oDDFLHbrw"></div>
                    <br>
                    <button id="btn_reg" class="btn btn-lg m-r-5" onclick="goog_report_conversion('https://ok-bit.com');"><@spring.message "INDEX_REGISTER"/></button>

                    <h5><a class="box-switcher" data-switch="box-login" href=""><@spring.message "INDEX_REGISTER_ALREADY"/></a></h5>
                </form>

                <!-- Forgot Password -->
                <form class="box animated tile form-validation-2" id="box-reset" method="post" action="findPassword">
                    <h2 class="m-t-0 m-b-15">Reset Password</h2>
                    <p><@spring.message "FIND_PASS_DESC"/></p>
                    <input type="text" name="loginId" class="login-control m-b-20 validate[required,custom[email]]" placeholder="Email Address">

                    <button type="submit" class="btn btn-lg m-r-5"><@spring.message "INDEX_RESET_PASSWORD"/></button>

                    <h5><a class="box-switcher" data-switch="box-login" href=""><@spring.message "INDEX_REGISTER_ALREADY"/></a></h5>
                </form>
            </div>
        </section>
        <div id="coinIndicator">
            <script id="indicatorTemplate" type="text/x-jquery-tmpl">
                <label class="indicator-block text-center" >
                    <div class="indicator-block-header">

                        <h5 style="text-shadow:none !important; font-weight:bolder; display: none; color:${r"${color}"};">${r"${changePoint}"} <i class="glyphicon glyphicon-arrow-${r"${pointer}"}" ></i>(${r"${change}"}%)
                            <br>
                            <small style="text-shadow:none !important; font-weight:bolder;">vol : ${r"${volume}"}</small>
                        </h5>

                    </div>
                    <div class="indicator-block-body">
                        <h4 style="text-shadow:none !important; font-weight:bold">${r"${coinName}"}</h4>
                        <h5 style="text-shadow:none !important; font-weight:bolder; color:${r"${color}"};" class="indicator-block-value">${r"${price}"}</h5>
                    </div>
                </label>
            </script>
            <div class="container">
                <div>
                    <div id="coinHolder" data-toggle="buttons" class="text-center">

                    </div>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>

        <#--<section class="block-volume">-->
            <#--<div class="row">-->
                <#--<div class="vol-data col-xs-12">-->
                <#--<div>Now</div>-->
                <#--<div class="desc">Preparing</div>-->
            <#--</div>-->
        <#--</section>-->

        <section class="block-volume hidden-sm hidden-xs">
            <div class="row">
                <div id="volumes" class="col-sm-10 col-sm-offset-1">
                    <div class="vol-data col-sm-12 col-md-4">
                        <div name="volume24hTxt">Loading..</div>
                        <div class="desc">24 HOUR VOLUME</div>
                    </div>
                    <div class="vol-data col-sm-12 col-md-4">
                        <div name="volume7dTxt">Loading..</div>
                        <div class="desc">7 DAY VOLUME</div>
                    </div>
                    <div class="vol-data col-sm-12 col-md-4">
                        <div name="volume30dTxt">Loading..</div>
                        <div class="desc">30 DAY VOLUME</div>
                    </div>
                </div>
            </div>

        </section>
        <section>
            <div class="bs-docs-featurette text-center">
                <div id="okbitFeatures" class="container">
                    <div class="col-lg-10 col-lg-offset-1 ">
                        <div class="col-sm-12" style="margin-bottom:40px; margin-top:40px;">
                            <div id="dropcaps" class="block-area">
                                <h2 style="margin-bottom:30px; font-weight: bold;">OK-BIT Features</h2>
                                <p class="col-md-10 col-md-offset-1" style="font-size:20px!important;"><@spring.message "INDEX_FEATURES"/></p>
                                <br/>
                            </div>
                        </div>
                        <div class="col-sm-3 features text-center">
                            <img src="/img/index/feature_security.png" class="img-responsive" style="margin:auto">

                        </div>
                        <div class="col-sm-3 features text-center">
                            <img src="/img/index/feature_lowfee.png" class="img-responsive" style="margin:auto">

                        </div>
                        <div class="col-sm-3 features text-center">
                            <img src="/img/index/feature_easychart.png" class="img-responsive" style="margin:auto">

                        </div>
                        <div class="col-sm-3 features text-center">
                            <img src="/img/index/feature_varietycoin.png" class="img-responsive" style="margin:auto">

                        </div>

                    </div>
                </div>
            </div>

        </section>
    </body>

    <#--<link href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet">-->
    <!--footer start from here-->
    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="col-sm-3 paddingtop-bottom ">
                        <div class="logofooter"><img src = "img/logo/footer_logo.png"></div>
                        <p>(주)오케이비트 | 대표 김주현 <br><br>사업자등록번호 885-88-00694 <br>통신판매업신고 제2017-경기부천-0981호</p>
                        <br><i class="fa fa-home"></i> 경기도 부천시 송내대로 31, 202호(송내동)<br>
                        <i class="fa fa-envelope"></i> E-mail : support@ok-bit.com<br>
                        <i class="fa fa-phone"></i> Phone : 1588-9520, 응답시간 평일 오전 10시 ~ 오후 5시</p>


                    </div>
                    <div class="col-sm-2 paddingtop-bottom">
                        <h6 class="heading7">SUPPORT</h6>
                        <ul class="footer-ul">
                            <li><a href="/media/privacy.pdf" target="_blank"> <@spring.message "INDEX_PRIVACY"/></a></li>
                            <li><a href="/media/terms.pdf" target="_blank">  <@spring.message "INDEX_TERMS"/></a></li>
                            <li><a href="/media/guide.pdf" target="_blank">  <@spring.message "INDEX_GUIDE"/></a></li>
                            <#--<li><a href=""> User Guide</a></li>-->
                            <li><a href="mailto:support@ok-bit.com">  <@spring.message "INDEX_ASK_AD"/></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2 paddingtop-bottom">
                        <h6 class="heading7">LANGUAGE</h6>
                        <ul class="footer-ul">
                            <li><a href="/?lang=en"> English</a></li>
                            <li><a href="/?lang=ko"> 한국어</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2 paddingtop-bottom">
                        <h6 class="heading7">LINKS</h6>
                        <ul class="footer-ul">
                            <li>
                                <blockquote>
                                    <a href="https://coinmarketcap.com/" target="_blank">Coin Market Cap</a>
                                </blockquote>
                            </li>

                        </ul>
                    </div>
                    <div class="col-sm-2 paddingtop-bottom">
                        <h6 class="heading7">NOTICES</h6>
                        <ul style="padding-left:20px;">
                            <#list notices as notice>
                                <li ><a href="#" onclick="javascript:noticeDetail('${notice.id}');">${notice.title}</a>
                                </li>
                            </#list>

                                <#--<blockquote>-->
                                    <#--<a href="https://coinmarketcap.com/" target="_blank">Coin Market Cap</a>-->
                                <#--</blockquote>-->
                            <#--</li>-->

                        </ul>
                    </div>
                    <form id="form_chk" name="form_chk" method="post">
                        <input type="hidden" name="id" >

                    </form>
                </div>
                <#--<div class="col-md-3 col-sm-6 paddingtop-bottom">-->
                    <#--<h6 class="heading7">LATEST POST</h6>-->
                    <#--<div class="post">-->
                        <#--<p>facebook crack the movie advertisment code:what it means for you <span>August 3,2015</span></p>-->
                        <#--<p>facebook crack the movie advertisment code:what it means for you <span>August 3,2015</span></p>-->
                        <#--<p>facebook crack the movie advertisment code:what it means for you <span>August 3,2015</span></p>-->
                    <#--</div>-->
                <#--</div>-->
            </div>
        </div>
    </footer>
    <!--footer start from here-->

    <div class="copyright">
        <div class="container-fluid col-md-10 col-md-offset-1">
            <div class="col-md-6">
                <p>© 2017 - All Rights OK-BIT</p>
            </div>
        </div>
    </div>

    <!-- Javascript Libraries -->
    <!-- jQuery -->
    <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->

    <!-- Bootstrap -->
    <script src="/js/bootstrap.min.js"></script>

    <!--  Form Related -->
    <script src="/js/validation/validate.min.js"></script> <!-- jQuery Form Validation Library -->
    <script src="/js/validation/validationEngine.min.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->
    <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->
    <script src="/js/select.min.js"></script> <!-- Custom Select -->

    <!-- Jqutemplateslate -->
    <script src="/js/jquery.tmpl.min.js"></script>

    <!-- Jquery cookie -->
    <script src="/js/jquery.cookie.js"></script>

    <!-- All JS functions -->
    <script src="/js/functions.js"></script>

    <!-- ticker -->
    <script src="/js/api/common_api.js"></script>
    <script src="/js/index/ticker.js"></script>

    <!-- timer -->
    <script src="/js/countdown-timer.min.js"></script>

    <!-- recaptcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- login -->
    <script src="/js/index/login.js"></script>



    <script type="text/javascript">
        $('document').ready(function(){

           $('#box-login input[name=loginId], #box-login input[name=password]').on('focusout', function(e){

                otpCheck();

           });

//            var myDate = new Date();
//            var dDay = new Date('2017','6','13','12','00','00');
//
//            if(myDate < dDay) {
//
//                $("#countdown").countdown(dDay, function (event) {
//                    $(this).html(
//                            event.strftime(
//                                '<div class="timer-wrapper"><div class="time">%D</div><span class="text">days</span></div><div class="timer-wrapper"><div class="time">%H</div><span class="text">hrs</span></div><div class="timer-wrapper"><div class="time">%M</div><span class="text">mins</span></div><div class="timer-wrapper"><div class="time">%S</div><span class="text">sec</span></div>'
//                            )
//                    );
//                });
//
//                $('#btn_reg').attr('disabled', true);
//
//            }else{
//
//                $("#countdown").parent().attr('style', 'display:none');
//
//            }
        });

    </script>
    <script type="text/javascript">
        /* <![CDATA[ */
        goog_snippet_vars = function() {
            var w = window;
            w.google_conversion_id = 852280674;
            w.google_conversion_label = "v9F5CLW6l3MQ4oqzlgM";
            w.google_remarketing_only = false;
        }
        // DO NOT CHANGE THE CODE BELOW.
        goog_report_conversion = function(url) {
            goog_snippet_vars();
            window.google_conversion_format = "3";
            var opt = new Object();
            opt.onload_callback = function() {
                if (typeof(url) != 'undefined') {
                    //window.location = url;
                }
            }
            var conv_handler = window['google_trackConversion'];
            if (typeof(conv_handler) == 'function') {
                conv_handler(opt);
            }
        }
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion_async.js">
    </script>
    <#include "common/validation_locale.ftl"/>

</html>
