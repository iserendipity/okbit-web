package io.dj.exchange.provider;

import feign.HeaderMap;
import feign.Headers;
import feign.Param;
import feign.RequestLine;
import io.dj.exchange.domain.dto.FeignResult;
import io.dj.exchange.domain.primary.Wallet;
import io.dj.exchange.enums.CoinEnum;
import io.dj.exchange.enums.StatusEnum;
import io.dj.exchange.exception.OkbitException;
import org.springframework.util.MultiValueMap;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 21.
 * Description
 */
public interface ApiProvider {

    @RequestLine("POST /v1/user/regist")
    @Headers("Content-Type: application/json")
    Map<String, Object> regist(@Param("email") String email, @Param("pwd") String pwd) throws OkbitException;


    @RequestLine("POST /v1/wallet/get")
    FeignResult getWallet(@Param("coinName") String coinName);

    @RequestLine("POST /v1/wallet/getAll")
    FeignResult getWalletAll();

    @RequestLine("POST /v1/coin/getAll")
    FeignResult getCoinAll();

    @RequestLine("POST /v1/wallet/createWallet")
    Wallet registWallet(@Param("coinName") String coinName);

    @RequestLine("POST /v1/order/getAll")
    FeignResult ordersGetAll(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize, @Param("coinName") String coinName);

    @RequestLine("POST /v1/wallet/transactions")
    FeignResult getTransactions(@Param("category") String category, @Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

    @RequestLine("POST /v1/order/getAll")
    FeignResult getOrdersAll(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize);

    @RequestLine("POST /v1/order/buy")
    FeignResult buyOrder(@Param("price") BigDecimal price, @Param("amount") BigDecimal amount, @Param("toCoin") String coinName);

    @RequestLine("POST /v1/order/buy")
    FeignResult buyOrder(@Param("price") BigDecimal price, @Param("amount") BigDecimal amount, @Param("toCoin") String coinName, @HeaderMap MultiValueMap<String, String> headers);

    @RequestLine("POST /v1/order/sell")
    FeignResult sellOrder(@Param("price") BigDecimal price, @Param("amount") BigDecimal amount, @Param("fromCoin") String coinName);

    @RequestLine("POST /v1/order/sell")
    FeignResult sellOrder(@Param("price") BigDecimal price, @Param("amount") BigDecimal amount, @Param("fromCoin") String coinName, @HeaderMap MultiValueMap<String, String> headers);

    @RequestLine("POST /v1/order/cancel")
    FeignResult cancelOrder(@Param("orderId") Long orderId);

    @RequestLine("POST /v1/wallet/reqSend")
    FeignResult reqSend(@Param("coinName") String coinName, @Param("address") String address,@Param("tag") String fromTag, @Param("amount") BigDecimal amount);

    @RequestLine("POST /v1/wallet/reqSend")
    FeignResult reqSend(@Param("coinName") String coinName, @Param("address") String address,@Param("tag") String fromTag, @Param("amount") BigDecimal amount, @HeaderMap MultiValueMap<String, String> headers);

    @RequestLine("POST /v1/wallet/reqSend")
    FeignResult reqSendKRW(@Param("coinName") String coinName, @Param("address") String address, @Param("amount") BigDecimal amount, @Param("userNm") String userNm, @Param("bankNm") String bankNm, @Param("bankCode") String bankCd, @Param("account") String account);

    @RequestLine("POST /v1/wallet/reqSend")
    FeignResult reqSend(@Param("coinName") String coinName, @Param("address") String address,@Param("tag") String fromTag, @Param("amount") BigDecimal amount, @Param("user_name") String userNm, @Param("bank_name") String bankNm, @Param("bank_code") String bankCd, @HeaderMap MultiValueMap<String, String> headers);

    @RequestLine("POST /v1/fds/unlock")
    void unlockFds(@HeaderMap MultiValueMap<String, String> headers);

    @RequestLine("POST /v1/wallet/reqReceive")
    FeignResult reqReceive(@Param("coinName") String coinName, @Param("amount") BigDecimal amount, @Param("userNm") String userNm, @Param("bankNm") String bankNm, @Param("bankCode") String bankCode, @Param("depositDt") String depositDt);

    @RequestLine("POST /v1/wallet/confirmKrwReceive")
    void confirmManualTransactions(@HeaderMap MultiValueMap<String, String> headerMap, @Param("id") String id, @Param("coinName") CoinEnum coinName, @Param("status") StatusEnum status);

    @RequestLine("POST /v1/wallet/confirmKrwSend")
    void confirmManualTransactionsSend(@HeaderMap MultiValueMap<String, String> headerMap, @Param("id") String id, @Param("coinName") String coinName, @Param("status") StatusEnum status);

    @RequestLine("POST /v1/wallet/reqReceive")
    void reqReceive(@HeaderMap MultiValueMap<String, String> headers, @Param("coinName") String coinName, @Param("amount") BigDecimal amount, @Param("userNm") String userNm, @Param("bankNm") String bankNm, @Param("bankCode") String bankCode, @Param("depositDt") String depositDt, @Param("account") String account, @Param("address") String address);

    @RequestLine("POST /v1/wallet/sendCancel")
    void cancelSendKrw(@HeaderMap MultiValueMap<String, String> headers, @Param("id") String id, @Param("coinName") String coinName);
}
