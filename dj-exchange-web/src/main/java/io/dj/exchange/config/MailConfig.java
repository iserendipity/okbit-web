//package io.dj.exchange.config;
//
//import lombok.Data;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.JavaMailSenderImpl;
//
//import java.util.Properties;
//import java.util.concurrent.ArrayBlockingQueue;
//import java.util.concurrent.BlockingQueue;
//
///**
// * Project dj-exchange-okbit.
// * Created by jeongwoo on 2017. 4. 24.
// * Description
// */
//@Data
//@Configuration
//public class MailConfig {
//
//    public static final String MAIL_QUEUE = "MAIL_QUEUE";
//    private static final String MAIL_DEBUG = "mail.debug";
//    private static final String MAIL_SMTP_STARTTLS_REQUIRED = "mail.smtp.starttls.required";
//    private static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
//    private static final String MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
//
//    @Bean
//    public JavaMailSender mailSender() {
//        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//        mailSender.setHost("smtp.gmail.com");
//        mailSender.setProtocol("smtp");
//        mailSender.setPort(587);
//        mailSender.setUsername("support@ok-bit.com");
//        mailSender.setPassword("nn4647mm11");
//        //mailSender.setUsername("kjwme1116@gmail.com");
//        //mailSender.setPassword("wjddn12!");
//        mailSender.setDefaultEncoding("UTF-8");
//        Properties properties = mailSender.getJavaMailProperties();
//        //properties.put(MAIL_SMTP_STARTTLS_REQUIRED, getSmtp().isStartTlsRequired());
//        properties.put(MAIL_SMTP_STARTTLS_ENABLE, true);
//        //properties.put(MAIL_SMTP_AUTH, getSmtp().isAuth());
//        //properties.put(MAIL_DEBUG, true);
//        mailSender.setJavaMailProperties(properties);
//        return mailSender;
//    }
//
//    @Bean
//    public BlockingQueue<String> confirmQue(){
//        return new ArrayBlockingQueue<String>(20);
//    }
//
//}
