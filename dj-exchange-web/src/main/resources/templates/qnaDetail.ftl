<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
    </head>
<#if (user.userSetting.theme)??>
<body id="skin-blur-${user.userSetting.theme}">
<#else>
<body id="skin-blur-blue">
</#if>

        <header id="header" class="media">
            <#include "common/top.ftl">
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            
            <!-- Sidebar -->
            <aside id="sidebar">

                <!-- Side Menu -->
                <ul class="list-unstyled side-menu">
                    <#include "common/sidemenu.ftl">
                </ul>

            </aside>
        
            <!-- Content -->
            <section id="content" class="container">

                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li style="font-size:14px;"><a href="/media/guide.pdf" target="_blank">GUIDE BOOK</a></li>
                </ol>
                
                <h4 class="page-title">SUPPORT</h4>

                <!-- Shortcuts -->

                <#include "common/shortcut.ftl">

                <hr class="whiter" />

                <!-- Main Widgets -->
                <div class="block-area">
                    <div class="row" >
                        <#include "rightInfo/questionForm.ftl">
                        <div class="col-md-10">
                            <div class="tile">
                                <div class="tile-title">
                                    <@spring.message "MY_QUESTION"/>
                                </div>
                                <div class="listview list-click">
                                    <#--<h2 class="page-title">${qna.title}</h2>-->
                                    <div class="media message-header o-visible">
                                        <div class="pull-right dropdown m-t-10">
                                            <a data-toggle="modal" href="#compose-message" title="" data-original-title="REPLY">
                                            <@spring.message "TITLE_REPLY"/></a>
                                            <#--<a href="" data-toggle="dropdown" class="p-5"><@spring.message "TITLE_REPLY"/></a>-->
                                            <#--<ul class="dropdown-menu text-right">-->
                                                <#--<li><a data-toggle="modal" href="#compose-message" title="" data-original-title="REPLY">-->
                                                    <#--REPLY</a></li>-->
                                                <#--<li><a href="">Forward</a></li>-->
                                                <#--<li><a href="">Mark as Spam</a></li>-->
                                            </ul>
                                        </div>
                                        <div class="media-body">
                                            <span class="f-bold pull-left m-b-5"><@spring.message "SUPPORT_QNA_TITLE"/>${qna.title}</span>
                                            <div class="clearfix"></div>
                                            <span class="dropdown m-t-5">
                                                ${qna.regDtTxt}
                                            <#--To <a href="" class="underline">Me</a> on 12th February 2014-->
                                            </span>
                                        </div>
                                    </div>

                                    <div class="p-15">
                                        ${qna.contents}
                                        <#--<p>Hi Wendy,</p>-->
                                        <#--<p>Morbi dignissim tortor urna, eget feugiat ipsum rutrum et. Aenean sem nisi, ultricies vel lorem nec, blandit venenatis tellus. Donec accumsan, turpis vel euismod ultrices, massa quam ultricies turpis, non pretium dolor nulla sit amet sem. Ut vel blandit sem, vitae tristique tellus. Suspendisse odio ligula, feugiat eu velit in, mattis pharetra libero. Nunc ac tincidunt nibh. Nullam mauris urna, elementum ac odio et, condimentum suscipit orci. Nulla pellentesque a magna et pellentesque. Suspendisse euismod convallis nisi sed placerat.</p>-->
                                        <#--<p>Morbi imperdiet tempor velit, sed sagittis ipsum. In gravida, enim ac commodo congue, purus augue faucibus tellus, vel tempus sapien enim eget urna. Mauris molestie fringilla orci, et scelerisque sem dapibus convallis. In ut erat quam. Donec facilisis ipsum ac nulla lobortis.</p>-->
                                        <#--<p>Regards,<br/> Malinda Hollaway</p>-->
                                    </div>

                                    <#if reply??>
                                        <#list reply as r>
                                            <hr class="whiter">
                                            <#if r.userId?? && r.userId == user.userSetting.userId>
                                                <div class="p-15">
                                                <#--<img src="img/profile-pics/6.jpg" alt="" class="pull-left" width="40">-->
                                                    <i class="fa fa-mail-forward fa-2x pull-left" style="width:40; height:40"></i>
                                                    <span class="f-bold pull-left m-b-5">${r.title}</span>
                                                    <div class="clearfix"></div>
                                                    <span class="dropdown m-t-5" style="margin-left:30px">
                                                    ${r.regDtTxt}
                                                </span>
                                                </div>
                                                <div class="p-15" style="margin-left:30px;">
                                                ${r.contents}
                                                </div>
                                            <#else>
                                                <div class="p-15">
                                                <#--<img src="img/profile-pics/6.jpg" alt="" class="pull-left" width="40">-->
                                                    <i class="fa fa-mail-forward fa-2x pull-left" style="width:40; height:40"></i>
                                                    <span class="f-bold pull-left m-b-5">${r.title} - From Support Team</span>
                                                    <div class="clearfix"></div>
                                                    <span class="dropdown m-t-5" style="margin-left:30px">
                                                    ${r.regDtTxt}
                                                </span>
                                                </div>
                                                <div class="p-15" style="margin-left:30px;">
                                                ${r.contents}
                                                </div>
                                            </#if>

                                        </#list>
                                    </#if>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <!-- COIN Info -->
                            <div id="coinInfoWrap" class="col-md-12">
                            <#include "rightInfo/infoCoin.ftl">
                            </div>

                            <!-- Order Form -->
                            <div id="orderFormWrap" class="col-md-12">
                                <div class="tile">
                                <#include "rightInfo/infoOrderForm.ftl">
                                </div>
                            </div>

                            <!-- Balance -->
                            <div id="balanceWrap" class="col-md-12">
                            <#include "rightInfo/infoBalance.ftl">
                            </div>

                            <!-- My Orders -->
                            <div id="myOrderWrap" class="col-md-12">
                                <div class="tile">
                                    <h2 class="tile-title"><@spring.message "TITLE_MYORDERS"/></h2>
                                    <div class="tile-config">
                                        <a data-toggle="collapse" href="#myOrder" class="tile-menu"></a>
                                    </div>
                                    <div id="myOrder" class="overflow collapse in">
                                    <#include "rightInfo/infoMyOrder.ftl">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </section>

            <!-- Older IE Message -->
            <!--[if lt IE 9]>
                <div class="ie-block">
                    <h1 class="Ops">Ooops!</h1>
                    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser in order to access the maximum functionality of this website. </p>
                    <ul class="browsers">
                        <li>
                            <a href="https://www.google.com/intl/en/chrome/browser/">
                                <img src="/img/browsers/chrome.png" alt="">
                                <div>Google Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.mozilla.org/en-US/firefox/new/">
                                <img src="/img/browsers/firefox.png" alt="">
                                <div>Mozilla Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com/computer/windows">
                                <img src="/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://safari.en.softonic.com/">
                                <img src="/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-10/worldwide-languages">
                                <img src="/img/browsers/ie.png" alt="">
                                <div>Internet Explorer(New)</div>
                            </a>
                        </li>
                    </ul>
                    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
                </div>   
            <![endif]-->
        </section>

        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->
        <script src="/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
        <script src="/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Charts -->
        <script src="/js/charts/jquery.flot.js"></script> <!-- Flot Main -->
        <script src="/js/charts/jquery.flot.time.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen -->

        <script src="/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts -->
        <script src="/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts -->
        <script src="/js/charts.js"></script> <!-- All the above chart related functions -->

        <!-- Map -->
        <script src="/js/maps/jvectormap.min.js"></script> <!-- jVectorMap main library -->
        <script src="/js/maps/usa.js"></script> <!-- USA Map for jVectorMap -->

        <!--  Form Related -->
        <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->
        <#--<script src="/js/select.min.js"></script>-->
        <!-- cookie -->
        <script src="/js/jquery.cookie.js"></script>
        <!-- UX -->
        <script src="/js/scroll.min.js"></script> <!-- Custom Scrollbar -->

        <!-- Other -->
        <script src="/js/calendar.min.js"></script> <!-- Calendar -->
        <script src="/js/feeds.min.js"></script> <!-- News Feeds -->

        <script src="js/editor.min.js"></script> <!-- WYSIWYG Editor -->
        <#--<script src="js/markdown.min.js"></script> <!-- Markdown Editor &ndash;&gt;-->

        <!--  Form Related -->
        <script src="js/validation/validate.min.js"></script> <!-- jQuery Form Validation Library -->
        <script src="js/validation/validationEngine.min.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->

        <!-- All JS functions -->
        <script src="/js/functions.js"></script>

        <!-- trade functions -->
        <script src="/js/api/trade_api.js"></script>
        <script src="/js/jquery.loading.min.js"></script> <!-- loading -->
        <!-- Jqutemplateslate -->
        <script src="/js/jquery.tmpl.min.js"></script>

        <!-- common functions -->
        <script src="/js/api/common_api.js"></script>
        <script src="/js/common/common.js"></script>

        <script src="/js/api/support_api.js"></script>
        <script src="/js/support/support.js"></script>

        <!-- custom utils -->
        <script src="/js/api/util.js"></script>

        <script type="text/javascript">

            $('document').ready(function(){
               commonInit();
               init();

               $('form').on('submit',function(){
                  return confirm(submitConfirm);
               });

                $('.message-editor').summernote({
                    toolbar: false,
                    height: 200,
                    resizable: false
                });


            });



        </script>
        <#include "common/validation_locale.ftl"/>
    </body>
</html>
