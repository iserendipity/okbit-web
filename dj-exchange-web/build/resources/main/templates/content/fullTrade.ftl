<div id="fullMarketTradeModal">
    <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->
    <div id="closebt-container" class="close-fullMarketTradeModal m-b-20">
        <img class="closebt" src="/img/closebt.svg">
    </div>

    <div class="modal-content container" style="width:calc(100% - 52px); min-height:100vh;">
        <div class="fixed-thead-wrap col-xs-12 text-center">
            <div class="fixed-thead col-xs-3"><@spring.message "TITLE_DATE"/></div>
            <div class="fixed-thead col-xs-3"><@spring.message "TITLE_TYPE"/></div>
            <div class="fixed-thead col-xs-3"><@spring.message "TITLE_PRICE"/></div>
            <div class="fixed-thead col-xs-3"><@spring.message "TITLE_AMOUNT"/></div>
        </div>
        <div style="width:100%;" class="pull-left overflow">
            <table class="table table-condensed table-hover table-fixed">
                <thead>
                <tr>
                    <th class="col-xs-3">
                    </th>
                    <th class="col-xs-3">
                    </th>
                    <th class="col-xs-3">
                    </th>
                    <th class="col-xs-3">
                    </th>
                </tr>

                </thead>
                <tbody id="fullTradeBody">
                <tr class="text-center">
                    <td colspan="4">
                    <@spring.message "EMPTY_DATA"/>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>