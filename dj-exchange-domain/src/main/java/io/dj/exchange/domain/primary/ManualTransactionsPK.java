package io.dj.exchange.domain.primary;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;

@Data
public class ManualTransactionsPK implements Serializable {
    @Id
    String id;

    @Id
    Long userId;

    @Id
    String coinName;
}
