<script id="messageTemplate" type="text/x-jquery-tmpl">
     <div class="media">
        <div class="pull-left">
            <img width="40" src="/img/profile-pics/1.jpg" alt="">
        </div>
        <div class="media-body">
            <small class="text-muted">${r"${user.email}"} - ${r"${regDtTxt}"} / ${r"${category}"}</small><br>
            <a class="t-overflow" href="javascript:moveDetail('${r"${id}"}');">${r"${title}"}</a>
        </div>
    </div>
</script>
<!-- Messages Drawer -->
<div id="messages" class="tile drawer animated">
    <div class="listview narrow">
        <div class="media">
            <a href="">Recent Support Request</a>
            <span class="drawer-close">&times;</span>

        </div>
        <div id="messageBody" class="overflow">

        </div>
        <div class="media text-center whiter l-100">
            <ul id="questionPaging" class="pagination pagination-sm">


            </ul>
        </div>
    </div>
</div>