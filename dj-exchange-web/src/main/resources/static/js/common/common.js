(function($) {

    $.ajaxSetup({
        cache : false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("AJAX", true);
        },
        error: function(xhr, status, err) {
            if (xhr.status == 401) {
                //alert("401");
            } else if (xhr.status == 403) {
                //alert("403");
                location.href='/?error=SESSION_EXPIRED'
            } else {
                // console.log(xhr);
                // console.log(status);
                // console.log(err);
                //alert("예외가 발생했습니다. 관리자에게 문의하세요.");
            }
        }
    });

    // $(document).ajaxStart(function(){
    //     $('body').loading({
    //         message : 'LOADING...',
    //         theme : 'dark'
    //         //overlay: $("#bank_loading_div")
    //     });
    // }).ajaxStop(function(){
    //     $('body').loading('stop');
    // });

})(jQuery);

/**
 * Created by jeongwoo on 2017. 4. 28..
 */
var commonApi = new CommonApi();
var tradeApi = new TradeApi();

makeShortcuts = function(){

    commonApi.coins(function(data){
        if(data.data && data.data.length > 0) {
            delete data.data[0];
            makeTemplate('shortcut', 'shortcutTemplate', data.data);
        }
    });

};

makeCoinStatus = function(coinName){

    var param = new Object();
    if(coinName) {
        param.name = coinName;
    }else{
        param.name = nowCoin;
    }
    param.page = '0';
    param.size = '1';

    commonApi.coinInfo(function(data){
        if(data.data) {
            makeTemplate('coinInfo', 'coinInfoTemplate', data.data);
        }
    }, param);


};
paramValidation = function(obj){

    var flag = true;

    $.each(obj, function(key, value) {

        if (key != 'fromTag') {

            if (value == '' || value == undefined || value.length <= 0) {
                // alert('코인 구매량이나 가격이 입력되지 않았습니다.');
                flag = false;
                return false;

            }
        }
        if(obj.toCoin){//buy

            switch (obj.toCoin) {

                case 'BITCOIN': //최소 0.0001개 amount, price = 1000단위

                    if(key == 'amount'){

                        if(value < 0.0001){
                            flag = false;
                            desc = obj.toCoin+'의 최소 구매단위는 0.0001 입니다.';
                            return false;

                        }else if(value.substring(value.indexOf('.')+1).length > 4){
                            flag = false;
                            desc = obj.toCoin+'의 최소 구매단위는 0.0001 입니다.';
                            return false;
                        }

                    } else if(key == 'price' && value % 1000 != 0){
                        flag = false;
                        desc = obj.toCoin+'의 구매가격 단위는 1000KRW 입니다';
                        return false;
                    }

                    break;

                default :
                    if(key =='amount') {
                        if (value < 0.01) {

                            flag = false;
                            desc = obj.toCoin + '의 최소 구매단위는 0.01 입니다';
                            return false;

                        } else if (value.indexOf('.') != -1 && value.substring(value.indexOf('.') + 1).length > 2) {
                            flag = false;
                            desc = obj.toCoin + '의 최소 구매단위는 0.01 입니다.';
                            return false;
                        }
                    } else if(key == 'price' && value % 50 != 0){
                            flag = false;
                            desc = obj.toCoin+'의 구매가격 단위는 50KRW 입니다';
                            return false;
                    }

                    break;

            }

        } else if(obj.fromCoin){//sell

            switch (obj.fromCoin) {

                case 'BITCOIN': //최소 0.0001개 amount, price = 1000단위

                    if(key == 'amount'){

                        if(value < 0.0001){

                            flag = false;
                            desc = obj.fromCoin+'의 최소 판매단위는 0.0001 입니다';
                            return false;

                        }else if(value.substring(value.indexOf('.')+1).length > 4){
                            flag = false;
                            desc = obj.fromCoin+'의 최소 판매단위는 0.0001입니다.';
                            return false;
                        }

                    } else if(key == 'price' && value % 1000 != 0){
                        flag = false;
                        desc = obj.fromCoin+'의 판매가격 단위는 1000KRW 입니다';
                        return false;
                    }

                    break;

                default :

                    if(key == 'amount'){

                        if(value < 0.01){

                            flag = false;
                            desc = obj.fromCoin+'의 최소 판매량은 0.0001 입니다';
                            return false;

                        }else if(value.indexOf('.') != -1 && value.substring(value.indexOf('.')+1).length > 2){
                            flag = false;
                            desc = obj.fromCoin+'의 최소 판매단위는 0.01 입니다.';
                            return false;
                        }

                    } else if(key == 'price' && value % 50 != 0){
                        flag = false;
                        desc = obj.fromCoin+'의 판매가격 단위는 50KRW 입니다';
                        return false;
                    }

                    break;

            }

        }

    });
    return flag;
};
buy = function(){

    var param = new Object();

    param.toCoin = $('#coinName').val();
    param.amount = $('#amount').val().replace(/\,/g,'').trim();
    param.price = $('#price').val().replace(/\,/g,'').trim();
    param.orderType = 'BUY';

    // if(param.toCoin == 'BITCOIN'){
    //     //alert(btc_not_now);
    //     return;
    // }

    if(!paramValidation(param)){
         $('#resultIndicatorSpan').text(desc);
         $('#resultIndicator').show();
        setTimeout(function(){
            $('#resultIndicator').hide();
        }, 2000);
         return false;
    }

    tradeApi.buyOrder(function(data){
        //makeTemplatePre('myOrders', 'myOrderTemplate', data.data);
        if(data.code == 'FAIL'){

            $('#resultIndicatorSpan').text(data.desc);
            $('#resultIndicator').show();
            setTimeout(function(){
                $('#resultIndicator').hide();
            }, 2000);


        }

        makeBalance();
    }, param);

};

sell = function(){

    var param = new Object();

    param.fromCoin = $('#coinName').val();
    param.amount = $('#amount').val().replace(/\,/g,'');
    param.price = $('#price').val().replace(/\,/g,'');
    param.orderType = 'SELL';

    // if(param.fromCoin == 'BITCOIN'){
    //     alert(btc_not_now);
    //     return;
    // }

    if(!paramValidation(param)){
        //alert(desc);
        $('#resultIndicatorSpan').text(desc);
        $('#resultIndicator').show();
        setTimeout(function(){
            $('#resultIndicator').hide();
        }, 2000);
        return false;
    }

    tradeApi.sellOrder(function(data){
        // alert('Sell Order Request Done.');
        //makeTemplatePre('myOrders', 'myOrderTemplate', data.data);
        if(data.code == 'FAIL'){

            $('#resultIndicatorSpan').text(data.desc);
            $('#resultIndicator').show();
            setTimeout(function(){
                $('#resultIndicator').hide();
            }, 2000);

        }

        makeBalance();
    }, param);

};

cancel = function(orderId, obj){
    var param = new Object();

    param.orderId = orderId;

    tradeApi.cancelOrder(function(data){
        // alert('Order cancel request done.');
        $(obj).closest('tr').remove();

    }, param);
}


var minUnitAmount = {'BITCOIN' : 0.0001, 'ETHEREUM' : 0.01, 'LITECOIN': 0.01, 'DASH' : 0.01, 'MONERO' : 0.01 };
var minUnitPrice = {'BITCOIN' : 1000, 'ETHEREUM' : 50,'LITECOIN': 50, 'DASH' : 50, 'MONERO': 50  };

makeOrderForm = function(coinName){

    var param = new Object();
    param.name = coinName;

    commonApi.coin(function(data){
        //console.log(data.data);
        if(data.data) {
            switch (coinName){
                case 'BITCOIN' :    data.data.unitPrice = minUnitPrice.BITCOIN;
                                    data.data.unitAmount = minUnitAmount.BITCOIN;
                break;
                default :           data.data.unitPrice = 50;
                                    data.data.unitAmount = 0.01;

                break;



            }

            setValues('orderForm', data.data);

            //makeTemplate('myOrders', 'myOrderTemplate', data.data);
        }

    }, param);

};

hoverBuy = function(){
    var param = new Object();
    param.name = $('#coinName').val();
    commonApi.coin(function(data){

        if(data.data){

            var fee = data.data.feePercent;

            //amount * fee - 수수료
            var totalFee = $('#amount').val() ? $('#amount').val() * fee * 1 / 100 : 0 ;

            //(amount)*(1-fee) - 실구매량
            var realAmount = $('#amount').val() ? $('#amount').val() - totalFee : 0 ;

            //price * amount - 총구매코인
            var totalPrice = $('#amount').val() && $('#price').val() ? $('#amount').val() * $('#price').val() : 0;

            var obj = new Object();
            obj.totalFee = totalFee;
            obj.realAmount = realAmount;//
            obj.totalPrice = totalPrice;//O

            console.log(obj);

            setValues('buyResult', obj);

        }

    }, param);

};


hoverSell = function(){
    var param = new Object();
    param.name = 'KRW';
    commonApi.coin(function(data){

        if(data.data){

            var fee = data.data.feePercent;

            //amount * fee - 수수료
            var totalFee = $('#amount').val() ? $('#amount').val() * fee * 1 / 100 : 0 ;

            //(amount)*(1-fee) - 실구매량
            //var realAmount = $('#amount').val() ? $('#amount').val() - totalFee : 0 ;

            //price * amount - 총구매금액
            var totalPrice = $('#amount').val() && $('#price').val() ? $('#amount').val() * $('#price').val() : 0;

            var realAmount = totalPrice && totalFee ? totalPrice - totalFee : 0;

            var obj = new Object();
            obj.totalFee = totalFee;
            obj.realAmount = realAmount;
            obj.totalPrice = totalPrice;

            console.log(obj);

            setValues('sellResult', obj);

        }

    }, param);

};

makeBalance = function(){
    commonApi.balanceAll(function(data){

        if(data.code =='FAIL'){

            alert(data.desc);
            location.href = '/signOut';

        }

        if(data.data && data.data.length > 0) {
            makeTemplate('balanceBody', 'balanceTemplate', data.data);
            //console.log('wallet');
            //console.log(data.data);
            wallet = data.data[0];
            // var obj = {};
            //
            // var val = 0.00;

            // $(data.data).each(function(i){
            //     val = eval(i + data.data[i].totalBalance);
            // });

            //console.log(val);

            //makeTemplateApp('balanceBody', 'totalBalanceTemplate', data.data);
        } else {

            $('#balanceBody').html('<tr><td>'+not_exist_wallet_msg+'</td></tr>');


        }
    });

};
getMyOrders = function(data){

    if(data && data.length > 0) {
        makeTemplate('myOrders', 'myOrderTemplate', data);
    }else{
        $('#myOrders').html('<tr class="text-center"><td colspan="5">'+no_data_msg+'</td></tr>');
    }
};
makeMyOrders = function(coinName){
    //console.log(nowCoin);
    var param = new Object();
    if(coinName) {
        param.name = coinName;
    }else{
        param.name = nowCoin;
    }

    param.page = '0';
    param.size = '25';

    commonApi.loadMyOrders(function(data){

        getMyOrders(data.data);

        //console.log(data.data);
        // if(data.data && data.data.length > 0) {
        //     makeTemplate('myOrders', 'myOrderTemplate', data.data);
        // }else{
        //     $('#myOrders').html('<tr><td colspan="5">데이터가 없습니다.</td></tr>');
        // }

    }, param);

};

makeMyOrdersFull = function(coinName){

    var param = new Object();
    if(coinName) {
        param.name = coinName;
    }else{
        param.name = nowCoin;
    }

    commonApi.loadMyOrdersFull(function(data){

        if(data.data && data.data.length > 0){
            makeTemplate('myOrdersFull', 'myOrderFullTemplate', data.data);
        }else{
            $('#myOrdersFull').html('<tr class="text-center"><td colspan="4">'+no_data_msg+'</td></tr>');
        }

    }, param);

};

changeTheme = function(code){
    var param = new Object();
        param.theme = code;

    commonApi.changeTheme(function(){
       $('body').attr('id','skin-blur-'+code);
       window.location.reload(true);
       //alert('Change Theme process success. It affects next login.');
    }, param);

};

previewTheme = function(code){
    nowTheme = $('body').attr('id');
    $('body').attr('id','skin-blur-'+code);

};

donePreview = function(){
    $('body').attr('id', nowTheme);
};

changeCoin = function(coinName){
    nowCoin = coinName;
    makeOrderForm(coinName);
    makeMyOrders(coinName);
    makeCoinStatus(coinName);
    if(now) {
        switch (now) {
            case 'console' :

                loadMyTrades(coinName);
                loadOrders(coinName);
                loadMarketTrades(coinName);
                makeMyOrders(coinName);
                chageChartSymbol(coinName, '15');
                break;

        }
    }
    $('.overflow').getNiceScroll().each(function(){
       $(this).hide();
    });

    $.cookie('nowCoin', coinName);

};

commonInit = function(){

    var cookieCoin = $.cookie('nowCoin');

    nowCoin = cookieCoin ? cookieCoin : 'ETHEREUM';

    makeShortcuts();
    makeBalance();
    makeOrderForm(nowCoin);
    makeMyOrders(nowCoin);
    makeCoinStatus(nowCoin);
    //loadOrders(nowCoin);
    getOtpSetting();
    //commonTimer = commonUpdate();

    $('#price').on('keyup', function(event){
        //var rxSplit = new RegExp('([0-9])([0-9][0-9][0-9][,.])');
        $(this).val(function(index, value) {
            return value
                .replace(/\D/g, "")
                //.replace(/([0-9])([0-9]{2})$/, '$1.$2')
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });

    });



};


getOtpSetting = function(){
    commonApi.getOtpSetting(function (data) {
        otpSetting = data.data;
    });
};

commonUpdate = function(){

    return setInterval(function(){

        if($('#coinName').val()){
            coinName = $('#coinName').val();
        }

        makeBalance();
        makeCoinStatus(coinName);
    }, 3000);

};

noticeDetail = function(id){

    var popOption = "width=370, height=460, resizable=no, scrollbars=no, status=no;";

    window.open("",'noticePop',popOption);

    document.form_chk.id.value = id;
    document.form_chk.action = "/noticeDetail";
    document.form_chk.target = "noticePop";
    document.form_chk.submit();

};
