
function CommonApi () {
    this.call = function(uri, callback, resultProcFunc, params) {
        $.ajax({
            url: "/api/common" + uri
            , type: "POST"
            , dataType: 'json'
            , contentType:"application/json; charset=UTF-8"
            , data: JSON.stringify(params)
            , success: function (data) {
                resultProcFunc(data);
                return callback(data);
            }
            , error:function(e){
                //showAlert("ERROR : " + JSON.parse(e.responseText).message);
            }
        });
        return true;
    };

    this.callGet = function(uri, callback, resultProcFunc, params) {
        $.ajax({
            url: "/api/common" + uri
            , type: "GET"
            , dataType: 'json'
            , contentType:"application/json; charset=UTF-8"
            , data: JSON.stringify(params)
            , success: function (data) {
                resultProcFunc(data);
                return callback(data);
            }
            , error:function(e){
                //showAlert("ERROR : " + JSON.parse(e.responseText).message);
            }
        });
        return true;
    };

    this.resetPassword = function(callback, params){
        return this.call("/passwordReset", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.tickers = function(callback, params) {
        return this.callGet("/coins", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.volumes = function(callback, params) {
        return this.callGet("/coins/getAllVolume", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.coins = function(callback, params) {
        return this.callGet("/coins/getAll", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.coin = function(callback, params) {
        return this.call("/coins/get", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.coinInfo = function(callback, params) {

        return this.call("/coinInfo", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.loadMyOrders = function(callback, params){
        return this.call("/myOrders", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.loadMyOrdersFull = function(callback, params){
        return this.call("/myOrdersFull", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.balanceAll = function(callback, params){
        return this.call("/balance", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.changeTheme = function(callback, params){
        return this.call("/changeTheme", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.getOtpSetting = function(callback, params) {
        return this.call("/getOtpSetting", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

}