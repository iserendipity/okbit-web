package io.dj.exchange.repository.hibernate.readonly;

import io.dj.exchange.domain.dto.SumHistory;
import io.dj.exchange.domain.dto.SumMarketHistoryOrders;
import io.dj.exchange.domain.dto.TodayUsers;
import io.dj.exchange.domain.dto.Volume;
import io.dj.exchange.domain.readonly.MarketHistoryOrders;
import io.dj.exchange.domain.readonly.MarketHistoryOrdersPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 12.
 * Description
 */
public interface MarketOrdersRepository extends JpaRepository<MarketHistoryOrders, MarketHistoryOrdersPK> {

    Page<MarketHistoryOrders> findAllByOrderByIdDesc(Pageable p);

    //Page<MarketHistoryOrders> findAllByOrderByIdDesc(Pageable p);

    Page<MarketHistoryOrders> findAllByFromCoinNameOrToCoinNameAndCompletedDtGreaterThanEqualOrderByIdAsc(String fromCoinName, String toCoinName, LocalDateTime dateTime, Pageable p);

    Page<MarketHistoryOrders> findOneByOrderByIdDesc(Pageable p);

    Page<MarketHistoryOrders> findAllByFromCoinNameOrToCoinNameOrderByIdDesc(String fromCoinName, String toCoinName, Pageable p);

    List<MarketHistoryOrders> findAllByFromCoinNameOrToCoinNameOrderByIdDesc(String fromCoinName, String toCoinName);

    Page<MarketHistoryOrders> findAllByFromCoinNameOrToCoinNameAndCompletedDtGreaterThanEqualOrderByPriceDesc(String fromCoinName, String toCoinName, LocalDateTime dateTime, Pageable p);

    Page<MarketHistoryOrders> findAllByFromCoinNameOrToCoinNameAndCompletedDtGreaterThanEqualOrderByPriceAsc(String fromCoinName, String toCoinName, LocalDateTime dateTime, Pageable p);

    void findSumAmountByCompletedDtGreaterThan(LocalDateTime dateTime);

    @Query("SELECT new io.dj.exchange.domain.dto.SumMarketHistoryOrders(sum(p.amount*p.price)) FROM MarketHistoryOrders p WHERE (p.fromCoin.name = :coinName or p.toCoin.name = :coinName) AND p.completedDt >= :completedDt")
    SumMarketHistoryOrders getSumMarketHistoryOrders(@Param("coinName") String name, @Param("completedDt") LocalDateTime completedDt);

    Page<MarketHistoryOrders> findAllByFromCoinNameOrToCoinNameAndCompletedDtLessThanEqualOrderByIdAsc(String name, String name1, LocalDateTime dateTime, Pageable p);

    @Query("select p from MarketHistoryOrders p where (p.fromCoin.name = :name or p.toCoin.name =:name1) and p.completedDt <= :dateTime order by p.id desc")
    Page<MarketHistoryOrders> findAllByFromCoinNameOrToCoinNameAndCompletedDtLessThanEqualOrderByIdDesc(@Param("name") String name, @Param("name1") String name1, @Param("dateTime") LocalDateTime dateTime, Pageable p);

    Page<MarketHistoryOrders> findAllByFromCoinNameIsOrToCoinNameIsAndCompletedDtLessThanEqualOrderByIdDesc(String name, String name1, LocalDateTime dateTime, Pageable p);

    Page<MarketHistoryOrders> findAllByFromCoinNameOrToCoinNameAndCompletedDtLessThanEqualOrderByPriceDescIdDesc(String name, String name1, LocalDateTime dateTime, Pageable p);

    Page<MarketHistoryOrders> findAllByFromCoinNameOrToCoinNameAndCompletedDtLessThanEqualOrderByPriceAscIdDesc(String name, String name1, LocalDateTime dateTime, Pageable p);

    //@Query("SELECT new io.dj.exchange.domain.chart.History((p.amount*p.price), p.completedDt, p.price ) FROM MarketHistoryOrders p WHERE (p.fromCoin.name = :coinName or p.toCoin.name = :coinName) AND p.completedDt between :from and :to")
    @Query("select p from MarketHistoryOrders p where (p.fromCoin.name = :coinName or p.toCoin.name =:coinName) and p.completedDt between :from AND :to order by p.id desc")
    List<MarketHistoryOrders> getSumMarketHistoryOrdersFromBetweenTo(@Param("coinName") String symbol, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);

    @Query("select new io.dj.exchange.domain.dto.SumHistory(max(p.price), min(p.price), sum(p.price*p.amount), function('unix_timestamp', p.completedDt)) from MarketHistoryOrders p where (p.fromCoin.name = :coinName or p.toCoin.name =:coinName) and p.completedDt between :from AND :to group by function('date_format', p.completedDt, :d)")
    List<SumHistory> getSumFromBetweenTo(@Param("coinName") String symbol, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to, @Param("d") String df);

    @Query("select new io.dj.exchange.domain.dto.SumHistory(p.price, p.price, p.price*p.amount, function('unix_timestamp', p.completedDt)) from MarketHistoryOrders p where (p.fromCoin.name = :coinName or p.toCoin.name =:coinName) and p.completedDt between :from AND :to order by p.id asc")
    List<SumHistory> getSumFromBetweenTo(@Param("coinName") String symbol, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);

    @Query("select new io.dj.exchange.domain.dto.SumHistory(max(p.price), min(p.price), (p.price*p.amount), function('unix_timestamp', p.completedDt), p.price ) from MarketHistoryOrders p where (p.fromCoin.name = :coinName or p.toCoin.name =:coinName) and p.completedDt between :from AND :to group by p.price, p.completedDt order by p.id asc")
    List<SumHistory> getSumFromBetweenToWithP(@Param("coinName") String symbol, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);

    @Query("select new io.dj.exchange.domain.dto.SumHistory(max(p.price), min(p.price), (p.price*p.amount), function('unix_timestamp', function('date_format', p.completedDt, :df)), p.price ) from MarketHistoryOrders p where (p.fromCoin.name = :coinName or p.toCoin.name =:coinName) and p.completedDt between :from AND :to group by p.price, function('date_format', p.completedDt, :df) order by p.id asc")
    List<SumHistory> getSumFromBetweenToWithP(@Param("coinName") String symbol, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to, @Param("df") String df);

    @Query("select new io.dj.exchange.domain.dto.SumHistory(p.id, function('unix_timestamp', function('date_format', p.completedDt, :df)), p.price, (p.price*p.amount) ) from MarketHistoryOrders p where (p.fromCoin.name = :coinName or p.toCoin.name =:coinName) and p.completedDt between :from AND :to order by p.id asc")
    List<SumHistory> getSumHistory(@Param("coinName") String symbol, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to, @Param("df") String df);

    List<MarketHistoryOrders> findFirst100ByFromCoinNameOrToCoinNameAndIdGreaterThan(String name, String name1, long lastId);

    List<MarketHistoryOrders> findDistinctFirst100ByFromCoinNameOrToCoinNameAndIdGreaterThan(String name, String name1, long lastId);

    List<MarketHistoryOrders> findDistinctFirst100ByFromCoinNameOrToCoinNameAndIdGreaterThanOrderByIdAsc(String name, String name1, long lastId);

    List<MarketHistoryOrders> findDistinctTop100ByFromCoinNameOrToCoinNameAndIdGreaterThanOrderByIdAsc(String name, String name1, long lastId, Pageable p);

    @Query("select new io.dj.exchange.domain.dto.SumHistory(p.id, function('unix_timestamp', function('date_format', p.completedDt, :df)), p.price, (p.price*p.amount) ) from MarketHistoryOrders p where (p.fromCoin.name = :coinName or p.toCoin.name =:coinName) and p.id > :lastId order by p.id asc")
    List<SumHistory> getSumHistoryCoins(@Param("coinName") String name, @Param("lastId") long lastId, @Param("df") String df, Pageable p);

    @Query("select new io.dj.exchange.domain.dto.SumHistory(p.id, function('unix_timestamp', function('date_format', p.completedDt, '%Y-%m-%d')), function('date_format', p.completedDt, '%Y-%m-%d %H'), p.price, (p.price*p.amount) ) from MarketHistoryOrders p where (p.fromCoin.name = :coinName or p.toCoin.name =:coinName) and p.id > :lastId order by p.id asc")
    List<SumHistory> getSumHistoryCoinsDay(@Param("coinName") String name, @Param("lastId") long lastId, Pageable p);

    @Query("select new io.dj.exchange.domain.dto.SumHistory(p.id, function('unix_timestamp', function('date_format', p.completedDt, '%Y-%m-%d')), function('date_format', p.completedDt, '%Y-%m-%d %H'), case when p.toCoin.name ='KRW' then p.fromCoin.name else p.toCoin.name end ,p.price, (p.price*p.amount) ) from MarketHistoryOrders p where p.id > :lastId order by p.id asc")
    List<SumHistory> getSumHistoryCoinsDay(@Param("lastId") long lastId, Pageable p);

    //@Query("select new io.dj.exchange.domain.dto.SumHistory(p.id, function('unix_timestamp', function('date_format', p.completedDt, '%Y-%m-%d %H:%i')), function('date_format', p.completedDt, '%Y-%m-%d %H'), case when p.toCoin.name ='KRW' then p.fromCoin.name else p.toCoin.name end ,p.price, (p.price*p.amount) ) from MarketHistoryOrders p where p.id > :lastId order by p.id asc")
    @Query("select new io.dj.exchange.domain.dto.SumHistory(p.id, p.completedDt, case when p.toCoin.name ='KRW' then p.fromCoin.name else p.toCoin.name end ,p.price, (p.price*p.amount) ) from MarketHistoryOrders p where p.id > :lastId order by p.id asc")
    List<SumHistory> getSumHistoryCoins1Min(@Param("lastId") long lastId, Pageable p);

    //@Query("select new io.dj.exchange.domain.dto.SumHistory(p.id, function('unix_timestamp', function('date_format', p.completedDt, '%Y-%m-%d %H:%i')), function('date_format', p.completedDt, '%Y-%m-%d %H'), case when p.toCoin.name ='KRW' then p.fromCoin.name else p.toCoin.name end ,p.price, (p.price*p.amount) ) from MarketHistoryOrders p where p.id > :lastId order by p.id asc")
    @Query("select new io.dj.exchange.domain.dto.SumHistory(p.id, p.completedDt, case when p.toCoin.name ='KRW' then p.fromCoin.name else p.toCoin.name end ,p.price, (p.price*p.amount) ) from MarketHistoryOrders p where p.id > :lastId order by p.id asc")
    List<SumHistory> getSumHistoryCoinsMin(@Param("lastId") long lastId, Pageable p);

    @Query("select new io.dj.exchange.domain.dto.Volume(sum(p.amount*p.price)) from MarketHistoryOrders p where p.completedDt >= :dt")
    Volume getSumVolumes(@Param("dt") LocalDateTime dateTime);

    List<MarketHistoryOrders> findAllByOrderIdAndCompletedDtNotNull(long id);

    @Query("select new io.dj.exchange.domain.dto.TodayUsers(function('count', p.id), function('date_format', p.completedDt, '%Y-%m-%d')) from MarketHistoryOrders p where p.completedDt >= :dt group by function('date_format', p.completedDt, '%Y-%m-%d') order by p.completedDt asc")
    List<TodayUsers> getTodayTrade(@Param("dt") LocalDateTime dateTime);

    @Query("select p from MarketHistoryOrders p where (p.fromCoin.name = :coinName or p.toCoin.name = :coinName) and function('date_format', p.completedDt, '%Y-%m-%d') = :dt" )
    List<MarketHistoryOrders> findAllByFromCoinNameOrToCoinNameAndCompletedDt(@Param("coinName") String coinName, @Param("dt") String dt);

//    @Query("select new io.dj.exchange.domain.dto.SumHistory(max(p.price), min(p.price), (p.price*p.amount), b.price, c.price, function('unix_timestamp', p.completedDt) ) from MarketHistoryOrders p join MarketHistoryOrders b where (p.fromCoin.name = :coinName or p.toCoin.name =:coinName) and p.completedDt between :from AND :to group by p.completedDt order by p.id asc")
//    List<SumHistory> getSumFromBetweenToFinal(@Param("coinName") String symbol, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);

}
