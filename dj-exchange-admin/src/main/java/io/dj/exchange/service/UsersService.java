//package io.dj.exchange.service;
//
//import com.github.mkopylec.recaptcha.validation.RecaptchaValidator;
//import io.dj.exchange.annotation.TransactionalEx;
//import io.dj.exchange.domain.primary.EmailConfirm;
//import io.dj.exchange.domain.primary.User;
//import io.dj.exchange.domain.primary.UserSetting;
//import io.dj.exchange.domain.primary.OtpSetting;
//import io.dj.exchange.enums.Role;
//import io.dj.exchange.provider.OtpProvider;
//import io.dj.exchange.repository.hibernate.primary.ConfirmRepository;
//import io.dj.exchange.repository.hibernate.primary.OtpRepository;
//import io.dj.exchange.repository.hibernate.primary.UserRepository;
//import io.dj.exchange.repository.hibernate.primary.UserSettingRepository;
//import io.dj.exchange.util.MailUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Service;
//import org.springframework.util.DigestUtils;
//
//import javax.servlet.http.HttpServletRequest;
//import java.math.BigInteger;
//import java.time.LocalDateTime;
//import java.util.Optional;
//
///**
// * Created by jeongwoo on 2017. 4. 4..
// */
//@Slf4j
//@Service
//public class UsersService {
//
//    @Autowired
//    UserRepository userRepository;
//
//    @Autowired
//    UserSettingRepository userSettingRepository;
//
//    @Autowired
//    ConfirmRepository confirmRepository;
//
//    @Autowired
//    OtpRepository otpRepository;
//
//    @Autowired
//    private OtpProvider otpProvider;
//
////    @Autowired
////    private RecaptchaValidator recaptchaValidator;
//
//    public void showhideOtp(Long userId) {
//        User user = userRepository.findOne(userId);
//        //UserSetting userSetting = userSettingRepository.findOneByUserId(user.getId());
//        if ("Y".equals(user.getUserSetting().getOtpActive())) {
//            user.getUserSetting().setOtpActive("N");
//        } else {
//            user.getUserSetting().setOtpActive("Y");
//        }
//        //userSettingRepository.save(user.getUserSetting());
//    }
//
//    public User getUser(Long userId) {
//        return userRepository.findOne(userId);
//    }
//
//    @TransactionalEx
//    public String doSignup(String loginId
//            , String password
//            , HttpServletRequest request) {
//
//        try {
////            ValidationResult result = recaptchaValidator.validate(request);
////            if (result.isFailure()) {
////                return "redirect:/?error=Recaptcha_Error";
////            }
//
//            Optional<User> existUser = userRepository.findOneByEmail(loginId);
//            if (existUser.isPresent()) {
//                return "redirect:/?error=Already_User";
//            }
//
//            User user = new User();
//            UserSetting userSetting = new UserSetting();
//
//            user.setEmail(loginId);
//            user.setPwd(new BCryptPasswordEncoder().encode(password));
//            user.setRole(Role.USER);
//            user.setActive("N");
//            user.setRegDt(LocalDateTime.now());
//
//            userRepository.save(user);
//
//            userSetting.setOtpActive("N");
//            userSetting.setOtpHash(otpProvider.genSecretKey(loginId + password));
//            userSetting.setLevel("LEV1");
//            userSetting.setRegDt(LocalDateTime.now());
//            userSetting.setUserId(user.getId());
//            userSetting.setTheme("blue");
//            userSetting.setApiKey(new BigInteger(1, DigestUtils.md5Digest((user.getEmail()+user.getPwd()).getBytes())).toString(16));
//
//            userSettingRepository.save(userSetting);
//
//            OtpSetting otpSetting = new OtpSetting();
//            otpSetting.setUseLogin("Y");
//            otpSetting.setUserId(user.getId());
//            otpSetting.setUseSetting("Y");
//            otpSetting.setUseWithdraw("Y");
//
//            otpRepository.save(otpSetting);
//
//            EmailConfirm confirm = MailUtil.getMailConfirm(user.getEmail());
//
//            confirmRepository.save(confirm);
//
//            //walletService.createWallets(user);
//            return "redirect:/?regist=yes";
//        } catch(Exception ex) {
//            ex.printStackTrace();
//            return "redirect:/?error=invalid";
//        }
//    }
//
//    public String findPassword(String loginId, HttpServletRequest request) {
//
//        //해당 loginId로 메일을 보낸다.
//        try {
//            return "redirect:/?find=yes";
//        }catch(Exception e){
//            return "redirect:/?error=invalid";
//        }
//
//
//    }
//
//    @TransactionalEx
//    public String confirmEmail(String hash, String confirmCode) {
//
//        try{
//                log.info("----requestConfirm, email - code : {} - {}", hash, confirmCode);
//                Optional<EmailConfirm> confirm = confirmRepository.findOneByHashEmailAndConfirmCode(hash,confirmCode);
//
//                //log.info("테스트 : "+new BCryptPasswordEncoder().matches(email + confirmCode, confirm.get().getHashEmail()));
//
//                if(confirm.isPresent() && hash.equals(new BigInteger(1, DigestUtils.md5Digest(confirm.get().getEmail().getBytes())).toString(16))){
//
//                    Optional<User> user = userRepository.findOneByEmail(confirm.get().getEmail());
//                    if(user.isPresent()){
//                        user.get().setActive("Y");
//                        userRepository.save(user.get());
//                        return "redirect:/confirm/result?confirm=ok";
//                    }
//                }
//
//        }catch(Exception e){
//            log.error(e.getMessage());
//            e.printStackTrace();
//
//        }
//        return "redirect:/confirm/result?confirm=no";
//
//    }
//}
