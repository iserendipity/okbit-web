package io.dj.exchange;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 14.
 * Description
 */
@Service
public class S3Wrapper {

    @Autowired
    private AmazonS3 amazonS3Client;

    @Value("${cloud.aws.s3.bucket}")
    private String bucket;

    public PutObjectResult upload(String filePath, String uploadKey) throws FileNotFoundException{

        return upload(new FileInputStream(filePath), uploadKey);

    }

    private PutObjectResult upload(InputStream inputStream, String uploadKey){

        PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, uploadKey, inputStream, new ObjectMetadata());

        PutObjectResult putObjectResult = amazonS3Client.putObject(putObjectRequest);

        IOUtils.closeQuietly(inputStream);

        return putObjectResult;

    }

    public List<PutObjectResult> upload(MultipartFile[] multipartFiles, String uploadKey){
        List<PutObjectResult> putObjectResults = new ArrayList<>();
        Arrays.stream(multipartFiles)
                .filter(multipartFile -> !StringUtils.isEmpty(multipartFile.getOriginalFilename()))
                .forEach(multipartFile -> {
                    try {
                        putObjectResults.add(upload(multipartFile.getInputStream(), uploadKey));
                    }catch(IOException e){

                    }
                });

        return putObjectResults;

    }

    public PutObjectResult upload(MultipartFile multipartFile, String uploadKey) throws IOException{
        return upload(multipartFile.getInputStream(), uploadKey);
    }


    public byte[] downloadImgByteArr(String key) throws IOException{

        GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, key);

        S3Object s3Object = amazonS3Client.getObject(getObjectRequest);

        S3ObjectInputStream objectInputStream = s3Object.getObjectContent();

        byte[] bytes = IOUtils.toByteArray(objectInputStream);

        return bytes;

    }

}
