//package io.dj.exchange.config;
//
///**
// * Project dj-exchange-okbit.
// * Created by jeongwoo on 2017. 5. 10.
// * Description
// */
//
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.context.MessageSource;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.support.ReloadableResourceBundleMessageSource;
//import org.springframework.web.servlet.LocaleResolver;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//import org.springframework.web.servlet.i18n.CookieLocaleResolver;
//import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
//
//@Configuration
//@EnableAutoConfiguration
//@ComponentScan
//public class I18nConfig extends WebMvcConfigurerAdapter
//{
//    @Bean
//    public LocaleResolver localeResolver()
//    {
//        // 쿠키를 사용한 예제
//        CookieLocaleResolver resolver = new CookieLocaleResolver();
//        resolver.setCookieName("lang");
//
//        return resolver;
//    }
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry)
//    {
//        LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
//        localeChangeInterceptor.setParamName("lang");
//        registry.addInterceptor(localeChangeInterceptor);
//    }
//
//    @Bean
//    public MessageSource messageSource()
//    {
//        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
//        messageSource.setBasename("classpath:/static/i18n/messages");
//        messageSource.setDefaultEncoding("UTF-8");
//        return messageSource;
//    }
//}