package io.dj.exchange.controller.advice;

import io.dj.exchange.domain.primary.Response;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.exception.OkbitException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 24.
 * Description
 */
@Slf4j
@ControllerAdvice(basePackages="io.dj.exchange.controller.api")
public class AdminControllerAdvice {

    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    @ExceptionHandler(OkbitException.class)
    public Response defaultErrorHandler(OkbitException e) {

        return Response.builder()
                .code(CodeEnum.FAIL)
                .desc(e.getMessage())
                .build();
    }

}
