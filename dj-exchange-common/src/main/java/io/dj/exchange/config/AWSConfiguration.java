package io.dj.exchange.config;

import com.amazonaws.auth.*;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.AmazonS3EncryptionClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 14.
 * Description
 */
@Configuration
public class AWSConfiguration {

    @Value("${cloud.aws.credentials.accessKey}")
    private String accessKey;

    @Value("${cloud.aws.credentials.secretKey}")
    private String secretKey;

//    @Value("${cloud.aws.region}")
//    private String region;

    @Bean
    public BasicAWSCredentials basicAWSCredentials(){
        return new BasicAWSCredentials(accessKey, secretKey);
    }

    @Bean
    public SystemPropertiesCredentialsProvider systemPropertiesCredentialsProvider(){

        return new SystemPropertiesCredentialsProvider();

    }

    @Bean
    public AmazonS3 amazonS3Client(AWSCredentials awsCredentials){

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withRegion(Regions.AP_NORTHEAST_2)
                .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials()))
                .build();

        return s3Client;

//        AmazonS3 client = AmazonS3ClientBuilder.withCredentials(basicAWSCredentials())
//                .withRegion(Region.getRegion(Regions.fromName(region)))
//                //.withForceGlobalBucketAccess(true)
//                .build();

        //AmazonS3EncryptionClient amazonS3Client = new AmazonS3EncryptionClient();
//        AmazonS3Client amazonS3Client = new AmazonS3Client();
//        amazonS3Client.setRegion(Region.getRegion(Regions.fromName(region)));
//        return amazonS3Client;

    }

}
