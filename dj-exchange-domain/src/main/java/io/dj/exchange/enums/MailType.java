package io.dj.exchange.enums;

/**
 * Created by kun7788 on 2016. 12. 11..
 */
public enum MailType {
    FORGETPASS, SIGNUP
}
