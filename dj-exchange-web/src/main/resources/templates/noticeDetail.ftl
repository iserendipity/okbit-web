<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<#include "common/head.ftl">
<body id="skin-blur-blue" style="overflow-x:hidden">
<#--<section>-->
<#--<article id="paragraph" class="block-area">-->
    <#--<h3>${notice.title}</h3>-->
    <#--<hr class="whiter">-->
    <#--<div style="padding:10px;">-->
        <#--<p style="font-size:13px;">${notice.contents}</p>-->
    <#--</div>-->
    <#--&lt;#&ndash;<p>Integer eu lectus sollicitudin, hendrerit est ac, sollicitudin nisl. Quisque viverra sodales lectus nec ultrices. Fusce elit dolor, dignissim a nunc id, varius suscipit turpis. Cras porttitor turpis vitae leo accumsan molestie. Morbi vitae luctus leo. Sed nec scelerisque magna, et adipiscing est. Proin lobortis lectus eu sem ullamcorper, commodo malesuada quam fringilla. Curabitur ac nunc dui. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sagittis enim eu est lacinia, ut egestas ligula imperdiet.</p>&ndash;&gt;-->
    <#--<br>-->
<#--</article>-->
<#--</section>-->

    <div id="detail" class="modal-content" style="height:460px">
        <div class="modal-header">
            <#--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">-->
                <#--×-->
            <#--</button>-->
            <h4>${notice.title}</h4>
        </div>
        <div class="modal-body" style="min-height:330px;">
            <p style="font-size:13px;">${notice.contents}</p>
        </div>
        <div class="modal-footer">
            <#--<button type="button" class="btn btn-sm" onclick="javascript:finalRequest();"><@spring.message "BTN_SEND"/></button>-->
            <button type="button" class="btn btn-sm" onclick="javascript:self.close();"><@spring.message "BTN_CLOSE"/></button>
        </div>
    </div>
</body>

<!-- Javascript Libraries -->
<!-- jQuery -->
<script src="/js/jquery.min.js"></script> <!-- jQuery Library -->

<!-- Bootstrap -->
<script src="/js/bootstrap.min.js"></script>

<!-- Jquery cookie -->
<script src="/js/jquery.cookie.js"></script>

<script type="text/javascript">

    $('document').ready(function(){

        //var height = $('#detail').css('height', 460);


    });

</script>

</html>
