<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
    </head>
<#if (user.userSetting.theme)??>
<body id="skin-blur-${user.userSetting.theme}">
<#else>
<body id="skin-blur-blue">
</#if>

        <header id="header" class="media">
            <#include "common/top.ftl">
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            
            <!-- Sidebar -->
            <aside id="sidebar">

                <!-- Side Menu -->
                <ul class="list-unstyled side-menu">
                    <#include "common/sidemenu.ftl">
                </ul>

            </aside>
        
            <!-- Content -->
            <section id="content" class="container">
                <!-- verification modal-->
                <#include "verification/cellphone.ftl">
                <#include "verification/id.ftl">
                <#include "verification/bankbook.ftl">

                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li style="font-size:14px;"><a href="/media/guide.pdf" target="_blank">GUIDE BOOK</a></li>
                </ol>
                
                <h4 class="page-title">PROFILE</h4>
                                
                <!-- Shortcuts -->

                <#include "common/shortcut.ftl">
                
                <hr class="whiter" />
                
                <!-- Main Widgets -->
                <div class="block-area">
                    <div class="row" >
                        <div id="accountInfo" class="col-md-4">
                            <!-- Account info -->
                            <#include "content/accountInfo.ftl">

                        </div>
                        <div class="col-md-5 col-lg-6">
                            <!-- Security Settings-->
                            <#include "content/security.ftl">

                        </div>

                        <div class="col-md-3 col-lg-2">
                            <!-- COIN Info -->
                            <div id="coinInfoWrap" class="col-md-12">
                            <#include "rightInfo/infoCoin.ftl">
                            </div>

                            <!-- Order Form -->
                            <div id="orderFormWrap" class="col-md-12">
                                <div class="tile">
                                <#include "rightInfo/infoOrderForm.ftl">
                                </div>
                            </div>

                            <!-- Balance -->
                            <div id="balanceWrap" class="col-md-12">
                            <#include "rightInfo/infoBalance.ftl">
                            </div>

                            <!-- My Orders -->
                            <div id="myOrderWrap" class="col-md-12">
                                <div class="tile">
                                    <h2 class="tile-title"><@spring.message "TITLE_MYORDERS"/></h2>
                                    <div class="tile-config">
                                        <a data-toggle="collapse" href="#myOrder" class="tile-menu"></a>
                                    </div>
                                    <div id="myOrder" class="overflow collapse in">
                                    <#include "rightInfo/infoMyOrder.ftl">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </section>

            <!-- Older IE Message -->
            <!--[if lt IE 9]>
                <div class="ie-block">
                    <h1 class="Ops">Ooops!</h1>
                    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser in order to access the maximum functionality of this website. </p>
                    <ul class="browsers">
                        <li>
                            <a href="https://www.google.com/intl/en/chrome/browser/">
                                <img src="/img/browsers/chrome.png" alt="">
                                <div>Google Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.mozilla.org/en-US/firefox/new/">
                                <img src="/img/browsers/firefox.png" alt="">
                                <div>Mozilla Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com/computer/windows">
                                <img src="/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://safari.en.softonic.com/">
                                <img src="/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-10/worldwide-languages">
                                <img src="/img/browsers/ie.png" alt="">
                                <div>Internet Explorer(New)</div>
                            </a>
                        </li>
                    </ul>
                    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
                </div>   
            <![endif]-->
        </section>
        
        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->
        <script src="/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
        <script src="/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!--  Form Related -->
        <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->
        <script src="js/fileupload.min.js"></script> <!-- File Upload -->

        <!-- UX -->
        <script src="/js/scroll.min.js"></script> <!-- Custom Scrollbar -->
        <script src="js/toggler.min.js"></script> <!-- Toggler -->

        <!-- Other -->
        <script src="/js/calendar.min.js"></script> <!-- Calendar -->
        <script src="/js/feeds.min.js"></script> <!-- News Feeds -->

        <script src="/js/jquery.loading.min.js"></script> <!-- loading -->

        <!-- All JS functions -->
        <script src="/js/functions.js"></script>

        <!-- Jqutemplateslate -->
        <script src="/js/jquery.tmpl.min.js"></script>
        <!-- cookie -->
        <script src="/js/jquery.cookie.js"></script>
        <!-- trade functions -->
        <script src="/js/api/trade_api.js"></script>

        <!-- common functions -->
        <script src="/js/api/common_api.js"></script>
        <script src="/js/common/common.js"></script>

        <!-- profile functions -->
        <script src="/js/api/profile_api.js"></script>
        <script src="/js/profile/profile.js"></script>

        <!-- custom utils -->
        <script src="/js/api/util.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                commonInit();
                init();

            });
        </script>
    </body>
</html>
