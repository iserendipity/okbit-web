<!-- Messages Drawer -->
<script id="postponeTemplate" type="text/x-jquery-tmpl">
    <#--<div id="${r"${coin.name}"}Account" class="tile animated custom drawer" data-toggle="false"><!-- .drawer &ndash;&gt;-->
    <div class="listview narrow">
        <div class="tile-title">
            <span name="unit">${r"${coin.name}"}</span>
            <span class="drawer-close">&times;</span>
        </div>
        <div class="overflow">
            <#--<div class="panel-heading">-->
                <#--Please send <span name="name">${r"${coin.name}"}</span> to one of your deposit addresses displayed below. The deposit will be credited in your corresponding wallet. To ensure your transaction is confirmed as soon as possible, a miner transaction fee of at least 0.0003 is recommended.-->
            <#--</div>-->
            <div class="panel-body">
                <div class="form-group">
                    <label for=""><@spring.message "DEPOSIT_WALLET"/></label>
                    <div class="well tile col-md-12 t-overflow" >
                        <span class="h4">
                            <@spring.message "NOT_TEMP_BITCOIN"/>
                        </span>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <p style="color:white; text-shadow:none;"><h5><span name="name">${r"${coin.name}"}</span><@spring.message "DEPOSIT_COIN_EXP_1"/> <span name="minConfirmation">${r"${coin.minConfirmation}"}</span> <@spring.message "DEPOSIT_COIN_EXP_2"/></h5></p>

            </div>
        </div>
    </div>
<#--</div>-->
</script>
<script id="walletTemplate" type="text/x-jquery-tmpl">
    <#--<div id="${r"${coin.name}"}Account" class="tile animated custom drawer" data-toggle="false"><!-- .drawer &ndash;&gt;-->
    <div class="listview narrow">
        <div class="tile-title">
            <span name="unit">${r"${coin.name}"}</span>
            <span class="drawer-close">&times;</span>
        </div>
        <div class="overflow">
            <#--<div class="panel-heading">-->
                <#--Please send <span name="name">${r"${coin.name}"}</span> to one of your deposit addresses displayed below. The deposit will be credited in your corresponding wallet. To ensure your transaction is confirmed as soon as possible, a miner transaction fee of at least 0.0003 is recommended.-->
            <#--</div>-->
            <div class="panel-body">
                <div class="form-group">
                    <#--<label for=""><@spring.message "DEPOSIT_WALLET"/></label>-->
                    <div class="well tile col-md-12 t-overflow" >
                        <span class="h4"><a href="#" class="tooltips" onclick="javascript:copy(this);" data-toggle="tooltip" data-placement="top" data-container="body" title="" data-original-title="<@spring.message "CLICK_TO_COPY"/>" name="address">${r"${address}"}</a></span>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <p style="color:white; text-shadow:none;"><h5><span name="name">${r"${coin.name}"}</span><@spring.message "DEPOSIT_COIN_EXP_1"/> <span name="minConfirmation">${r"${coin.minConfirmation}"}</span> <@spring.message "DEPOSIT_COIN_EXP_2"/></h5></p>

            </div>
        </div>
    </div>
<#--</div>-->
</script>
<script id="walletTemplateTag" type="text/x-jquery-tmpl">
    <#--<div id="${r"${coin.name}"}Account" class="tile animated custom drawer" data-toggle="false"><!-- .drawer &ndash;&gt;-->
    <div class="listview narrow">
        <div class="tile-title">
            <span name="unit">${r"${coin.name}"}</span>
            <span class="drawer-close">&times;</span>
        </div>
        <div class="overflow">
            <#--<div class="panel-heading">-->
                <#--Please send <span name="name">${r"${coin.name}"}</span> to one of your deposit addresses displayed below. The deposit will be credited in your corresponding wallet. To ensure your transaction is confirmed as soon as possible, a miner transaction fee of at least 0.0003 is recommended.-->
            <#--</div>-->
            <div class="panel-body">
                <div class="form-group">
                    <#--<label for=""><@spring.message "DEPOSIT_WALLET"/></label>-->
                    <div class="well tile col-md-12 t-overflow" >
                        <span class="h4"><a href="#" class="tooltips" onclick="javascript:copy(this);" data-toggle="tooltip" data-placement="top" data-container="body" title="" data-original-title="<@spring.message "CLICK_TO_COPY"/>" name="address">${r"${address}"}</a></span>
                    </div>
                    <#--<div class="well tile col-md-4">Tag : ${r"${tag}"}</div>-->
                    <div class="clearfix"></div>
                </div>
                <p style="color:white; text-shadow:none;"><h5><@spring.message "DEPOSIT_NOT_PAYMENTID"/></h5></p>
                <p style="color:white; text-shadow:none;"><h5><span name="name">${r"${coin.name}"}</span><@spring.message "DEPOSIT_COIN_EXP_1"/> <span name="minConfirmation">${r"${coin.minConfirmation}"}</span> <@spring.message "DEPOSIT_COIN_EXP_2"/></h5></p>
            </div>
        </div>
    </div>
<#--</div>-->
</script>
<script id="walletTemplateKRW" type="text/x-jquery-tmpl">
    <#--<div id="${r"${coin.name}"}Account" class="tile animated custom drawer" data-toggle="false"><!-- .drawer &ndash;&gt;-->
    <div class="listview narrow">
        <div class="tile-title">
            <span name="unit">${r"${coin.name}"}</span>
            <span class="drawer-close">&times;</span>
        </div>
        <div class="overflow">
            <#--<div class="panel-heading">-->
                <#--Please send <span name="name">${r"${coin.name}"}</span> to one of your deposit addresses displayed below. The deposit will be credited in your corresponding wallet. To ensure your transaction is confirmed as soon as possible, a miner transaction fee of at least 0.0003 is recommended.-->
            <#--</div>-->
            <div class="panel-body">
                <div class="form-group">
                    <#--<label for=""><@spring.message "DEPOSIT_WALLET"/></label>-->
                    <div class="well tile col-md-12 t-overflow" >
                        <#if user.userSetting.level != 'LEV1'>
                            <span class="h4"><a href="#" class="tooltips" onclick="javascript:copy(this);" data-toggle="tooltip" data-placement="top" data-container="body" title="" data-original-title="<@spring.message "CLICK_TO_COPY"/>" name="address">${r"${address}"}</a> &nbsp;<span style="color:red; text-shadow:none!important; font-size:15px;">(<@spring.message "MUST_KRW"/>)</span>
                            <#else>
                            <span class="h4"><@spring.message "NOT_YET_USE"/></span>
                        </#if>
                    </div>
                    <div class="well tile col-md-4"><span style="font-size:16px;"><@spring.message "DEPOSIT_PRIVATE_ID"/> : ${r"${depositDvcd}"}</span></div>
                    <div class="clearfix"></div>
                </div>
                <p style="color:white; text-shadow:none"><h5><span name="name">${r"${coin.name}"}</span> <@spring.message "DEPOSIT_KRW_EXP"/></h5></p>

            </div>
        </div>
    </div>
<#--</div>-->
</script>
<!-- Messages Drawer -->
<script id="newTemplate" type="text/x-jquery-tmpl">
<div id="${r"${name}"}Account" class="tile animated custom drawer" data-toggle="false"><!-- .drawer -->
    <div class="listview narrow">
        <div class="tile-title">
            <span name="name">${r"${name}"}</span>
            <span class="drawer-close">&times;</span>
        </div>
        <div class="overflow">
            <div class="panel-heading">
                <h5><@spring.message "WALLET_NEW"/></h5>
            </div>
            <div class="panel-body" style="padding-top:0px;">
                <div class="form-group">
                    <#--<label for="">Get new address</label>-->
                    <#--<div class="well tile col-md-12 t-overflow" >-->
                        <a href="#" onclick="javascript:getAddress('${r"${name}"}');" class="btn btn-primary btn-alt btn-sm" ><i class="fa fa-plus"></i>&nbsp;${r"${name}"} <@spring.message "BTN_WALLET_NEW"/></a>
                    <#--<span class="h4"><a href="#" class="tooltips" onclick="javascript:copy(this);" data-toggle="tooltip" data-placement="top" data-container="body" title="" data-original-title="Copy to clipboard" name="address">1GueuEupVXfy59coiiycMELNinFobG5LEX</a></span>-->
                    <#--</div>-->
                    <div class="clearfix"></div>
                </div>
                <#--<p style="color:white; text-shadow:none; font-weight:bold"><span name="name">Bitcoin</span> deposits are credited after a minimum of <span name="minConfirmation">3</span> confirmations (about 30 minutes, sometimes longer)</p>-->

            </div>
        </div>
    </div>
</div>
</script>
<div id="depositWallets">

</div>