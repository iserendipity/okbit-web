/**
 * Created by jeongwoo on 2017. 4. 27..
 */
var commonApi = new CommonApi();
var withdrawApi = new WithdrawApi();
var depositApi = new DepositApi();

function coinList(){

    commonApi.coins(function(data){
        makeTemplate('coinList','coinTemplate',data.data);
    });

}

function coinWallet(coinName){

    var param = new Object();
    param.coinName = coinName;
    param.name = coinName;

    withdrawApi.wallet(function(data){
        // $('.overflow').getNiceScroll().each(function(){
        //     //console.log('----------');
        //     $(this).hide();
        // });
        if(data.data) {

            switch(coinName){

                case "KRW" :    makeTemplate('withdrawForm', 'walletTemplateKRW', data.data);
                                // //console.log(otpSetting);
                                // if(otpSetting.useWithdraw == 'Y'){
                                //     $('input[name=otpCode]').attr('readonly', false);
                                // }else{
                                //     $('input[name=otpCode]').attr('readonly', true);
                                // }
                    break;
                case "MONERO" : makeTemplate('withdrawForm', 'walletTemplateMonero', data.data);
                    break;
                case "BITCOIN" : makeTemplate('withdrawForm', 'walletTemplateBitcoin', data.data);
                    break;
                default : makeTemplate('withdrawForm', 'walletTemplate', data.data);

                    break;

            }
            //console.log(blockFee.BITCOIN);

            var fee = 0;

            switch(coinName){
                case 'BITCOIN': fee = blockFee.BITCOIN;
                break;
                case 'ETHEREUM': fee = blockFee.ETHEREUM;
                    break;
                case 'LITECOIN': fee = blockFee.LITECOIN;
                    break;
                case 'MONERO': fee = blockFee.MONERO;
                    break;
                case 'DASH': fee = blockFee.DASH;
                    break;
                case 'KRW' : fee = blockFee.KRW;

            }

            $('#indicatorFee').text(fee);

            //setValues('withdrawForm', data.data);
        }else{
            makeTemplate('withdrawForm', 'walletTemplateNoWallet', param);
            //$('#withdrawForm form').find('input[name=from]').val('');
        }

        $('.overflow').getNiceScroll().each(function(){
            $(this).resize();
        });

    }, param);

    transactions('send', 1, coinName);

    if(coinName == 'KRW'){

        $('#krwModal').modal({
            'backdrop': 'static'
        });

    }else {

        $('#infoModal').modal({
            'backdrop': 'static'
        });
    }
}
var timer;

function getWithdraw(pageNo, coinName){

    //transactions('send', pageNo, coinName);
    transactions('send', pageNo, coinName);

    clearInterval(timer);
    timer = setInterval(function(){
        transactions('send', pageNo, coinName);
        //makeBalance();
    }, 5000);

}

function transactions(category, pageNo, coinName){
    //console.log("----------------");
    var params = new Object();
    params.page = pageNo-1;
    params.size = 20;
    params.category = category;
    params.coinName = nowCoin ? nowCoin : coinName;

    depositApi.transactions(function(data){
        if(data.page && data.page.totalPages > 0 ) {
            setPageValues('withdrawPage', data.page, 'getWithdraw', pageNo);
        }

        if(data._embedded && data._embedded.transactionses && data._embedded.transactionses.length > 0){

            makeTemplate('recentWithdrawTableBody','recentWithdrawTemplate', data._embedded.transactionses);

        }else{

            $('#recentWithdrawTableBody').html('<tr class="text-center"><td colspan="6">'+no_withdraw_msg+'</td></tr>')


        }

    }, params);

}


//
// function transactions(category, coinName){
//     var params = new Object();
//     params.category = category;
//     params.coinName = coinName;
//
//     withdrawApi.transactions(function(data){
//
//         if(data.data && data.data.length > 0){
//
//             makeTemplate('recentWithdrawTableBody','recentWithdrawTemplate', data.data);
//
//         }
//
//     }, params);
//
// }

var blockFee = { 'BITCOIN' : 0.0005, 'ETHEREUM' : 0.01, 'LITECOIN': 0.01, 'DASH': 0.01, 'MONERO' : 0.05, 'KRW' : 1000};

function getRealAmount(coinName){

    var regNumber = /^\d(.\d+)?/;
    //var regNumber = ^d(.d+)?+$;

    var amountPre = $('#formWithdraw input[name=amountPre]').val();

     if(!regNumber.test(amountPre)) {
         return false;
     }
    // console.log('-----------2');
    if(amountPre){

        $.each(blockFee, function(i, v){

            if(coinName == i){

                $('#formWithdraw input[name=amount]').val(eval(amountPre) + v);
                $('#indicatorAmount').text(eval(amountPre) + v);
            }


        });


    }
}

function requestWithdrawalCoin(coinName){

    var params = $('#formWithdraw').serializeObject();

    if(!paramValidation(params)){
        return;
    }

    var amount = params.amount;
    var maxVal;

    switch(coinName){

        case 'BITCOIN' :

            maxVal = 50;

            break;

        case 'ETHEREUM' :

            maxVal = 500;

            break;

        case 'MONERO' :

            maxVal = 3000;

            break;

        case 'DASH' :

            maxVal = 1500;

            break;

        case 'LITECOIN' :

            maxVal = 5000;

            break;
        default : 10000;
            break;
    }

    if(amount > maxVal){
        alert(coinName + '의 1회 최대 출금한도는 '+maxVal+'입니다');
        return false;
    }

    if(otpSetting && otpSetting.useWithdraw == 'Y' && !$('#otpCode').val()){

        $('#otpModal').modal();
        return;

    }else{

        finalRequest();

    }


}

function requestWithdrawalKRW(){

    var params = $('#formWithdraw').serializeObject();

    if(!paramValidation(params)){
        return;
    }

    if(otpSetting && otpSetting.useWithdraw == 'Y' && !$('#otpCode').val()){

        $('#otpModal').modal();
        return;

    }else{

        finalRequest();

    }

}

function finalRequest(){

    var params = $('#formWithdraw').serializeObject();

    $('#otpCode').val() ? params.otpCode = $('#otpCode').val() : '';

    //console.log(params);
    if(params.coinName == 'KRW') {
        withdrawApi.withdrawKRW(function (data) {
            //location.href = '/withdraw';
            //console.log(data);

        }, params);
    } else {
        withdrawApi.withdrawCoin(function (data) {
            //location.href = '/withdraw';
            //console.log(data);

        }, params);
    }
}


function getAddress(coinName){

    var params = new Object();
    params.coinName = coinName;

    depositApi.regist(function(data){

        if(data.data){
            coinWallet(coinName);
        }

    }, params);

}

function init(){

    coinList();
    getWithdraw('1', 'BITCOIN');

    now = 'withdraw';
    //transactions('send', 'BITCOIN');

}


