<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
    </head>
<#if (user.userSetting.theme)??>
<body id="skin-blur-${user.userSetting.theme}">
<#else>
<body id="skin-blur-blue">
</#if>

        <header id="header" class="media">
            <#include "common/top.ftl">
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            
            <!-- Sidebar -->
            <aside id="sidebar">

                <!-- Side Menu -->
                <ul class="list-unstyled side-menu">
                    <#include "common/sidemenu.ftl">
                </ul>

            </aside>
        
            <!-- Content -->
            <section id="content" class="container">
            
                <!-- Notification Drawer -->
                <div id="notifications" class="tile drawer animated">
                    <div class="listview narrow">
                        <div class="media">
                            <a href="">Notification Settings</a>
                            <span class="drawer-close">&times;</span>
                        </div>
                        <div class="overflow" style="height: 254px">
                            <div class="media">
                                <div class="pull-left">
                                    <img width="40" src="/img/profile-pics/1.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <small class="text-muted">Nadin Jackson - 2 Hours ago</small><br>
                                    <a class="t-overflow" href="">Mauris consectetur urna nec tempor adipiscing. Proin sit amet nisi ligula. Sed eu adipiscing lectus</a>
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <img width="40" src="/img/profile-pics/2.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <small class="text-muted">David Villa - 5 Hours ago</small><br>
                                    <a class="t-overflow" href="">Suspendisse in purus ut nibh placerat Cras pulvinar euismod nunc quis gravida. Suspendisse pharetra</a>
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <img width="40" src="/img/profile-pics/3.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <small class="text-muted">Harris worgon - On 15/12/2013</small><br>
                                    <a class="t-overflow" href="">Maecenas venenatis enim condimentum ultrices fringilla. Nulla eget libero rhoncus, bibendum diam eleifend, vulputate mi. Fusce non nibh pulvinar, ornare turpis id</a>
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <img width="40" src="/img/profile-pics/4.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <small class="text-muted">Mitch Bradberry - On 14/12/2013</small><br>
                                    <a class="t-overflow" href="">Phasellus interdum felis enim, eu bibendum ipsum tristique vitae. Phasellus feugiat massa orci, sed viverra felis aliquet quis. Curabitur vel blandit odio. Vestibulum sagittis quis sem sit amet tristique.</a>
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <img width="40" src="/img/profile-pics/1.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <small class="text-muted">Nadin Jackson - On 15/12/2013</small><br>
                                    <a class="t-overflow" href="">Ipsum wintoo consectetur urna nec tempor adipiscing. Proin sit amet nisi ligula. Sed eu adipiscing lectus</a>
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <img width="40" src="/img/profile-pics/2.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <small class="text-muted">David Villa - On 16/12/2013</small><br>
                                    <a class="t-overflow" href="">Suspendisse in purus ut nibh placerat Cras pulvinar euismod nunc quis gravida. Suspendisse pharetra</a>
                                </div>
                            </div>
                        </div>
                        <div class="media text-center whiter l-100">
                            <a href=""><small>VIEW ALL</small></a>
                        </div>
                    </div>
                </div>
                
                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li style="font-size:14px;"><a href="/media/guide.pdf" target="_blank">GUIDE BOOK</a></li>
                </ol>
                
                <h4 class="page-title">Withdraw</h4>
                                
                <!-- Shortcuts -->
                <#include "common/shortcut.ftl">
                
                <hr class="whiter" />
                
                <!-- Main Widgets -->
                <div class="block-area">
                    <div class="row" >
                        <div class="col-md-2 col-lg-1">
                            <!-- Withdraw Coin Menu -->
                                <#include "content/withdrawCoinMenu.ftl">
                            <div class="clearfix"></div>
                        </div>

                        <div id="accordion" class="col-md-7 col-lg-9">
                            <#include "common/krwModal.ftl">
                            <div id="infoModal" class="modal fade"> <!-- Style for just preview -->
                                <div class="modal-dialog modal-lg" style="margin-top:180px;">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <#--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">-->
                                                <#--×-->
                                            <#--</button>-->
                                            <h4 class="modal-title"><@spring.message "TITLE_WITHDRAW_INFO"/></h4>
                                        </div>
                                        <div class="modal-body">
                                            <@spring.message "WITHDRAW_RECENT_NOTICE"/><br>
                                            <@spring.message "WITHDRAW_RECENT_NOTICE2"/><br>
                                            <@spring.message "WITHDRAW_RECENT_NOTICE3"/><br>
                                            <@spring.message "WITHDRAW_RECENT_NOTICE4"/><br>
                                            <@spring.message "WITHDRAW_RECENT_NOTICE5"/>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-sm" data-dismiss="modal"><@spring.message "BTN_AGREE"/></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <#include "content/withdrawForms.ftl">
                            <!--  Recent Withdraw -->
                            <div id="recentWithdrawWrap" class="tile">
                                <h2 class="tile-title"><@spring.message "WITHDRAW_RECENT"/>
                                </h2>
                                <div class="no-padding clearfix">

                                    <div class="fixed-thead-wrap col-xs-12">
                                        <div class="fixed-thead col-xs-2"><@spring.message "TITLE_DATE"/></div>
                                        <div class="fixed-thead col-xs-1"><@spring.message "TITLE_STATUS"/></div>
                                        <div class="fixed-thead col-xs-1"><@spring.message "TITLE_COIN"/></div>
                                        <div class="fixed-thead col-xs-2"><@spring.message "TITLE_AMOUNT"/></div>
                                        <div class="fixed-thead col-xs-6">TX</div>
                                    </div>
                                    <div id="recentWithdraw" class="col-xs-12">
                                        <#include "content/recentWithdraw.ftl">
                                    </div>
                                </div>
                                <div class="panel-footer text-center">
                                    <ul id="withdrawPage" class="pagination pagination-sm">
                                        <li class="active"><a href="javascript:getWithdraw('1');">1</a></li>

                                    </ul>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-lg-2">
                            <!-- COIN Info -->
                            <div id="coinInfoWrap" class="col-md-12">
                            <#include "rightInfo/infoCoin.ftl">
                            </div>

                            <!-- Order Form -->
                            <div id="orderFormWrap" class="col-md-12">
                                <div class="tile">
                                <#include "rightInfo/infoOrderForm.ftl">
                                </div>
                            </div>

                            <!-- Balance -->
                            <div id="balanceWrap" class="col-md-12">
                            <#include "rightInfo/infoBalance.ftl">
                            </div>

                            <!-- My Orders -->
                            <div id="myOrderWrap" class="col-md-12">
                                <div class="tile">
                                    <h2 class="tile-title"><@spring.message "TITLE_MYORDERS"/></h2>
                                    <div class="tile-config">
                                        <a data-toggle="collapse" href="#myOrder" class="tile-menu"></a>
                                    </div>
                                    <div id="myOrder" class="overflow collapse in">
                                    <#include "rightInfo/infoMyOrder.ftl">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </section>

            <!-- Older IE Message -->
            <!--[if lt IE 9]>
                <div class="ie-block">
                    <h1 class="Ops">Ooops!</h1>
                    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser in order to access the maximum functionality of this website. </p>
                    <ul class="browsers">
                        <li>
                            <a href="https://www.google.com/intl/en/chrome/browser/">
                                <img src="/img/browsers/chrome.png" alt="">
                                <div>Google Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.mozilla.org/en-US/firefox/new/">
                                <img src="/img/browsers/firefox.png" alt="">
                                <div>Mozilla Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com/computer/windows">
                                <img src="/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://safari.en.softonic.com/">
                                <img src="/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-10/worldwide-languages">
                                <img src="/img/browsers/ie.png" alt="">
                                <div>Internet Explorer(New)</div>
                            </a>
                        </li>
                    </ul>
                    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
                </div>   
            <![endif]-->
        </section>

        <form id="loadingForm"></form>

        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->
        <script src="/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
        <script src="/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Charts -->
        <script src="/js/charts/jquery.flot.js"></script> <!-- Flot Main -->
        <script src="/js/charts/jquery.flot.time.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen -->

        <script src="/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts -->
        <script src="/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts -->
        <script src="/js/charts.js"></script> <!-- All the above chart related functions -->

        <!-- Map -->
        <script src="/js/maps/jvectormap.min.js"></script> <!-- jVectorMap main library -->
        <script src="/js/maps/usa.js"></script> <!-- USA Map for jVectorMap -->

        <!--  Form Related -->
        <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->

        <!-- cookie -->
        <script src="/js/jquery.cookie.js"></script>

        <!-- UX -->
        <script src="/js/scroll.min.js"></script> <!-- Custom Scrollbar -->

        <!-- Other -->
        <script src="/js/calendar.min.js"></script> <!-- Calendar -->
        <script src="/js/feeds.min.js"></script> <!-- News Feeds -->

        <!-- All JS functions -->
        <script src="/js/functions.js"></script>

        <!-- Jqutemplateslate -->
        <script src="/js/jquery.tmpl.min.js"></script>

        <!-- custom utils -->
        <script src="/js/api/util.js"></script>

        <!-- trade functions -->
        <script src="/js/api/trade_api.js"></script>

        <!-- common functions -->
        <script src="/js/api/common_api.js"></script>
        <script src="/js/common/common.js"></script>
        <script src="/js/api/deposit_api.js"></script>
        <script src="/js/api/withdraw_api.js"></script>
        <script src="/js/withdraw/withdraw.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){

                commonInit();

                init();

            });

        </script>
        <#include "common/validation_locale.ftl"/>
    </body>
</html>
