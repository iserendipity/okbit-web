package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.WrongDeposit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WrongDepositRepository extends JpaRepository<WrongDeposit, Long> {

    Page<WrongDeposit> findAllByStatusOrderByIdAsc(String pending, Pageable p);
}
