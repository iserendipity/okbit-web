/**
 * Created by jeongwoo on 2017. 5. 10..
 */

var profileApi = new ProfileApi();

function profile(){

    // var param = new Object();
    //
    // profileApi.user(function(data){
    //     setValues('', data.data);
    //
    // }, param);

}

function addBank(){

    $('#bankForm').loading({
        message : 'UPLOADING...',
        theme : 'dark'
        //overlay: $("#bank_loading_div")
    });

    var data = new FormData();

    $.each($("input[name=bankbook]")[0].files, function (i, file) {
        data.append('bankbook', file);
    });

    profileApi.addBank(function (data) {
        $('#bankForm').loading('stop');

        if(data.code != 'SUCCESS'){
            alert(data.desc);
            return;
        }

        window.location.reload(true);
        //$('#bankForm').modal('close');
    }, data);


}

function addIdcard(){

    $('#idForm').loading({
        message : 'UPLOADING...',
        theme : 'dark'
        //overlay: $("#bank_loading_div")
    });

    var data = new FormData();

    $.each($("input[name=idcard]")[0].files, function (i, file) {
        data.append('idcard', file);
    });

    profileApi.addIdCard(function (data) {
        $('#idForm').loading('stop');
        if(data.code != 'SUCCESS'){
            alert(data.desc);
            return;
        }

        window.location.reload(true);
        //$('#bankForm').modal('close');
    }, data);




}

function changeOtpSetting(){

    if(otpSetting.useSetting == 'Y' && !$('#otpCode').val()){

        $('#otpModal').modal('show');
        return;

    }else{

        finalRequest();

    }

}

function finalRequest(){

    var param = $('#otpSettingForm').serializeObject();

    $('#otpCode').val() ? param.otpCode = $('#otpCode').val() : '';

    profileApi.changeOtpSetting(function(data){
        if(data.code =='FAIL'){
            //$('#otpModal').modal('hide');
            alert(data.desc);
        }else{
            window.location.reload(true);
        }
    }, param);

}

function activateOtp(){

    var param = new Object();
    param.otpCode = $('#otpActive').val();

    profileApi.activateOtp(function(data){
        if(data.code =='FAIL'){
            //$('#otpModal').modal('hide');
            alert(data.desc);
        }else{
            window.location.reload(true);
        }
    }, param);

}

function fnPopup(){
    window.open('', 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
    document.form_chk.action='/verify/pop';
    document.form_chk.target = "popupChk";
    document.form_chk.submit();
    // document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
    // document.form_chk.target = "popupChk";
    // document.form_chk.submit();
    //window.name ="Parent_window";
}

function init(){

    getOtpSetting();
    //profile();

    // $('.otpSetting').on('ifUnchecked', function(event){
    //     changeOtpSetting();
    //
    // });
    //
    // $('.otpSetting').on('ifChecked', function(event){
    //     changeOtpSetting();
    //
    // });

}
