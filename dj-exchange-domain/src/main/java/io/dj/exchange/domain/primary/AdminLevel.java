package io.dj.exchange.domain.primary;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 12.
 * Description
 */
@Data
@Entity
@Table(name = "admin_level")
public class AdminLevel implements Serializable{

    @Id
    @Column(name="user_id")
    Long userId;

    @Column(name = "idcard")
    String idcard;

    @Column(name = "idcard_yn")
    String idcardYn;

    @Column(name = "bankbook")
    String bankbook;

    @Column(name = "bankbook_yn")
    String bankbookYn;

    @Column(name = "idcard_reg_dt")
    LocalDateTime idcardRegDt;

    @Column(name = "idcard_mod_dt")
    LocalDateTime idcardModDt;

    @Column(name = "bankbook_reg_dt")
    LocalDateTime bankbookRegDt;

    @Column(name = "bankbook_mod_dt")
    LocalDateTime bankbookModDt;

    @Column(name = "cellphone_yn")
    String cellphoneYn;

    @Column(name = "cellphone")
    String cellphone;

    @Column(name = "real_name")
    String realName;

    @Column(name = "withdraw_bank_code")
    String withdrawBankCode;

    @Column(name = "withdraw_bank_nm")
    String withdrawBankNm;

    @Column(name = "withdraw_bank_account")
    String withdrawBankAccount;

    @Column(name = "level_modify_dt")
    LocalDateTime levelModifyDt;

    @Column(name = "level")
    String level;

    @Column(name = "di")
    String di;

    @Transient
    String email;

    @Transient
    String levelModifyDtTxt;

    @Transient
    String bankbookRegDtTxt;

    @Transient
    String idcardRegDtTxt;

}
