<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
    </head>
    <body id="skin-blur-chrome" style="overflow-x:hidden">
        <#--<div class="pull-right m-t-15 m-r-15" style="width:120px">-->
            <#--<select class="select" style="display: none;">-->
                <#--<option>Divider</option>-->
                <#--<option>Toyota Avalon</option>-->
                <#--<option>Toyota Crown</option>-->
                <#--<option>Toyota FT86 </option>-->
                <#--<option data-divider="true">&nbsp;</option>-->
                <#--<option>Lexus LS600</option>-->
                <#--<option>Lexus LFA</option>-->
                <#--<option>Lexus LX570</option>-->
            <#--</select>-->
        <#--</div>-->

        <section id="login">
            <header class="text-center">
                <div class="row text-center">
                    <img src="/img/logo/logo_main.png" style="width: 350px;">
                </div>
                <br>
                <br>

                    <#--<h3>패스워드 변경페이지</h3>-->
            </header>

            <div class="clearfix"></div>
            <div class="container-fluid">

                <!-- Register -->
                <form class="box animated tile form-validation-3 active" id="box-reset" method="post" action="javascript:doResetPassword();">
                    <h2 class="m-t-0 m-b-15"><@spring.message "RESET_PASSWORD"/></h2>
                    <input type="hidden" name="hashEmail" class="login-control m-b-10 " value="${data.hashEmail}">
                    <input type="hidden" name="confirmCode" class="login-control m-b-10 " value="${data.confirmCode}">
                    <input type="password" id="password" name="password" class="login-control m-b-10 validate[required, custom[password]]" placeholder="Password">
                    <input type="password" name="re_password" class="login-control m-b-20 validate[required, equals[password]]" placeholder="Confirm Password">
                    <br>
                    <button class="btn btn-lg m-r-5"><@spring.message "BTN_SEND"/></button>
                </form>

            </div>
        </section>

    </body>

    <!-- Javascript Libraries -->
    <!-- jQuery -->
    <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->

    <!-- Bootstrap -->
    <script src="/js/bootstrap.min.js"></script>

    <!--  Form Related -->
    <script src="/js/validation/validate.min.js"></script> <!-- jQuery Form Validation Library -->
    <script src="/js/validation/validationEngine.min.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->
    <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->
    <script src="/js/select.min.js"></script> <!-- Custom Select -->

    <!-- Jqutemplateslate -->
    <script src="/js/jquery.tmpl.min.js"></script>

    <!-- Jquery cookie -->
    <script src="/js/jquery.cookie.js"></script>

    <!-- All JS functions -->
    <script src="/js/functions.js"></script>

    <script src="/js/api/common_api.js"></script>
    <script src="/js/api/util.js"></script>

    <!-- login -->
    <script src="/js/index/login.js"></script>

    <script type="text/javascript">

        function doResetPassword(){

            var commonApi = new CommonApi();

            var param = $('#box-reset').serializeObject();

            commonApi.resetPassword(function(data){

                if(data.code == 'SUCCESS'){

                    alert('패스워드 변경이 완료되었습니다. 로그인 페이지로 이동합니다.');
                    location.href = '/';

                } else {

                    alert(data.desc);

                }


            }, param);



        }


    </script>
    <#include "common/validation_locale.ftl"/>
</html>
