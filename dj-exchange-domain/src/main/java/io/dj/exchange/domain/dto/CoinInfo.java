package io.dj.exchange.domain.dto;

import lombok.Data;

import java.text.DecimalFormat;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 12.
 * Description
 */
@Data
public class CoinInfo {

    private String lastPrice = "0.0";
    private String vol = "0.0";
    private String low = "0.0";
    private String high = "0.0";
    private String priceDiff = "0.0";
    private String upDown;
    private String percent;
    private String name;
    private String unit;

    public void setLastPrice(String lastPrice){
        if(Double.parseDouble(lastPrice) > 0) {
            DecimalFormat df = new DecimalFormat(",000");
            this.lastPrice = df.format(Double.parseDouble(lastPrice));
        }
    }

    public void setLow(String low){
        if(Double.parseDouble(low) > 0) {
            DecimalFormat df = new DecimalFormat(",000");
            this.low = df.format(Double.parseDouble(low));
        }
    }

    public void setVol(String vol){
        if(Double.parseDouble(vol) > 0) {
            DecimalFormat df = new DecimalFormat(",000");
            this.vol = df.format(Double.parseDouble(vol));
        }
    }
    public void setHigh(String high){
        if(Double.parseDouble(high) > 0) {
            DecimalFormat df = new DecimalFormat(",000");
            this.high = df.format(Double.parseDouble(high));
        }
    }
    public void setPriceDiff(String priceDiff){
        if(Double.parseDouble(priceDiff) > 0) {
            DecimalFormat df = new DecimalFormat(",000");
            this.priceDiff = df.format(Double.parseDouble(priceDiff));
        }
    }



}
