package io.dj.exchange.domain.primary;

import io.dj.exchange.enums.OrderStatus;
import io.dj.exchange.enums.OrderType;
import io.dj.exchange.util.DataUtil;
import lombok.Data;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by kun7788 on 2016. 8. 7..
 */
@Entity
@Data
@IdClass(MyHistoryOrdersPK.class)
public class MyHistoryOrders implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, unique = true)
    Long id;

    @Id
    @Column(name = "user_id", nullable = false)
    Long userId;

    @Id
    @Column(name = "to_user_id", nullable = false)
    Long toUserId;

    @Id
    @Column(name = "order_id", nullable = false)
    Long orderId;

    @OneToOne
    @JoinColumn(name="fromCoinName")
    private Coin fromCoin;

    @OneToOne
    @JoinColumn(name="toCoinName")
    private Coin toCoin;

    private LocalDateTime regDt;

    @Enumerated(EnumType.STRING)
    private OrderType orderType;

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @Digits(integer=24, fraction=8)
    @DecimalMin("0.00000000")
    private BigDecimal amount;

    @Transient
    private String amountTxt;

    @Digits(integer=24, fraction=8)
    @DecimalMin("0.00000000")
    private BigDecimal price;

    @Transient
    private String priceTxt;

    @Digits(integer=24, fraction=8)
    @DecimalMin("0.00000000")
    @Transient
    private BigDecimal totalPrice;

    @Transient
    @NumberFormat(style = NumberFormat.Style.CURRENCY)
    private String totalPriceTxt;

    private String dt;
    private LocalDateTime completedDt;

    public BigDecimal getTotalPrice() {
        return amount.multiply(price);
    }

    public void initAmountTxt() {
        //amountTxt =  DataUtil.decimal(amount);
        amountTxt =  DataUtil.decimal(amount, "#,##0.####");
    }

    public void initPriceTxt() {
        //priceTxt =  DataUtil.decimal(price);
        priceTxt =  DataUtil.decimal(price, "#,##0");
    }

    public void initTotalPriceTxt() {
        totalPriceTxt = DataUtil.decimal(amount.multiply(price), "#,##0");
    }
}
