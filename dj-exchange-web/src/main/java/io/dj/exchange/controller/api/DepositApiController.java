package io.dj.exchange.controller.api;

import io.dj.exchange.domain.dto.FeignResult;
import io.dj.exchange.domain.dto.FeignResultData;
import io.dj.exchange.domain.primary.Response;
import io.dj.exchange.domain.primary.Transactions;
import io.dj.exchange.domain.primary.User;
import io.dj.exchange.domain.primary.Wallet;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.repository.hibernate.primary.TransactionsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@Slf4j
@RestController
@RequestMapping("/api/deposit")
public class DepositApiController {

    @Autowired
    private ApiProvider apiProvider;

    @Autowired
    private TransactionsRepository transactionsRepository;

    @RequestMapping(value="/wallet", method=RequestMethod.POST)
    public Response<Wallet> wallet(@Valid @RequestBody Wallet wallet, @ModelAttribute("user") User user) {

        log.info("data : {}", apiProvider.getWallet(wallet.getCoinName()));

        FeignResult result = apiProvider.getWallet(wallet.getCoinName());

        return Response.<Wallet>builder()
                .code(CodeEnum.SUCCESS)
                .data(result.getResult().getWallet())
                .build();

    }

    @RequestMapping(value="/wallets", method=RequestMethod.POST)
    public Response<List<Wallet>> wallet(@PageableDefault(sort = { "id" }, direction = Sort.Direction.DESC, size = 30) Pageable p) throws Exception{

            FeignResult result = apiProvider.getWalletAll();

            FeignResultData data = result.getResult();

            return Response.<List<Wallet>>builder()
                    .code(CodeEnum.SUCCESS)
                    //.data(apiProvider.getWalletAll(headers).getResult().getWallets())
                    .build();

    }

    @PostMapping("/registWallet")
    public Response<Wallet> registWallet(@Valid @RequestBody Wallet wallet) {

        return Response.<Wallet>builder()
                .code(CodeEnum.SUCCESS)
                .data(apiProvider.registWallet(wallet.getCoinName()))
                .build();

    }

//    @PostMapping(value = "/console/reqWithdraws", produces = "application/json")
//    public PagedResources<ManualTransactions> reqWithdraws(Pageable p, PagedResourcesAssembler assembler){
//
//        Page<ManualTransactions> result = confirmRepository.findAllByIsConfirmationAndStatusAndCategoryOrderByRegDtDesc("N","PENDING", "send", p);
//
//        result.getContent().stream().forEach(data -> {
//            data.setRegDtTxt(data.getRegDt().format(formatter));
//
//        });
//
//        return assembler.toResource(result);
//    }

    @RequestMapping(value="/transactions", method=RequestMethod.POST, produces = "application/json")
    public PagedResources<Transactions> transactions(PagedResourcesAssembler assembler,
                                                     @RequestParam(required = false) String category,
                                                     @RequestParam(required = false) String coinName,
                                                     @ModelAttribute("user") User user,
                                                     Pageable p){

        Page<Transactions> result = transactionsRepository.findAllByCategoryAndUserIdOrderByDtDesc(category, user.getId(), p);

        for(Transactions t : result){

            t.initDtTxt();

        }

        //log.info("---------------{}", result.getContent());

        return assembler.toResource(result);

    }

//    @RequestMapping(value="/transactions", method=RequestMethod.POST)
//    public Response<List<Transactions>> transactions(@Valid @RequestBody Transactions transactions, @ModelAttribute("user") User user, @PageableDefault(sort = { "dt" }, direction = Sort.Direction.DESC, size = 30) Pageable p){
//
//        return Response.<List<Transactions>>builder()
//                .code(CodeEnum.SUCCESS)
//                //.data(apiProvider.getTransactions(transactions.getCategory(), p.getPageNumber(), p.getPageSize()).getResult().getTransactions())
//                .data(transactionsRepository.findAllByCategoryAndUserIdAndCoinName(transactions.getCategory(), user.getId(), transactions.getCoinName(), p).getContent())
//                //.data(transRepository.findAllByCategory(transactions.getCategory(), p).getContent())
//                .build();
//
//    }

}
