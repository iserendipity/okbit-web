package io.dj.exchange.config;


import com.warrenstrange.googleauth.GoogleAuthenticator;
import io.dj.exchange.filter.AjaxSessionTimeoutFilter;
import io.dj.exchange.filter.CsrfHeaderFilter;
import io.dj.exchange.security.GotpAuthenticationDetailsSource;
import io.dj.exchange.security.GotpAuthenticationFailureHandler;
import io.dj.exchange.security.GotpAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.servlet.Filter;

/**
 * Created by kun7788 on 2016. 10. 28..
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/doSignup", "/liveCheck", "/media/**", "/verify/**","/api/common/**", "/websock/**", "/findPassword", "/resetPassword", "/api/common/**", "/ad/**","/static/**", "/img/**", "/css/**", "/fonts/**", "/js/**", "/less/**", "/sass/**", "/vendor/**", "/").permitAll()
                .antMatchers("/confirm", "/confirm/**","/notices/**","/noticeDetail", "/api/chart/**").permitAll()
                .antMatchers("/admin").hasAuthority("ADMIN")
                .anyRequest().fullyAuthenticated()
                .and()
                .formLogin().authenticationDetailsSource(new GotpAuthenticationDetailsSource())
                .loginPage("/")
                .failureUrl("/?error=").failureHandler(new GotpAuthenticationFailureHandler())
                .loginProcessingUrl("/loginProcess").permitAll()
                .defaultSuccessUrl("/console")
                .usernameParameter("loginId")
                .passwordParameter("password")
                .permitAll()
                .and()
                .logout()
                .logoutUrl("/signOut").permitAll()
                .deleteCookies("remember-me")
                .deleteCookies("nowCoin")
                .deleteCookies("JSESSIONID").permitAll()
                .logoutSuccessUrl("/").permitAll()
                .permitAll()
                .and()
                .sessionManagement()
                .maximumSessions(1).maxSessionsPreventsLogin(false)
                .expiredUrl("/?error=Session_expired")
                .and()
                .sessionCreationPolicy(SessionCreationPolicy.NEVER).sessionFixation().changeSessionId()
                .invalidSessionUrl("/?error=Invalid_session")
                .and()
//                .rememberMe().rememberMeCookieName("remember-me")
//                .rememberMeParameter("remember")
//                .tokenValiditySeconds(15*60*60)
//                .and()
                .headers().frameOptions().disable()
                .and()
                .addFilterBefore(characterEncodingFilter(), CsrfFilter.class)
//                .addFilterBefore(new CharacterEncodingFilter("UTF-8", true), CsrfFilter.class)
                .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
                .addFilterAfter(new AjaxSessionTimeoutFilter(), ExceptionTranslationFilter.class)
                .csrf().csrfTokenRepository(csrfTokenRepository()).disable();


    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public Filter characterEncodingFilter() {
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        return filter;
    }

    private CsrfTokenRepository csrfTokenRepository() {
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName("X-XSRF-TOKEN");
        return repository;
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .authenticationProvider(authenticationProvider())
                .userDetailsService(userDetailsService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }


    @Bean
    AuthenticationProvider authenticationProvider() {
        GotpAuthenticationProvider authenticationProvider = new GotpAuthenticationProvider();
        authenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());
        authenticationProvider.setUserDetailsService(this.userDetailsService);
        authenticationProvider.setAuthenticator(googleAuthenticator());
        return authenticationProvider;
    }

    @Bean
    GoogleAuthenticator googleAuthenticator() {
        return new GoogleAuthenticator();
    }

}
