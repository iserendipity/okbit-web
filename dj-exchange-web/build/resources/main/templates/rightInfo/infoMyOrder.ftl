<script id="myOrderTemplate" type="text/x-jquery-tmpl">
    <tr>
        <td>${r"${orderType}"}</td>
        <td>${r"${regDt}"}</td>
        <td>${r"${price}"}</td>
        <td>${r"${amountRemaining}"}/${r"${amount}"}</td>
        <td><a href="#" onclick="javascript:cancel('${r"${id}"}', this);"><i class="fa fa-times"></i></a></td>
    </tr>
</script>
<div class="col-xs-12">
    <table class="table table-condensed table-hover text-center table-fixed">
        <thead class="text-center">
        <tr>
            <th class="col-xs-2"><@spring.message "TITLE_TYPE"/>
            </th>
            <th class="col-xs-3"><@spring.message "TITLE_DATE"/>
            </th>
            <th class="col-xs-3"><@spring.message "TITLE_PRICE"/>
            </th>
            <th class="col-xs-3"><@spring.message "TITLE_AMOUNT"/>
            </th>
            <th class="col-xs-1"><i class="fa fa-cog"></i>
            </th>
        </tr>

        </thead>
        <tbody id="myOrders" style="height:366px; overflow:scroll">
            <tr>
                <td colspan="5">
                    <@spring.message "EMPTY_DATA"/>
                </td>
            </tr>
        </tbody>
    </table>
</div>