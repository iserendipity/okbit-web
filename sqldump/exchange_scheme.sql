create table action_logs
(
	id bigint auto_increment
		primary key,
	guid varchar(100) not null,
	user_id bigint null,
	uri varchar(255) null,
	dt datetime null,
	ip varchar(100) null,
	req blob null,
	res blob null,
	error varchar(5000) null
)
;

create table admin_level
(
	user_id bigint not null,
	idcard varchar(255) null,
	idcard_yn varchar(10) null,
	bankbook varchar(255) null,
	bankbook_yn varchar(10) null,
	idcard_reg_dt datetime null,
	idcard_mod_dt datetime null,
	bankbook_reg_dt datetime null,
	bankbook_mod_dt datetime null,
	cellphone varchar(255) null,
	cellphone_yn varchar(10) null,
	real_name varchar(255) null,
	withdraw_bank_code varchar(50) null,
	withdraw_bank_nm varchar(50) null,
	withdraw_bank_account varchar(100) null,
	level_modify_dt datetime null,
	level varchar(30) null,
	di varchar(200) null,
	constraint admin_level_user_id_uindex
		unique (user_id)
)
;

create table bank_code
(
	code varchar(10) not null
		primary key,
	bank_name varchar(20) null
)
;

create table batch_offset
(
	offset_name varchar(50) not null
		primary key,
	offset_value bigint not null
)
;

create table block_explorer
(
	coin_name varchar(30) not null
		primary key,
	explorer_url varchar(200) null
)
;

create table coin
(
	name varchar(30) not null
		primary key,
	fee_percent decimal(32,8) null,
	mark varchar(255) null,
	min_confirmation int not null,
	priority int not null,
	unit varchar(255) null,
	min_amount decimal(32,8) null,
	auto_withdrawal_amount decimal(32,8) null,
	auto_collect_amount decimal(32,8) null,
	withdrawal_tx_fee decimal(32,8) null,
	once decimal(32,8) null,
	one_day decimal(32,8) null,
	low_limit decimal(32,8) null
)
;

create table constant
(
	name varchar(100) not null
		primary key,
	value varchar(512) not null
)
;

create table email_confirm
(
	hash_email varchar(80) not null
		primary key,
	confirm_code varchar(6) null,
	email varchar(50) null,
	del_dtm datetime null,
	send_yn varchar(10) default 'N' null,
	reg_dt datetime null
)
;

create table history_orders
(
	user_id bigint not null,
	order_id bigint not null,
	id bigint not null,
	amount decimal(32,8) null,
	completed_dt datetime null,
	dt varchar(255) null,
	order_type varchar(255) null,
	price decimal(32,8) null,
	reg_dt datetime null,
	status varchar(255) null,
	from_coin_name varchar(255) null,
	to_coin_name varchar(255) null,
	primary key (user_id, order_id, id),
	constraint FKieth9bwbuxubseoow9gmh9uvx
		foreign key (from_coin_name) references exchange.coin (name),
	constraint FK9phfogut0r7lwarljp51a0coa
		foreign key (to_coin_name) references exchange.coin (name)
)
;

create index FK9phfogut0r7lwarljp51a0coa
	on history_orders (to_coin_name)
;

create index FKieth9bwbuxubseoow9gmh9uvx
	on history_orders (from_coin_name)
;

create table manual_transaction
(
	tx_id varchar(150) null,
	user_id bigint not null,
	coin_name varchar(30) not null,
	category varchar(10) null,
	reg_dt datetime null,
	completed_dt datetime null,
	amount decimal(32,8) default '0.00000000' null,
	is_confirmation varchar(1) null,
	status varchar(10) null,
	reason varchar(255) null,
	account varchar(100) null,
	user_nm varchar(60) null,
	bank_nm varchar(60) null,
	bank_code varchar(30) null,
	address varchar(250) null,
	tag varchar(150) null,
	id varchar(150) not null,
	deposit_dt datetime null,
	primary key (id, user_id, coin_name)
)
;

create table market_history_orders
(
	id bigint auto_increment,
	user_id bigint not null,
	order_id bigint not null,
	amount decimal(32,8) null,
	completed_dt datetime null,
	dt varchar(255) null,
	order_type varchar(255) null,
	price decimal(32,8) null,
	reg_dt datetime null,
	status varchar(255) null,
	from_coin_name varchar(255) null,
	to_coin_name varchar(255) null,
	primary key (user_id, order_id, id),
	constraint market_history_orders_id_uindex
		unique (id),
	constraint FKficco4yldft62qi141fdk133r
		foreign key (from_coin_name) references exchange.coin (name),
	constraint FKjkgrji3bs5tw60bvvqf3cufwh
		foreign key (to_coin_name) references exchange.coin (name)
)
;

create index FKficco4yldft62qi141fdk133r
	on market_history_orders (from_coin_name)
;

create index FKjkgrji3bs5tw60bvvqf3cufwh
	on market_history_orders (to_coin_name)
;

create table my_history_orders
(
	id bigint auto_increment,
	user_id bigint not null,
	order_id bigint not null,
	amount decimal(32,8) null,
	completed_dt datetime null,
	dt varchar(255) null,
	order_type varchar(255) null,
	price decimal(32,8) null,
	reg_dt datetime null,
	status varchar(255) null,
	from_coin_name varchar(255) null,
	to_coin_name varchar(255) null,
	to_user_id bigint null,
	to_order_id bigint null,
	primary key (user_id, order_id, id),
	constraint my_history_orders_id_uindex
		unique (id),
	constraint FKripxngjayhjot1c8not7v19a6
		foreign key (from_coin_name) references exchange.coin (name),
	constraint FK4twra4ndo9ef2l78wk779yj5s
		foreign key (to_coin_name) references exchange.coin (name)
)
;

create index FK4twra4ndo9ef2l78wk779yj5s
	on my_history_orders (to_coin_name)
;

create index FKripxngjayhjot1c8not7v19a6
	on my_history_orders (from_coin_name)
;

create table orders
(
	user_id bigint not null,
	amount decimal(32,8) null,
	amount_remaining decimal(32,8) null,
	completed_dt datetime null,
	order_type varchar(255) null,
	price decimal(32,8) null,
	reg_dt datetime null,
	status varchar(255) null,
	from_coin_name varchar(255) null,
	to_coin_name varchar(255) null,
	cancel_dt datetime null,
	id bigint auto_increment
		primary key
)
;

create index FKj21c1p46tcvjbpaymef4xbjfe
	on orders (to_coin_name)
;

create index FKjao1n98lkxadyhny5dxf71qco
	on orders (from_coin_name)
;

create index orders_fk
	on orders (user_id)
;

create table otp_setting
(
	user_id bigint not null,
	use_login varchar(10) null,
	use_withdraw varchar(10) null,
	use_setting varchar(10) null
)
;

create index FKqcbuxv6yedrxh7ivhvwlp1rc9
	on otp_setting (user_id)
;

create table support_qna
(
	id bigint auto_increment
		primary key,
	title varchar(255) null,
	user_id bigint not null,
	reg_dt datetime null,
	mod_dt datetime null,
	contents varchar(2048) null,
	show_yn varchar(10) null,
	parent_id bigint null,
	category varchar(30) null,
	status varchar(20) null,
	read_yn varchar(20) null
)
;

create index FKq550smchblg35cle854yp7srq
	on support_qna (user_id)
;

create table token
(
	id bigint auto_increment
		primary key,
	access_token varchar(255) null,
	code varchar(255) null,
	expires_in varchar(255) null,
	fintech_use_num varchar(255) null,
	scope varchar(255) null,
	token_type varchar(255) null,
	user_seq_no varchar(255) null
)
;

create table user
(
	id bigint auto_increment
		primary key,
	email varchar(60) not null,
	pwd varchar(60) not null,
	active varchar(255) null,
	reg_dt datetime not null,
	role varchar(255) not null,
	constraint UK_ob8kqyqqgmefl0aco34akdtpe
		unique (email)
)
;

alter table admin_level
	add constraint FKjvq7jck1lj983mruwn1ofa3jm
		foreign key (user_id) references exchange.user (id)
;

alter table otp_setting
	add constraint FKqcbuxv6yedrxh7ivhvwlp1rc9
		foreign key (user_id) references exchange.user (id)
;

alter table otp_setting
	add constraint otp_setting_fk
		foreign key (user_id) references exchange.user (id)
			on update cascade on delete cascade
;

alter table support_qna
	add constraint FKq550smchblg35cle854yp7srq
		foreign key (user_id) references exchange.user (id)
;

create table user_setting
(
	user_id bigint not null
		primary key,
	api_key varchar(255) null,
	level varchar(255) null,
	otp_active varchar(255) null,
	otp_hash varchar(255) null,
	reg_dt datetime null,
	theme varchar(10) null,
	constraint UK_aq5q998x4b33hm6c0m6h6c6ox
		unique (user_id),
	constraint user_setting_fk
		foreign key (user_id) references exchange.user (id)
			on update cascade on delete cascade
)
;

create table wallet
(
	id bigint auto_increment,
	user_id bigint not null,
	address varchar(255) null,
	reg_dt datetime null,
	using_balance decimal(32,8) null,
	coin_name varchar(255) null,
	deposit_dvcd varchar(10) null,
	tag varchar(100) null,
	available_balance decimal(32,8) null,
	total_balance decimal(32,8) null,
	name varchar(255) null,
	primary key (user_id, id),
	constraint wallet_id_uindex
		unique (id),
	constraint wallet_user_id_id_pk
		unique (user_id, id),
	constraint FKmt42o0ph9vxa7fihva5h2xiyn
		foreign key (coin_name) references exchange.coin (name)
)
;

create index FKmt42o0ph9vxa7fihva5h2xiyn
	on wallet (coin_name)
;

create table wallet_transaction
(
	tx_id varchar(150) null,
	user_id bigint not null,
	coin_name varchar(30) not null,
	category varchar(10) null,
	address varchar(255) not null,
	dt datetime null,
	confirmation int null,
	status varchar(20) null,
	reason varchar(255) null,
	tag varchar(255) null,
	amount decimal(32,8) null,
	id varchar(150) not null,
	primary key (id, user_id, coin_name)
)
;

create table web_notices
(
	id bigint auto_increment
		primary key,
	title varchar(100) null,
	contents varchar(2048) null,
	dt datetime null,
	user_id bigint null,
	status varchar(20) null
)
;

create table wrong_deposit
(
	id bigint auto_increment
		primary key,
	amount bigint not null,
	status varchar(10) not null,
	reg_dt datetime null,
	mod_dt datetime null,
	deposit_dt datetime null,
	user_nm varchar(30) not null,
	bank_nm varchar(30) not null
)
;

