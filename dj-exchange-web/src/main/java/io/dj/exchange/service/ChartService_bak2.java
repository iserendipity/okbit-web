package io.dj.exchange.service;

import io.dj.exchange.annotation.TransactionalEx;
import io.dj.exchange.domain.cache.RedisChart;
import io.dj.exchange.domain.chart.History;
import io.dj.exchange.domain.dto.ChartDTO;
import io.dj.exchange.domain.primary.BatchOffset;
import io.dj.exchange.repository.cache.HistoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 23.
 * Description
 */
@Service
@Slf4j
public class ChartService_bak2 {

//    @Autowired
//    @Qualifier("redisTemplate")
//    private RedisTemplate template;

    @Autowired
    private HistoryRepository historyRepository;

//    @Autowired
//    MarketOrdersRepository marketOrdersRepository;
//
//    @Autowired
//    private BatchOffsetRepository batchOffsetRepository;

    @TransactionalEx
    public ChartDTO.History getHistory(String symbol, String resolution, long from, long to) throws Exception {

        ChartDTO.History chartHistory = new ChartDTO.History();

        //from - to 와 resolution을 가지고 ticker를 구한다.
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH");

        Date fromDt = new Date(from*1000);
        Date toDt = new Date(to*1000);

        BatchOffset bo = new BatchOffset();

        //두 날짜 사이의 모든 날짜와 시간을 구한다.
        ArrayList<String> dates = new ArrayList<>();
        Date currentDate = fromDt;
        while (currentDate.compareTo(toDt) <= 0) {
            dates.add(sdf.format(currentDate));
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(sdf.format(currentDate)));
            c.add(Calendar.HOUR_OF_DAY, 1);
            currentDate = c.getTime();
        }


        for(String date : dates){

//            log.info("------------------- :{}", date);

            RedisChart chart = historyRepository.findOneByTicker(date+"-"+symbol+"-"+resolution);

            log.info("--------------------- : {}", date+"-"+symbol+"-"+resolution);
            log.info("---------------------2 : {}", chart);
//            log.info("------------------- :{}", chart);
            if(chart != null && chart.getHistories() != null) {

                List<History> historyList = chart.getHistories();

                for(History history : historyList) {
//                    log.info("from :{}", from);
//                    log.info("to :{}", to);

                    if(history.getT() >= from && history.getT() < to) {

                        chartHistory.getL().add(history.getL());
                        chartHistory.getH().add(history.getH());
                        chartHistory.getO().add(history.getO());
                        chartHistory.getC().add(history.getC());
                        chartHistory.getV().add(history.getV());
                        chartHistory.getT().add(history.getT());

                    }

                }

                chartHistory.setS("ok");

            }

        }

//        log.info("chartHistory : {}", chartHistory);

        return chartHistory;
    }

//    public List<SumHistory> test() {
//
//        String resolution = "1";
//        DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//
//        List<SumHistory> result = marketOrdersRepository.getSumHistoryCoinsMin(0, new PageRequest(0, 100));
//        for(SumHistory sumHistory : result) {
//
//            String tickerDtFormatted = sumHistory.getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
//
//            try {
//                sumHistory.setT((dfm.parse(tickerDtFormatted)).getTime() / 1000);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return result;
//
//    }






}
