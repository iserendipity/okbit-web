var tickerSize = 0;
function loadTickers() {

    var colors = {black : 'BLACK', red : 'RED', blue : '#3582FF', white : 'WHITE', green : 'GREEN' };
    var pointers = { up : 'up', down : 'down'};

    var commonApi = new CommonApi();

    commonApi.tickers(function(data) {
        var result = data.data;
        tickerSize = result.length;//전체 지수의 개수를 구함

        var arr = [];

        $.each(result, function(i, v) {

            var color = colors.white;

            if(v.upDown == 'up'){
                v.color = colors.green;
                v.pointer = pointers.up;
            }else if(v.upDown =='down'){
                v.color = colors.red;
                v.pointer = pointers.down;
            }
            //console.log(v);
            v.width = result.length == 0 ? 100 : 100 / result.length;
            v.volume = v.vol;
            v.change = v.percent;
            v.changeDiff = v.priceDiff;
            v.price = v.lastPrice;
            v.coinName = v.unit + '/KRW';

            // if (v.change > 0) {
            //     color = colors.red;
            //     change = "+" + v.change;
            //     v.change = change;
            //     pointer = pointers.up;
            //
            // } else if (v.change < 0) {
            //     color = colors.blue;
            //     pointer = pointers.down;
            //
            // }
            //v.width = result.length == 0 ? 100 : 100 / result.length;
            arr.push(v);
        });

        //console.log(arr);

        $('#coinHolder').html($('#indicatorTemplate').tmpl(arr));
        //showRollingloadTickers();
    }, null);
};

setValues = function(targetId, jsonData){

    //폼의 name 과 obj의 키값이 동일한 경우 해당 폼에 값을 넣어준다.
    $('#'+targetId).find('input, select, textarea, div, span, td, a').each(function(){

        var name = $(this).attr('name');
        var cls = $(this).attr('class');

        var $element = $(this);

        $.each(jsonData, function(key, value){
            if(name == key){
                if($element.prop('tagName') === 'INPUT' | $element.prop('tagName') === 'SELECT' ){
                    //console.log('111111'+value);
                    $element.val(value);

                }else{
                    $element.text(value);

                }

            }else if(cls && cls.indexOf(key) > 0){

                if($element.prop('tagName') === 'INPUT' | $element.prop('tagName') === 'SELECT' ){
                    $element.val(value);

                }else{
                    $element.text(value);

                }

            }
        });
    });
};

getVolumes = function(){

    var commonApi = new CommonApi();

    commonApi.volumes(function(data) {

        setValues('volumes', data.data);

    });
};

var rollingCnt = 0;
function showRollingloadTickers() {

    var index = rollingCnt % tickerSize;

    $('.indicator-block-header > h5').eq(index).css('display', 'block');
    $('.indicator-block-header > h5').not(':eq('+index+')').css('display', 'none');

    rollingCnt ++;
}

loadTickers();

setInterval(function() {
    showRollingloadTickers();
}, 1800);

getVolumes();
