package io.dj.exchange.controller.api;

import io.dj.exchange.domain.dto.ChartDTO;
import io.dj.exchange.repository.hibernate.readonly.MarketOrdersRepository;
import io.dj.exchange.service.ChartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 22.
 * Description
 */
@RestController
@Slf4j
@RequestMapping("/api/chart")
public class ChartController {

    @Autowired
    private MarketOrdersRepository marketOrdersRepository;

    @Autowired
    private ChartService chartService;

    @GetMapping("/time")
    public Long time() {
        return new Date().getTime()/1000;
    }

    @GetMapping("/symbols")
    public ChartDTO.Symbol symbols(@RequestParam("symbol") String symbol) {
        return new ChartDTO.Symbol(symbol);
    }

    @GetMapping("/config")
    public ChartDTO.Config config() {
        return new ChartDTO.Config();
    }


    @GetMapping("/history")
    public ChartDTO.History history(@RequestParam("symbol") String symbol, @RequestParam("resolution") String resolution, @RequestParam("from") long from, @RequestParam("to") long to) throws Exception {
        return chartService.getHistory(symbol, resolution, from, to);
    }
//@GetMapping("/history")
//public String history(@RequestParam("symbol") String symbol, @RequestParam("resolution") String resolution, @RequestParam("from") long from, @RequestParam("to") long to) {
//
//
//    return "[[1495166400000,1952.4,1948.3,1956,1945.1,135.1723184],[1495164600000,1955.9,1952.4,1960.7,1948.7,238.8946169],[1495162800000,1942.6,1955.3,1961.7,1942.6,296.75150142],[1495161000000,1950.2,1942.5,1952.4,1940,311.32757244],[1495159200000,1959.5,1950.1,1962.8,1946,515.95135636],[1495157400000,1970.7,1959.2,1971.3,1955,507.8529166],[1495155600000,1971.6,1971.3,1979.6,1960.5,829.29049965],[1495153800000,1964.4,1971.1,1980,1961.7,444.2186066],[1495152000000,1941.9,1964.3,1964.3,1939.5,329.32752793],[1495150200000,1941.6,1941.5,1945.9,1935.2,234.71415716],[1495148400000,1946,1941,1954.6,1924.9,1190.266788],[1495146600000,1940.1,1945.8,1948,1939.2,331.4186151],[1495144800000,1937.1,1940.9,1940.9,1932.2,242.27103269],[1495143000000,1927.3,1938.9,1940,1924.1,388.69948667],[1495141200000,1926.3,1925.7,1929.4,1923.2,117.16211137],[1495139400000,1928.7,1927.1,1930,1924,211.61327491],[1495137600000,1920.9,1929.3,1929.4,1920.9,247.57855567],[1495135800000,1923.6,1920.9,1926.6,1920.6,74.41664631],[1495134000000,1923.8,1923.4,1926.6,1921.4,106.66458123],[1495132200000,1927,1925.7,1928.3,1921.2,210.60152569],[1495130400000,1913,1927,1930,1913,275.20630199],[1495128600000,1921.9,1913,1925,1910.1,372.45046103],[1495126800000,1923.1,1921.9,1926,1918,153.02729734],[1495125000000,1929,1922.6,1930.5,1921,138.02670195],[1495123200000,1919.4,1930.2,1930.4,1919.3,313.67578125],[1495121400000,1925.8,1918.1,1925.8,1917,217.10600509],[1495119600000,1927.5,1926.5,1932.2,1918.4,379.00957276],[1495117800000,1896.3,1927.5,1927.5,1896.3,873.98763905],[1495116000000,1886.3,1896.3,1905.9,1875.2,1435.58000078],[1495114200000,1876.5,1886.2,1889.6,1875.7,214.70793476],[1495112400000,1881.7,1876,1887,1876,201.14195638],[1495110600000,1886.6,1881.9,1886.6,1875,180.40931012],[1495108800000,1877.8,1887,1887.2,1877.3,325.57895785],[1495107000000,1870.1,1877.8,1879.1,1870.1,134.33158238],[1495105200000,1873.9,1870.2,1875.7,1870,88.63137868],[1495103400000,1870.3,1873.8,1878.5,1867.7,94.65479809],[1495101600000,1869.1,1867.8,1875.1,1867.2,88.50227273],[1495099800000,1878.6,1869.3,1878.6,1864.8,144.26369045],[1495098000000,1876.8,1878.6,1879,1866,223.99979992],[1495096200000,1883.3,1876.1,1883.3,1874.4,140.33357523],[1495094400000,1883.8,1882,1885.7,1880.8,89.68871036],[1495092600000,1881.1,1884,1885.6,1877.3,90.59630277],[1495090800000,1882.4,1881,1889.5,1881,171.05545236],[1495089000000,1869.1,1882.4,1885,1868.1,388.94127385],[1495087200000,1883.1,1870.4,1885,1865.8,367.024184],[1495085400000,1875,1882.9,1895.2,1874.9,643.80759985],[1495083600000,1880,1874.9,1880.9,1871.4,114.77930304],[1495081800000,1874.5,1880,1886.1,1871.2,125.84212956],[1495080000000,1863.1,1871.5,1874.3,1862.2,115.4780297],[1495078200000,1868.6,1861.7,1871.2,1861,302.0122006],[1495076400000,1886.9,1867.9,1888,1865.2,345.61374501],[1495074600000,1882.5,1886.9,1888,1879,113.21875922],[1495072800000,1883.5,1882.4,1887,1874.8,127.52666043],[1495071000000,1881.1,1883.6,1886.9,1879.6,117.62774864],[1495069200000,1874.1,1881,1885,1867,172.43986952],[1495067400000,1876.1,1870.8,1878.3,1869,146.44239519],[1495065600000,1870,1875,1877.6,1870,125.18828565],[1495063800000,1874.5,1870,1875,1867.1,101.92481405],[1495062000000,1874.9,1873.5,1875,1865.7,137.62772761],[1495060200000,1859.3,1875,1875,1854.8,326.00694148],[1495058400000,1852.1,1858.3,1864.5,1850.9,261.99944325],[1495056600000,1855.8,1852.5,1857,1852.2,288.61827539],[1495054800000,1853.1,1855.4,1856.9,1850,100.58832911],[1495053000000,1850.5,1850.2,1859.2,1849.2,247.56404184],[1495051200000,1867.2,1850.6,1869,1849.1,349.15369798],[1495049400000,1867.5,1867,1872.5,1867,48.7521908],[1495047600000,1875.2,1867.5,1875.9,1866.5,68.19533644],[1495045800000,1874.9,1874.5,1879.4,1869.9,228.94526633],[1495044000000,1862.9,1874.9,1875,1861,212.02296392],[1495042200000,1881.6,1862.5,1883.5,1862.1,264.19134708],[1495040400000,1880,1881.4,1883.7,1878.5,177.30548411],[1495038600000,1880.2,1879.8,1883,1875.5,120.92237754],[1495036800000,1879.7,1878.4,1885.8,1869.8,324.54112705],[1495035000000,1892.4,1879.7,1892.5,1873,202.34431585],[1495033200000,1889.9,1891.5,1894,1880.7,163.88279641],[1495031400000,1889.8,1889.8,1892,1889,242.87358912],[1495029600000,1878.9,1889,1890,1878.9,251.80108328],[1495027800000,1886.9,1879.1,1891.1,1875.7,504.54304854],[1495026000000,1871.9,1887.3,1890,1869.3,376.30131053],[1495024200000,1869.5,1870.5,1874,1864.5,230.03183275],[1495022400000,1861.9,1869.4,1870,1861,225.04687978],[1495020600000,1870.9,1859.3,1870.9,1844,507.4048293],[1495018800000,1882.2,1870.6,1885,1868.1,178.5439522],[1495017000000,1879.9,1882.8,1886.6,1877,414.28803729],[1495015200000,1872.8,1880,1880,1872.8,341.92908737],[1495013400000,1874.8,1873,1879,1866,322.07420125],[1495011600000,1850.4,1874.1,1874.8,1848.9,532.25368038],[1495009800000,1853.5,1849.2,1854.9,1847.4,178.67691979],[1495008000000,1858.1,1854,1865.5,1846.7,191.07017639],[1495006200000,1844.2,1858.1,1858.1,1844.2,221.13309188],[1495004400000,1854.5,1846.3,1866.9,1840.4,342.67399334],[1495002600000,1861.1,1854.5,1867,1851,415.86578939],[1495000800000,1848.8,1861.6,1868,1847.4,1008.79220791],[1494999000000,1841,1848.3,1849.1,1841,184.68898896],[1494997200000,1836.8,1841.7,1848.7,1835.1,113.33487412],[1494995400000,1843,1834.7,1849.1,1834.2,254.02829634],[1494993600000,1840.7,1842,1849.8,1839.1,410.74949099],[1494991800000,1845,1840.1,1847,1813.5,477.46695577],[1494990000000,1806.6,1845,1847.2,1804.7,1385.12643518],[1494988200000,1790.6,1805.6,1812,1790,1025.40302137],[1494986400000,1769,1790,1790.9,1766.3,589.10590354],[1494984600000,1773.5,1769,1789.9,1767.3,346.73981958],[1494982800000,1779.2,1775,1785.1,1768.5,369.63102864],[1494981000000,1779.5,1781,1788.8,1776.2,185.58369511],[1494979200000,1785.4,1779.5,1786.9,1767.9,391.77996936],[1494977400000,1780.7,1786.2,1786.4,1779.9,184.67335081],[1494975600000,1781.7,1781,1781.7,1778.1,91.82838364],[1494973800000,1781.5,1781.6,1786.3,1777.4,62.1824655],[1494972000000,1779.2,1781.1,1783.4,1771.8,149.51539412],[1494970200000,1790.9,1779.2,1791,1767.3,491.35052695],[1494968400000,1788.9,1790.9,1791,1788.8,133.37357877],[1494966600000,1789.5,1788.9,1790.6,1788.9,210.03447332],[1494964800000,1790.2,1789.5,1790.2,1786,211.46528688],[1494963000000,1789.4,1790.1,1793.2,1788.1,64.72464672],[1494961200000,1786.4,1789.4,1789.4,1784.2,98.85582168],[1494959400000,1782.8,1786.4,1789.4,1781.5,151.57279752],[1494957600000,1790.1,1782.5,1793,1779.3,155.61723637],[1494955800000,1790.6,1791.7,1791.8,1777.1,264.03689436],[1494954000000,1790,1790.6,1794.9,1783.2,204.8433058],[1494950400000,1793.3,1788.5,1793.9,1784.4,175.81165566]]";
//}

//    @RequestMapping(value="/config")
//    public Response<Config> config(@RequestParam Map<String, Object> params){
//
//        log.info("-------params config : {}", params);
//
//        Config config = new Config();
//        config.setSupports_search(true);
//        config.setSupports_group_request(false);
//        config.setSupports_marks(false);
//
////        Gson gson = new Gson();
////        ViewContorller.Customization config = new ViewContorller.Customization();
////        config.setSupports_search(true);
////        config.setSupports_group_request(false);
////        config.setSupports_marks(true);
////
////        ViewContorller.Exchange exchange = new ViewContorller.Exchange();
////        exchange.setValue("OK-BIT");
////        exchange.setName("OK-BIT");
////        exchange.setDesc("OK-BIT");
////
////        List<ViewContorller.Exchange> exArr = new ArrayList<>();
////        exArr.add(exchange);
////
////        config.setExchanges(exArr);
////
////        ViewContorller.SymbolsType sType = new ViewContorller.SymbolsType();
////        sType.setName("Stock");
////        sType.setValue("stock");
////
////        List<ViewContorller.SymbolsType> typeArr = new ArrayList<>();
////        typeArr.add(sType);
////
////        config.setSymbolsTypes(typeArr);
////
////        log.info("-------------config : {}", gson.toJson(config).toString());
//
//        return Response.<Config>builder().code(CodeEnum.SUCCESS).data(config).build();
//    }
//
//    @RequestMapping(value="/symbols")
//    public Response<Symbol> symbols(@RequestParam Map<String, Object> params){
//
//        log.info("-------params symbols : {}", params);
//
//        Symbol symbol = new Symbol();
//        symbol.setTicker("BITCOIN");
//        symbol.setCurrency_code("KRW");
//
//        return Response.<Symbol>builder().code(CodeEnum.SUCCESS).data(symbol).build();
//    }
//
//    @RequestMapping(value="/history")
//    public Response<History> history(@RequestParam Map<String, Object> params){
//
//        log.info("-------params history : {}", params);
//        long from = Long.parseLong(params.get("from").toString());
//        long to = Long.parseLong(params.get("to").toString());
//        //LocalDateTime.ofInstant(Instant.ofEpochMilli(from), ZoneId.systemDefault());
//
//        List<MarketHistoryOrders> result =  marketOrdersRepository.getSumMarketHistoryOrdersFromBetweenTo(params.get("symbol").toString(), LocalDateTime.ofInstant(Instant.ofEpochSecond(from), ZoneId.systemDefault()), LocalDateTime.ofInstant(Instant.ofEpochSecond(to), ZoneId.systemDefault()));
//
//
//
//        History history = new History();
//
//        if(result.isEmpty()){
//
//            history.setS("no_data");
//            history.setErrmsg("no data");
//            //history.setNextTime(to);
//
//        }else{
//
//            List<Long> tList = new ArrayList<>();
//            List<BigDecimal> oList = new ArrayList<>();
//            List<BigDecimal> cList = new ArrayList<>();
//            List<BigDecimal> vList = new ArrayList<>();
//            List<BigDecimal> lList = new ArrayList<>();
//            List<BigDecimal> hList = new ArrayList<>();
//
//            BigDecimal o = new BigDecimal(0);
//            BigDecimal c = new BigDecimal(0);
//
//            BigDecimal a = BigDecimal.valueOf(0);
//            BigDecimal b;
//
//            BigDecimal h = BigDecimal.valueOf(0);
//            BigDecimal l = BigDecimal.valueOf(0);
//
//            for(int i=0; i< result.size(); i++){
//                long t = result.get(i).getCompletedDt().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() / 1000;
//                tList.add(t);
//
//                BigDecimal price = result.get(i).getPrice();
//                BigDecimal amount = result.get(i).getAmount();
//
//                if(i == 0){
//                    o = price;
//                }
//
//                oList.add(o);
//
//                if(i == result.size()-1){
//                    c = price;
//                }
//
//                cList.add(c);
//
//                b = price.multiply(amount);
//
//                a = a.add(b);
//
//                vList.add(a);//볼륨총합
//
//                if(h.compareTo(price) != -1){
//                    h = price;
//                }
//
//                hList.add(h);
//
//                if(l.compareTo(price) == -1){
//                    l = price;
//                }
//
//                lList.add(l);
//
//            }
//
//
//            history.setS("ok");
//            history.setC(cList);
//            history.setH(hList);
//            history.setL(lList);
//            history.setO(oList);
//            history.setT(tList);
//            history.setV(vList);
//
//            log.info("----------history result : {}", history);
//
//        }
//
//        return Response.<History>builder().code(CodeEnum.SUCCESS).data(history).build();
//    }

}
