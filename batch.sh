#!/bin/sh -xe

echo "compile ....."
./gradlew clean build -x test

ssh -i /Users/jeongwoo/keyholder/okbit-web.pem ubuntu@13.124.146.73 "sudo cp /home/ubuntu/exchange-batch.jar /home/ubuntu/exchange-batch.jar.bak"

cd /Users/jeongwoo/IdeaProjects/okbit-web/dj-exchange-batch/build/libs

scp -i /Users/jeongwoo/keyholder/okbit-web.pem exchange-batch.jar ubuntu@13.124.146.73:/home/ubuntu

ssh -i /Users/jeongwoo/keyholder/okbit-web.pem ubuntu@13.124.146.73 "sudo nohup /data/services/startbatch.sh > /dev/null 2>&1 &"


