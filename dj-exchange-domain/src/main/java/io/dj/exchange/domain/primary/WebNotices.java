package io.dj.exchange.domain.primary;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 27.
 * Description
 */
@Data
@Entity
public class WebNotices implements Serializable{

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "contents")
    private String contents;

    @Column(name = "dt")
    private LocalDateTime dt;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "status")
    private String status;

    @Transient
    private String dtTxt;

}
