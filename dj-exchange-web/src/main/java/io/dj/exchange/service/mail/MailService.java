package io.dj.exchange.service.mail;

import io.dj.exchange.config.RabbitMQConfig;
import io.dj.exchange.domain.dto.mq.MessagePacket;
import io.dj.exchange.domain.primary.EmailConfirm;
import io.dj.exchange.domain.readonly.MarketHistoryOrders;
import io.dj.exchange.enums.CommandEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 24.
 * Description
 */
@Component
@Slf4j
public class MailService  {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMailQue(EmailConfirm confirmObj){

        MessagePacket mp = MessagePacket.builder().cmd(CommandEnum.MAIL).data(confirmObj).build();
        rabbitTemplate.setRoutingKey(RabbitMQConfig.QUEUE_NAME_MAIL);
        rabbitTemplate.setExchange(RabbitMQConfig.QUEUE_NAME_MAIL);
        rabbitTemplate.convertAndSend(mp);

    }

    public void sendPasswordResetMailQue(MarketHistoryOrders orders) {

        MessagePacket mp = MessagePacket.builder().cmd(CommandEnum.TRADE).data(orders).build();
        rabbitTemplate.setRoutingKey("message");
        rabbitTemplate.setExchange("message");
        rabbitTemplate.convertAndSend(mp);

    }

    public void sendTradeQue(Map<String, Object> dataMap, long userId) {

        MessagePacket mp = MessagePacket.builder().cmd(CommandEnum.ORDER).userId(userId).data(dataMap).build();
        rabbitTemplate.setRoutingKey("message");
        rabbitTemplate.setExchange("message");
        rabbitTemplate.convertAndSend(mp);

    }
}
