package io.dj.exchange.domain.dto;

import lombok.Data;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 19.
 * Description
 */
@Data
public class OtpRequest {

    Integer otpCode;

}
