package io.dj.exchange.enums;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 22.
 * Description
 */
public enum UserLevels {
    LEV1, LEV2, LEV3
}
