<#--<templateslate &ndash;&gt;-->
<script id="buyTemplate" type="text/x-jquery-tmpl">
    <tr data-price="${r"${price}"}">
        <td style="width:20%">${r"${count}"}</td>
        <td style="width:20%">${r"${totalPriceTxt}"}</td>
        <td style="width:20%">${r"${totalCoinTxt}"}</td>
        <td style="width:20%">${r"${amountTxt}"}</td>
        <td style="color:green; font-size:14px !important; width:20%; text-shadow:none !important; font-weight:900">${r"${priceTxt}"}</td>
    </tr>
</script>
<script id="sellTemplate" type="text/x-jquery-tmpl">
    <tr data-price="${r"${price}"}">
        <td style="color:red; font-size:14px !important; width:20%; text-shadow:none !important; font-weight:900">${r"${priceTxt}"}</td>
        <td style="width:20%">${r"${amountTxt}"}</td>
        <td style="width:20%">${r"${totalCoinTxt}"}</td>
        <td style="width:20%">${r"${totalPriceTxt}"}</td>
        <td style="width:20%">${r"${count}"}</td>
    </tr>
</script>
<div style="width:50%" class="pull-left scroll-table overflow">
    <table class="table table-condensed table-hover table-fixed pull-left table-responsive text-center">
        <thead>
        <tr>
            <th style="width:20%"></th>
            <th style="width:20%"></th>
            <th style="width:20%"></th>
            <th style="width:20%"></th>
            <th style="width:20%"></th>
        </tr>
        </thead>
        <tbody id="buyOrders">
            <tr>
                <td colspan="5" class="text-center">
                    <@spring.message "EMPTY_DATA"/>
                </td>
            </tr>
        </tbody>
    </table>

</div>
<div style="width:50%; " class="pull-right scroll-table overflow">
<table class="table table-condensed table-hover table-fixed table-responsive text-center">
    <thead>
    <tr>
        <th style="width:20%"></th>
        <th style="width:20%"></th>
        <th style="width:20%"></th>
        <th style="width:20%"></th>
        <th style="width:20%"></th>
    </tr>
    </thead>
    <tbody id="sellOrders">
        <tr>
            <td colspan="5" class="text-center">
            <@spring.message "EMPTY_DATA"/>
            </td>
        </tr>
    </tbody>
</table>
</div>