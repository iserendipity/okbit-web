<div class="tile">
    <div class="tile-title">
        2FA
    </div>
    <div class="overflow">
        <div class="row-fluid">
            <div class="col-xs-12">
                <h4 class="block-title">GOOGLE OTP</h4>
            </div>
            <div class="col-md-12 m-b-10">
                <div class="col-md-12 m-b-10">
                    <@spring.message "SECURITY_OTP_EXP"/>
                    <#--The first step is to download the Google Authenticator app for your Android or iOS device. If you need help getting started, please see Google's Support Page.-->
                    <br>
                    <br>
                        <@spring.message "SECURITY_OTP_EXP2"/>&nbsp;
                    <#--If you do not have access to the Android Market or App Store, there are other options for getting Google Authenticator:-->
                    <a style="color:blue; text-shadow:none; font-weight:bold; color:red" href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2">Android Download</a>,
                    <a style="color:blue; text-shadow:none; font-weight:bold; color:red" href="https://chrome.google.com/webstore/detail/authenticator/bhghoamapcdpbohphigoooaddinpkbai?hl=en">Google Chrome Plugin</a>, <@spring.message "SECURITY_OTP_EXP5"/>
                    <a style="color:blue; text-shadow:none; font-weight:bold; color:red" href="https://appsto.re/kr/fdakx.i">iTunes App Store</a>
                    <@spring.message "SECURITY_OTP_EXP7"/>
                    <br>
                </div>
            </div>
            <hr class="whiter">
            <div class="col-md-12 m-b-10 m-t-10">
            <#if (user.userSetting.otpActive)?? && (user.userSetting.otpActive) != 'Y'>
                <#if qrcode??>
                <div class="col-md-4">
                    <img src="${qrcode}" class="img-responsive" style="width:200px; height:200px;">
                    <p class="m-t-10">OTP Key : ${otpHash}
                    <br><span style="color:red"><@spring.message "SECURITY_OTP_CAUTION"/>
                            <br><@spring.message "SECURITY_OTP_CAUTION2"/></span></p>
                </div>

                </#if>
            </#if>
                <div class="col-md-8">
                    <#if otpActive?? && otpActive != 'Y'>
                    <p><span class="block-title"><@spring.message "SECURITY_OTP_HOWTO1"/></span></p>
                        <p class="col-md-12">1. <@spring.message "SECURITY_OTP_HOWTO4"/></p>
                        <p class="col-md-12">2. <@spring.message "SECURITY_OTP_HOWTO2"/></p>
                    <p class="col-md-12">3. <@spring.message "SECURITY_OTP_HOWTO3"/></p>

                        <div class="form-group col-md-4">
                            <input type="text" name="otpActive" id="otpActive" class="form-control" placeholder="OTP CODE">
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-alt btn-sm" onclick="javascript:activateOtp();"><@spring.message "BTN_ACTIVATE"/></button>
                        </div>
                    <#else>
                        <p><span class="block-title">2FA Using</span></p>
                        <form id="otpSettingForm" method="post">
                            <p class="col-md-3"><label>
                                <#if (otpSetting.useLogin)?? && otpSetting.useLogin = "Y">
                                    <input type="checkbox" name="useLogin" class="form-control otpSetting" checked value="Y">
                                <#else>
                                    <input type="checkbox" name="useLogin" class="form-control otpSetting" value="Y">
                                </#if>
                                <@spring.message "SECURITY_OTP_USE_LOGIN"/></label></p>
                            <p class="col-md-3"><label>
                                <#if (otpSetting.useWithdraw)?? && otpSetting.useWithdraw = "Y">
                                    <input type="checkbox" name="useWithdraw" class="form-control otpSetting" checked value="Y">
                                <#else>
                                    <input type="checkbox" name="useWithdraw" class="form-control otpSetting" value="Y">
                                </#if>
                                <@spring.message "SECURITY_OTP_USE_WITHDRAW"/>
                            </label></p>
                            <p class="col-md-3" ><label>
                                <#if (otpSetting.useSetting)?? && otpSetting.useSetting = "Y">
                                    <input type="checkbox" name="useSetting" class="form-control otpSetting" checked value="Y">
                                <#else>
                                    <input type="checkbox" name="useSetting" class="form-control otpSetting" value="Y">
                                </#if>
                                <@spring.message "SECURITY_OTP_USE_SETTING"/></label></p>
                            <button type="button" class="btn btn-alt btn-xs pull-right " onclick="javascript:changeOtpSetting();"><@spring.message "BTN_SAVE"/></button>
                        </form>
                    </#if>
                    <#include "../verification/gotp.ftl">
                </div>


            </div>
            <div class="clearfix"></div>
        </div>


    </div>
</div>