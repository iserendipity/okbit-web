<script id="myOrderTemplate" type="text/x-jquery-tmpl">
    <tr class="text-center">
        <td>${r"${orderType}"}</td>
        <td>${r"${regDtTxt}"}</td>
        <td>${r"${price}"}</td>
        <td>${r"${amountRemaining}"}/${r"${amount}"}</td>
        <td><a href="javascript:void(0);" onclick="javascript:cancel('${r"${id}"}', this);"><i class="fa fa-times"></i></a></td>
    </tr>
</script>
<div id="myOrderTab" style="width:100%" class="overflow scroll-table">
    <table class="table table-condensed table-hover table-fixed" style="font-weight:normal !important">
        <thead class="text-center">
        <tr>
            <th class="col-xs-2">
            </th>
            <th class="col-xs-4">
            </th>
            <th class="col-xs-2">
            </th>
            <th class="col-xs-3">
            </th>
            <th class="col-xs-1">
            </th>
        </tr>

        </thead>
        <tbody id="myOrders">
            <tr class="text-center">
                <td colspan="5">
                    <@spring.message "EMPTY_DATA"/>
                </td>
            </tr>
        </tbody>
    </table>
</div>