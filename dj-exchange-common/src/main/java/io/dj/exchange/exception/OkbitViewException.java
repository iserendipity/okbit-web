package io.dj.exchange.exception;

import io.dj.exchange.domain.dto.FeignResult;
import io.dj.exchange.enums.ErrorCode;
import io.dj.exchange.util.LogUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OkbitViewException extends Exception {

    public OkbitViewException(String desc) {
        super(desc);
        LogUtil.info(LogUtil.ANSI_RED, desc);
    }

    public OkbitViewException(ErrorCode code) {
        super(code.name());
        LogUtil.info(LogUtil.ANSI_RED, code.name());
    }

    public OkbitViewException(FeignResult result){

        super(result.getCode());
        LogUtil.info(LogUtil.ANSI_RED, result.getCode());


    }
}
