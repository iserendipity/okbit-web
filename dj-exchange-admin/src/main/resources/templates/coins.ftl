<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
    </head>
    <body id="skin-blur-chrome">

        <header id="header" class="media">
            <#include "common/top.ftl">
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            
            <!-- Sidebar -->
            <aside id="sidebar">

                <!-- Side Menu -->
                <ul class="list-unstyled side-menu">
                    <#include "common/sidemenu.ftl">
                </ul>

            </aside>
        
            <!-- Content -->
            <section id="content" class="container">
                <#include "common/messages.ftl">
                
                <!-- Notification Drawer -->
                <div id="notifications" class="tile drawer animated">
                    <div class="listview narrow">
                        <div class="media">
                            <a href="">Notification Settings</a>
                            <span class="drawer-close">&times;</span>
                        </div>
                        <div class="overflow" style="height: 254px">
                            <div class="media">
                                <div class="pull-left">
                                    <img width="40" src="/img/profile-pics/1.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <small class="text-muted">Nadin Jackson - 2 Hours ago</small><br>
                                    <a class="t-overflow" href="">Mauris consectetur urna nec tempor adipiscing. Proin sit amet nisi ligula. Sed eu adipiscing lectus</a>
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <img width="40" src="/img/profile-pics/2.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <small class="text-muted">David Villa - 5 Hours ago</small><br>
                                    <a class="t-overflow" href="">Suspendisse in purus ut nibh placerat Cras pulvinar euismod nunc quis gravida. Suspendisse pharetra</a>
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <img width="40" src="/img/profile-pics/3.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <small class="text-muted">Harris worgon - On 15/12/2013</small><br>
                                    <a class="t-overflow" href="">Maecenas venenatis enim condimentum ultrices fringilla. Nulla eget libero rhoncus, bibendum diam eleifend, vulputate mi. Fusce non nibh pulvinar, ornare turpis id</a>
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <img width="40" src="/img/profile-pics/4.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <small class="text-muted">Mitch Bradberry - On 14/12/2013</small><br>
                                    <a class="t-overflow" href="">Phasellus interdum felis enim, eu bibendum ipsum tristique vitae. Phasellus feugiat massa orci, sed viverra felis aliquet quis. Curabitur vel blandit odio. Vestibulum sagittis quis sem sit amet tristique.</a>
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <img width="40" src="/img/profile-pics/1.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <small class="text-muted">Nadin Jackson - On 15/12/2013</small><br>
                                    <a class="t-overflow" href="">Ipsum wintoo consectetur urna nec tempor adipiscing. Proin sit amet nisi ligula. Sed eu adipiscing lectus</a>
                                </div>
                            </div>
                            <div class="media">
                                <div class="pull-left">
                                    <img width="40" src="/img/profile-pics/2.jpg" alt="">
                                </div>
                                <div class="media-body">
                                    <small class="text-muted">David Villa - On 16/12/2013</small><br>
                                    <a class="t-overflow" href="">Suspendisse in purus ut nibh placerat Cras pulvinar euismod nunc quis gravida. Suspendisse pharetra</a>
                                </div>
                            </div>
                        </div>
                        <div class="media text-center whiter l-100">
                            <#--<a href=""><small>VIEW ALL</small></a>-->

                        </div>
                    </div>
                </div>
                
                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li class="active">Home</li>
                    <#--<li><a href="#">Library</a></li>-->
                    <#--<li class="active">Data</li>-->
                </ol>
                
                <h4 class="page-title">ADMIN CONSOLE</h4>
                                
                <!-- Shortcuts -->
                <div class="block-area shortcut-area">
                    <a class="shortcut tile" href="">
                        <img src="/img/shortcuts/BITCOIN.png" alt="">
                        <small class="t-overflow">BIT</small>
                    </a>
                    <a class="shortcut tile" href="">
                        <img src="/img/shortcuts/ETHEREUM.png" alt="">
                        <small class="t-overflow">ETH</small>
                    </a>
                    <a class="shortcut tile" href="">
                        <img src="/img/shortcuts/DASH.png" alt="">
                        <small class="t-overflow">DASH</small>
                    </a>
                    <a class="shortcut tile" href="">
                        <img src="/img/shortcuts/MONERO.png" alt="">
                        <small class="t-overflow">XMR</small>
                    </a>
                    <a class="shortcut tile" href="">
                        <img src="/img/shortcuts/LITECOIN.png" alt="">
                        <small class="t-overflow">LITE</small>
                    </a>
                    <#--<a class="shortcut tile" href="">-->
                        <#--<img src="/img/shortcuts/reports.png" alt="">-->
                        <#--<small class="t-overflow">Reports</small>-->
                    <#--</a>-->
                </div>
                
                <hr class="whiter" />
                
                <!-- Quick Stats -->
                <#--<div class="block-area">-->
                    <#--<div class="row">-->
                        <#--<div class="col-md-3 col-xs-6">-->
                            <#--<div class="tile quick-stats">-->
                                <#--<div id="stats-line-2" class="pull-left"></div>-->
                                <#--<div class="data">-->
                                    <#--<h2 data-value="98">0</h2>-->
                                    <#--<small>Tickets Today</small>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->

                        <#--<div class="col-md-3 col-xs-6">-->
                            <#--<div class="tile quick-stats media">-->
                                <#--<div id="stats-line-3" class="pull-left"></div>-->
                                <#--<div class="media-body">-->
                                    <#--<h2 data-value="1452">0</h2>-->
                                    <#--<small>Shipments today</small>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->

                        <#--<div class="col-md-3 col-xs-6">-->
                            <#--<div class="tile quick-stats media">-->

                                <#--<div id="stats-line-4" class="pull-left"></div>-->

                                <#--<div class="media-body">-->
                                    <#--<h2 data-value="4896">0</h2>-->
                                    <#--<small>Orders today</small>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->

                        <#--<div class="col-md-3 col-xs-6">-->
                            <#--<div class="tile quick-stats media">-->
                                <#--<div id="stats-line" class="pull-left"></div>-->
                                <#--<div class="media-body">-->
                                    <#--<h2 data-value="29356">0</h2>-->
                                    <#--<small>Site visits today</small>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->

                    <#--</div>-->

                <#--</div>-->

                <#--<hr class="whiter" />-->
                
                <!-- Main Widgets -->
               
                <div class="block-area">
                    <div class="row">
                        <div class="col-md-9">
                            <!-- Main Chart -->
                            <div class="tile">
                                <h2 class="tile-title">Statistics</h2>
                                <div class="p-10">
                                    <div id="line-chart" class="main-chart" style="height: 250px"></div>

                                    <div class="chart-info">
                                        <ul class="list-unstyled">
                                            <li class="m-b-10">
                                                Total Sales 1200
                                                <span class="pull-right text-muted t-s-0">
                                                    <i class="fa fa-chevron-up"></i>
                                                    +12%
                                                </span>
                                            </li>
                                            <li>
                                                <small>
                                                    Local 640
                                                    <span class="pull-right text-muted t-s-0"><i class="fa m-l-15 fa-chevron-down"></i> -8%</span>
                                                </small>
                                                <div class="progress progress-small">
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <small>
                                                    Foreign 560
                                                    <span class="pull-right text-muted t-s-0">
                                                        <i class="fa m-l-15 fa-chevron-up"></i>
                                                        -3%
                                                    </span>
                                                </small>
                                                <div class="progress progress-small">
                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 60%"></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>  
                            </div>
                            <!--  ORDERS -->
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="tile">
                                        <h2 class="tile-title">ORDERS
                                        </h2>
                                        <div class="overflow">
                                            1111


                                        </div>
                                        <div class="panel-footer">
                                            <a href="" class="nb pull-right" style="font-size:8px;">Full Book</a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <!-- TRADES -->
                                <div class="col-md-5">
                                    <div class="tab-container tile">
                                        <h2 class="tile-title">TRADES
                                        </h2>
                                        <div class="tile-config dropdown">
                                            <#--<a data-toggle="dropdown" href="" class="tile-menu"></a>-->
                                            <#--<ul class="dropdown-menu animated pull-right text-right">-->
                                                <#--<li><a href="">Market</a></li>-->
                                                <#--<li><a href="">Mine</a></li>-->
                                            <#--</ul>-->
                                            <ul class="nav tab nav-tabs tab-right tab-xs">
                                                <li><a href="#mine">MINE</a></li>
                                                <li class="active"><a href="#market">MARKET</a></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="trades" class="tab-content no-padding overflow">
                                            <!-- Market tab -->
                                            <div class="tab-pane active" id="market">
                                                <div class="fixed-thead-wrap col-xs-12">
                                                    <div class="fixed-thead col-xs-1">&nbsp;</div>
                                                    <div class="fixed-thead col-xs-4">TIME</div>
                                                    <div class="fixed-thead col-xs-4">PRICE</div>
                                                    <div class="fixed-thead col-xs-3">AMOUNT</div>
                                                </div>
                                                <div id="trade" class="scroll-table overflow col-xs-12">
                                                    <table class="table table-condensed table-hover table-fixed">
                                                        <thead style="height:0px;">
                                                        <tr>
                                                            <th class="col-xs-1">
                                                            </th>
                                                            <th class="col-xs-4">
                                                            </th>
                                                            <th class="col-xs-4">
                                                            </th>
                                                            <th class="col-xs-3">
                                                            </th>
                                                        </tr>

                                                        </thead>
                                                        <tbody style="height:366px; overflow:scroll">
                                                            <#list 1..25 as i>
                                                            <tr>
                                                                <td><i class="fa fa-chevron-up"></i></td>
                                                                <td>16:02:45</td>
                                                                <td>1,214.5</td>
                                                                <td>0.39</td>
                                                            </tr>
                                                            </#list>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <a href="" class="nb pull-right" style="font-size:8px;">Full History</a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="col-md-3">

                            <!-- Balance -->
                                <script id="balanceTemplate" type="text/x-jquery-tmpl">
                                    <tr>
                                        <td style="width:40px" class="visible-lg balanceIcon">
                                            <img src="/img/shortcuts/${r"${coin.name}"}.png" alt="${r"${coin.name}"}">
                                        </td>
                                        <td style="width:30%">
                                            ${r"${coin.name}"}
                                        </td>
                                        <td>
                                            Real Balance : ${r"${realBalanceTxt}"} ${r"${coin.unit}"}
                                        </td>
                                    </tr>
                                </script>
                                <div id="balance" class="tile">
                                    <h2 class="tile-title">Balance</h2>
                                    <div class="tile-config dropdown">
                                        <a data-toggle="dropdown" href="" class="tile-menu"></a>
                                        <ul class="dropdown-menu pull-right text-right">
                                            <li><a href="">Refresh</a></li>
                                        </ul>
                                    </div>
                                    <div class="overflow">
                                        <table id="balanceTab" class="table table-responsive">
                                            <tbody id="balanceBody">
                                            <tr>
                                                <td>
                                                    지갑이 없습니다.
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            <#--<div class="clearfix"></div>-->
                                <script id="coinTemplate" type="text/x-jquery-tmpl">
                                    <tr>
                                        <td style="width:40px" class="balanceIcon">
                                            <img src="/img/shortcuts/${r"${name}"}.png" alt="${r"${name}"}">
                                        </td>
                                        <td>
                                            <input type="text" value="${r"${minConfirmation}"}" class="form-control input-sm" name="minConfirmation">
                                        </td>
                                        <td>
                                            <input type="text" value="${r"${autoCollectAmount}"}" class="form-control input-sm" name="autoCollectAmount">
                                        </td>
                                        <td>
                                            <input type="text" value="${r"${feePercent}"}" class="form-control input-sm" name="feePercent">
                                        </td>
                                        <td>
                                            <input type="text" value="${r"${withdrawalTxFee}"}" class="form-control input-sm" name="withdrawalTxFee">
                                        </td>
                                        <td><button type="button" onclick="javascript:modifyCoin(this);" class="btn btn-primary btn-sm btn-alt">저장</button></td>
                                    </tr>
                                </script>
                                <div class="tile">
                                    <h2 class="tile-title">COINS</h2>
                                    <#--<div class="tile-config dropdown">-->
                                        <#--<a data-toggle="dropdown" href="" class="tile-menu"></a>-->
                                        <#--<ul class="dropdown-menu pull-right text-right">-->
                                            <#--<li><a href="">Refresh</a></li>-->
                                        <#--</ul>-->
                                    <#--</div>-->
                                    <div class="overflow">
                                        <table class="table table-responsive">
                                            <thead>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>최소 승인
                                                    </td>
                                                    <td>자동수집량
                                                    </td>
                                                    <td>거래수수료(%)
                                                    </td>
                                                    <td>출금수수료
                                                    </td>
                                                    <td><i class="fa fa-cog"></i>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody id="coinBody">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <!-- Chat -->
                <div class="chat">
                    
                    <!-- Chat List -->
                    <div class="pull-left chat-list">
                        <div class="listview narrow">
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/1.jpg" width="30" alt="">
                                <div class="media-body p-t-5">
                                    Alex Bendit
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/2.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">David Volla Watkinson</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/3.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Mitchell Christein</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/4.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Wayne Parnell</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/5.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Melina April</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/6.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Ford Harnson</span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <!-- Chat Area -->
                    <div class="media-body">
                        <div class="chat-header">
                            <a class="btn btn-sm" href="">
                                <i class="fa fa-circle-o status m-r-5"></i> Chat with Friends
                            </a>
                        </div>
                    
                        <div class="chat-body">
                            <div class="media">
                                <img class="pull-right" src="/img/profile-pics/1.jpg" width="30" alt="" />
                                <div class="media-body pull-right">
                                    Hiiii<br/>
                                    How you doing bro?
                                    <small>Me - 10 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-left">
                                <img class="pull-left" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    I'm doing well buddy. <br/>How do you do?
                                    <small>David - 9 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-right">
                                <img class="pull-right" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    I'm Fine bro <br/>Thank you for asking
                                    <small>Me - 8 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-right">
                                <img class="pull-right" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    Any idea for a hangout?
                                    <small>Me - 8 Mins ago</small>
                                </div>
                            </div>
                        
                        </div>
                    
                        <div class="chat-footer media">
                            <i class="chat-list-toggle pull-left fa fa-bars"></i>
                            <i class="pull-right fa fa-picture-o"></i> 
                            <div class="media-body">
                                <textarea class="form-control" placeholder="Type something..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Older IE Message -->
            <!--[if lt IE 9]>
                <div class="ie-block">
                    <h1 class="Ops">Ooops!</h1>
                    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser in order to access the maximum functionality of this website. </p>
                    <ul class="browsers">
                        <li>
                            <a href="https://www.google.com/intl/en/chrome/browser/">
                                <img src="/img/browsers/chrome.png" alt="">
                                <div>Google Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.mozilla.org/en-US/firefox/new/">
                                <img src="/img/browsers/firefox.png" alt="">
                                <div>Mozilla Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com/computer/windows">
                                <img src="/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://safari.en.softonic.com/">
                                <img src="/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-10/worldwide-languages">
                                <img src="/img/browsers/ie.png" alt="">
                                <div>Internet Explorer(New)</div>
                            </a>
                        </li>
                    </ul>
                    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
                </div>   
            <![endif]-->
        </section>
        
        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->
        <script src="/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
        <script src="/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Charts -->
        <script src="/js/charts/jquery.flot.js"></script> <!-- Flot Main -->
        <script src="/js/charts/jquery.flot.time.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen -->

        <script src="/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts -->
        <script src="/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts -->
        <script src="/js/charts.js"></script> <!-- All the above chart related functions -->

        <!--scroll -->
        <script src="/js/scroll.min.js"></script>

        <!--  Form Related -->
        <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->

        <!-- UX -->
        <#--<script src="/js/scroll.min.js_bak"></script> <!-- Custom Scrollbar &ndash;&gt;-->

        <!-- Jqutemplateslate -->
        <script src="/js/jquery.tmpl.min.js"></script>

        <!-- Other -->
        <script src="/js/calendar.min.js"></script> <!-- Calendar -->
        <script src="/js/feeds.min.js"></script> <!-- News Feeds -->

        <script src="/js/jquery.loading.min.js"></script> <!-- loading -->

        <!-- All JS functions -->
        <script src="/js/functions.js"></script>
        <script src="/js/api/admin_api.js"></script>
        <script src="/js/view/admin.js"></script>

        <script type="text/javascript">

            $('document').ready(function(){
               init();
            });

        </script>


    </body>
</html>
