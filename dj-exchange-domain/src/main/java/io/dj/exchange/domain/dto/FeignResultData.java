package io.dj.exchange.domain.dto;

import io.dj.exchange.domain.primary.Coin;
import io.dj.exchange.domain.primary.Transactions;
import io.dj.exchange.domain.primary.Wallet;
import lombok.Data;

import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 28.
 * Description
 */
@Data
public class FeignResultData {

    private Wallet wallet;
    private List<Wallet> wallets;
    private Coin coin;
    private List<Coin> coins;
//    private Orders order;
//    private List<Orders> orders;
    private Transactions transaction;
    private List<Transactions> transactions;

}
