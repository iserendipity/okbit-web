<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <style type="text/css">
        .modal-backdrop
        {
            background-color:transparent !important;
        }
        /*.modal-backdrop {*/
            /*background-color: transparent;*/
        /*}*/
        /*.modal-backdrop-transparent {*/
            /*position: fixed;*/
            /*top: 0;*/
            /*right: 0;*/
            /*bottom: 0;*/
            /*left: 0;*/
            /*background-color: transparent;*/
            /*opacity: 0;*/
            /*width: 100%;*/
            /*height: 100%;*/
            /*z-index: -1000;*/
        /*}*/
    </style>
        <#include "common/head.ftl">
    </head>
    <body id="skin-blur-ocean">

        <header id="header" class="media">
            <#include "common/top.ftl">
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            
            <!-- Sidebar -->
            <aside id="sidebar">

                <!-- Side Menu -->
                <ul class="list-unstyled side-menu">
                    <#include "common/sidemenu.ftl">
                </ul>

            </aside>
        
            <!-- Content -->
            <section id="content" class="container">

                <div id="modal" class="modal fade"> <!-- Style for just preview -->
                    <div id="modal-dialog" class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    ×
                                </button>
                                <h4 class="modal-title">통장정보 저장</h4>
                            </div>
                            <div class="modal-body">
                                <#--<form role="form" id="bankForm" class="form-validation-2" method="post">-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="withdrawBankNm">은행명</label>-->
                                        <#--<input type="text" name="withdrawBankNm" id="withdrawBankNm" class="form-control" placeholder="은행명을 입력하세요">-->
                                    <#--</div>-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="withdrawBankAccount">계좌번호</label>-->
                                        <#--<input type="text" name="withdrawBankAccount" id="withdrawBankAccount" class="form-control validate[required,custom[number]]" placeholder="계좌번호를 입력하세요">-->
                                    <#--</div>-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="realNm">예금주</label>-->
                                        <#--<input type="text" name="realNm" id="realNm" class="form-control" placeholder="은행명을 입력하세요">-->
                                    <#--</div>-->
                                    <#--<input type="hidden" name="userId">-->
                                <#--</form>-->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm" onclick="javascript:approveBank();">Send</button>
                                <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


            <#include "common/messages.ftl">

                <div class="clearfix"></div>

                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li><a href="/console">Home</a></li>
                    <li class="active">Verify</li>
                    <#--<li class="active">Data</li>-->
                </ol>
                
                <h4 class="page-title">&nbsp;</h4>
                                
                <div class="block-area">
                    <div class="row">
                        <div class="col-md-12">
                            <!--  ORDERS -->
                            <div class="row">
                                <div class="col-md-3">
                                    <script id="verifyBankTemplate" type="text/x-jquery-tmpl">
                                    <tr>
                                        <td onclick="javascript:getVerifyBankPreview('${r"${userId}"}','${r"${email}"}', '${r"${realName}"}');">${r"${bankbookRegDtTxt}"}</td>
                                        <td onclick="javascript:getVerifyBankPreview('${r"${userId}"}','${r"${email}"}', '${r"${realName}"}');">${r"${bankbookYn}"}</td>
                                        <td onclick="javascript:getVerifyBankPreview('${r"${userId}"}','${r"${email}"}', '${r"${realName}"}');">${r"${email}"}</td>
                                        <td onclick="javascript:getVerifyBankPreview('${r"${userId}"}','${r"${email}"}', '${r"${realName}"}');">${r"${realName}"}</td>
                                    </tr>
                                    </script>
                                    <div class="tile">
                                        <h2 class="tile-title">통장사본 인증
                                        </h2>
                                        <div class="overflow">
                                            <table class="table table-hover table-condensed text-center">
                                                <thead>
                                                    <tr>
                                                        <td>인증 요청일</td>
                                                        <td>승인여부</td>
                                                        <td>ID</td>
                                                        <td>실명</td>
                                                    </tr>
                                                </thead>
                                                <tbody id="verifyReqBody">
                                                    <tr class="text-center">
                                                        <td colspan="4">데이터가 없습니다.</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="panel-footer text-center">

                                            <ul id="paging" class="pagination pagination-sm">
                                                <li class="active"><a href="javascript:getReqBankVerify('1');">1</a></li>
                                            </ul>

                                            <#--<a href="" class="nb pull-right" style="font-size:8px;">Full Book</a>-->
                                            <#--<div class="clearfix"></div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="tile">
                                        <h2 class="tile-title">통장 미리보기
                                        </h2>
                                        <div class="overflow">
                                            <div class="preview" style="height:400px;" id="preview">

                                            </div>
                                        </div>
                                        <#--<div class="panel-footer">-->
                                            <#--&lt;#&ndash;<a href="#modal" data-toggle="modal" data-target="#modal" class="nb pull-right" style="font-size:8px;">통장 승인</a>&ndash;&gt;-->
                                            <#--<div class="clearfix"></div>-->
                                        <#--</div>-->
                                    </div>
                                    <div class="tile">
                                        <h2 class="tile-title">통장 정보입력
                                            <span id="bankPreviewInfo" class="pull-right"></span>
                                        </h2>
                                        <div class="overflow">
                                            <div class="col-md-12">
                                                <form role="form" id="bankForm" class="form-validation-2" method="post">
                                                    <div class="form-group">
                                                        <label for="withdrawBankCode">은행선택</label>
                                                        <select name="withdrawBankCode" id="withdrawBankCode" class="form-control validate[required]">
                                                            <option value="">선택하세요.</option>
                                                            <#list bankList as b>
                                                                <option value="${b.code}">${b.bankName}</option>
                                                            </#list>
                                                        </select>
                                                    </div>
                                                    <#--<div class="form-group">-->
                                                        <#--<label for="withdrawBankNm">은행명</label>-->
                                                        <#--<input type="text" name="withdrawBankNm" id="withdrawBankNm" class="form-control" placeholder="은행명을 입력하세요">-->
                                                    <#--</div>-->
                                                    <div class="form-group">
                                                        <label for="withdrawBankAccount">계좌번호</label>
                                                        <input type="text" name="withdrawBankAccount" id="withdrawBankAccount" class="form-control validate[required,custom[number]]" placeholder="계좌번호를 입력하세요">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="realNm">예금주</label>
                                                        <input type="text" name="realNm" id="realNm" class="form-control" placeholder="예금주를 입력하세요">
                                                    </div>
                                                    <input type="hidden" name="userId">
                                                </form>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <button type="button" class="btn btn-sm" onclick="javascript:approveBank();">통장 승인</button>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="tile">
                                        <h2 class="tile-title">신분증 미리보기
                                        </h2>

                                        <div class="overflow">
                                            <div class="preview" style="height:400px;" id="idCardPreview">

                                            </div>
                                        </div>
                                        <#--<div class="panel-footer">-->

                                            <#--<a href="#" onclick="javascript:approveId();" class="nb pull-right" style="font-size:8px;">신분증 승인</a>-->
                                            <#--<div class="clearfix"></div>-->
                                        <#--</div>-->
                                    </div>
                                    <div class="tile">
                                        <h2 class="tile-title">신분증 승인
                                            <span id="idPreviewInfo" class="pull-right"></span>
                                        </h2>
                                        <div class="overflow">
                                            <div class="col-md-12">
                                                <form role="form" id="idForm" class="form-validation-2" method="post">
                                                    <input type="hidden" name="userId">
                                                    <div class="form-group">
                                                        <input type="text" name="realName" class="form-control" placeholder="이름을 입력하세요">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    <div class="panel-footer">
                                        <a href="#" onclick="javascript:approveId();" class="nb pull-right" style="font-size:8px;">신분증 승인</a>
                                        <div class="clearfix"></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <script id="verifyIdTemplate" type="text/x-jquery-tmpl">
                                    <tr>
                                        <td onclick="javascript:getVerifyIdPreview('${r"${userId}"}','${r"${email}"}', '${r"${realName}"}');">${r"${idcardRegDtTxt}"}</td>
                                        <td onclick="javascript:getVerifyIdPreview('${r"${userId}"}','${r"${email}"}', '${r"${realName}"}');">${r"${idcardYn}"}</td>
                                        <td onclick="javascript:getVerifyIdPreview('${r"${userId}"}','${r"${email}"}', '${r"${realName}"}');">${r"${email}"}</td>
                                        <td onclick="javascript:getVerifyIdPreview('${r"${userId}"}','${r"${email}"}', '${r"${realName}"}');">${r"${realName}"}</td>
                                    </tr>
                                    </script>
                                    <div class="tile">
                                        <h2 class="tile-title">신분증 인증
                                        </h2>
                                        <div class="overflow">
                                            <table class="table table-hover table-condensed text-center">
                                                <thead>
                                                <tr>
                                                    <td>인증 요청일</td>
                                                    <td>승인여부</td>
                                                    <td>ID</td>
                                                    <td>실명</td>
                                                </tr>
                                                </thead>
                                                <tbody id="verifyReqIdcardBody">
                                                <tr class="text-center">
                                                    <td colspan="4">데이터가 없습니다.</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="panel-footer text-center">

                                            <ul id="idCardPaging" class="pagination pagination-sm">
                                                <li class="active"><a href="javascript:getReqIdcardVerify('1');">1</a></li>
                                            </ul>

                                        <#--<a href="" class="nb pull-right" style="font-size:8px;">Full Book</a>-->
                                        <#--<div class="clearfix"></div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
                <!-- Chat -->
                <div class="chat">
                    
                    <!-- Chat List -->
                    <div class="pull-left chat-list">
                        <div class="listview narrow">
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/1.jpg" width="30" alt="">
                                <div class="media-body p-t-5">
                                    Alex Bendit
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/2.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">David Volla Watkinson</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/3.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Mitchell Christein</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/4.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Wayne Parnell</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/5.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Melina April</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/6.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Ford Harnson</span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <!-- Chat Area -->
                    <div class="media-body">
                        <div class="chat-header">
                            <a class="btn btn-sm" href="">
                                <i class="fa fa-circle-o status m-r-5"></i> Chat with Friends
                            </a>
                        </div>
                    
                        <div class="chat-body">
                            <div class="media">
                                <img class="pull-right" src="/img/profile-pics/1.jpg" width="30" alt="" />
                                <div class="media-body pull-right">
                                    Hiiii<br/>
                                    How you doing bro?
                                    <small>Me - 10 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-left">
                                <img class="pull-left" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    I'm doing well buddy. <br/>How do you do?
                                    <small>David - 9 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-right">
                                <img class="pull-right" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    I'm Fine bro <br/>Thank you for asking
                                    <small>Me - 8 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-right">
                                <img class="pull-right" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    Any idea for a hangout?
                                    <small>Me - 8 Mins ago</small>
                                </div>
                            </div>
                        
                        </div>
                    
                        <div class="chat-footer media">
                            <i class="chat-list-toggle pull-left fa fa-bars"></i>
                            <i class="pull-right fa fa-picture-o"></i> 
                            <div class="media-body">
                                <textarea class="form-control" placeholder="Type something..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Older IE Message -->
            <!--[if lt IE 9]>
                <div class="ie-block">
                    <h1 class="Ops">Ooops!</h1>
                    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser in order to access the maximum functionality of this website. </p>
                    <ul class="browsers">
                        <li>
                            <a href="https://www.google.com/intl/en/chrome/browser/">
                                <img src="/img/browsers/chrome.png" alt="">
                                <div>Google Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.mozilla.org/en-US/firefox/new/">
                                <img src="/img/browsers/firefox.png" alt="">
                                <div>Mozilla Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com/computer/windows">
                                <img src="/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://safari.en.softonic.com/">
                                <img src="/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-10/worldwide-languages">
                                <img src="/img/browsers/ie.png" alt="">
                                <div>Internet Explorer(New)</div>
                            </a>
                        </li>
                    </ul>
                    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
                </div>   
            <![endif]-->
        </section>

        <form id="loadingForm"></form>

        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->
        <script src="/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
        <script src="/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Charts -->
        <#--<script src="/js/charts/jquery.flot.js"></script> <!-- Flot Main &ndash;&gt;-->
        <#--<script src="/js/charts/jquery.flot.time.js"></script> <!-- Flot sub &ndash;&gt;-->
        <#--<script src="/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub &ndash;&gt;-->
        <#--<script src="/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen &ndash;&gt;-->

        <#--<script src="/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts &ndash;&gt;-->
        <#--<script src="/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts &ndash;&gt;-->
        <#--<script src="/js/charts.js"></script> <!-- All the above chart related functions &ndash;&gt;-->

        <!--scroll -->
        <script src="/js/scroll.min.js"></script>

        <!--  Form Related -->
        <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->

        <!-- UX -->
        <#--<script src="/js/scroll.min.js_bak"></script> <!-- Custom Scrollbar &ndash;&gt;-->

        <!-- Jqutemplateslate -->
        <script src="/js/jquery.tmpl.min.js"></script>

        <!-- Other -->
        <script src="/js/calendar.min.js"></script> <!-- Calendar -->
        <script src="/js/feeds.min.js"></script> <!-- News Feeds -->

        <script src="/js/jquery.loading.min.js"></script> <!-- loading -->

        <script src="js/validation/validate.min.js"></script> <!-- jQuery Form Validation Library -->
        <script src="js/validation/validationEngine.min.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->

        <!-- All JS functions -->
        <script src="/js/functions.js"></script>
        <script src="/js/api/admin_api.js"></script>
        <script src="/js/view/admin.js"></script>

        <script type="text/javascript">

            $('document').ready(function(){

                //getReqWithdraw('1');
                getReqBankVerify('1');
                getReqIdcardVerify('1');

                $('#modal-dialog').draggable({
                   handle : '.modal-header'
                });

            });

        </script>
    </body>
</html>
