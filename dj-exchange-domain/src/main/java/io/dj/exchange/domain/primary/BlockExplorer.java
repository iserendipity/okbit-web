package io.dj.exchange.domain.primary;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 12.
 * Description
 */
@Entity
@Data
public class BlockExplorer implements Serializable {

    @Id
    @Column(name = "coin_name")
    private String coinName;

    @Column(name = "explorer_url")
    private String explorerUrl;

}
