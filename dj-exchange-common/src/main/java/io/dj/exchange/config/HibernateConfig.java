package io.dj.exchange.config;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 10.
 * Description
 */
@Configuration
public class HibernateConfig {

//    private static final String DEFAULT_NAMING_STRATEGY
//            = "org.springframework.boot.orm.jpa.hibernate.SpringNamingStrategy";
//
//    @Bean
//    @Primary
//    @ConfigurationProperties(prefix="spring.datasource")
//    public DataSource dataSource() {
//        return DataSourceBuilder.create().build();
//    }
//
//    @Primary
//    @Bean(name = "entityManagerFactory")
//    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
//            EntityManagerFactoryBuilder builder) {
//
//        Map<String, String> propertiesHashMap = new HashMap<>();
//        propertiesHashMap.put("hibernate.ejb.naming_strategy",DEFAULT_NAMING_STRATEGY);
//
//        return builder.dataSource(dataSource())
//                .packages("com.millky.dev.database.multi.domain.article")
//                .properties(propertiesHashMap)
//                .build();
//    }
//
//    @Bean
//    @ConfigurationProperties(prefix="spring.datasource.readonly")
//    public DataSource readonlyDataSource() {
//        return DataSourceBuilder.create().build();
//    }



}
