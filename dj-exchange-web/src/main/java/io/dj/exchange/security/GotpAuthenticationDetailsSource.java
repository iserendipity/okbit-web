package io.dj.exchange.security;

import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import javax.servlet.http.HttpServletRequest;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 18.
 * Description
 */
public class GotpAuthenticationDetailsSource extends WebAuthenticationDetailsSource {

    @Override
    public WebAuthenticationDetails buildDetails(HttpServletRequest context) {
        return new GotpWebAuthenticationDetails(context);
    }

}
