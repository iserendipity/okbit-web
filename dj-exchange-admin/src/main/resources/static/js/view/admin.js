/**
 * Created by jeongwoo on 2017. 5. 24..
 */

(function($) {

    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader("AJAX", true);

        },
        cache : false,
        error: function(xhr, status, err) {

            if (xhr.status == 401) {
                alert("401");
            } else if (xhr.status == 403) {
                location.href='/?error=SESSION_EXPIRED'
            } else {
                alert("예외가 발생했습니다. 관리자에게 문의하세요.");
            }

        }

    });

    $(document).ajaxStart(function(){
        $('body').loading({
            message : 'LOADING...',
            theme : 'dark'
        });
    }).ajaxStop(function(){
        $('body').loading('stop');
    });

})(jQuery);

var adminApi = new AdminApi();
getQuestion = function(pageNo){

    var param = new Object();
    param.page = pageNo-1;
    param.size = 20;

    adminApi.getQuestion(function(data){
        if(data.page && data.page.totalPages > 0 ) {
            setPageValues('questionPaging', data.page, 'getQuestion', pageNo);
        }

        if(data._embedded && data._embedded.supportQnas && data._embedded.supportQnas.length > 0){
            makeTemplate('messageBody', 'messageTemplate', data._embedded.supportQnas);
            //$('#messageBody').html(d);

        }


    }, param);

};

getReqWithdraw = function(pageNo){

    var param = new Object();
    param.page = pageNo-1;
    param.size = 20;

    adminApi.getReqWithdraw(function(data){
        console.log(data);
        if(data.page && data.page.totalPages > 0 ) {
            setPageValues('withdrawPaging', data.page, 'getReqWithdraw', pageNo);
        }

        if(data._embedded && data._embedded.manualTransactionses && data._embedded.manualTransactionses.length > 0){
            makeTemplate('withReqBody', 'withdrawTemplate', data._embedded.manualTransactionses);
            //$('#messageBody').html(d);

        }else{
            $('#withReqBody').html('<tr><td colspan="7" class="text-center">데이터가 없습니다</td></tr>');
        }

    }, param);
};

getReqWithdrawCoin = function(pageNo){

    var param = new Object();
    param.page = pageNo-1;
    param.size = 20;

    adminApi.getReqWithdrawCoin(function(data){
        if(data.page && data.page.totalPages > 0 ) {
            setPageValues('withdrawCoinPaging', data.page, 'getReqWithdrawCoin', pageNo);
        }

        if(data._embedded && data._embedded.manualTransactionses && data._embedded.manualTransactionses.length > 0){
            makeTemplate('withReqCoinBody', 'withdrawCoinTemplate', data._embedded.manualTransactionses);
            //$('#messageBody').html(d);

        }else{
            $('#withReqCoinBody').html('<tr><td colspan="7" class="text-center">데이터가 없습니다</td></tr>');
        }

    }, param);
};

getReqVerify = function(pageNo){

    // var param = new Object();
    // param.page = pageNo-1;
    // param.size = 20;
    //
    // adminApi.getReqVerify(function(data){
    //
    //     console.log(data);
    //
    //     setPageValues('paging', data.page, 'getReqVerify', pageNo);
    //
    //     if(data._embedded && data._embedded.adminLevels && data._embedded.adminLevels.length > 0){
    //
    //         makeTemplate('verifyReqBody', 'verifyTemplate', data._embedded.adminLevels);
    //         //$('#messageBody').html(d);
    //
    //     }
    //
    // }, param);
};


getReqIdcardVerify = function(pageNo){

    var param = new Object();
    param.page = pageNo-1;
    param.size = 20;

    adminApi.getReqIdVerify(function(data){
        if(data.page && data.page.totalPages > 0 ) {
            setPageValues('idCardPaging', data.page, 'getReqIdcardVerify', pageNo);
        }

        if(data._embedded && data._embedded.adminLevels && data._embedded.adminLevels.length > 0){

            makeTemplate('verifyReqIdcardBody', 'verifyIdTemplate', data._embedded.adminLevels);
            //$('#messageBody').html(d);

        }else{

            $('#verifyReqIdcardBody').html('<tr><td colspan="4" class="text-center">데이터가 없습니다</td></tr>');

        }

    }, param);
};

getReqBankVerify = function(pageNo){

    var param = new Object();
    param.page = pageNo-1;
    param.size = 20;

    adminApi.getReqVerify(function(data){
        if(data.page && data.page.totalPages > 0 ) {
            setPageValues('paging', data.page, 'getReqVerify', pageNo);
        }

        if(data._embedded && data._embedded.adminLevels && data._embedded.adminLevels.length > 0){

            makeTemplate('verifyReqBody', 'verifyBankTemplate', data._embedded.adminLevels);
            //$('#messageBody').html(d);

        }else{

            $('#verifyReqBody').html('<tr><td colspan="4" class="text-center">데이터가 없습니다</td></tr>');

        }

    }, param);
};

getWrongDeposit = function(pageNo){

    var param = new Object();
    param.page = pageNo-1;
    param.size = 10;

    adminApi.getWrongDeposit(function(data){
        if(data.page && data.page.totalPages > 0 ) {
            setPageValues('wrongPaging', data.page, 'getWrongDeposit', pageNo);
        }

        if(data._embedded && data._embedded.wrongDeposits && data._embedded.wrongDeposits.length > 0){
            makeTemplate('wrongBody', 'wrongTemplate', data._embedded.wrongDeposits);
            //$('#messageBody').html(d);

        }

    }, param);
};

submitReply = function(){

    var param = $('#replyForm').serializeObject();

    adminApi.reply(function(data){

        if(data.code != 'SUCCESS'){

            alert(data.desc);
        }

        location.reload(true);
        //alert('완료되었습니다.');

    }, param);

};

moveDetail = function(id){

    var form = document.createElement('form');
    form.setAttribute('method', 'post');
    form.setAttribute('action', '/messageDetail');

    var x = document.createElement('input');
    x.setAttribute('name', 'id');
    x.setAttribute('value', id);

    form.appendChild(x);
    document.body.appendChild(form);
    form.submit();

};

approveWrongDeposit = function(id){

    var email = prompt('회원의 이메일 주소를 적어주세요', '');
    if(email == null || email == ''){
        return false;
    }

    var param = new Object();
    param.id = id;
    param.email = email;

    adminApi.approveWrongDeposit(function(data){

        if(data.code != 'SUCCESS'){

            alert(data.desc);

        }

        window.location.reload(true);

        //getWrongDeposit('1');


    }, param);

};

getReqDeposit = function(pageNo){

    var param = new Object();
    param.page = pageNo-1;
    param.size = 20;

    adminApi.getReqDeposit(function(data){

        if(data.page && data.page.totalPages > 0 ) {
            setPageValues('withdrawPaging', data.page, 'getReqDeposit', pageNo);
        }

        if(data._embedded && data._embedded.manualTransactionses && data._embedded.manualTransactionses.length > 0){
            makeTemplate('withReqBody', 'withdrawTemplate', data._embedded.manualTransactionses);
            //$('#messageBody').html(d);

        }

    }, param);
};

// insertNewDeposit = function(){
//
//     var param = new Object();
//     param.depositDvcd = $('#depositDvcd').val();
//
//     adminApi.getUserIdByDvcdCode(function(data) {
//
//     }, param);
//
//     //return $('#depositForm').submit();
//
// };

getPreview = function(userId){

    var param = new Object();
    param.userId = userId;

    adminApi.getUserBankBook(function(data){

        if(data.code == 'SUCCESS') {
            var x = document.createElement('img');
            //x.setAttribute('src', "http://ipfs.io/ipfs/"+data.data);
            x.setAttribute('src', "data:image/jpg;base64, "+data.data);
            //x.setAttribute('style', 'width:100%; height:100%');
            $('#preview').html(x);

        }else{

            if(data.desc == 'NO_BANK_BOOK_APPROVED'){
                alert('인증된 통장사본이 없습니다');
            }
            $('#preview').html('');

        }

    }, param);

};

getVerifyBankPreview = function(userId, email, realName){

    var param = new Object();
    param.userId = userId;

    $('#bankForm input[name=userId]').val(userId);

    adminApi.getUserVerifyBankBook(function(data){

        if(data.code == 'SUCCESS') {
            var x = document.createElement('img');
            //x.setAttribute('src', "http://ipfs.io/ipfs/"+data.data);
            x.setAttribute('src', "data:image/jpg;base64, "+data.data);
            //x.setAttribute('style', 'width:100%; height:100%');
            $('#preview').html(x);

        }else{

            if(data.desc == 'NO_BANK_BOOK_APPROVED'){
                alert('업로드한 통장사본이 없습니다');
            }
            $('#preview').html('');

        }

    }, param);

    $('#bankPreviewInfo').text(email + " " +realName);

};

approveBank = function(){

    var param = $('#bankForm').serializeObject();

    if(!$('#bankForm input[name=userId]').val()){
        return false;
    }

    if(param.userId) {

        adminApi.approveBank(function (data) {
            if (data.code == 'SUCCESS') {
                alert('인증 성공');
                //getReqBankVerify('1');
                window.location.reload(true);

            } else {
                alert('인증실패 : ' + data.desc);
            }

        }, param);
    }
};

approveId = function(){

    var param = new Object();

    if(!$('#idForm input[name=userId]').val()){
        return false;
    }

    param.userId = $('#idForm input[name=userId]').val();
    param.realName = $('#idForm input[name=realName]').val();

    adminApi.approveId(function(data){

        if(data.code == 'SUCCESS') {
            alert('신분증 인증 성공');
            //getReqIdcardVerify('1');
            window.location.reload(true);

        }else{
            alert('신분증 인증 실패 : '+ data.desc);
        }

    }, param);

};

getVerifyIdPreview = function(userId, email, realName){

    var param = new Object();
    param.userId = userId;

    $('#idForm input[name=userId]').val(userId);

    adminApi.getUserVerifyIdcard(function(data){

        if(data.code == 'SUCCESS') {
            var x = document.createElement('img');
            // x.setAttribute('src', "http://ipfs.io/ipfs/"+data.data);
            x.setAttribute('src', "data:image/jpg;base64, "+data.data);
            //x.setAttribute('style', 'width:100%; height:100%');
            $('#idCardPreview').html(x);

        }else{

            if(data.desc == 'NO_BANK_BOOK_APPROVED'){
                alert('업로드한 통장사본이 없습니다');
            }
            $('#idCardPreview').html('');

        }

    }, param);

    $('#idPreviewInfo').text(email +' '+realName);

};

getIdPreview = function(userId){

    var param = new Object();
    param.userId = userId;

    adminApi.getUserIdCard(function(data){

        if(data.code == 'SUCCESS') {
            var x = document.createElement('img');
            // x.setAttribute('src', "http://ipfs.io/ipfs/"+data.data);
            x.setAttribute('src', "data:image/jpg;base64, "+data.data);
            //x.setAttribute('style', 'width:100%; height:100%');
            $('#idCardPreview').html(x);

        }else{

            if(data.desc == 'NO_ID_CARD_APPROVED'){
                alert('인증된 신분증이 없습니다');
            }
            $('#idCardPreview').html('');

        }

    }, param);


};
reasonTransaction = function(txId, obj){

    var reason = prompt('반려사유를 적어주세요', '');

    if(reason == null || reason == ''){
        return false;
    }

    var param = new Object();
    param.id = txId;
    param.reason = reason;

    adminApi.reasonTransaction(function(data) {

        if (data.code == 'SUCCESS') {

            alert('완료되었습니다.');

            getReqWithdraw('1');

        } else {

        }

    }, param);

};
reasonDepTransaction = function(txId, obj){

    var reason = prompt('반려사유를 적어주세요', '');

    if(reason == null || reason == ''){
        return false;
    }

    var param = new Object();
    param.id = txId;
    param.reason = reason;

    adminApi.reasonDepTransaction(function(data) {

        if (data.code == 'SUCCESS') {

            alert('완료되었습니다.');

            getReqDeposit('1');

        } else {

            alert(data.desc);

        }

    }, param);

};
approveTransaction = function(id, obj){

    if(!confirm('승인하시겠습니까?')){
        return false;
    }
    var param = new Object();
    param.id = id;

    adminApi.approveTransaction(function(data) {

        if (data.code == 'SUCCESS') {

            alert('완료되었습니다.');
            window.location.reload(true);


        } else {

            alert(data.desc);

        }

    }, param);

};

approveSendTransaction = function(id, obj){

    if(!confirm('승인하시겠습니까?')){
        return false;
    }
    var param = new Object();
    param.id = id;

    adminApi.approveSendTransaction(function(data) {

        if (data.code == 'SUCCESS') {

            alert('완료되었습니다.');
            $(obj).parent().parent().remove();


        } else {

            alert(data.desc);

        }

    }, param);

};

sendExcel = function(){

    adminApi.sendExcel(function(data) {

        if (data.code == 'SUCCESS') {

            alert('완료되었습니다.');
            window.location.reload(true);

        } else {
            alert(data.desc);
        }

    }, jsonFile);

};

setPageValues = function(targetId, pageData, functionName, nowPage){

    var $li = '';

    for(var i=1; i<pageData.totalPages+1; i++){
        if(nowPage == i){
            $li += '<li class="active"><a href="javascript:'+functionName+'('+i+')">'+i+'</a>';
        }else{
            $li += '<li><a href="javascript:'+functionName+'('+i+')">'+i+'</a>';
        }


    }

    $('#'+targetId).html($li);

};

/*jquery tmpl required*/
makeTemplate = function(targetId, templateId, jsonData){

    if(jsonData) {
        $("#" + targetId).html($('#' + templateId).tmpl(jsonData));
    }

};

/*jquery tmpl required*/
makeTemplatePre = function(targetId, templateId, jsonData){

    if(jsonData) {
        $("#" + targetId).prepend($('#' + templateId).tmpl(jsonData));
    }

};

makeTemplateApp = function(targetId, templateId, jsonData){

    if(jsonData) {
        $("#" + targetId).append($('#' + templateId).tmpl(jsonData));
    }

};

makeBalance = function(){

    adminApi.balanceAll(function(data){

        if(data.code =='FAIL'){

            alert(data.desc);
            //언락
            return false;

        }

        if(data.data && data.data.length > 0) {
            makeTemplate('balanceBody', 'balanceTemplate', data.data);

        }

    });

};

makeCoinInfo = function(){

    adminApi.coinAll(function(data){
        if(data.data && data.data.length > 0) {
            makeTemplate('coinBody', 'coinTemplate', data.data);

        }
    });

};


init = function(){

    makeBalance();
    makeCoinInfo();

};

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};