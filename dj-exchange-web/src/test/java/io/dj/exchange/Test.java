package io.dj.exchange;

import com.google.gson.Gson;
import io.dj.exchange.annotation.TransactionalEx;
import io.dj.exchange.domain.primary.UserSetting;
import io.dj.exchange.domain.readonly.MarketHistoryOrders;
import io.dj.exchange.domain.dto.ChartDTO;
import io.dj.exchange.domain.dto.SumHistory;
import io.dj.exchange.domain.primary.EmailConfirm;
import io.dj.exchange.domain.primary.User;
import io.dj.exchange.enums.MailType;
import io.dj.exchange.exception.OkbitException;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.provider.BankServiceProvider;
import io.dj.exchange.provider.BitfinexProvider;
import io.dj.exchange.provider.OtpProvider;
import io.dj.exchange.repository.hibernate.primary.ConfirmRepository;
import io.dj.exchange.repository.hibernate.primary.UserRepository;
import io.dj.exchange.repository.hibernate.primary.UserSettingRepository;
import io.dj.exchange.repository.hibernate.readonly.MarketOrdersRepository;
import io.dj.exchange.repository.hibernate.primary.TransactionsRepository;
import io.dj.exchange.service.UsersService;
import io.dj.exchange.service.mail.MailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.DigestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.mock;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 18.
 * Description
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DjExchangeWebApplication.class})
@TestPropertySource(locations = "classpath:application.properties")
@WebAppConfiguration
public class Test {

    @Autowired
    MarketOrdersRepository marketOrdersRepository;

    @Autowired
    BankServiceProvider bProvider;

    @Autowired
    MailService mailService;

    @Autowired
    UsersService usersService;

    @Autowired
    ApiProvider apiProvider;

    @Autowired
    ApiProvider testProvider;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BitfinexProvider bitfinexProvider;

    @Autowired
    TransactionsRepository transactionsRepository;

    @Autowired
    UserSettingRepository userSettingRepository;

    @Autowired
    private ConfirmRepository confirmRepository;

    String client_id = "l7xxe88b3120d5b546eb9d0fbb85fd8325db";
    String secret = "cbcc0a69cf124b398fe4dab6f6f40016";
    String login_token = "";
    String inquiry_token = "";
    String scope = "login";
    String redirectUrl = "http://localhost:8080/test";

    @Autowired
    private OtpProvider otpProvider;

//    @Autowired
//    private MailService mailService;

    @org.junit.Test
    public void sendEmail(){

        EmailConfirm confirm = confirmRepository.findOne("44b68ec7ac614baaedf7e9618c1f6a49");

        confirm.setType(MailType.SIGNUP);

        mailService.sendMailQue(confirm);


    }

    @org.junit.Test
    public void genAdminPwApiKey(){

        //String email = "support@ok-bit.com";

        String password = "okbitGuest0713!";
        //String password = "$2a$10$XT0t6SB";
        //String password = "okbitdepositadmin12!";
        //String password = "okbitwithadmin12!";
        //
        //
        User admin = userRepository.findOne(4L);
        //User admin = userRepository.findOne(2L);
        //User admin = userRepository.findOne(3L);

        //UserSetting userSetting = userSettingRepository.findOne(1L);

        admin.setPwd(new BCryptPasswordEncoder().encode(password));
        admin.getUserSetting().setApiKey(new BigInteger(1, DigestUtils.md5Digest((admin.getEmail()+admin.getPwd()).getBytes())).toString(16));


        userRepository.save(admin);


        //userSettingRepository.save(userSetting);

    }

    @org.junit.Test
    public void tokenTest(){
      Gson gson = new Gson();

      String str = "{\"t\":[1461801600,1461888000,1462147200,1462233600,1462320000,1462406400,1462492800,1462752000,1462838400,1462924800,1463011200,1463097600,1463356800,1463443200,1463529600,1463616000,1463702400,1463961600,1464048000,1464134400,1464220800,1464307200,1464652800,1464739200,1464825600,1464912000,1465171200,1465257600,1465344000,1465430400,1465516800,1465776000,1465862400,1465948800,1466035200,1466121600,1466380800,1466467200,1466553600,1466640000,1466726400,1466985600,1467072000,1467158400,1467244800,1467331200,1467676800,1467763200,1467849600,1467936000,1468195200,1468281600,1468368000,1468454400,1468540800,1468800000,1468886400,1468972800,1469059200,1469145600,1469404800,1469491200,1469577600,1469664000,1469750400,1470009600,1470096000,1470182400,1470268800,1470355200,1470614400,1470700800,1470787200,1470873600,1470960000,1471219200,1471305600,1471392000,1471478400,1471564800,1471824000,1471910400,1471996800,1472083200,1472169600,1472428800,1472515200,1472601600,1472688000,1472774400,1473120000,1473206400,1473292800,1473379200,1473638400,1473724800,1473811200,1473897600,1473984000,1474243200,1474329600,1474416000,1474502400,1474588800,1474848000,1474934400,1475020800,1475107200,1475193600,1475452800,1475539200,1475625600,1475712000,1475798400,1476057600,1476144000,1476230400,1476316800,1476403200,1476662400,1476748800,1476835200,1476921600,1477008000,1477267200,1477353600,1477440000,1477526400,1477612800,1477872000,1477958400,1478044800,1478131200,1478217600,1478476800,1478563200,1478649600,1478736000,1478822400,1479081600,1479168000,1479254400,1479340800,1479427200,1479686400,1479772800,1479859200,1480032000,1480291200,1480377600,1480464000,1480550400,1480636800,1480896000,1480982400,1481068800,1481155200,1481241600,1481500800,1481587200,1481673600,1481760000,1481846400,1482105600,1482192000,1482278400,1482364800,1482451200,1482796800,1482883200,1482969600,1483056000,1483401600,1483488000,1483574400,1483660800,1483920000,1484006400,1484092800,1484179200,1484265600,1484611200,1484697600,1484784000,1484870400,1485129600,1485216000,1485302400,1485388800,1485475200,1485734400,1485820800,1485907200,1485993600,1486080000,1486339200,1486425600,1486512000,1486598400,1486684800,1486944000,1487030400,1487116800,1487203200,1487289600,1487635200,1487721600,1487808000,1487894400,1488153600,1488240000,1488326400,1488412800,1488499200,1488758400,1488844800,1488931200,1489017600,1489104000,1489363200,1489449600,1489536000,1489622400,1489708800,1489968000,1490054400,1490140800,1490227200,1490313600,1490572800,1490659200,1490745600,1490832000,1490918400,1491177600,1491264000,1491350400,1491436800,1491523200,1491782400,1491868800,1491955200,1492041600,1492387200,1492473600,1492560000,1492646400,1492732800],\"s\":\"ok\"}";

      ChartDTO.History history = gson.fromJson(str, ChartDTO.History.class);

        List<Long> t = history.getT();

        for(Long time : t){

            log.info("날짜 : {}", LocalDateTime.ofInstant(Instant.ofEpochSecond(time), ZoneId.systemDefault()));

        }

    }


    @org.junit.Test
    public void testprovider(){

       log.info("-------- : {}", bitfinexProvider.getCandlesLast());

    }


    @org.junit.Test
    public void passwordUpdateTest() throws OkbitException{

        User user = userRepository.findOneByEmail("support@ok-bit.com").orElseThrow(() -> new OkbitException("NO_USER"));

        user.setPwd(new BCryptPasswordEncoder().encode("kk4647kk-"));

        userRepository.save(user);


    }

    @org.junit.Test
    public void testSocket(){
        List<MarketHistoryOrders> orders = marketOrdersRepository.findAll();
        for(MarketHistoryOrders order : orders) {
            mailService.sendPasswordResetMailQue(order);
        }
    }

    @org.junit.Test
    public void testForFeignRequestInterceptor(){

        String email = "iserendipity@me.com";
        String pwd = "1234";
        HttpServletRequest request = mock(HttpServletRequest.class);

        apiProvider.getWalletAll();

    }


    @org.junit.Test
    public void test123213213(){

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.put(ACCEPT, singletonList(APPLICATION_JSON_VALUE));
        headers.put(CONTENT_TYPE, singletonList(APPLICATION_JSON_VALUE));
        headers.put("apiKey", singletonList("31e201f145d97984ea442ee4be18360c"));

        testProvider.reqSend("KRW", "1G9NasKjCyBeLQQeQpiWzyVTt3gzTyUMfg", "", new BigDecimal(10000), "KJW", "기업", "31" ,headers);

    }




    @org.junit.Test
    public void testMail() throws Exception{

        User user = new User();
        user.setEmail("iserendipity@me.com");

        EmailConfirm e= new EmailConfirm();
        e.setConfirmCode("mdtxAs");
        e.setHashEmail("1e984e253256d342e5ab37dc38e05450");
        e.setEmail("jabo15@naver.com");

        //mailService.sendMailJmsQue(e);
        log.info("------테스트 완료 : {}", e);

        mailService.sendMailQue(e);
//        IntStream.range(1, 15).parallel().forEach(val -> {
//            mailService.sendMailQue(user, "OK-BIT Email Confirm","20140101");
//        });
        //mailService.sendEmail(user, "OK-BIT Email Confirm","20140101");

    }

    @org.junit.Test
    public void testRegist(){

        String email = "iserendipity@me.com";
        String pwd = "1234";
        HttpServletRequest request = mock(HttpServletRequest.class);
        usersService.doSignup(email, pwd, request );

    }

    @org.junit.Test
    public void testEmailConfirm() throws Exception{

        long from = 1359943443;
        long to = 1360271762;
        String symbol = "BITCOIN";

        String df = "%Y-%m-%d %H%i";

        List<SumHistory> result = marketOrdersRepository.getSumHistory(symbol, LocalDateTime.ofInstant(Instant.ofEpochSecond(from), ZoneOffset.UTC), LocalDateTime.ofInstant(Instant.ofEpochSecond(to), ZoneOffset.UTC), df);

        log.info("------------------ : {}", result);



    }

    @org.junit.Test
    @Scheduled(cron = "10 * * * * * ")
    public void testGetWallet(){
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.put(ACCEPT, singletonList(APPLICATION_JSON_VALUE));
        headers.put(CONTENT_TYPE, singletonList(APPLICATION_JSON_VALUE));
        headers.put("apiKey", singletonList("test3"));
        //apiProvider.getWallet("BITCOIN", headers);

        apiProvider.buyOrder(new BigDecimal(1000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.buyOrder(new BigDecimal(2000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.buyOrder(new BigDecimal(3000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.buyOrder(new BigDecimal(4000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.buyOrder(new BigDecimal(5000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.buyOrder(new BigDecimal(6000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.buyOrder(new BigDecimal(7000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.buyOrder(new BigDecimal(8000), new BigDecimal(1), "BITCOIN", headers);


        apiProvider.sellOrder(new BigDecimal(1000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.sellOrder(new BigDecimal(2000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.sellOrder(new BigDecimal(3000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.sellOrder(new BigDecimal(4000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.sellOrder(new BigDecimal(5000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.sellOrder(new BigDecimal(6000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.sellOrder(new BigDecimal(7000), new BigDecimal(1), "BITCOIN", headers);
        apiProvider.sellOrder(new BigDecimal(8000), new BigDecimal(1), "BITCOIN", headers);

    }


    @org.junit.Test
    public void te2s2t(){
        log.info("---------- {}", new BCryptPasswordEncoder().encode("1234"));
    }

    @org.junit.Test
    public void testMailQue(){

    }

    @org.junit.Test
    public void test(){
        Pageable request = mock(Pageable.class);
        //log.info("--------------gmrgmr-----{}", transactionsRepository.findAllByUserId(1L, request).getContent());

        //log.info("---------test : {}", marketOrdersRepository.findAllByOrderByPriceDesc(request));

        DecimalFormat df = new DecimalFormat(",000.00");
        //this.lastPrice = df.format(2222222222.444444);
        log.info("-----------fnfn : {}", df.format(2222222222.444444));

    }

    @org.junit.Test
    public void test2(){
        log.info("------------랜덤 : {}", RandomStringUtils.randomAlphanumeric(6));
        log.info("md5 : {}", new BigInteger(1, DigestUtils.md5Digest("iserendipity@me.com".getBytes())).toString(16));



    }
}
