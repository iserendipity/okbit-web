package io.dj.exchange.repository.hibernate.readonly;

import io.dj.exchange.domain.readonly.BankCode;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 12.
 * Description
 */
public interface BankCodeRepository extends JpaRepository<BankCode, String> {

    BankCode findOneByBankNameContaining(String bankName);
}
