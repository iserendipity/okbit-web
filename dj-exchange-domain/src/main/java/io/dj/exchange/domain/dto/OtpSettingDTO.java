package io.dj.exchange.domain.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 19.
 * Description
 */
public class OtpSettingDTO {

    @Data
    @EqualsAndHashCode(callSuper = false)
    public static class OtpSettingRequest extends OtpRequest{

        private String useLogin;
        private String useSetting;
        private String useWithdraw;

    }

}
