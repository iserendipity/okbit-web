package io.dj.exchange.service.currentuser;

import io.dj.exchange.domain.primary.CurrentUser;
import io.dj.exchange.domain.primary.User;
import io.dj.exchange.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by tommy on 2016. 2. 29..
 */
@Service
public class CurrentUserDetailsService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrentUserDetailsService.class);
    private final UserService userService;

    @Autowired
    public CurrentUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public CurrentUser loadUserByUsername(String loginId) throws UsernameNotFoundException {
        LOGGER.info("Authenticating user with loginId={}", loginId);
        User user = userService.getUserByLoginId(loginId).orElseThrow(() -> new UsernameNotFoundException(String.format("User with loginId=%s was not found", loginId)));
        return new CurrentUser(user);
    }
}
