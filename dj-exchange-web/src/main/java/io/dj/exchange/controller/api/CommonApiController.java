package io.dj.exchange.controller.api;

import io.dj.exchange.annotation.TransactionalEx;
import io.dj.exchange.domain.dto.*;
import io.dj.exchange.domain.primary.*;
import io.dj.exchange.domain.readonly.MarketHistoryOrders;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.enums.ErrorCode;
import io.dj.exchange.enums.OrderStatus;
import io.dj.exchange.exception.OkbitException;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.repository.hibernate.primary.*;
import io.dj.exchange.service.CommonService;
import io.dj.exchange.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;


@Slf4j
@RestController
@RequestMapping("/api/common")
public class CommonApiController {

    @Autowired
    private CoinRepository coinRepository;

    @Autowired
    private UserSettingRepository userSettingRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ApiProvider apiProvider;

    @Autowired
    private CommonService commonService;

    @Autowired
    private OtpRepository otpRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private ConfirmRepository confirmRepository;

    @RequestMapping(value="/coins", method=RequestMethod.GET)
    public Response<ArrayList<CoinInfo>> coins(){

        ArrayList<Map<String, Object>> coinList = new ArrayList<Map<String, Object>>();
        Map<String, Object> coinMap = new HashMap<>();

        ArrayList<CoinInfo> list = new ArrayList<>();

        List<Coin> coins = coinRepository.findAllByNameNotOrderByPriorityAsc("KRW");

        CoinInfo coinInfo;

        Pageable p = new PageRequest(0, 1);

        for(Coin coin : coins){

            coinInfo = new CoinInfo();
            coinInfo.setName(coin.getName());

            coinInfo = commonService.coinInfo(coinInfo, p);

            //coinMap = new HashMap<>();

            list.add(coinInfo);

        }

//
//
////        commonService.coinInfo(coinInfo, p)
//
//
//        coinMap.put("coinName", "PRC/KRW");
//        coinMap.put("change", 0.36);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "4009 PRC");
//        coinMap.put("volume", 1230242);
//
//        coinList.add(coinMap);
//
//        coinMap = new HashMap<String, Object>();
//
//        coinMap.put("coinName", "ETH/KRW");
//        coinMap.put("change", -0.49);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "5000 ETH");
//        coinMap.put("volume", 4004909);
//
//        coinList.add(coinMap);
//
//        coinMap.put("coinName", "PRC/KRW");
//        coinMap.put("change", 0.36);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "4009 PRC");
//        coinMap.put("volume", 1230242);
//
//        coinList.add(coinMap);
//
//        coinMap = new HashMap<String, Object>();
//
//        coinMap.put("coinName", "ETH/KRW");
//        coinMap.put("change", -0.49);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "5000 ETH");
//        coinMap.put("volume", 4004909);
//
//        coinList.add(coinMap);
//
//        coinMap.put("coinName", "PRC/KRW");
//        coinMap.put("change", 0.36);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "4009 PRC");
//        coinMap.put("volume", 1230242);
//
//        coinList.add(coinMap);
//
//        coinMap = new HashMap<String, Object>();
//
//        coinMap.put("coinName", "ETH/KRW");
//        coinMap.put("change", -0.49);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "5000 ETH");
//        coinMap.put("volume", 4004909);
//
//        coinList.add(coinMap);
//
//        coinMap.put("coinName", "PRC/KRW");
//        coinMap.put("change", 0.36);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "4009 PRC");
//        coinMap.put("volume", 1230242);
//
//        coinList.add(coinMap);
//
//        coinMap = new HashMap<String, Object>();
//
//        coinMap.put("coinName", "ETH/KRW");
//        coinMap.put("change", -0.49);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "5000 ETH");
//        coinMap.put("volume", 4004909);
//
//        coinList.add(coinMap);
//
//        coinMap.put("coinName", "PRC/KRW");
//        coinMap.put("change", 0.36);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "4009 PRC");
//        coinMap.put("volume", 1230242);
//
//        coinList.add(coinMap);
//
//        coinMap = new HashMap<String, Object>();
//
//        coinMap.put("coinName", "ETH/KRW");
//        coinMap.put("change", -0.49);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "5000 ETH");
//        coinMap.put("volume", 4004909);
//
//        coinList.add(coinMap);
//
//        coinMap.put("coinName", "PRC/KRW");
//        coinMap.put("change", 0.36);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "4009 PRC");
//        coinMap.put("volume", 1230242);
//
//        coinList.add(coinMap);
//
//        coinMap = new HashMap<String, Object>();
//
//        coinMap.put("coinName", "ETH/KRW");
//        coinMap.put("change", -0.49);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "5000 ETH");
//        coinMap.put("volume", 4004909);
//
//        coinList.add(coinMap);
//
//        coinMap.put("coinName", "PRC/KRW");
//        coinMap.put("change", 0.36);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "4009 PRC");
//        coinMap.put("volume", 1230242);
//
//        coinList.add(coinMap);
//
//        coinMap = new HashMap<String, Object>();
//
//        coinMap.put("coinName", "ETH/KRW");
//        coinMap.put("change", -0.49);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "5000 ETH");
//        coinMap.put("volume", 4004909);
//
//        coinList.add(coinMap);
//
//        coinMap.put("coinName", "PRC/KRW");
//        coinMap.put("change", 0.36);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "4009 PRC");
//        coinMap.put("volume", 1230242);
//
//        coinList.add(coinMap);
//
//        coinMap = new HashMap<String, Object>();
//
//        coinMap.put("coinName", "ETH/KRW");
//        coinMap.put("change", -0.49);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "5000 ETH");
//        coinMap.put("volume", 4004909);
//
//        coinList.add(coinMap);
//
//        coinMap.put("coinName", "PRC/KRW");
//        coinMap.put("change", 0.36);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "4009 PRC");
//        coinMap.put("volume", 1230242);
//
//        coinList.add(coinMap);
//
//        coinMap = new HashMap<String, Object>();
//
//        coinMap.put("coinName", "ETH/KRW");
//        coinMap.put("change", -0.49);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "5000 ETH");
//        coinMap.put("volume", 4004909);
//
//        coinList.add(coinMap);
//
//        coinMap.put("coinName", "PRC/KRW");
//        coinMap.put("change", 0.36);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "4009 PRC");
//        coinMap.put("volume", 1230242);
//
//        coinList.add(coinMap);
//
//        coinMap = new HashMap<String, Object>();
//
//        coinMap.put("coinName", "ETH/KRW");
//        coinMap.put("change", -0.49);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "5000 ETH");
//        coinMap.put("volume", 4004909);
//
//        coinList.add(coinMap);
//
//        coinMap.put("coinName", "PRC/KRW");
//        coinMap.put("change", 0.36);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "4009 PRC");
//        coinMap.put("volume", 1230242);
//
//        coinList.add(coinMap);
//
//        coinMap = new HashMap<String, Object>();
//
//        coinMap.put("coinName", "ETH/KRW");
//        coinMap.put("change", -0.49);
//        coinMap.put("changePoint", 24.81);
//        coinMap.put("price", "5000 ETH");
//        coinMap.put("volume", 4004909);
//
//        coinList.add(coinMap);

        return Response.<ArrayList<CoinInfo>>builder()
                .code(CodeEnum.SUCCESS)
                .data(list)
                .build();

    }

    @GetMapping(value="/coins/getAllVolume")
    public Response<Volume> coinsAllVolume(){

        Volume volume = commonService.getVolumeAll();

        //1d volume
        //24h volume
        //30day volume

        //List<String> coins = coinRepository.findAllByOrderByPriorityAsc();
        //coins.remove(0);

        return Response.<Volume>builder()
                .code(CodeEnum.SUCCESS)
                .data(volume)
                //.data(apiProvider.getCoinAll(headerMap).getResult().getCoins())
                .build();

    }

    @RequestMapping(value="/coins/getAll", method=RequestMethod.GET)
    public Response<List<Coin>> coinsAll(){

        List<Coin> coins = coinRepository.findAllByOrderByPriorityAsc();
        //coins.remove(0);

        return Response.<List<Coin>>builder()
            .code(CodeEnum.SUCCESS)
            .data(coins)
            //.data(apiProvider.getCoinAll(headerMap).getResult().getCoins())
            .build();

    }

    @RequestMapping(value="/coins/get", method=RequestMethod.POST)
    public Response<Coin> coin(@Valid @RequestBody Coin coin){

        return Response.<Coin>builder()
                .code(CodeEnum.SUCCESS)
                .data(coinRepository.findOneByName(coin.getName()))
                //.data(apiProvider.getCoinAll(headerMap).getResult().getCoins())
                .build();

    }

    @RequestMapping(value = "/myOrders", method = RequestMethod.POST)
    public Response<List<Orders>> myOrders(@Valid @RequestBody OrdersDTO.RequestToMyOrderList request, @ModelAttribute("user") User user, @PageableDefault(sort = { "id" }, direction = Sort.Direction.DESC, size = 30) Pageable p) throws Exception{

        //FeignResult result = apiProvider.ordersGetAll(p.getPageNumber(), p.getPageSize(), request.getName());
        log.info("P : {}", p);
        return initOrder(orderRepository.findAllByUserIdAndStatusAndFromCoinNameOrToCoinName(user.getId(), OrderStatus.PLACED, request.getName(), request.getName(), p).getContent());
        //return initOrder(orderRepository.findAllByUserIdAndStatusAndFromCoinNameOrToCoinName(user.getId(), OrderStatus.PLACED, request.getName(), request.getName(), p).getContent());
//        return Response.<List<Orders>>builder()
//                .code(CodeEnum.SUCCESS)
//                .data(initOrder(orderRepository.findAllByUserIdAndStatusAndFromCoinNameOrToCoinName(user.getId(), OrderStatus.PLACED, request.getName(), request.getName(), p).getContent()))
//                .build();

    }

    @RequestMapping(value = "/myOrdersFull", method = RequestMethod.POST)
    public Response<List<Orders>> myOrdersFull(@Valid @RequestBody OrdersDTO.RequestToMyOrderList request, @ModelAttribute("user") User user) throws Exception{

        //FeignResult result = apiProvider.ordersGetAll(p.getPageNumber(), p.getPageSize(), request.getName());
        return initOrder(orderRepository.findAllByUserIdAndStatusAndFromCoinNameOrToCoinName(user.getId(), OrderStatus.PLACED, request.getName(), request.getName()));
        //return initOrder(orderRepository.findAllByUserIdAndStatusAndFromCoinNameOrToCoinName(user.getId(), OrderStatus.PLACED, request.getName(), request.getName(), p).getContent());
//        return Response.<List<Orders>>builder()
//                .code(CodeEnum.SUCCESS)
//                .data(initOrder(orderRepository.findAllByUserIdAndStatusAndFromCoinNameOrToCoinName(user.getId(), OrderStatus.PLACED, request.getName(), request.getName(), p).getContent()))
//                .build();

    }

    @RequestMapping(value = "/getOtpSetting", method = RequestMethod.POST)
    public Response<OtpSetting> getOtpSetting(@ModelAttribute("user") User user) throws Exception {

        UserSetting setting = userSettingRepository.findOneByUserId(user.getId());
        if(!"Y".equals(setting.getOtpActive())){
            return Response.<OtpSetting>builder()
                    //.data(otpRepository.findOneByUserId(user.getId()))
                    .code(CodeEnum.SUCCESS)
                    .build();
            //throw new OkbitException("OTP_DISABLED");
        }

        return Response.<OtpSetting>builder()
                .data(otpRepository.findOneByUserId(user.getId()))
                .code(CodeEnum.SUCCESS)
                .build();
    }

    @RequestMapping(value="/balance", method=RequestMethod.POST)
    public Response<List<Wallet>> balanceAll() throws Exception{

        FeignResult wallets = apiProvider.getWalletAll();

        FeignResultData resultData = wallets.getResult();

        //log.info("--------------resultData : {}", resultData);

        if(resultData.getWallets() != null){

            Collections.sort(resultData.getWallets(), new Comparator<Wallet>() {
                @Override
                public int compare(Wallet o1, Wallet o2) {
                    return o1.getCoin().getPriority() < o2.getCoin().getPriority() ? -1 : (o1.getCoin().getPriority() > o2.getCoin().getPriority()) ? 1 : 0;
                }
            });

            for(int i=0;i<resultData.getWallets().size(); i++){

                resultData.getWallets().get(i).setTotalBalance(resultData.getWallets().get(i).getAvailableBalance().add(resultData.getWallets().get(i).getUsingBalance()));
                resultData.getWallets().get(i).initTotalBalance();
                resultData.getWallets().get(i).initAvailableBalance();
                resultData.getWallets().get(i).initUsingBalance();
            }

        }

        return Response.<List<Wallet>>builder()
                .code(CodeEnum.SUCCESS)
                .data(resultData != null ? resultData.getWallets() : null)
                //.data(wallets.)
                //.data(coinRepository.findAllByOrderByPriorityAsc())
                .build();

    }

    @RequestMapping(value="/changeTheme", method=RequestMethod.POST)
    @TransactionalEx
    public Response changeTheme(@ModelAttribute("user") User user, @RequestBody Map<String, String> paramMap, HttpServletRequest request){

//        user.getUserSetting().setTheme(paramMap.get("theme"));
//        userSettingRepository.save(user.getUserSetting());
        UserSetting userSetting = userSettingRepository.findOneByUserId(user.getId());
        userSetting.setTheme(paramMap.get("theme"));

        userSettingRepository.save(userSetting);

        log.info("theme : {}", paramMap.get("theme"));
        log.info("userSetting : {}", userSetting);
        user.setUserSetting(userSetting);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof UsernamePasswordAuthenticationToken) {
            UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authentication;
            auth.setDetails(user);
        }

        //userDetailsService.loadUserByUsername(user.getEmail());

        request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());

        return Response.builder()
                .code(CodeEnum.SUCCESS)
                //.data(apiProvider.getWallets(headerMap).getResult().getWallets())
                //.data(coinRepository.findAllByOrderByPriorityAsc())
                .build();

    }

    public static Response<List<Orders>> initOrder(List<Orders> orderList){

        log.info("Order Init : myOrder chk : {}", orderList);

        for(Orders order : orderList){
            order.initAmountRemainingTxt();
            order.initAmountTxt();
            order.initPriceTxt();
            order.initTotalPriceTxt();
            order.initRegDtTxt();
        }

        return Response.<List<Orders>>builder().data(orderList).code(CodeEnum.SUCCESS).build();

    }

    public static Response<List<OrdersCount>> initOrdersCount(List<OrdersCount> orderList){


        BigDecimal a = BigDecimal.valueOf(0);
        BigDecimal b;
        BigDecimal c = BigDecimal.valueOf(0);
        BigDecimal d;

        for (int n = 0; n < orderList.size(); n++) {

            b = orderList.get(n).getAmount();

            a = a.add(b);

            d = orderList.get(n).getTotalPrice();

            c = c.add(d);

            orderList.get(n).setTotalCoin(a);
            orderList.get(n).setTotalPrice(c);
            orderList.get(n).initAmount();
            orderList.get(n).initPrice();
            orderList.get(n).initTotalCoin();
            orderList.get(n).initTotalPrice();

        }

        //log.info("--------오더리스트 : {}",orderList);

        return Response.<List<OrdersCount>>builder().data(orderList).code(CodeEnum.SUCCESS).build();

    }


    public static Response<List<MarketHistoryOrders>> initHistoryOrder(List<MarketHistoryOrders> orderList){

        for(MarketHistoryOrders order : orderList){
            //order.initAmountRemainingTxt();
            order.initAmountTxt();
            order.initPriceTxt();
            order.initTotalPriceTxt();
        }

        return Response.<List<MarketHistoryOrders>>builder().data(orderList).code(CodeEnum.SUCCESS).build();

    }

    public static Response<List<MyHistoryOrders>> initMyHistoryOrder(List<MyHistoryOrders> orderList){

        for(MyHistoryOrders order : orderList){
            //order.initAmountRemainingTxt();
            order.initAmountTxt();
            order.initPriceTxt();
            order.initTotalPriceTxt();
        }

        return Response.<List<MyHistoryOrders>>builder().data(orderList).code(CodeEnum.SUCCESS).build();

    }

    @RequestMapping(value = "/coinInfo", method = RequestMethod.POST)
    public Response<CoinInfo> coinInfo(@Valid @RequestBody CoinInfo coinInfo, @PageableDefault(size = 1) Pageable p) throws Exception{

        return Response.<CoinInfo>builder().data(commonService.coinInfo(coinInfo, p)).code(CodeEnum.SUCCESS).build();

    }

    @RequestMapping(value = "/otpCheck")
    public Response<String> otpCheck(@Valid @RequestBody LoginRequest request) throws Exception{

        User user = userService.getUserByLoginId(request.getLoginId()).orElseThrow(() -> new OkbitException("NO_USER"));
        String ret = "N";
        if("Y".equals(user.getUserSetting().getOtpActive()) && "Y".equals(user.getOtpSetting().getUseLogin())){
            ret = "Y";
        }
        return Response.<String>builder().data(ret).code(CodeEnum.SUCCESS).build();

    }

    @PostMapping(value = "/passwordReset")
    @TransactionalEx
    public Response<String> passwordReset(@Valid @RequestBody LoginRequest request) throws Exception{

        EmailConfirm confirm = confirmRepository.findOneByHashEmailAndConfirmCode(request.getHashEmail(), request.getConfirmCode()).orElseThrow(()->new OkbitException(ErrorCode.EMAIL_EXPIRED.name()));

        User user = userRepository.findOneByEmail(confirm.getEmail()).orElseThrow(()->new OkbitException("NO_USER"));

        user.setPwd(new BCryptPasswordEncoder().encode(request.getPassword()));

        userRepository.save(user);

        confirm.setDelDtm(LocalDateTime.now());
        confirmRepository.save(confirm);

        return Response.<String>builder().code(CodeEnum.SUCCESS).build();
    }

}
