//package io.dj.exchange.handler;
//
//import io.dj.exchange.domain.chart.Symbol;
//import io.dj.exchange.domain.dto.ChartDTO;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.messaging.handler.annotation.MessageMapping;
//import org.springframework.messaging.handler.annotation.SendTo;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// * Project dj-exchange-okbit.
// * Created by jeongwoo on 2017. 6. 9.
// * Description
// */
//@RestController
//@Slf4j
//public class WsMessageHandler {
//
//
//    @MessageMapping("/chart")
//    @SendTo("/topic/okbit")
//    public ChartDTO.History sendChartData(Symbol params){
//
//        log.info("----------- : {}", params);
//        return new ChartDTO.History(1496996324);//"{1496996324, 10000, 10000000, 100000, 100000, 10000}";
//    }
//
//
//}
