package io.dj.exchange.config;

import freemarker.template.utility.XmlEscape;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 10.
 * Description
 */
@Configuration
public class FreeMarkerConfig {

    @Bean(name = "freemarkerConfig")
    @Primary
    public FreeMarkerConfigurer freemarkerConfig() {
        FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
        configurer.setTemplateLoaderPaths("classpath:/templates");
        return configurer;
    }

    @Bean
    public freemarker.template.Configuration configuration(){
        return freemarkerConfig().getConfiguration();
    }

}
