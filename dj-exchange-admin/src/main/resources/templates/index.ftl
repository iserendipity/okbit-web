<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
    </head>
    <body id="skin-blur-chrome">
        <section id="login">
            <header class="text-center">
                <div class="row text-center">
                    <img src="https://static.wixstatic.com/media/a36f24_ed150e6566b24afe87f02db2984e2e24~mv2.png/v1/fill/w_714,h_501,al_c,usm_0.66_1.00_0.01/a36f24_ed150e6566b24afe87f02db2984e2e24~mv2.png" style="width: 350px;">
                </div>
                <br>
                <br>

                <h3>고객의 희망과 밝은 미래를 함께 하는 최고의 거래소를 시작합니다.</h3>
            </header>
        
            <div class="clearfix"></div>
            <div class="container-fluid">
                <!-- Login -->
                <form class="box tile animated form-validation-2 active" id="box-login" method="post" action="loginProcess">
                    <#if error?has_content>
                        <div class="login-control alert alert-dismissable fade in" style="padding-right:35px;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <#--<#if error.null??>-->
                            ${error}
                            <#--<#else>-->
                                <#--Email or password is invalid.-->
                            <#--</#if>-->
                        </div>
                    </#if>
                    <#if regist?has_content>
                        <div class="login-control alert alert-dismissable fade in" style="padding-right:35px;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            Register success. Check your mail.
                        </div>
                    </#if>
                    <#if find?has_content>
                        <div class="login-control alert alert-dismissable fade in" style="padding-right:35px;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            Success Mail Sending.
                        </div>
                    </#if>
                    <h2 class="m-t-0 m-b-15">
                        Login</h2>
                    <input type="text" name="loginId" class="login-control m-b-10 validate[required,custom[email]]" placeholder="Email Address">
                    <input type="password" name="password" class="login-control validate[required]" placeholder="Password">
                    <div class="m-t-20">
                        <button type="submit" class="btn btn-sm m-r-5">Sign In</button>

                        <small>
                            <a class="box-switcher" data-switch="box-register" href="">Don't have an Account?</a> or
                            <a class="box-switcher" data-switch="box-reset" href="">Forgot Password?</a>
                        </small>
                    </div>
                </form>

            </div>
        </section>
        <#--<div>-->
            <#--<a id="prcoin_landing">-->
                <#--<img id="prcoin_img" style="width:480px;height:120px;">-->
                <#--<script src="https://ipfs.io/ipfs/QmdQhHkBqTi3yC35Btt8NYEcZsBUF4LHPie2XakqkzrVQK"></script>-->
                <#--<script>callPrcoinRequest(33, 33, "A");</script>-->
            <#--</a>-->
        <#--</div>-->


    <#--<div class="copyright">-->
        <#--<div class="container-fluid col-md-10 col-md-offset-1">-->
            <#--<div class="col-md-6">-->
                <#--<p>© 2016 - All Rights OK-BIT</p>-->
            <#--</div>-->
        <#--</div>-->
    <#--</div>-->
    <!-- Javascript Libraries -->
    <!-- jQuery -->
    <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->

    <!-- Bootstrap -->
    <script src="/js/bootstrap.min.js"></script>

    <!--  Form Related -->
    <script src="/js/validation/validate.min.js"></script> <!-- jQuery Form Validation Library -->
    <script src="/js/validation/validationEngine.min.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->
    <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->

    <!-- Jqutemplateslate -->
    <script src="/js/jquery.tmpl.min.js"></script>

    <!-- Jquery cookie -->
    <script src="/js/jquery.cookie.js"></script>

    <!-- All JS functions -->
    <script src="/js/functions.js"></script>

    <!-- login -->
    <script src="/js/index/login.js"></script>
    </body>
</html>
