package io.dj.exchange.domain.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kun7788 on 2017. 5. 21..
 */
public class ChartDTO {

//    @Data
//    public static class MarketHistory{
//
//        private long t;
//        private BigDecimal o;
//        private BigDecimal c;
//        private BigDecimal
//
//
//    }

    @Data
    public static class History implements Serializable{
        private List<Long> t;
        private List<BigDecimal> c;
        private List<BigDecimal> o;
        private List<BigDecimal> h;
        private List<BigDecimal> l;
        private List<Double> v;
        private String s;

        public History(long from) {
            t = new ArrayList<>();
            c = new ArrayList<>();
            o = new ArrayList<>();
            h = new ArrayList<>();
            l = new ArrayList<>();
            v = new ArrayList<>();

            t.add(from);
            c.add(new BigDecimal(39.51));
            o.add(new BigDecimal(39.33));
            h.add(new BigDecimal(39.58));
            l.add(new BigDecimal(39.25));
            v.add(687724D);
            s = "ok";
        }

        public History() {

            t = new ArrayList<>();
            c = new ArrayList<>();
            o = new ArrayList<>();
            h = new ArrayList<>();
            l = new ArrayList<>();
            v = new ArrayList<>();

            s = "no_data";

        }
    }

    @Data
    public static class Config {
       private boolean supports_search;
        private boolean supports_group_request;
        private boolean supports_marks;
        private boolean supports_timescale_marks;
        private boolean supports_time;
        private List<Exchange> exchanges;
        private List<SymbolsType> symbolsTypes;
        private List<String> supportedResolutions;
        private boolean has_empty_bars;
        private String data_status;
        private boolean has_seconds;

        public Config() {
            supports_search = true;
            supports_group_request = false;
            supports_marks = false;
            supports_timescale_marks = false;
            supports_time = true;
            has_empty_bars = true;
            has_seconds = true;

            data_status = "streaming";

            exchanges = new ArrayList<>();

            Exchange exchange1 = new Exchange();
            exchange1.setName("All Exchanges");
            exchange1.setValue("");
            exchange1.setDesc("");
            exchanges.add(exchange1);

            Exchange exchange2 = new Exchange();
            exchange2.setName("OKBIT");
            exchange2.setValue("OKBIT");
            exchange2.setDesc("OKBIT");
            exchanges.add(exchange2);

//            symbolsTypes = new ArrayList<>();
//            SymbolsType symbolsType1 = new SymbolsType();
//            symbolsType1.setName("All types");
//            symbolsType1.setValue("");
//            symbolsTypes.add(symbolsType1);
//
//            SymbolsType symbolsType2 = new SymbolsType();
//            symbolsType2.setName("Stock");
//            symbolsType2.setValue("stock");
//            symbolsTypes.add(symbolsType2);
//
//            SymbolsType symbolsType3 = new SymbolsType();
//            symbolsType3.setName("Index");
//            symbolsType3.setValue("index");
//            symbolsTypes.add(symbolsType3);

            supportedResolutions = Arrays.asList("1","5","10","15","30", "60", "D");

        }

        @Data
        public static class Exchange {
            private String value;
            private String name;
            private String desc;
        }

        @Data
        public static class SymbolsType {
            private String name;
            private String value;
        }
    }

    @Data
    public static class Symbol {
        private String name;

        @SerializedName("exchange-traded")
        private String exchangeTraded;

        @SerializedName("exchange-listed")
        private String exchangeListed;
        private String timezone;
        private double minmov;
        private double minmov2;
        private int pointvalue;
        private String session;
        private boolean has_intraday;
        private boolean has_no_volume;
        private String description;
        private String type;
        private List<String> supported_resolutions;
        private int pricescale;
        private String ticker;
        private String data_status;

        private boolean has_empty_bars;
        private boolean has_seconds;
        private boolean force_session_rebuild;
        private List<String> intraday_multipliers;

        public Symbol(String ticker) {
            this.ticker = ticker;
            name = ticker;
            exchangeTraded = "OKBIT";
            exchangeListed = "OKBIT";
            timezone = "Asia/Seoul";
            minmov = 1;
            minmov2 = 0;
            pointvalue = 1;
            session = "24x7";
            has_intraday = true;
            has_no_volume = false;
            description = ticker;
            type = "stock";
            supported_resolutions = Arrays.asList("1", "5", "10","15","30", "60", "D");
            intraday_multipliers = Arrays.asList("1", "5", "10","15","30", "60", "D");
            pricescale = 1;
            has_empty_bars = true;
            has_seconds = false;
            data_status = "streaming";
            force_session_rebuild = true;
        }
    }

}
