<h2 class="tile-title"><@spring.message "TITLE_ORDERS"/></h2>
<div class="overflow panel-body">
    <form role="form" style="font-size:10px;" id="orderForm" name="orderForm">
        <input type="hidden" id="coinName" name="name" class="name form-control input-sm">
        <div class="col-sm-6">
            <div class="form-group">
                <label for="price">KRW <@spring.message "TITLE_PRICE"/></label>
                <input type="text" class="form-control input-sm" id="price" name="price" placeholder="">
            </div>
            <#--<button type="button" tabindex="-1" class="btn btn-sm btn-alt col-sm-12" onclick="javascript:buy();" onmouseover="javascript:$('#buyResult').collapse('show'); hoverBuy();" onmouseout="javascript:$('#buyResult').collapse('hide');">BUY</button>-->
            <button type="button" tabindex="-1" class="btn btn-sm btn-alt col-sm-12" onclick="javascript:buy();" ><@spring.message "BTN_BUY"/></button>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="amount"><span name="unit">BTC</span> <@spring.message "TITLE_AMOUNT"/></label>
                <input type="text" class="form-control input-sm" id="amount" name="amount" placeholder="">

            </div>
            <#--<button type="button" class="btn btn-sm btn-alt col-sm-12" tabindex="-1" onclick="javascript:sell();" onmouseover="javascript:$('#sellResult').collapse('show'); hoverSell();" onmouseout="javascript:$('#sellResult').collapse('hide');">SELL</button>-->
            <button type="button" class="btn btn-sm btn-alt col-sm-12" tabindex="-1" onclick="javascript:sell();" ><@spring.message "BTN_SELL"/></button>
        </div>
        <#--<br>-->
        <div class="col-sm-12" style="margin-top:8px;">

            <div id="resultIndicator" class="alert alert-dismissable alert-danger" style="display:none">
                <#--<button type="button" class="close" data-dismiss="alert" aria-hidden="false">×</button>-->
                <span id="resultIndicatorSpan"></span>

            </div>

            <div class="well tile">
                <div class="row h6">
                <@spring.message "AMOUNT_UNIT" />&nbsp;:&nbsp;<span name="unitAmount"></span>&nbsp;<span name="unit"></span>
                </div>
                <div class="row h6">
                <@spring.message "PRICE_UNIT" />&nbsp;:&nbsp;<span name="unitPrice"></span>&nbsp;KRW
                </div>

            </div>

            <div class="well tile h6">
                <@spring.message "TRADINGFEE_EXP"/> : <span name="feePercent"></span>%

            </div>

            <#--<div id="buyResult" class="collapse in">-->
                <#--&lt;#&ndash;<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>&ndash;&gt;-->

            <#--</div>-->

            <#--<div id="sellResult" class="collapse in">-->

            <#--</div>-->

        </div>
    </form>

</div>