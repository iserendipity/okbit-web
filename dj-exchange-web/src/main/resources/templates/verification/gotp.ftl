<div id="otpModal" class="modal fade"> <!-- Style for just preview -->
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h4 class="modal-title">Two-factor Authentication</h4>
            </div>
            <div class="modal-body">
                <input type="text" id="otpCode" name="otpCode" placeholder="Type your Google Authenticator code" class="form-control">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm" onclick="javascript:finalRequest();"><@spring.message "BTN_SEND"/></button>
                <button type="button" class="btn btn-sm" data-dismiss="modal"><@spring.message "BTN_CLOSE"/></button>
            </div>
        </div>
    </div>
</div>