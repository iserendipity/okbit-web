package io.dj.exchange.domain.readonly;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by kun7788 on 2016. 8. 7..
 */
@Data
public class MarketHistoryOrdersPK implements Serializable {
    @Id
    Long id;

    @Id
    Long userId;

    @Id
    Long orderId;
}
