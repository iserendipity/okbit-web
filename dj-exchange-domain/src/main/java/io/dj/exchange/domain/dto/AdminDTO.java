package io.dj.exchange.domain.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 1.
 * Description
 */
public class AdminDTO {

    @Data
    public class ReqCoinAssets{

        @NotNull
        String coinName;



    }

    @Data
    public class ResCoinAssets{

        @NotNull
        String coinName;

        long realCoin;

        long coinUser;





    }

    @Data
    public class requestBankbook{

        long userId;

    }

    @Data
    public class approveWrongDepositRequest{

        @NotNull
        long id;

        @NotNull
        String email;

    }


    @Data
    public class approveBankRequest{

        @NotNull
        long userId;

        String withdrawBankNm;

        @NotNull
        String withdrawBankCode;

        @NotNull
        String withdrawBankAccount;

        @NotNull
        String realNm;

    }

    @Data
    public static class replyRequest{

        long id;

        @NotNull
        String title;

        String email;

        @NotNull
        String contents;

        long parentId;

    }

    @Data
    public static class replyResponse{

        @NotNull
        String title;

        String email;

        @NotNull
        String contents;

        @NotNull
        String repTitle;

        @NotNull
        String repContents;

    }

}
