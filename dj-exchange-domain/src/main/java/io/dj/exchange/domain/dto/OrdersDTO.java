package io.dj.exchange.domain.dto;

import io.dj.exchange.domain.primary.HistoryOrders;
import io.dj.exchange.domain.primary.Orders;
import io.dj.exchange.domain.readonly.MarketHistoryOrders;
import io.dj.exchange.enums.OrderType;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by kun7788 on 2016. 11. 21..
 */
public class OrdersDTO {

    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class ResponseToOrderCancel {
        @NotNull
        Boolean isCancel;

    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class RequestToOrderCancel {
        @NotNull
        Long orderId;
    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class RequestToOrderList {
        @NotNull
        String fromCoin;

        @NotNull
        String toCoin;

        Pageable pageRequest;

        OrderType orderType;

    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class RequestToMyOrderList {

        String name;
        Pageable pageRequest;
        OrderType orderType;

    }


    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class RequestToOrderListByOrderType {
        @NotNull
        String fromCoin;

        Integer buyPageNo;
        Integer buyPageSize;

        Integer sellPageNo;
        Integer sellPageSize;
    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class ResponseToOrderListByOrderType {
        List<Orders> buy;
        List<Orders> sell;
    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class RequestToMarketHistoryOrders {
        @NotNull
        String coinName;

        Integer pageNo;
        Integer pageSize;
    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    @Builder
    public static class ResponseToMarketHistoryOrders {
        List<MarketHistoryOrders> historyOrderses;
    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class RequestToMyHistoryOrders {
        @NotNull
        String fromCoin;

        Integer pageNo;
        Integer pageSize;
    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    @Builder
    public static class ResponseToMyHistoryOrders {
        List<HistoryOrders> historyOrderses;
    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class RequestBuy{

        @NotNull
        @DecimalMin("1")
        BigDecimal price;

        String orderType;
        @NotNull
        @DecimalMin("0.0001")
        BigDecimal amount;

        @NotNull
        String toCoin;

    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class RequestSell{

        @NotNull
        @DecimalMin("1")
        BigDecimal price;

        String orderType;
        @NotNull
        @DecimalMin("0.0001")
        BigDecimal amount;

        @NotNull
        String fromCoin;

    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class ResponseOrderList extends Orders{

        int count;

    }

}
