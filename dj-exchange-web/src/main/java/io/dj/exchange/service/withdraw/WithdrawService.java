package io.dj.exchange.service.withdraw;

import io.dj.exchange.annotation.TransactionalEx;
import io.dj.exchange.domain.dto.FeignResult;
import io.dj.exchange.domain.dto.ManualTransactionDTO;
import io.dj.exchange.domain.primary.*;
import io.dj.exchange.domain.readonly.BankCode;
import io.dj.exchange.enums.StatusEnum;
import io.dj.exchange.enums.UserLevels;
import io.dj.exchange.exception.OkbitException;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.repository.hibernate.primary.*;
import io.dj.exchange.repository.hibernate.readonly.BankCodeRepository;
import io.dj.exchange.security.GotpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 19.
 * Description
 */
@Service
@Slf4j
public class WithdrawService {

    @Autowired
    private ManualTransactionsRepository manualTransactionsRepository;

    @Autowired
    private AdminLevelRepository adminLevelRepository;

    @Autowired
    private ApiProvider apiProvider;

    @Autowired
    private CoinRepository coinRepository;

    @Autowired
    private TransactionsRepository transactionsRepository;

    @Autowired
    private UserSettingRepository userSettingRepository;

    @Autowired
    private BankCodeRepository bankCodeRepository;

    public boolean checkWithdrawalLimit(User user, ManualTransactionDTO.RequestKrw request) throws OkbitException{

        //1회와 1일 출금한도 체크
        //WithdrawUtil.withdrawLimitCheck();
        Coin coin = coinRepository.findOneByName(request.getCoinName());
        //WithdrawLimit limit = withdrawLimitRepository.findOneByCoinName(request.getCoinName());
        if(request.getAmount().compareTo(coin.getOnce()) == 1){

            throw new OkbitException("EXCEED_WITHDRAW_AMOUNT");

        }

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime toDay = now.withHour(0).withMinute(0).withSecond(0).withNano(0);

        //해당 유저의 오늘자 send 금액을 가져온다.
        List<Transactions> todayTransaction = transactionsRepository.findAllByCategoryAndUserIdAndCoinNameAndDtBetweenOrderByDtAsc("send", user.getId(), request.getCoinName(), now, toDay);

        BigDecimal todaySended = new BigDecimal(0);

        for(Transactions t : todayTransaction){

            todaySended = todaySended.add(t.getAmount());

        }

        if(todaySended.compareTo(coin.getOneDay()) == 1){

            throw new OkbitException("EXCEED_WITHDRAW_AMOUNT_DAY");

        }

        return true;
    }

    public boolean checkWithdrawalLimitCoin(User user, ManualTransactionDTO.RequestCoin request) throws OkbitException{

        //1회와 1일 출금한도 체크
        //WithdrawUtil.withdrawLimitCheck();
        //WithdrawLimit limit = withdrawLimitRepository.findOneByCoinName(request.getCoinName());
        Coin coin = coinRepository.findOneByName(request.getCoinName());
        if(request.getAmount().compareTo(coin.getOnce()) == 1){

            throw new OkbitException("EXCEED_WITHDRAW_AMOUNT");

        }

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime toDay = now.withHour(0).withMinute(0).withSecond(0).withNano(0);

        //해당 유저의 오늘자 send 금액을 가져온다.
        List<Transactions> todayTransaction = transactionsRepository.findAllByCategoryAndUserIdAndCoinNameAndDtBetweenOrderByDtAsc("send", user.getId(), request.getCoinName(), now, toDay);

        BigDecimal todaySended = new BigDecimal(0);

        for(Transactions t : todayTransaction){

            todaySended = todaySended.add(t.getAmount());

        }

        if(todaySended.compareTo(coin.getOneDay()) == 1){

            throw new OkbitException("EXCEED_WITHDRAW_AMOUNT_DAY");

        }

        return true;
    }

    public boolean checkFirstDepositAfter72Hours(User user, ManualTransactionDTO.RequestKrw request) throws OkbitException{

        //원화 입금이 있는지 확인한다.
        Optional<Transactions> t = transactionsRepository.findTopByCategoryAndUserIdAndCoinNameOrderByDtAsc("receive", user.getId(), request.getCoinName());

        if(t.isPresent()){//있으면 72시간이 지났는지 체크한다.//근데 있으면 이미 레벨 3이다.

            if(t.get().getDt().isAfter(LocalDateTime.now().minusHours(72))){

                throw new OkbitException("FIRST_DEPOSIT_AFTER_72");

            }

        }

        return true;
    }

    public boolean checkFirstDepositAfter72Hours(User user) throws OkbitException{

        //원화 입금이 있는지 확인한다.
        Optional<Transactions> t = transactionsRepository.findTopByCategoryAndUserIdAndCoinNameOrderByDtAsc("receive", user.getId(), "KRW");

        if(t.isPresent()){//있으면 72시간이 지났는지 체크한다.//근데 있으면 이미 레벨 3이다.

            if(t.get().getDt().isAfter(LocalDateTime.now().minusHours(72))){

                throw new OkbitException("FIRST_DEPOSIT_AFTER_72");

            }

        }

        return true;
    }

    @TransactionalEx
    public ManualTransactions withdrawKrw(User user, ManualTransactionDTO.RequestKrw request) throws Exception {

        //인증정보 체크
        Optional<AdminLevel> adminLevel = adminLevelRepository.findOneByUserId(user.getId());
        if(!adminLevel.isPresent() || !"Y".equals(adminLevel.get().getBankbookYn())){
            throw new OkbitException("NO_BANK_BOOK_APPROVED");
        }
        //인증받은 어카운트 체크
        if(!request.getAccount().equals(adminLevel.get().getWithdrawBankAccount())){
            throw new OkbitException("NOT_THIS_ACCOUNT");
        }
        //인증받은 어카운트의 뱅크코드 체크
        if(!request.getBankCode().equals(adminLevel.get().getWithdrawBankCode())){
            throw new OkbitException("NOT_THIS_BANK_CODE");
        }
//        //은행이름 체크
//        if(!request.getBankNm().equals(adminLevel.get().getWithdrawBankNm())){
//            throw new OkbitException("NOT_THIS_BANK_NAME");
//        }

        //실명확인
        if(!request.getUserNm().equals(adminLevel.get().getRealName())){
            throw new OkbitException("NOT_THIS_USER_NAME");
        }

        //otp 쓰는 사람인지, 오티피 코드는 맞는지 확인
        GotpUtils.otpCheckWithdraw(user, request);

        //일일한도 일회한도 확인
        checkWithdrawalLimit(user, request);

        //보안레벨과 금액비교
        UserSetting us = userSettingRepository.findOneByUserId(user.getId());
        String level = us.getLevel();
        switch(level){

            case "LEV1" : throw new OkbitException("NOT_THIS_USERLEVEL"); //1레벨은 원화 입출금이 전혀 안됨

            case "LEV2" : //가상화폐를 거래한 후 생성된 원화에 대한 출금제한을 두지 않는다. 1회한도 천만원 천원을 수수료로 제외하고 처리한다.

                checkFirstDepositAfter72Hours(user, request);

                break;

            case "LEV3" :
                break;
        }

        //지갑이 존재하는지 체크
        FeignResult result = apiProvider.getWallet(request.getCoinName());
        if(result == null || result.getResult() == null || result.getResult().getWallet() == null){
            throw new OkbitException("NO_KRW_WALLET");
        }

        //사용가능한 밸런스인지 체크
        if(result.getResult().getWallet().getAvailableBalance().compareTo(request.getAmount()) == -1){
            throw new OkbitException("NOT_ENOUGH_BALANCE");
        }

        ManualTransactions mt = new ManualTransactions();
        mt.setAccount(request.getAccount());
        mt.setAmount(request.getAmount());
        mt.setBankCode(request.getBankCode());
        mt.setBankNm(request.getBankNm());
        mt.setCategory("send");
        mt.setCoinName(request.getCoinName());
        mt.setIsConfirmation("N");
        mt.setRegDt(LocalDateTime.now());
        mt.setStatus(StatusEnum.PENDING.name());
        mt.setUserId(user.getId());
        mt.setUserNm(request.getUserNm());
        //mt.setTxId(RandomStringUtils.randomAlphanumeric(64));

        BankCode bankCode = bankCodeRepository.findOne(mt.getBankCode());
        mt.setBankNm(bankCode.getBankName());

        apiProvider.reqSendKRW(mt.getCoinName(), "UNKNOWN_ADDRESS", mt.getAmount(), mt.getUserNm(), mt.getBankNm(), mt.getBankCode(), mt.getAccount());

        //manualTransactionsRepository.save(mt);

        return mt;

    }

    public FeignResult withdrawCoin(User user, ManualTransactionDTO.RequestCoin request) throws Exception{

        //otp 쓰는 사람인지, 오티피 코드는 맞는지 확인
        GotpUtils.otpCheckWithdraw(user, request);

        //일일한도 일회한도 확인
        checkWithdrawalLimitCoin(user, request);

        //보안레벨과 금액비교
        UserSetting us = userSettingRepository.findOneByUserId(user.getId());
        String level = us.getLevel();

        if(UserLevels.LEV2.name().equals(level)){
            checkFirstDepositAfter72Hours(user);
        }

        //지갑이 존재하는지 체크
        FeignResult result = apiProvider.getWallet(request.getCoinName());
        if(result == null || result.getResult() == null || result.getResult().getWallet() == null){
            throw new OkbitException("NO_WALLET");
        }

        //사용가능한 밸런스인지 체크
        if(result.getResult().getWallet().getAvailableBalance().compareTo(request.getAmount()) == -1){
            throw new OkbitException("NOT_ENOUGH_BALANCE");
        }

        return apiProvider.reqSend(request.getCoinName(), request.getFromAddress(), request.getFromTag(), request.getAmount());

    }
}
