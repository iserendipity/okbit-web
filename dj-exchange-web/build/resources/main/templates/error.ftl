<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <meta name="format-detection" content="telephone=no">
        <meta charset="UTF-8">

    <meta name="description" content="OK-BIT">
    <meta name="keywords" content="OK-BIT, Bitcoin, Ehterium, Monero, Litecoin, Bitfinex, Poloniex">

        <title>OK-BIT</title>
            
        <!-- CSS -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">
        <link href="/css/generics.css" rel="stylesheet">
    </head>
    <body id="skin-blur-blue">
        <section id="error-page" class="tile">
            <h1 class="m-b-10">ERROR</h1>
            <p>Sorry, but the page you are looking for has not been found. Try checking the URL for errors. Don't worry though, we will help you get to the right place.</p>
            <a class="underline" href="">Back to previous page</a> or <a class="underline" href="">Go to Home page</a>
        </section>
        
    </body>
</html>
