package io.dj.exchange.enums;

/**
 * Created by jeongwoo on 2017. 4. 4..
 *
 */
public enum Role {
    USER, ADMIN
}
