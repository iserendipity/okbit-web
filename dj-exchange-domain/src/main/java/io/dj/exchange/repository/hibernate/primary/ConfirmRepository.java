package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.EmailConfirm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by jeongwoo on 2017. 4. 26..
 */
public interface ConfirmRepository extends JpaRepository<EmailConfirm, String> {
    Optional<EmailConfirm> findOneByEmail(String email);

    Page<EmailConfirm> findAllByDelDtmIsNull(Pageable p);

    Optional<EmailConfirm> findOneByEmailAndConfirmCode(String email, String confirmCode);

    Optional<EmailConfirm> findOneByHashEmailAndConfirmCode(String hash, String confirmCode);

    Optional<EmailConfirm> findOneByHashEmailAndConfirmCodeAndDelDtmIsNull(String hash, String confirmCode);

    Optional<EmailConfirm> findOneByHashEmailAndConfirmCodeAndSendYn(String hash, String confirmCode, String sendYn);

    //Page<EmailConfirm> findAllBySended(String sended, Pageable p);
}
