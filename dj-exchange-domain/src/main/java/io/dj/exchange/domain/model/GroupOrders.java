//package io.dj.exchange.domain.model;
//
//import io.dj.exchange.util.DataUtil;
//import lombok.Data;
//
//import java.math.BigDecimal;
//
///**
// * Created by kun7788 on 2016. 11. 21..
// */
//@Data
//public class GroupOrders {
//    private String price;
//    private String amount;
//    private String totalPrice;
//
//    public GroupOrders(BigDecimal price, BigDecimal amount, BigDecimal totalPrice) {
//        this.price = DataUtil.decimal(price);
//        this.amount = DataUtil.decimal(amount);
//        this.totalPrice = DataUtil.decimal(totalPrice);
//    }
//}
