package io.dj.exchange.service;

import com.amazonaws.services.s3.model.PutObjectResult;
import io.dj.exchange.S3Wrapper;
import io.dj.exchange.annotation.TransactionalEx;
import io.dj.exchange.config.ConstantsConfig;
import io.dj.exchange.domain.dto.FileDTO;
import io.dj.exchange.domain.primary.AdminLevel;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.exception.OkbitException;
import io.dj.exchange.repository.hibernate.primary.AdminLevelRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by jeongwoo on 2017. 5. 17..
 */
@Slf4j
@Service
public class FileService {

    @Autowired
    private AdminLevelRepository adminLevelRepository;

    @Autowired
    private S3Wrapper s3Wrapper;

    @TransactionalEx
    public String add(
            Long userId
            , String type
            , MultipartFile uploadfile1
            , boolean isEdit) throws Exception {

        Map<String, MultipartFile> uploadfiles = new HashMap<>();
        uploadfiles.put(type, uploadfile1);


        if (isEdit == false && (uploadfile1 == null)) {
            throw new OkbitException("NO_UPLOAD_FILE");
        }

        Set<String> checkSet = new HashSet<>();
        for (MultipartFile uploadFile : uploadfiles.values()) {
            if (uploadFile != null) {
                if (uploadFile.getSize() > 1024768) {
                    throw new OkbitException("FILE_SIZE_OVER_1024768");
                }

                if (!"image/png".equals(uploadFile.getContentType()) &&
                        !"image/gif".equals(uploadFile.getContentType()) &&
                        !"image/jpeg".equals(uploadFile.getContentType())) {
                    throw new OkbitException("FILE_NOT_IMAGE");
                }

                String filename = uploadFile.getOriginalFilename();
                Long size = uploadFile.getSize();
                String fileType = uploadFile.getContentType();
                String hash = filename + "_" + size + "_" + fileType;
                checkSet.add(hash);
            }
        }

        String nowTime = LocalDateTime.now().toString();
        Map<String, String> filenames = new HashedMap();
        List<String> keys = new ArrayList<>();
        for (String key : uploadfiles.keySet()) {
            MultipartFile uploadfile = uploadfiles.get(key);

            if (uploadfile != null) {
                String filename = org.apache.commons.codec.digest.DigestUtils.md5Hex(uploadfile.getOriginalFilename()
                        + nowTime
                        + userId);
                String contentType = "";
                contentType = key;
                filenames.put(filename, contentType);
                keys.add(key);
            }
        }

        Set<FileDTO.UploadInfo> fileHashes = new HashSet<>();
        if (isEdit == false || (isEdit == true && checkSet.size() > 0)) {
            fileHashes = Stream.of(keys.toArray()).parallel().unordered().map(k -> {
                try {
                    log.info("key == " + k);
                    MultipartFile uploadFile = uploadfiles.get(k);
                    if (uploadFile == null) {
                        return null;
                    }

                    String filename = org.apache.commons.codec.digest.DigestUtils.md5Hex(uploadFile.getOriginalFilename()
                            + nowTime
                            + userId);
                    String directory = ConstantsConfig.DOWNLOAD_TMP_FOLDER;
                    String filepath = Paths.get(directory, filename).toString();

                    // Save the file locally
//                    BufferedOutputStream stream =
//                            new BufferedOutputStream(new FileOutputStream(new File(filepath)));
//                    stream.write(uploadFile.getBytes());
//                    stream.close();

                    PutObjectResult result = s3Wrapper.upload(uploadFile, filename);

                    return FileDTO.UploadInfo.builder()
                            .contentType(filenames.get(filename))
                            .orgFilename(uploadFile.getOriginalFilename())
                            .hashFileName(filename)
                            .build();
                } catch (Exception ex) {
                    log.error(" upload error : " + ex.getMessage());
                }
                return null;
            }).filter(uploadInfo -> {
                return uploadInfo == null ? false : true;
            }).collect(Collectors.toSet());
        }
        log.info(" upload completed cnt : {}", fileHashes.size());

        if (isEdit == false && fileHashes.size() != 1) {
            return "error upload file.";
        }


        AdminLevel adminLevel = adminLevelRepository.findOne(userId);

        if (adminLevel == null) {
            adminLevel = new AdminLevel();
            adminLevel.setUserId(userId);
        }

        Iterator<FileDTO.UploadInfo> iterator = fileHashes.iterator();
        while (iterator.hasNext()) {
            FileDTO.UploadInfo uploadInfo = iterator.next();
            log.info(" upload completed : {}", uploadInfo.getOrgFilename());

            switch (type) {

                case "bankbook":
                    adminLevel.setBankbook(uploadInfo.getHashFileName());
                    adminLevel.setBankbookYn("P");
                    adminLevel.setBankbookRegDt(LocalDateTime.now());
                    break;

                case "idcard":
                    adminLevel.setIdcard(uploadInfo.getHashFileName());
                    adminLevel.setIdcardRegDt(LocalDateTime.now());
                    adminLevel.setIdcardYn("P");
                    break;

            }

        }
        adminLevelRepository.save(adminLevel);

        return CodeEnum.SUCCESS.name();
    }
}
