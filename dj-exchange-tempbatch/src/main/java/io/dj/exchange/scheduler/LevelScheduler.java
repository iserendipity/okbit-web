package io.dj.exchange.scheduler;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimaps;
import io.dj.exchange.domain.primary.Coin;
import io.dj.exchange.domain.primary.Orders;
import io.dj.exchange.enums.OrderStatus;
import io.dj.exchange.enums.OrderType;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.repository.hibernate.primary.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.math.BigDecimal;
import java.util.*;

import static java.util.Collections.singletonList;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 22.
 * Description
 */
@Slf4j
@Component
public class LevelScheduler {

    @Autowired
    private AdminLevelRepository adminLevelRepository;

    @Autowired
    private UserSettingRepository userSettingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CoinRepository coinRepository;

    @Autowired
    private ManualTransactionsRepository manualTransactionsRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ApiProvider adminProvider;

    //@Scheduled(fixedRate=60000)
    public void makeFakeTransaction() throws Exception{

        BigDecimal maximumPrice = new BigDecimal(4000000);
        BigDecimal minimumPrice = new BigDecimal(50000);

        Map<String, BigDecimal> unitPriceMap = new HashMap<>();

        unitPriceMap.put("BITCOIN", new BigDecimal(1000));
        unitPriceMap.put("ETHEREUM", new BigDecimal(50));
        unitPriceMap.put("LITECOIN", new BigDecimal(50));
        unitPriceMap.put("DASH", new BigDecimal(50));
        unitPriceMap.put("MONERO", new BigDecimal(50));

        Map<String, BigDecimal> unitAmountMap = new HashMap<>();

        unitAmountMap.put("BITCOIN", new BigDecimal(0.0001));
        unitAmountMap.put("ETHEREUM", new BigDecimal(0.01));
        unitAmountMap.put("LITECOIN", new BigDecimal(0.01));
        unitAmountMap.put("DASH", new BigDecimal(0.01));
        unitAmountMap.put("MONERO", new BigDecimal(0.01));

        Map<String, BigDecimal> unitMaxPriceMap = new HashMap<>();

        unitMaxPriceMap.put("BITCOIN", new BigDecimal(4000000));
        unitMaxPriceMap.put("ETHEREUM", new BigDecimal(350000));
        unitMaxPriceMap.put("LITECOIN", new BigDecimal(100000));
        unitMaxPriceMap.put("DASH", new BigDecimal(400000));
        unitMaxPriceMap.put("MONERO", new BigDecimal(100000));

        Map<String, BigDecimal> unitMinPriceMap = new HashMap<>();

        unitMinPriceMap.put("BITCOIN", new BigDecimal(2000000));
        unitMinPriceMap.put("ETHEREUM", new BigDecimal(150000));
        unitMinPriceMap.put("LITECOIN", new BigDecimal(20000));
        unitMinPriceMap.put("DASH", new BigDecimal(100000));
        unitMinPriceMap.put("MONERO", new BigDecimal(10000));


        //List<Coin> coins = coinRepository.findAll();
        List<Coin> coins = coinRepository.findAllByName("ETHEREUM");

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.put(ACCEPT, singletonList(APPLICATION_JSON_VALUE));
        headers.put(CONTENT_TYPE, singletonList(APPLICATION_JSON_VALUE));
        headers.put("apiKey", singletonList("d0a6ca7c318917f7dfc8cc570682f088"));

        for(Coin coin : coins){

            String coinName = coin.getName();

            if(coinName.equals("KRW")){continue;}

            BigDecimal minAmount = coin.getMinAmount();

            List<Orders> buyList = orderRepository.findAllByOrderTypeAndToCoinNameAndStatusOrderByPriceDesc(OrderType.BUY, coinName, OrderStatus.PLACED);

            //log.info("--------------------- {} : {}", coinName, buyList);

            //buyList를 price기준 높은 것부터 낮은 순으로 정렬
//            if(buyList != null && buyList.size() > 0) {
//                BigDecimal maxPrice = buyList.get(0).getPrice();//buyList.parallelStream().distinct().sorted(Comparator.comparing(Orders::getPrice).reversed()).findFirst().get().getPrice();
//                //BigDecimal minPrice = buyList.parallelStream().distinct().sorted(Comparator.comparing(Orders::getPrice)).findFirst().get().getPrice();
//
//                ImmutableListMultimap group = Multimaps.index(buyList, order -> order.getPrice());
//
//                for (BigDecimal p = maxPrice; p.compareTo(unitMinPriceMap.get(coinName)) == 1; p = p.subtract(unitPriceMap.get(coinName))) {
//
//                    //log.info("----------- p : {} ", p);
//                    Thread.sleep(1000);
//
//                    //log.info("----------------- {} : {}", p, group.containsKey(p));
//                    if(!group.containsKey(p)){
//
//                        //adminProvider.buyOrder(p, minAmount, coinName, headers);
//
//                    }
//
//                }
//
//            }

            List<Orders> sellList = orderRepository.findAllByOrderTypeAndFromCoinNameAndStatusOrderByPriceAsc(OrderType.SELL, coinName, OrderStatus.PLACED);
            if(sellList != null && sellList.size() > 0) {
                BigDecimal minPrice = sellList.get(0).getPrice();

                ImmutableListMultimap group = Multimaps.index(buyList, order -> order.getPrice());

                for (BigDecimal p = minPrice; p.compareTo(unitMaxPriceMap.get(coinName)) == -1; p = p.add(unitPriceMap.get(coinName))) {

                    //log.info("----------- p : {} ", p);
                    Thread.sleep(1000);

                    //log.info("----------------- {} : {}", p, group.containsKey(p));
                    if(!group.containsKey(p)){

                        adminProvider.sellOrder(p, minAmount, coinName, headers);

                    }

                }

            }

        }

    }

}
