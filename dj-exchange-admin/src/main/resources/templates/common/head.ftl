<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="format-detection" content="telephone=no">
    <meta charset="UTF-8">

    <meta name="description" content="OK-BIT">
    <meta name="keywords" content="OK-BIT, Bitcoin, Ehterium, Monero, Litecoin, Bitfinex, Poloniex">

    <title>OK-BIT</title>

    <!-- CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/animate.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/form.css" rel="stylesheet">
    <link href="/css/calendar.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/icons.css" rel="stylesheet">
    <link href="/css/generics.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/jquery.loading.min.css" rel="stylesheet">
</head>
