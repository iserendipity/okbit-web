package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.UserSetting;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by jeongwoo on 2017. 4. 4..
 */
public interface UserSettingRepository extends JpaRepository<UserSetting, Long> {
    UserSetting findOneByUserId(Long id);

    //Optional<UserSetting> findOneByConfirmKey(String confirmKey);

    //UserSetting findOneByUserId(Long id);

    //Page<UserSetting> findAllBySended(String sended, Pageable p);
}
