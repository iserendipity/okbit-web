package io.dj.exchange.domain.chart;

import lombok.Data;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 22.
 * Description
 */
@Data
public class History implements Serializable{

    private Long t;//bartime
    private BigDecimal c;//closing
    private BigDecimal o;//opening
    private BigDecimal h;//high
    private BigDecimal l;//low
    private Double v;//volume
    private long id;

    //private long nextTime;//if no_data


}
