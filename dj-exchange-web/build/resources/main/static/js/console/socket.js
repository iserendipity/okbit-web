// var sock = new SockJS('https://prcoin.io/websock');
//var sock = new SockJS('http://107.21.192.95:8383/websock');
//var sock = new SockJS('http://localhost:8080/websock');
//var sock = new SockJS('http://172.30.1.58:8083/websock');
//var sock = new SockJS('https://www.ok-bit.com/websock');
//var sock = new SockJS('http://www.ok-bit.com:8282/websock');
//var sock = new SockJS('http://13.124.78.187:8282/websock');
//var sock = new SockJS('http://okbit-socket-elb-837563982.ap-northeast-2.elb.amazonaws.com/websock');
var sock = new SockJS('https://socket.ok-bit.com/websock');
var client = Stomp.over(sock);
client.debug = null;

client.connect({}, function(frame) {

    client.subscribe('/topic/okbit', function(message) {

        var payload = JSON.parse(message.body);

        //console.log(payload);

        var cmd = payload.cmd;
        var userId = payload.id;
        var data = payload.data;

        if (cmd == "ORDER_REFRESH") {//구매오더, 판매오더 발생시 전체 order data를 받는다.

            if(nowCoin == data.coinName) {
                getBuyTab(data.buyOrders);
                getSellTab(data.sellOrders);
            }
            // loadOrders();
            loadMarketTrades();
            makeBalance();

            if (userId == wallet.userId) {

                loadMyTrades();
                makeMyOrders();
                makeCoinStatus();
            }

        }

        // else if(cmd == 'TRADE'){
        //
        //     // // console.log(widget.options.datafeed.getServerTime(function(data){
        //     // //     return data;
        //     // // }));
        //     //
        //     // var df = widget.options.datafeed;
        //     //
        //     // // console.log(df);
        //     //
        //     // var chart = widget.chart();
        //     // // console.log(chart);
        //     // // console.log(chart.symbolExt());
        //     // //
        //     // // console.log(chart.resolution());
        //     // //
        //     // // console.log();
        //     //
        //     // //df.getBars(chart.symbolExt(), chart.resolution());
        //     //
        //     // console.log(widget.activeChart());
        //     //
        //     // _data = data;
        //     //
        //     // //df.subscribeBars(chart.symbolExt(),chart.resolution(), socketSend, '','');
        //
        //
        // }

    });
});
