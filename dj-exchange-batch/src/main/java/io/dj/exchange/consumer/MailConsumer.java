package io.dj.exchange.consumer;

import com.google.gson.Gson;
import io.dj.exchange.config.RabbitMQConfig;
import io.dj.exchange.domain.dto.mq.MessagePacket;
import io.dj.exchange.domain.primary.EmailConfirm;
import io.dj.exchange.service.mail.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 1.
 * Description
 */
@Slf4j
@Component
public class MailConsumer {

    private final MailService mailService;

    public MailConsumer(MailService mailService) {
        this.mailService = mailService;
    }

    @RabbitListener(queues = RabbitMQConfig.QUEUE_NAME_MAIL)
    public void onMessage(MessagePacket mp) throws Exception{

        Gson gson = new Gson();

        switch (mp.getCmd()) {

            case MAIL:
                mailService.sendEmail(gson.fromJson(mp.getData().toString(), EmailConfirm.class));
                break;


        }


    }

}
