//package io.dj.exchange.config;
//
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.Queue;
//import org.springframework.amqp.core.TopicExchange;
//import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
//import org.springframework.amqp.rabbit.connection.ConnectionFactory;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * Project dj-exchange-okbit.
// * Created by jeongwoo on 2017. 4. 26.
// * Description
// */
//@Configuration
////@EnableRabbit
//public class RabbitMQConfig_bak {
//
//    public static final String QUEUE_NAME_MAIL = "mailQueue";
//    public static final String EXCHANGE = QUEUE_NAME_MAIL+"-exchange";
//
//    @Bean
//    public RabbitTemplate rabbitTemplate(){
//        RabbitTemplate templates = new RabbitTemplate(connectionFactory());
//        templates.setRoutingKey(QUEUE_NAME_MAIL);
//        templates.setMessageConverter(jsonMessageConverter());
//        return templates;
//    }
//
////    @Bean
////    public SimpleMessageListenerContainer container(){
////        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
////        container.setConnectionFactory(connectionFactory());
////        container.setQueueNames(QUEUE_NAME_MAIL);
////        container.setMessageConverter(jsonMessageConverter());
////        return container;
////    }
//
//    @Bean
//    public Queue queue(){
//        return new Queue(QUEUE_NAME_MAIL, false);
//    }
//
//    @Bean
//    public TopicExchange exchange(){
//        return new TopicExchange(EXCHANGE);
//    }
//
//    @Bean
//    public Binding binding(Queue queue, TopicExchange exchange){
//        return BindingBuilder.bind(queue).to(exchange).with(QUEUE_NAME_MAIL);
//    }
//
//    @Bean
//    public Jackson2JsonMessageConverter jsonMessageConverter(){
//        return new Jackson2JsonMessageConverter();
//    }
//
////    @Bean
////    public MailMessage mailMessage(){
////        return new MailMessage();
////    }
//
//    @Bean
//    public ConnectionFactory connectionFactory(){
//        CachingConnectionFactory factory = new CachingConnectionFactory();
//        factory.setHost("localhost");
//        factory.setUsername("guest");
//        factory.setPassword("guest");
//        return factory;
//    }
//
//
//}
