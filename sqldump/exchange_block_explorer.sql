INSERT INTO exchange.block_explorer (coin_name, explorer_url) VALUES ('BITCOIN', 'https://blockchain.info/tx/');
INSERT INTO exchange.block_explorer (coin_name, explorer_url) VALUES ('DASH', 'https://chainz.cryptoid.info/dash/tx.dws?');
INSERT INTO exchange.block_explorer (coin_name, explorer_url) VALUES ('ETHEREUM', 'https://etherscan.io/tx/');
INSERT INTO exchange.block_explorer (coin_name, explorer_url) VALUES ('KRW', null);
INSERT INTO exchange.block_explorer (coin_name, explorer_url) VALUES ('LITECOIN', 'http://explorer.litecoin.net/search?q=');
INSERT INTO exchange.block_explorer (coin_name, explorer_url) VALUES ('MONERO', 'http://moneroblocks.info/search/');