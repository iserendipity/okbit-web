package io.dj.exchange.controller.advice;

import io.dj.exchange.domain.primary.Response;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.exception.OkbitAlertException;
import io.dj.exchange.exception.OkbitException;
import io.dj.exchange.exception.OkbitViewException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

@Slf4j
@ControllerAdvice(basePackages="io.dj.exchange.controller")
public class ExceptionHandlerControllerAdvice {

    @Resource(name = "messageSourceAccessor")
    private MessageSourceAccessor accessor;

    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    @ExceptionHandler(OkbitException.class)
    public Response defaultErrorHandler(OkbitException e) {

        return Response.builder()
                .code(CodeEnum.FAIL)
                .desc(accessor.getMessage(e.getMessage()))
                .build();
    }

    @ExceptionHandler(OkbitViewException.class)
    public ModelAndView okbitViewExceptionHandler(OkbitViewException e){

        ModelAndView mv = new ModelAndView("index");
        mv.addObject("error", accessor.getMessage(e.getMessage()));

        return mv;
    }

    @ExceptionHandler(OkbitAlertException.class)
    public ModelAndView okbitAlertExceptionHandler(OkbitAlertException e){

        ModelAndView mv = new ModelAndView("errorAlert");
        mv.addObject("error", accessor.getMessage(e.getMessage()));

        return mv;
    }

//    @ResponseBody
//    @ResponseStatus(value = HttpStatus.OK)
//    @ExceptionHandler(value = {OkbitException.class})
//    public Response okbitErrorHandler(OkbitException e) {
//
//        log.info("-----ok? : {}", e.getMessage());
//
//        return Response.builder()
//                .code(CodeEnum.FAIL)
//                .desc(accessor.getMessage(e.getMessage()))
//                .build();
//    }
}
