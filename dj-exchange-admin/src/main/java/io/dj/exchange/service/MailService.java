//package io.dj.exchange.service;
//
//import freemarker.template.Configuration;
//import freemarker.template.Template;
//import freemarker.template.TemplateException;
//import io.dj.exchange.domain.dto.AdminDTO;
//import io.dj.exchange.dto.ReportDTO;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mail.MailException;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.stereotype.Component;
//import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
//
//import javax.mail.MessagingException;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeMessage;
//import java.io.IOException;
//
///**
// * Project dj-exchange-okbit.
// * Created by jeongwoo on 2017. 4. 24.
// * Description
// */
//@Component
//@Slf4j
//public class MailService {
//
//    @Autowired
//    JavaMailSender javaMailSender;
//
//    //freemarker configuration
//    @Autowired
//    Configuration configuration;
//
//    public boolean sendEmail(AdminDTO.replyResponse res) {
//        MimeMessage message = javaMailSender.createMimeMessage();
//        try {
//
//            Template t = configuration.getTemplate("reply.ftl");
//
//            String readyParsedTemplate = FreeMarkerTemplateUtils
//                    .processTemplateIntoString(t, res);
//
//            message.setSubject("OKBIT에서 답변이 도착했습니다.");
//            message.setFrom(new InternetAddress("support@ok-bit.com"));
//            message.addRecipient(MimeMessage.RecipientType.TO, res.getEmail() != null ? new InternetAddress(res.getEmail()) : new InternetAddress("support@ok-bit.com"));
//            message.setText(readyParsedTemplate, "UTF-8", "html");
//
//            javaMailSender.send(message);
//
//            return true;
//
//        } catch (MessagingException e) {
//            e.printStackTrace();
//            return false;
//        } catch (MailException e) {
//            e.printStackTrace();
//            return false;
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            return false;
//
//        }// try - catch
//        catch (TemplateException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
//
//    public boolean sendReportEmail(ReportDTO res) {
//        MimeMessage message = javaMailSender.createMimeMessage();
//        try {
//
//            Template t = configuration.getTemplate("report.ftl");
//
//            String readyParsedTemplate = FreeMarkerTemplateUtils
//                    .processTemplateIntoString(t, res);
//
//            message.setSubject("OKBIT의 보고서를 보내드립니다.");
//            message.setFrom(new InternetAddress("support@ok-bit.com"));
//            message.addRecipient(MimeMessage.RecipientType.TO, res.getEmail() != null ? new InternetAddress(res.getEmail()) : new InternetAddress("support@ok-bit.com"));
//            message.setText(readyParsedTemplate, "UTF-8", "html");
//
//            javaMailSender.send(message);
//
//            return true;
//
//        } catch (MessagingException e) {
//            e.printStackTrace();
//            return false;
//        } catch (MailException e) {
//            e.printStackTrace();
//            return false;
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            return false;
//
//        }// try - catch
//        catch (TemplateException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
//
//}
