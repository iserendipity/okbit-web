package io.dj.exchange.domain.readonly;

import io.dj.exchange.domain.primary.Coin;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by jeongwoo on 2017. 7. 19..
 */
@Entity
@Data
@IdClass(WalletTransactionPK.class)
public class WalletTransaction implements Serializable{

    @Id
    @Column(length = 150)
    private String id;

    @Id
    private Long userId;

    @Id
    @Column(name="coin_name")
    private String coinName;

    private String address;

    @Column(name="tx_id")
    private String txId;

    @DecimalMin("0.00000000")
    private BigDecimal amount;

    private String tag;

    private String category;

    private LocalDateTime dt;

    private Long confirmation;

    private String status;

    @Column(length = 255)
    private String reason;


}
