var tickerSize = 0;
function loadTickers() {

    var colors = {black : 'BLACK', red : 'RED', blue : '#3582FF', white : 'WHITE' };
    var pointers = { up : 'up', down : 'down'};

    var commonApi = new CommonApi();

    commonApi.tickers(function(data) {
        var result = data.data;
        tickerSize = result.length;//전체 지수의 개수를 구함

        $.each(result, function(i, v) {

            var color = colors.white;

            if (v.change > 0) {
                color = colors.red;
                change = "+" + v.change;
                v.change = change;
                pointer = pointers.up;

            } else if (v.change < 0) {
                color = colors.blue;
                pointer = pointers.down;

            }
            //v.width = result.length == 0 ? 100 : 100 / result.length;
            v.color = color;
            v.pointer = pointer;

        });

        $('#coinHolder').html($('#indicatorTemplate').tmpl(result));
        showRollingloadTickers();
    }, null);
}

var rollingCnt = 0;
function showRollingloadTickers() {

    var index = rollingCnt % tickerSize;

    $('.indicator-block-header > h5').eq(index).css('display', 'block');
    $('.indicator-block-header > h5').not(':eq('+index+')').css('display', 'none');

    rollingCnt ++;
}

setInterval(function() {
    loadTickers();
}, 3000);

loadTickers();