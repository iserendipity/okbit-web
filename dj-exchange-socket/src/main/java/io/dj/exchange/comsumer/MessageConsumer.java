package io.dj.exchange.comsumer;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimaps;
import io.dj.exchange.domain.chart.History;
import io.dj.exchange.domain.dto.SumHistory;
import io.dj.exchange.domain.dto.mq.MessagePacket;
import io.dj.exchange.domain.readonly.MarketHistoryOrders;
import io.dj.exchange.enums.CommandEnum;
import io.dj.exchange.handler.WebsockMsgBrocker;
import io.dj.exchange.repository.hibernate.readonly.MarketOrdersRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 7.
 * Description
 */

@Slf4j
@Component
public class MessageConsumer {

    private final DateTimeFormatter tickerFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH");

    @Autowired
    private WebsockMsgBrocker websockMsgBrocker;

    @Autowired
    private MarketOrdersRepository marketOrdersRepository;

    @RabbitListener(queues = "message")
    public void onMessage(MessagePacket mp) throws Exception {

        switch (mp.getCmd()){

            case TRADE:
                //SumHistory history = new SumHistory();
                Map<String, Object> orderMap = (HashMap<String, Object>)((HashMap<String, Object>)mp.getData()).get("order");

                List<MarketHistoryOrders> marketHistory = marketOrdersRepository.findAllByOrderIdAndCompletedDtNotNull((int)orderMap.get("id"));

                List<SumHistory> sumHistories = new ArrayList<>();

                String coinName = "";

                for(MarketHistoryOrders history : marketHistory){

                    SumHistory sum =
                            new SumHistory(
                                    history.getId(),
                                    history.getCompletedDt(),
                                    history.getFromCoin().getName().equals("KRW") ? history.getToCoin().getName() : history.getFromCoin().getName(),
                                    history.getPrice(),
                                    history.getAmount().multiply(history.getPrice())
                            );
                    sumHistories.add(sum);

                    coinName = history.getFromCoin().getName().equals("KRW") ? history.getToCoin().getName() : history.getFromCoin().getName();

                }

                DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                for(int i=0; i<sumHistories.size(); i++){

                    sumHistories.get(i).setTicker(sumHistories.get(i).getDt().format(tickerFormatter));

                    String tickerDtFormatted = sumHistories.get(i).getDt().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));

                    try {
                        sumHistories.get(i).setT((dfm.parse(tickerDtFormatted)).getTime() / 1000);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                websockMsgBrocker.sendToChart(CommandEnum.TRADE.name(), mp.getUserId(), makeHistory(sumHistories), coinName);

                break;

            case MAIL:
                break;

            case ORDER:

                websockMsgBrocker.sendTo("ORDER_REFRESH", mp.getUserId(), mp.getData());

                break;
        }

    }

    public List<History> makeHistory(List<SumHistory> result){

        ImmutableListMultimap group = Multimaps.index(result, sumHistory -> sumHistory.getCoinName());
        Iterator<String> coinNameKeyIterator = group.asMap().keySet().iterator();

        List<History> historyList = new ArrayList<>();

        while (coinNameKeyIterator.hasNext()) {

            String coinName = coinNameKeyIterator.next();
            List<SumHistory> coinNameHistory = group.get(coinName);

            ImmutableListMultimap minuteGroupList = Multimaps.index(coinNameHistory, sumHistory -> sumHistory.getT());

            Iterator<Long> minuteKeyIterator = minuteGroupList.asMap().keySet().iterator();

            while (minuteKeyIterator.hasNext()) {

                long key = minuteKeyIterator.next();

                List<SumHistory> minuteHistory = minuteGroupList.get(key);

                //log.info(minuteHistory.get(0).getTicker() + "-" + coinName + "-" + resolution+" : {}", minuteHistory);

                BigDecimal c = minuteHistory.get(minuteHistory.size()-1).getP();
                BigDecimal l = minuteHistory.parallelStream().sorted(Comparator.comparing(SumHistory::getP)).findFirst().get().getP();
                BigDecimal h = minuteHistory.parallelStream().sorted(Comparator.comparing(SumHistory::getP).reversed()).findFirst().get().getP();
                BigDecimal o = minuteHistory.get(0).getP();

                long t = key;
                double v = minuteHistory.parallelStream().mapToDouble(SumHistory::getVol).sum();

                History history = new History();
                history.setL(l);
                history.setT(t);
                history.setO(o);
                history.setC(c);
                history.setH(h);
                history.setV(v);

                historyList.add(history);

                historyList.parallelStream().sorted(Comparator.comparing(History::getT)).collect(Collectors.toList());

            }

        }
        return historyList;
    }

}
