package io.dj.exchange.service;

import io.dj.exchange.domain.dto.CoinInfo;
import io.dj.exchange.domain.dto.SumMarketHistoryOrders;
import io.dj.exchange.domain.dto.Volume;
import io.dj.exchange.domain.primary.Coin;
import io.dj.exchange.domain.readonly.MarketHistoryOrders;
import io.dj.exchange.repository.hibernate.primary.CoinRepository;
import io.dj.exchange.repository.hibernate.readonly.MarketOrdersRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDateTime;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 12.
 * Description
 */
@Slf4j
@Service
public class CommonService {

    @Autowired
    private MarketOrdersRepository marketOrdersRepository;

    @Autowired
    private CoinRepository coinRepository;

//
//    CommonIntergratedService {
//         @TransactionalEx
//        public ccc() {
//            CommonService.a();
//            CommonService.b();
//        }
//    }
//
//    @TransactionalEx
//    public void a() {
//        a.save();
//        b();
//        throw new Exception(xx);
//    }
//
//    @TransactionalEx
//    public void b() {
//        b.save();
//    }

    public CoinInfo coinInfo(CoinInfo coinInfo, Pageable p){

        int loc = 0;//소수점

        //log.info("---------tlsqkf : {}", marketOrdersRepository.findAllByFromCoinNameOrToCoinNameAndCompletedDtLessThanEqualOrderByIdDesc(coinInfo.getName(), coinInfo.getName(), LocalDateTime.now().minusDays(1), p).getContent());


        String price = "0";
        String volume = "0";
        String high = "0";
        String low = "0";
        String percent = "0";
        String priceDiff = "0";
        String upDown ="";

        Page<MarketHistoryOrders> marketHistoryOrders = marketOrdersRepository.findAllByFromCoinNameOrToCoinNameOrderByIdDesc(coinInfo.getName(), coinInfo.getName(), p);
        MarketHistoryOrders recent = marketHistoryOrders.getContent().size() == 0 ? null : marketHistoryOrders.getContent().get(0);

        if (recent != null &&  recent.getPrice() != null) {
            price = recent.getPrice().setScale(loc, BigDecimal.ROUND_HALF_UP).toPlainString();

            Page<MarketHistoryOrders> priorMarketHistoryOrders = marketOrdersRepository.findAllByFromCoinNameOrToCoinNameAndCompletedDtLessThanEqualOrderByIdDesc(coinInfo.getName(), coinInfo.getName(), LocalDateTime.now().minusDays(1), p);
            MarketHistoryOrders prior24hMarketHistoryOrders = priorMarketHistoryOrders.getContent().size() == 0 ? null : priorMarketHistoryOrders.getContent().get(0);

            if (prior24hMarketHistoryOrders != null && prior24hMarketHistoryOrders.getPrice() != null) {
                if (recent.getPrice().compareTo(prior24hMarketHistoryOrders.getPrice()) == 1) {//recent큼
                    upDown = "up";
                    priceDiff = recent.getPrice().subtract(prior24hMarketHistoryOrders.getPrice()).setScale(loc, BigDecimal.ROUND_HALF_UP).toPlainString();
                    percent = recent.getPrice().subtract(prior24hMarketHistoryOrders.getPrice()).divide(recent.getPrice(), MathContext.DECIMAL32).multiply(new BigDecimal(100)).setScale(loc, BigDecimal.ROUND_HALF_UP).toPlainString();

                } else if (recent.getPrice().compareTo(prior24hMarketHistoryOrders.getPrice()) == -1) {
                    upDown = "down";
                    priceDiff = prior24hMarketHistoryOrders.getPrice().subtract(recent.getPrice()).setScale(loc, BigDecimal.ROUND_HALF_UP).toPlainString();
                    percent = prior24hMarketHistoryOrders.getPrice().subtract(recent.getPrice()).divide(recent.getPrice(), MathContext.DECIMAL32).multiply(new BigDecimal(100)).setScale(loc, BigDecimal.ROUND_HALF_UP).toPlainString();
                }

            }

            Page<MarketHistoryOrders> HighOrders = marketOrdersRepository.findAllByFromCoinNameOrToCoinNameAndCompletedDtLessThanEqualOrderByPriceDescIdDesc(coinInfo.getName(), coinInfo.getName(), LocalDateTime.now().minusDays(1), p);
            MarketHistoryOrders highOrders = HighOrders.getContent().size() == 0 ? null : HighOrders.getContent().get(0);

            if (highOrders != null && highOrders.getPrice() != null) {
                high = highOrders.getPrice().setScale(loc, BigDecimal.ROUND_HALF_UP).toPlainString();
            }

            Page<MarketHistoryOrders> LowOrders = marketOrdersRepository.findAllByFromCoinNameOrToCoinNameAndCompletedDtLessThanEqualOrderByPriceAscIdDesc(coinInfo.getName(), coinInfo.getName(), recent.getCompletedDt().minusDays(1), p);
            MarketHistoryOrders lowOrders = LowOrders.getContent().size() == 0 ? null : LowOrders.getContent().get(0);

            if (lowOrders != null && lowOrders.getPrice() != null) {
                low = lowOrders.getPrice().setScale(loc, BigDecimal.ROUND_HALF_UP).toPlainString();
            }

            SumMarketHistoryOrders sum24HMarketHistoryOrders = marketOrdersRepository.getSumMarketHistoryOrders(coinInfo.getName(), LocalDateTime.now().minusDays(1));

            if (sum24HMarketHistoryOrders != null && sum24HMarketHistoryOrders.getSumAmount() != null) {
                volume = sum24HMarketHistoryOrders.getSumAmount().setScale(loc, BigDecimal.ROUND_HALF_UP).toPlainString();
            }
        }
        Coin coin = coinRepository.findOneByName(coinInfo.getName());

        coinInfo.setUnit(coin.getUnit());
        coinInfo.setHigh(high);
        coinInfo.setLow(low);
        coinInfo.setVol(volume);
        coinInfo.setLastPrice(price);
        coinInfo.setPriceDiff(priceDiff);
        coinInfo.setUpDown(upDown);
        coinInfo.setPercent(percent);

        return coinInfo;
    }

    public Volume getVolumeAll() {

        Volume v = new Volume();

        Volume volume24h = marketOrdersRepository.getSumVolumes(LocalDateTime.now().minusDays(1));
        Volume volume7d = marketOrdersRepository.getSumVolumes(LocalDateTime.now().minusDays(7));
        Volume volume30d = marketOrdersRepository.getSumVolumes(LocalDateTime.now().minusDays(30));

        v.setVolume7d(volume7d.getVolume() != null ? volume7d.getVolume() : new BigDecimal(0));
        v.setVolume24h(volume24h.getVolume() != null ? volume24h.getVolume() : new BigDecimal(0));
        v.setVolume30d(volume30d.getVolume() != null ? volume30d.getVolume() : new BigDecimal(0));
//        v.setVolume24h(volume24h.getVolume());
//
//        v.setVolume30d(volume30d.getVolume());

        return v;


    }
}
