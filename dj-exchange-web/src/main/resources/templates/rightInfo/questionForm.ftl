<div class="modal fade" id="compose-message">
<form role="form" class="form-validation-1" id="qnaForm" action="javascript:submitQuestion();" method="post">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">NEW QUESTION</h4>
            </div>
            <div class="modal-header p-0">
                <input type="text" name="title" class="form-control input-sm input-transparent validate[required]" placeholder="Title...">
            </div>
            <div class="p-relative">
                <textarea name="contents" class="message-editor" placeholder="Message..."></textarea>
            </div>
            <div class="modal-footer m-0">
                <button class="btn"><@spring.message "BTN_SEND"/></button>
            </div>
        </div>
    </div>
    <#if qna??>
        <input name="parentId" type="hidden" value="${qna.id}"/>
    </#if>
</form>
</div>
