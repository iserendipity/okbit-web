function AdminApi () {
    this.call = function(uri, callback, resultProcFunc, params) {
        $.ajax({
            url: "/api/console" + uri
            , type: "POST"
            , dataType: 'json'
            //, contentType:"application/json; charset=UTF-8"
            , data: params
            , success: function (data) {
                resultProcFunc(data);
                return callback(data);
            }
            , error:function(e){
                //showAlert("ERROR : " + JSON.parse(e.responseText).message);
            }
        });
        return true;
    };

    this.callPost = function(uri, callback, resultProcFunc, params) {
        $.ajax({
            url: "/api/console" + uri
            , type: "POST"
            , dataType: 'json'
            , contentType:"application/json; charset=UTF-8"
            , data: JSON.stringify(params)
            , success: function (data) {
                resultProcFunc(data);
                return callback(data);
            }
            , error:function(xhr, status, err){
                console.log(xhr);
                console.log(status);
                console.log(err);
                if (xhr.status == 401) {
                    alert("401");
                } else if (xhr.status == 403) {
                    location.href='/?error=SESSION_EXPIRED'
                } else {
                    alert("예외가 발생했습니다. 관리자에게 문의하세요.");
                }
                //showAlert("ERROR : " + JSON.parse(e.responseText).message);
            }
        });
        return true;
    };

    this.getQuestion = function(callback, params) {
        return this.call("/questions", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.getReqWithdraw = function(callback, params) {
        return this.call("/reqWithdraws", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.getReqWithdrawCoin = function(callback, params) {
        return this.call("/reqCoinWithdraws", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.getWrongDeposit = function(callback, params) {
        return this.call("/getWrongDeposit", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.getReqDeposit = function(callback, params) {
        return this.call("/reqDeposit", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.getUserBankBook = function(callback, params) {
        return this.call("/bankbooks", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.getReqVerify = function(callback, params) {
        return this.call("/getReqVerify", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.getReqIdVerify = function(callback, params) {
        return this.call("/getReqIdVerify", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.getUserIdCard = function(callback, params) {
        return this.call("/idCard", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };
    this.approveBank = function(callback, params) {
        return this.callPost("/approveBank", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };
    this.approveId = function(callback, params) {
        return this.call("/approveId", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.reply = function(callback, params) {
        return this.callPost("/replyQna", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.approveWrongDeposit= function(callback, params) {
        return this.callPost("/approveWrongDeposit", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.getUserVerifyBankBook = function(callback, params) {
        return this.call("/getBankbook", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.getUserVerifyIdcard = function(callback, params) {
        return this.call("/getIdcard", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.getUserIdByDvcdCode = function(callback, params) {
        return this.call("/getUserIdByDvcdCode", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.approveTransaction = function(callback, params) {
        return this.call("/approveTransaction", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.approveSendTransaction = function(callback, params) {
        return this.call("/approveSendTransaction", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.sendExcel = function(callback, params) {
        return this.callPost("/sendExcel", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };


    this.reasonTransaction = function(callback, params) {
        return this.call("/reasonTransaction", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.reasonDepTransaction = function(callback, params) {
        return this.call("/reasonDepTransaction", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.balanceAll = function(callback, params){
        return this.call("/balance", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.coinAll = function(callback, params){
        return this.call("/coin", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };
}