<div class="tile">
    <div class="tile-title">
        <@spring.message "PROFILE_MEMBERINFO"/>
    </div>
    <div class="overflow">
        <div class="col-md-12 m-b-10">
            <h5 class="block-title"><@spring.message "PROFILE_MEMBERINFO"/></h5>
            <ol class="lists-caret">
                <li class="col-md-12 t-overflow m-b-10"><@spring.message "PROFILE_SECURITY_LEVEL"/> : <#if (adminLevel.level)??>${adminLevel.level}<#else>${user.userSetting.level}</#if> </li>
                <li class="col-md-12 t-overflow m-b-5"><@spring.message "PROFILE_EMAIL"/> : <#if user.email??>${user.email}</#if>************* </li>
                <li class="col-md-12 t-overflow m-b-5"><@spring.message "PROFILE_PASSWORD"/> : ************ </li>
                <#--<a href="" class="pull-right btn btn-xs btn-alt" style=" text-shadow: none; font-weight: bold">Change</a>-->
                <li class="col-md-12 t-overflow m-b-5"><@spring.message "PROFILE_NAME"/> :  <#if (adminLevel.realName)??>${adminLevel.realName}**********<#else><@spring.message "VERIFY_NO"/></#if><span class="t-overflow" style="color:white; text-shadow:none;">* <@spring.message "PROFILE_ACCOUNT_CAUTION"/></span></li>
            </ol>
        </div>
        <hr class="whiter">
        <div class="col-md-12 m-b-10">
            <h5 class="block-title"><@spring.message "TITLE_VERIFICATION"/></h5>
            <ol class="lists-caret">
                <li class="col-md-12 m-b-5">Email <label class="label label-primary"><@spring.message "VERIFY_YES"/></label></li>
                <li class="col-md-12 m-b-5">Cell Phone <#if (adminLevel.cellphoneYn)?? && (adminLevel.cellphoneYn) = "Y"><label class="label label-primary"><@spring.message "VERIFY_YES"/></label> <#else><label class="label label-danger"><@spring.message "VERIFY_NO"/></label> <a href="#" onclick="fnPopup();" class="pull-right btn btn-xs btn-alt" style="padding-top:1px; padding-bottom:1px; text-shadow: none; font-weight: bold"><@spring.message "VERIFY_BTN_REGIST"/></a></#if></li>
                <li class="col-md-12 m-b-5">ID card(Passport) <#if (adminLevel.idcardYn)?? && (adminLevel.idcardYn) = "Y"><label class="label label-primary"><@spring.message "VERIFY_YES"/></label> <#elseif (adminLevel.idcardYn)?? && (adminLevel.idcardYn) = "P"><label class="label label-success"><@spring.message "VERIFY_PENDING"/></label><a href="#idForm" class="pull-right btn btn-xs btn-alt" style="padding-top:1px; padding-bottom:1px; text-shadow: none; font-weight: bold" data-toggle="modal"><@spring.message "VERIFY_BTN_RETRY"/></a><#else><label class="label label-danger"><@spring.message "VERIFY_NO"/></label> <#if (adminLevel.cellphoneYn)?? && (adminLevel.cellphoneYn) = "Y"><a href="#idForm" class="pull-right btn btn-xs btn-alt" style="padding-top:1px; padding-bottom:1px; text-shadow: none; font-weight: bold" data-toggle="modal"><#else ><a href="#idForm" class="pull-right btn btn-xs btn-alt disabled" style="padding-top:1px; padding-bottom:1px; text-shadow: none; font-weight: bold" data-toggle="modal"></#if><@spring.message "VERIFY_BTN_REGIST"/></a></#if></li>
                <li class="col-md-12 m-b-5">Bank Account <#if (adminLevel.bankbookYn)?? && (adminLevel.bankbookYn) = "Y"><label class="label label-primary"><@spring.message "VERIFY_YES"/></label> <#elseif (adminLevel.bankbookYn)?? && (adminLevel.bankbookYn) = "P"><label class="label label-success"><@spring.message "VERIFY_PENDING"/></label><a href="#bankForm" class="pull-right btn btn-xs btn-alt" style="padding-top:1px; padding-bottom:1px; text-shadow: none; font-weight: bold" data-toggle="modal"><@spring.message "VERIFY_BTN_RETRY"/></a>  <#else><label class="label label-danger"><@spring.message "VERIFY_NO"/></label><#if (adminLevel.cellphoneYn)?? && (adminLevel.cellphoneYn) = "Y"><a href="#bankForm" class="pull-right btn btn-xs btn-alt" style="padding-top:1px; padding-bottom:1px; text-shadow: none; font-weight: bold" data-toggle="modal"><#else ><a href="#bankForm" class="pull-right btn btn-xs btn-alt disabled" style="padding-top:1px; padding-bottom:1px; text-shadow: none; font-weight: bold" data-toggle="modal"></#if><@spring.message "VERIFY_BTN_REGIST"/></a></#if></li>
                <#--<li class="col-md-12 t-overflow m-b-5">2FA <#if (adminLevel.idcardYn)??><label class="label label-primary">Verified</label> <#else><label class="label label-danger">Unverified</label> <a href="#cellForm" class="pull-right btn btn-xs btn-alt" style=" text-shadow: none; font-weight: bold" data-toggle="modal">Verify</a></#if>-->
                <#--<a href="#bankForm" class="pull-right btn btn-xs btn-alt" style="padding-top:1px; padding-bottom:1px; text-shadow: none; font-weight: bold" data-toggle="modal"><@spring.message "VERIFY_BTN_RETRY"/></a>-->
            </ol>
        </div>
        <hr class="whiter">
        <div class="col-md-12 m-b-10">
            <h5 class="block-title"><@spring.message "TITLE_LEVEL_INFO"/></h5>
            <ol class="lists-caret">
                <li class="col-md-12 m-b-5">Level 1 : <@spring.message "VERIFY_LEV1"/></li>
                <p style="margin-left:18px;"><@spring.message "VERIFY_LEV1_EX"/></p>
                <li class="col-md-12 m-b-5">Level 2 : <@spring.message "VERIFY_LEV2"/></li>
                <p style="margin-left:18px;"><@spring.message "VERIFY_LEV2_EX"/></p>
                <li class="col-md-12 m-b-5">Level 3 : <@spring.message "VERIFY_LEV3"/></li>
                <p style="margin-left:18px;"><@spring.message "VERIFY_LEV3_EX"/></p>
            </ol>
        </div>
        <hr class="whiter">
        <div class="col-md-12 m-b-10">
            <h5 class="block-title"><@spring.message "TITLE_WITHDRAWAL_LIMIT"/></h5>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <ol class="lists-caret">
                    <li class="col-md-12 m-b-5"><@spring.message "ONCE_LIMIT"/></li>
                    <p style="margin-left:18px;">KRW : 10,000,000W <br> BITCOIN : 50BTC <br> LITECOIN : 5000LTC <br> ETHEREUM : 500ETH <br> DASH : 1500DASH <br> MONERO : 5000XMR</p>
                </ol>
            </div>
            <div class="col-md-6">
                <ol class="lists-caret">
                    <li class="col-md-12 m-b-5"><@spring.message "ONEDAY_LIMIT"/></li>
                    <p style="margin-left:18px;">KRW : 500,000,000W <br> BITCOIN : 200BTC <br> LITECOIN : 20000LTC <br> ETHEREUM : 2000ETH <br> DASH : 5000DASH <br> MONERO : 20000XMR</p>
                </ol>
            </div>
            <#--<ol class="lists-caret">-->
                <#--<li class="col-md-12 m-b-5">1회 출금한도</li>-->
                <#--<p style="margin-left:18px;">KRW : 10,000,000W <br> BITCOIN : 50BTC <br> LITECOIN : 5000LTC <br> ETHEREUM : 500ETH <br> DASH : 1500DASH <br> MONERO : 5000XMR</p>-->
                <#--<li class="col-md-12 m-b-5">1일 출금한도</li>-->
                <#--<p style="margin-left:18px;">KRW : 100,000,000W <br> BITCOIN : 200BTC <br> LITECOIN : 20000LTC <br> ETHEREUM : 2000ETH <br> DASH : 5000DASH <br> MONERO : 20000XMR</p>-->
            <#--</ol>-->
        </div>
    </div>
    <form name="form_chk" method="post">

    </form>
</div>