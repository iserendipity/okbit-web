package io.dj.exchange.provider;

import feign.RequestLine;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 7. 3.
 * Description
 */
public interface BitfinexProvider {

//    @RequestLine("GET /candles/")
//    List<List<Double>> getCandlesLast(@Param("start") long start, @Param("end") long end);

    @RequestLine("GET /candles/trade:1m:tBTCUSD/last")
    List<List<Double>> getCandlesLast();

    @RequestLine("GET /candles/trade:1m:tBTCUSD/hist")
    List<List<BigDecimal>> getCandlesHist();

//    @RequestLine("GET /candles/trade:1m:tBTCUSD/hist")
//    List<List<BigDecimal>> getCandlesHist(@Param("start") long from, @Param("end") long to);


    //https://api.bitfinex.com/v2/candles/trade::TimeFrame::Symbol/Section


}
