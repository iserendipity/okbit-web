package io.dj.exchange.controller.api;

import io.dj.exchange.domain.dto.OrdersCount;
import io.dj.exchange.domain.dto.OrdersDTO;
import io.dj.exchange.domain.primary.MyHistoryOrders;
import io.dj.exchange.domain.primary.Response;
import io.dj.exchange.domain.primary.User;
import io.dj.exchange.domain.readonly.MarketHistoryOrders;
import io.dj.exchange.enums.OrderStatus;
import io.dj.exchange.provider.ApiProvider;
import io.dj.exchange.repository.hibernate.primary.MyOrdersRepository;
import io.dj.exchange.repository.hibernate.primary.OrderRepository;
import io.dj.exchange.repository.hibernate.readonly.MarketOrdersRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 21.
 * Description
 */
@RestController
@Slf4j
@RequestMapping("/api/console")
public class ConsoleApiController {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private MarketOrdersRepository marketOrdersRepository;

    @Autowired
    private MyOrdersRepository myOrdersRepository;

    @Autowired
    private ApiProvider apiProvider;

    @RequestMapping(value = "/ordersBuy", method = RequestMethod.POST)
    public Response<List<OrdersCount>> ordersBuy(@Valid @RequestBody OrdersDTO.RequestToOrderList request, @PageableDefault(sort = { "id" }, direction = Sort.Direction.DESC, size = 20) Pageable p) throws Exception{
            return CommonApiController.initOrdersCount(orderRepository.getBuy(request.getOrderType(), request.getFromCoin(), request.getToCoin(), OrderStatus.PLACED, p).getContent());
        //return Response.<List<OrdersCount>>builder().data(orderRepository.getBuy(request.getOrderType(), request.getFromCoin(), request.getToCoin(), OrderStatus.PLACED, p).getContent()).code(CodeEnum.SUCCESS).build();

    }

    @RequestMapping(value = "/ordersSell", method = RequestMethod.POST)
    public Response<List<OrdersCount>> ordersSell(@Valid @RequestBody OrdersDTO.RequestToOrderList request, @PageableDefault(sort = { "id" }, direction = Sort.Direction.DESC, size = 20) Pageable p) throws Exception{
        return CommonApiController.initOrdersCount(orderRepository.getSell(request.getOrderType(), request.getFromCoin(), request.getToCoin(), OrderStatus.PLACED, p).getContent());
        //return Response.<List<OrdersCount>>builder().data(orderRepository.getSell(request.getOrderType(), request.getFromCoin(), request.getToCoin(), OrderStatus.PLACED, p).getContent()).code(CodeEnum.SUCCESS).build();

    }

    @RequestMapping(value = "/ordersFullBuy", method = RequestMethod.POST)
    public Response<List<OrdersCount>> ordersFullBuy(@Valid @RequestBody OrdersDTO.RequestToOrderList request) throws Exception{
        return CommonApiController.initOrdersCount(orderRepository.getBuy(request.getOrderType(), request.getFromCoin(), request.getToCoin(), OrderStatus.PLACED));
        //return Response.<List<OrdersCount>>builder().data(orderRepository.getBuy(request.getOrderType(), request.getFromCoin(), request.getToCoin(), OrderStatus.PLACED, p).getContent()).code(CodeEnum.SUCCESS).build();

    }

    @RequestMapping(value = "/ordersFullSell", method = RequestMethod.POST)
    public Response<List<OrdersCount>> ordersFullSell(@Valid @RequestBody OrdersDTO.RequestToOrderList request) throws Exception{
        return CommonApiController.initOrdersCount(orderRepository.getSell(request.getOrderType(), request.getFromCoin(), request.getToCoin(), OrderStatus.PLACED));
        //return Response.<List<OrdersCount>>builder().data(orderRepository.getSell(request.getOrderType(), request.getFromCoin(), request.getToCoin(), OrderStatus.PLACED, p).getContent()).code(CodeEnum.SUCCESS).build();

    }

    @RequestMapping(value = "/trades", method = RequestMethod.POST)
    public Response<List<MarketHistoryOrders>> trades(@Valid @RequestBody OrdersDTO.RequestToMarketHistoryOrders request, @PageableDefault(sort = { "id" }, direction = Sort.Direction.DESC, size = 20) Pageable p) throws Exception{

        return CommonApiController.initHistoryOrder(marketOrdersRepository.findAllByFromCoinNameOrToCoinNameOrderByIdDesc(request.getCoinName(), request.getCoinName(), p).getContent());

    }

    @RequestMapping(value = "/fullTrades", method = RequestMethod.POST)
    public Response<List<MarketHistoryOrders>> fullTrades(@Valid @RequestBody OrdersDTO.RequestToMarketHistoryOrders request) throws Exception{

        return CommonApiController.initHistoryOrder(marketOrdersRepository.findAllByFromCoinNameOrToCoinNameOrderByIdDesc(request.getCoinName(), request.getCoinName()));

    }

    @RequestMapping(value = "/myTrades", method = RequestMethod.POST)
    public Response<List<MyHistoryOrders>> myTrades(@Valid @RequestBody OrdersDTO.RequestToMarketHistoryOrders request, @ModelAttribute("user") User user, @PageableDefault(sort = { "id" }, direction = Sort.Direction.DESC, size = 20) Pageable p) throws Exception{

        return CommonApiController.initMyHistoryOrder(myOrdersRepository.findAllByUserIdAndFromCoinNameOrToCoinNameOrderByIdDesc(user.getId(),request.getCoinName(), request.getCoinName(), p).getContent());

    }

    @RequestMapping(value = "/myFullTrades", method = RequestMethod.POST)
    public Response<List<MyHistoryOrders>> myFullTrades(@Valid @RequestBody OrdersDTO.RequestToMarketHistoryOrders request, @ModelAttribute("user") User user) throws Exception{

        return CommonApiController.initMyHistoryOrder(myOrdersRepository.findAllByUserIdAndFromCoinNameOrToCoinNameOrderByIdDesc(user.getId(),request.getCoinName(), request.getCoinName()));

    }

}
