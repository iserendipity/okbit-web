package io.dj.exchange.controller;

import freemarker.template.Configuration;
import freemarker.template.Template;
import io.dj.exchange.domain.dto.AdminDTO;
import io.dj.exchange.domain.primary.SupportQna;
import io.dj.exchange.domain.readonly.BankCode;
import io.dj.exchange.exception.OkbitException;
import io.dj.exchange.repository.hibernate.primary.SupportQnaRepository;
import io.dj.exchange.repository.hibernate.readonly.BankCodeRepository;
import io.dj.exchange.service.MakeExcel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 11.
 * Description
 */
@Controller
@Slf4j
public class AdminController {

    @Autowired
    private SupportQnaRepository qnaRepository;

    @Autowired
    private BankCodeRepository bankCodeRepository;

    @Autowired
    private Configuration configuration;

    @RequestMapping("/")
    public ModelAndView home(){

        ModelAndView mv = new ModelAndView("index");
        return mv;

    }


    @RequestMapping("/report")
    public void report(HttpServletRequest request, HttpServletResponse response) throws Exception{

        //ModelAndView mv = new ModelAndView("daily");

        Map<String, Object> beans = new HashMap<>();

        Template t = configuration.getTemplate("daily.ftl");

        String readyParsedTemplate = FreeMarkerTemplateUtils
                .processTemplateIntoString(t, beans);


        MakeExcel me = new MakeExcel();
        me.download(request, response, beans, me.get_Filename("OKBIT_DAILY_"), readyParsedTemplate);

        //return mv;

    }


    @RequestMapping("/console")
    public ModelAndView console(@RequestParam(name="coin", defaultValue = "BITCOIN") String coinName){

        ModelAndView mv = new ModelAndView("console");

        //문의사항 카운트
        long cnt = qnaRepository.countByShowYn("Y");
        mv.addObject("qnaCount", cnt);
        mv.addObject("coin", coinName);

        return mv;

    }

//    @RequestMapping("/coinInfo")
//    public ModelAndView coins(){
//        ModelAndView mv = new ModelAndView("coins");
//
//        //문의사항 카운트
//        long cnt = qnaRepository.countByShowYn("Y");
//        mv.addObject("qnaCount", cnt);
//
//        return mv;
//
//    }


    @RequestMapping("/deposit")
    public ModelAndView text(){

        //문의사항 카운트
        long cnt = qnaRepository.countByShowYn("Y");

//        List<BankCode> bankCodeList = bankCodeRepository.findAll();

        ModelAndView mv = new ModelAndView("deposit");

        mv.addObject("qnaCount", cnt);
//        mv.addObject("bankList", bankCodeList);

        return mv;

    }

    @RequestMapping("/withdraw")
    public ModelAndView withdraw(){

        //문의사항 카운트
        long cnt = qnaRepository.countByShowYn("Y");

        ModelAndView mv = new ModelAndView("withdraw");
        mv.addObject("qnaCount", cnt);
        return mv;

    }

    @RequestMapping("/verify")
    public ModelAndView verify(){

        //문의사항 카운트
        long cnt = qnaRepository.countByShowYn("Y");

        ModelAndView mv = new ModelAndView("verify");
        mv.addObject("qnaCount", cnt);
        List<BankCode> bankCodeList = bankCodeRepository.findAll();
        mv.addObject("bankList", bankCodeList);

        return mv;

    }

    @RequestMapping("/messageDetail")
    public ModelAndView detail(@RequestParam long id, @PageableDefault Pageable p) throws OkbitException{

        long cnt = qnaRepository.countByShowYn("Y");

        SupportQna question = qnaRepository.findOneById(id).orElseThrow(()->new OkbitException("잘못된 액션입니다"));

        if(question.getParentId() != null){

            question = qnaRepository.findOneById(question.getParentId()).orElseThrow(() -> new OkbitException("WRONG_ACTION"));

        }

        ModelAndView mv = new ModelAndView("messageDetail");
        mv.addObject("qnaCount", cnt);
        //mv.addObject("qna", qna);

        //SupportQna question = qnaRepository.findOneById(id).orElseThrow(()->new OkbitException("WRONG_ACTION"));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        question.setRegDtTxt(question.getRegDt().format(formatter));

        Page<SupportQna> reply = qnaRepository.findAllByParentIdOrderByRegDtDesc(question.getId(), p);

        mv.addObject("qna", question);

        if(reply.hasContent()){
            reply.getContent().stream().forEach(ans -> ans.setRegDtTxt(ans.getRegDt().format(formatter)));
            mv.addObject("reply", reply.getContent());
            mv.addObject("pageNo", p.getPageNumber());
            mv.addObject("pageSize", p.getPageSize());
            mv.addObject("totalPage", reply.getTotalPages());

        }

        return mv;

    }

    @RequestMapping("/reply")
    public ModelAndView reply(){

        AdminDTO.replyResponse res = new AdminDTO.replyResponse();

        res.setEmail("iserendipity@me.com");
        res.setContents("질문 내용 입니다");
        res.setTitle("질문 제목입니다.");
        res.setRepTitle("답변 제목입니다.");
        res.setRepContents("답변 내용입니다.");

        ModelAndView mv = new ModelAndView("reply");
        mv.addObject("res", res);
        return mv;

    }

}
