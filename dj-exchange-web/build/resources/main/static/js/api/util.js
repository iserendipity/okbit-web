String.prototype.trim = function() {
    return this.replace(/(^\s*)|(\s*$)/gi, "");
};

Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";
 
   
    var d = this;
     
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);        
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            default: return $1;
        }
    });
};
 
String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

/*jquery tmpl required*/
makeTemplate = function(targetId, templateId, jsonData){

    if(jsonData) {
        $("#" + targetId).html($('#' + templateId).tmpl(jsonData));
    }

};

/*jquery tmpl required*/
makeTemplatePre = function(targetId, templateId, jsonData){

    if(jsonData) {
        $("#" + targetId).prepend($('#' + templateId).tmpl(jsonData));
    }

};

makeTemplateApp = function(targetId, templateId, jsonData){

    if(jsonData) {
        $("#" + targetId).append($('#' + templateId).tmpl(jsonData));
    }

};


/* json parser */
setValues = function(targetId, jsonData){

    //폼의 name 과 obj의 키값이 동일한 경우 해당 폼에 값을 넣어준다.
    $('#'+targetId).find('input, select, textarea, div, span, td, a').each(function(){

        var name = $(this).attr('name');
        var cls = $(this).attr('class');

        var $element = $(this);

        $.each(jsonData, function(key, value){
            if(name == key){
                if($element.prop('tagName') === 'INPUT' | $element.prop('tagName') === 'SELECT' ){
                    //console.log('111111'+value);
                    $element.val(value);

                }else{
                    $element.text(value);

                }

            }else if(cls && cls.indexOf(key) > 0){

                if($element.prop('tagName') === 'INPUT' | $element.prop('tagName') === 'SELECT' ){
                    $element.val(value);

                }else{
                    $element.text(value);

                }

            }
        });
    });
};

setPageValues = function(targetId, pageData, functionName, nowPage){

    var $li = '';

    for(var i=1; i<pageData.totalPages+1; i++){
        if(nowPage == i){
            $li += '<li class="active"><a href="javascript:'+functionName+'('+i+')">'+i+'</a>';
        }else{
            $li += '<li><a href="javascript:'+functionName+'('+i+')">'+i+'</a>';
        }


    }

    $('#'+targetId).html($li);

};

copy = function(elem){
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }

    alert(COPY_DONE);

    return succeed;
};