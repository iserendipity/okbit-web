<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
    </head>
    <body id="skin-blur-greenish">

        <header id="header" class="media">
            <#include "common/top.ftl">
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            
            <!-- Sidebar -->
            <aside id="sidebar">

                <!-- Side Menu -->
                <ul class="list-unstyled side-menu">
                    <#include "common/sidemenu.ftl">
                </ul>

            </aside>
        
            <!-- Content -->
            <section id="content" class="container">
                <div id="modal" class="modal fade"> <!-- Style for just preview -->
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    ×
                                </button>
                                <h4 class="modal-title">New Deposit</h4>
                            </div>
                            <div class="modal-body">
                                <form id="depositForm" role="form" class="form-validation-2" method="post" enctype="multipart/form-data">
                                    <input type="file" name="depositFile" id="depositFile" >
                                    <#--<div class="fileupload fileupload-new" data-provides="fileupload">-->
                                        <#--<span class="btn btn-file btn-sm btn-alt">-->
                                            <#--<span class="fileupload-new">Select file</span>-->
                                            <#--<span class="fileupload-exists">Change</span>-->
                                            <#--<input type="file" />-->
                                        <#--</span>-->
                                        <#--<span class="fileupload-preview"></span>-->
                                        <#--<a href="#" class="close close-pic fileupload-exists" data-dismiss="fileupload">-->
                                            <#--<i class="fa fa-times"></i>-->
                                        <#--</a>-->
                                    <#--</div>-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="depositDvcd">개인식별코드</label>-->
                                        <#--<input type="text" id="depositDvcd" name="depositDvcd" placeholder="개인식별코드를 입력하세요." class="form-control validate[required]">-->
                                    <#--</div>-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="amount">입금액</label>-->
                                        <#--<input type="text" id="amount" name="amount" placeholder="입금액을 입력하세요." class="form-control validate[required, custom[onlyNumberSp]]">-->
                                    <#--</div>-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="bankNm">입금은행</label>-->
                                        <#--<input type="text" id="bankNm" name="bankNm" placeholder="입금은행을 입력하세요." class="form-control validate[required]">-->
                                    <#--</div>-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="userNm">예금주</label>-->
                                        <#--<input type="text" id="userNm" name="userNm" placeholder="입금한 사람의 이름을 입력하세요." class="form-control validate[required]">-->
                                    <#--</div>-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="depositDt">입금일시</label>-->
                                        <#--<input type="text" id="depositDt" name="depositDt" placeholder="입금일과 시간을 입력하세요" class="form-control mask-date_time validate[required]">-->
                                    <#--</div>div-->
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm" onclick="javascript:sendExcel();">Send</button>
                                <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <#include "common/messages.ftl">

                <div class="clearfix"></div>

                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li><a href="/console">Home</a></li>
                    <li class="active">Qna</li>
                    <#--<li class="active">Data</li>-->
                </ol>
                
                <h4 class="page-title">&nbsp;</h4>
                                
                <div class="block-area">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="tile">
                                        <div class="listview list-click">
                                        <#--<h2 class="page-title">${qna.title}</h2>-->
                                            <div class="media message-header o-visible">
                                                <#--<div class="pull-right dropdown m-t-10">-->
                                                    <#--<a href="" data-toggle="dropdown" class="p-5">Options</a>-->
                                                    <#--<ul class="dropdown-menu text-right">-->
                                                        <#--<li><a data-toggle="modal" href="#compose-message" title="" data-original-title="REPLY">-->
                                                            <#--REPLY</a></li>-->
                                                    <#--&lt;#&ndash;<li><a href="">Forward</a></li>&ndash;&gt;-->
                                                    <#--&lt;#&ndash;<li><a href="">Mark as Spam</a></li>&ndash;&gt;-->
                                                    <#--</ul>-->
                                                <#--</div>-->
                                                <div class="media-body">
                                                    <span class="f-bold pull-left m-b-5">Title : ${qna.title}</span>
                                                    <div class="clearfix"></div>
                                                    <span class="dropdown m-t-5">
                                                ${qna.regDtTxt}
                                            <#--To <a href="" class="underline">Me</a> on 12th February 2014-->
                                            </span>
                                                </div>
                                            </div>

                                            <div class="p-15">
                                            ${qna.contents}
                                        <#--<p>Hi Wendy,</p>-->
                                        <#--<p>Morbi dignissim tortor urna, eget feugiat ipsum rutrum et. Aenean sem nisi, ultricies vel lorem nec, blandit venenatis tellus. Donec accumsan, turpis vel euismod ultrices, massa quam ultricies turpis, non pretium dolor nulla sit amet sem. Ut vel blandit sem, vitae tristique tellus. Suspendisse odio ligula, feugiat eu velit in, mattis pharetra libero. Nunc ac tincidunt nibh. Nullam mauris urna, elementum ac odio et, condimentum suscipit orci. Nulla pellentesque a magna et pellentesque. Suspendisse euismod convallis nisi sed placerat.</p>-->
                                        <#--<p>Morbi imperdiet tempor velit, sed sagittis ipsum. In gravida, enim ac commodo congue, purus augue faucibus tellus, vel tempus sapien enim eget urna. Mauris molestie fringilla orci, et scelerisque sem dapibus convallis. In ut erat quam. Donec facilisis ipsum ac nulla lobortis.</p>-->
                                        <#--<p>Regards,<br/> Malinda Hollaway</p>-->
                                            </div>
                                        <#if reply??>
                                            <#list reply as r>
                                                <hr class="whiter">

                                                <div class="p-15">
                                                <#--<img src="img/profile-pics/6.jpg" alt="" class="pull-left" width="40">-->
                                                    <i class="fa fa-mail-forward fa-2x pull-left" style="width:40; height:40"></i>
                                                    <span class="f-bold pull-left m-b-5">${r.title}</span>
                                                    <div class="clearfix"></div>
                                                    <span class="dropdown m-t-5" style="margin-left:30px">
                                                    ${r.regDtTxt}
                                                </span>
                                                </div>
                                                <div class="p-15" style="margin-left:30px;">
                                                ${r.contents}
                                                </div>
                                            </#list>
                                        </#if>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <#include "common/questionForm.ftl">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
                <!-- Chat -->
                <div class="chat">
                    
                    <!-- Chat List -->
                    <div class="pull-left chat-list">
                        <div class="listview narrow">
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/1.jpg" width="30" alt="">
                                <div class="media-body p-t-5">
                                    Alex Bendit
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/2.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">David Volla Watkinson</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/3.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Mitchell Christein</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/4.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Wayne Parnell</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/5.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Melina April</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/6.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Ford Harnson</span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <!-- Chat Area -->
                    <div class="media-body">
                        <div class="chat-header">
                            <a class="btn btn-sm" href="">
                                <i class="fa fa-circle-o status m-r-5"></i> Chat with Friends
                            </a>
                        </div>
                    
                        <div class="chat-body">
                            <div class="media">
                                <img class="pull-right" src="/img/profile-pics/1.jpg" width="30" alt="" />
                                <div class="media-body pull-right">
                                    Hiiii<br/>
                                    How you doing bro?
                                    <small>Me - 10 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-left">
                                <img class="pull-left" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    I'm doing well buddy. <br/>How do you do?
                                    <small>David - 9 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-right">
                                <img class="pull-right" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    I'm Fine bro <br/>Thank you for asking
                                    <small>Me - 8 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-right">
                                <img class="pull-right" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    Any idea for a hangout?
                                    <small>Me - 8 Mins ago</small>
                                </div>
                            </div>
                        
                        </div>
                    
                        <div class="chat-footer media">
                            <i class="chat-list-toggle pull-left fa fa-bars"></i>
                            <i class="pull-right fa fa-picture-o"></i> 
                            <div class="media-body">
                                <textarea class="form-control" placeholder="Type something..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Older IE Message -->
            <!--[if lt IE 9]>
                <div class="ie-block">
                    <h1 class="Ops">Ooops!</h1>
                    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser in order to access the maximum functionality of this website. </p>
                    <ul class="browsers">
                        <li>
                            <a href="https://www.google.com/intl/en/chrome/browser/">
                                <img src="/img/browsers/chrome.png" alt="">
                                <div>Google Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.mozilla.org/en-US/firefox/new/">
                                <img src="/img/browsers/firefox.png" alt="">
                                <div>Mozilla Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com/computer/windows">
                                <img src="/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://safari.en.softonic.com/">
                                <img src="/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-10/worldwide-languages">
                                <img src="/img/browsers/ie.png" alt="">
                                <div>Internet Explorer(New)</div>
                            </a>
                        </li>
                    </ul>
                    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
                </div>   
            <![endif]-->
        </section>

        <form id="loadingForm"></form>

        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->
        <script src="/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
        <script src="/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Charts -->
        <script src="/js/charts/jquery.flot.js"></script> <!-- Flot Main -->
        <script src="/js/charts/jquery.flot.time.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub -->
        <script src="/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen -->

        <script src="/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts -->
        <script src="/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts -->
        <script src="/js/charts.js"></script> <!-- All the above chart related functions -->

        <!--scroll -->
        <script src="/js/scroll.min.js"></script>

        <!--  Form Related -->
        <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->

        <!-- UX -->
        <#--<script src="/js/scroll.min.js_bak"></script> <!-- Custom Scrollbar &ndash;&gt;-->
        <#--<script src="js/markdown.min.js"></script> <!-- Markdown Editor &ndash;&gt;-->
        <script src="js/editor.min.js"></script>

        <!-- Jqutemplateslate -->
        <script src="/js/jquery.tmpl.min.js"></script>

        <!-- Other -->
        <script src="/js/calendar.min.js"></script> <!-- Calendar -->
        <script src="/js/feeds.min.js"></script> <!-- News Feeds -->

        <script src="/js/jquery.loading.min.js"></script> <!-- loading -->

        <script src="/js/validation/validate.min.js"></script> <!-- jQuery Form Validation Library -->
        <script src="/js/validation/validationEngine.min.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->
        <script src="js/input-mask.min.js"></script> <!-- Input Mask -->
        <!-- All JS functions -->
        <script src="/js/functions.js"></script>
        <script src="/js/api/admin_api.js"></script>
        <script src="/js/view/admin.js"></script>

        <script type="text/javascript">
            $('document').ready(function(){
                $('form').on('submit',function(){
                    return confirm('답변하겠습니까?');
                });

                $('.message-editor').summernote({
                    toolbar: false,
                    height: 200,
                    resizable: false
                });
            });

        </script>

    </body>
</html>
