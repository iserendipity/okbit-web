import io.dj.exchange.Application;
import io.dj.exchange.scheduler.ChartScheduler;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 15.
 * Description
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Application.class})
@TestPropertySource(locations = "classpath:application.properties")
@WebAppConfiguration
public class Test {

//    @Autowired
//    private MailService mailService;

    @Autowired
    private ChartScheduler chartScheduler;

    @org.junit.Test
    public void test() throws Exception{

        chartScheduler.chartResolution1MCachingTest();

    }

}
