package io.dj.exchange.enums;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 8.
 * Description
 */
public enum ScopeEnum {
    PUBLIC, PRIVATE
}
