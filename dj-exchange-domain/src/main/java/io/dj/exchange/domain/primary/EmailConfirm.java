package io.dj.exchange.domain.primary;

import io.dj.exchange.enums.MailType;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
public class EmailConfirm implements Serializable {

    @Id
    @Column(name = "hash_email", nullable = false, unique = true)
    String hashEmail;

    @Column(name = "confirm_code", nullable = false)
    private String confirmCode;

    @Column(name = "email")
    private String email;

    @Column(name = "del_dtm")
    private LocalDateTime delDtm;

    @Column(name = "send_yn")
    private String sendYn;

    @JsonIgnore
    @Column(name = "reg_dt")
    private LocalDateTime regDt;

    @Transient
    private MailType type;

    @Transient
    private String url;

}
