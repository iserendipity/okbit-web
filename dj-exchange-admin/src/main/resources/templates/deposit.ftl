<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
    </head>
    <body id="skin-blur-blue">

        <header id="header" class="media">
            <#include "common/top.ftl">
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            
            <!-- Sidebar -->
            <aside id="sidebar">

                <!-- Side Menu -->
                <ul class="list-unstyled side-menu">
                    <#include "common/sidemenu.ftl">
                </ul>

            </aside>
        
            <!-- Content -->
            <section id="content" class="container">
                <div id="modal" class="modal fade"> <!-- Style for just preview -->
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    ×
                                </button>
                                <h4 class="modal-title">New Deposit</h4>
                            </div>
                            <div class="modal-body">
                                <form id="depositForm" role="form" class="form-validation-2" method="post" enctype="multipart/form-data">
                                    <input type="file" name="depositFile" id="depositFile" >
                                    <#--<div class="fileupload fileupload-new" data-provides="fileupload">-->
                                        <#--<span class="btn btn-file btn-sm btn-alt">-->
                                            <#--<span class="fileupload-new">Select file</span>-->
                                            <#--<span class="fileupload-exists">Change</span>-->
                                            <#--<input type="file" />-->
                                        <#--</span>-->
                                        <#--<span class="fileupload-preview"></span>-->
                                        <#--<a href="#" class="close close-pic fileupload-exists" data-dismiss="fileupload">-->
                                            <#--<i class="fa fa-times"></i>-->
                                        <#--</a>-->
                                    <#--</div>-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="depositDvcd">개인식별코드</label>-->
                                        <#--<input type="text" id="depositDvcd" name="depositDvcd" placeholder="개인식별코드를 입력하세요." class="form-control validate[required]">-->
                                    <#--</div>-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="amount">입금액</label>-->
                                        <#--<input type="text" id="amount" name="amount" placeholder="입금액을 입력하세요." class="form-control validate[required, custom[onlyNumberSp]]">-->
                                    <#--</div>-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="bankNm">입금은행</label>-->
                                        <#--<input type="text" id="bankNm" name="bankNm" placeholder="입금은행을 입력하세요." class="form-control validate[required]">-->
                                    <#--</div>-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="userNm">예금주</label>-->
                                        <#--<input type="text" id="userNm" name="userNm" placeholder="입금한 사람의 이름을 입력하세요." class="form-control validate[required]">-->
                                    <#--</div>-->
                                    <#--<div class="form-group">-->
                                        <#--<label for="depositDt">입금일시</label>-->
                                        <#--<input type="text" id="depositDt" name="depositDt" placeholder="입금일과 시간을 입력하세요" class="form-control mask-date_time validate[required]">-->
                                    <#--</div>div-->
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm" onclick="javascript:sendExcel();">Send</button>
                                <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <#include "common/messages.ftl">

                <div class="clearfix"></div>

                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li><a href="/console">Home</a></li>
                    <li class="active">Deposit</li>
                    <#--<li class="active">Data</li>-->
                </ol>
                
                <h4 class="page-title">&nbsp;</h4>
                                
                <div class="block-area">
                    <div class="row">
                        <div class="col-md-12">
                            <!--  ORDERS -->
                            <div class="row">
                                <div class="col-md-6">
                                    <script id="withdrawTemplate" type="text/x-jquery-tmpl">
                                    <tr>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${regDtTxt}"}</td>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${depositDtTxt}"}</td>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${amount}"}</td>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${bankNm}"}</td>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${userNm}"}</td>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${level}"}</td>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${realName}"}</td>
                                        <#--<td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${reason}"}</td>-->
                                        <td class="text-center" ><button onclick="javascript:approveTransaction('${r"${id}"}', this);" class="btn btn-alt btn-xs" type="button">입금승인</button><button href="#" onclick="javascript:reasonDepTransaction('${r"${id}"}', this);" class="btn btn-alt btn-xs" type="button">입금반려</button></td>
                                    </tr>
                                    </script>
                                    <div class="tile">
                                        <h2 class="tile-title">Request Deposit
                                        </h2>
                                        <div class="tile-config dropdown">
                                            <a data-toggle="dropdown" href="" class="tile-menu"></a>
                                            <ul class="dropdown-menu pull-right text-right">
                                                <li><a href="#modal" data-toggle="modal" data-target="#modal">new Deposit</a></li>
                                            </ul>
                                        </div>
                                        <div class="overflow">
                                            <table class="table table-hover table-condensed text-center">
                                                <thead>
                                                    <tr>
                                                        <td class="col-md-2">등록일</td>
                                                        <td class="col-md-2">입금일</td>
                                                        <td class="col-md-1">입금요청금액</td>
                                                        <td class="col-md-1">은행명</td>
                                                        <td class="col-md-1">식별코드</td>
                                                        <td class="col-md-1">유저레벨</td>
                                                        <td class="col-md-1">실명</td>
                                                        <#--<td class="col-md-1">반려사유</td>-->
                                                        <td class="text-center col-md-2"><i class="fa fa-cog"></i></td>
                                                    </tr>
                                                </thead>
                                                <tbody id="withReqBody">
                                                    <tr class="text-center">
                                                        <td colspan="9">데이터가 없습니다.</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="panel-footer text-center">

                                            <ul id="withdrawPaging" class="pagination pagination-sm">
                                                <li class="active"><a href="javascript:getReqDeposit('1');">1</a></li>
                                            </ul>

                                            <#--<a href="" class="nb pull-right" style="font-size:8px;">Full Book</a>-->
                                            <#--<div class="clearfix"></div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="tile">
                                            <h2 class="tile-title">신분증
                                            </h2>
                                            <div class="overflow">
                                                <div class="preview" style="height:205px;" id="idCardPreview">



                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                                &nbsp;
                                                <#--<a href="" class="nb pull-right" style="font-size:8px;">입금 승인</a>-->
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="tile">
                                            <h2 class="tile-title">통장사본
                                            </h2>
                                            <div class="overflow">
                                                <div class="preview" style="height:205px;" id="preview">

                                                </div>
                                            </div>
                                            <div class="panel-footer">
                                                &nbsp;
                                            <#--<a href="" class="nb pull-right" style="font-size:8px;">입금 승인</a>-->
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <script id="wrongTemplate" type="text/x-jquery-tmpl">
                                    <tr>
                                        <td >${r"${depositDtTxt}"}</td>
                                        <td >${r"${amount}"}</td>
                                        <td >${r"${bankNm}"}</td>
                                        <td >${r"${userNm}"}</td>
                                        <td class="text-center" ><button onclick="javascript:approveWrongDeposit('${r"${id}"}');" class="btn btn-alt btn-xs" type="button">입금자 확인</button></td>
                                    </tr>
                                    </script>
                                    <div class="tile">
                                        <h2 class="tile-title">착오입금목록
                                        </h2>
                                        <div class="overflow">
                                            <table class="table table-hover table-condensed text-center">
                                                <thead>
                                                <tr>
                                                    <td class="col-md-2">입금일</td>
                                                    <td class="col-md-2">금액</td>
                                                    <td class="col-md-2">은행명</td>
                                                    <td class="col-md-2">입금자</td>
                                                    <td class="text-center col-md-1"><i class="fa fa-cog"></i></td>
                                                </tr>
                                                </thead>
                                                <tbody id="wrongBody">
                                                <tr class="text-center">
                                                    <td colspan="5">데이터가 없습니다.</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="panel-footer text-center">

                                            <ul id="wrongPaging" class="pagination pagination-sm">
                                                <li class="active"><a href="javascript:getWrongDeposit('1');">1</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
                <!-- Chat -->
                <div class="chat">
                    
                    <!-- Chat List -->
                    <div class="pull-left chat-list">
                        <div class="listview narrow">
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/1.jpg" width="30" alt="">
                                <div class="media-body p-t-5">
                                    Alex Bendit
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/2.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">David Volla Watkinson</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/3.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Mitchell Christein</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/4.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Wayne Parnell</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/5.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Melina April</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/6.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Ford Harnson</span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <!-- Chat Area -->
                    <div class="media-body">
                        <div class="chat-header">
                            <a class="btn btn-sm" href="">
                                <i class="fa fa-circle-o status m-r-5"></i> Chat with Friends
                            </a>
                        </div>
                    
                        <div class="chat-body">
                            <div class="media">
                                <img class="pull-right" src="/img/profile-pics/1.jpg" width="30" alt="" />
                                <div class="media-body pull-right">
                                    Hiiii<br/>
                                    How you doing bro?
                                    <small>Me - 10 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-left">
                                <img class="pull-left" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    I'm doing well buddy. <br/>How do you do?
                                    <small>David - 9 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-right">
                                <img class="pull-right" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    I'm Fine bro <br/>Thank you for asking
                                    <small>Me - 8 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-right">
                                <img class="pull-right" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    Any idea for a hangout?
                                    <small>Me - 8 Mins ago</small>
                                </div>
                            </div>
                        
                        </div>
                    
                        <div class="chat-footer media">
                            <i class="chat-list-toggle pull-left fa fa-bars"></i>
                            <i class="pull-right fa fa-picture-o"></i> 
                            <div class="media-body">
                                <textarea class="form-control" placeholder="Type something..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Older IE Message -->
            <!--[if lt IE 9]>
                <div class="ie-block">
                    <h1 class="Ops">Ooops!</h1>
                    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser in order to access the maximum functionality of this website. </p>
                    <ul class="browsers">
                        <li>
                            <a href="https://www.google.com/intl/en/chrome/browser/">
                                <img src="/img/browsers/chrome.png" alt="">
                                <div>Google Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.mozilla.org/en-US/firefox/new/">
                                <img src="/img/browsers/firefox.png" alt="">
                                <div>Mozilla Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com/computer/windows">
                                <img src="/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://safari.en.softonic.com/">
                                <img src="/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-10/worldwide-languages">
                                <img src="/img/browsers/ie.png" alt="">
                                <div>Internet Explorer(New)</div>
                            </a>
                        </li>
                    </ul>
                    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
                </div>   
            <![endif]-->
        </section>

        <form id="loadingForm"></form>

        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->
        <script src="/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
        <script src="/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Charts -->
        <#--<script src="/js/charts/jquery.flot.js"></script> <!-- Flot Main &ndash;&gt;-->
        <#--<script src="/js/charts/jquery.flot.time.js"></script> <!-- Flot sub &ndash;&gt;-->
        <#--<script src="/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub &ndash;&gt;-->
        <#--<script src="/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen &ndash;&gt;-->

        <#--<script src="/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts &ndash;&gt;-->
        <#--<script src="/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts &ndash;&gt;-->
        <#--<script src="/js/charts.js"></script> <!-- All the above chart related functions &ndash;&gt;-->

        <!--scroll -->
        <script src="/js/scroll.min.js"></script>

        <!--  Form Related -->
        <script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->

        <!-- UX -->
        <#--<script src="/js/scroll.min.js_bak"></script> <!-- Custom Scrollbar &ndash;&gt;-->

        <!-- Jqutemplateslate -->
        <script src="/js/jquery.tmpl.min.js"></script>

        <!-- Other -->
        <script src="/js/calendar.min.js"></script> <!-- Calendar -->
        <script src="/js/feeds.min.js"></script> <!-- News Feeds -->

        <script src="/js/jquery.loading.min.js"></script> <!-- loading -->

        <script src="/js/validation/validate.min.js"></script> <!-- jQuery Form Validation Library -->
        <script src="/js/validation/validationEngine.min.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->
        <script src="js/input-mask.min.js"></script> <!-- Input Mask -->
        <!-- All JS functions -->
        <script src="/js/functions.js"></script>
        <script src="/js/api/admin_api.js"></script>
        <script src="/js/view/admin.js"></script>

        <script src="/js/xlsx.full.min.js" ></script>

        <script type="text/javascript">
            var X = XLSX;
            var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";

            function to_json(workbook) {
                var result = {};
                workbook.SheetNames.forEach(function(sheetName) {
                    var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                    if(roa.length > 0){
                        result[sheetName] = roa;
                    }
                });
                return result;
            }

            function handleFile(e) {
                var files = e.target.files;
                var i,f;
                for (i = 0; i != files.length; ++i) {
                    f = files[i];
                    var reader = new FileReader();
                    var name = f.name;
                    reader.onload = function(e) {
                        var data = e.target.result;

                        var workbook;
                        if(rABS) {
                            /* if binary string, read with type 'binary' */
                            workbook = XLSX.read(data, {type: 'binary'});
                        } else {
                            /* if array buffer, convert to base64 */
                            var arr = fixdata(data);
                            workbook = XLSX.read(btoa(arr), {type: 'base64'});
                        }

                        jsonFile = (to_json(workbook));

                    };
                    reader.readAsBinaryString(f);
                }
            }
            $('document').ready(function(){

                getReqDeposit('1');
                getWrongDeposit('1');

                var control = document.getElementById("depositFile");
                control.addEventListener("change", function(event) {
                    // When the control has changed, there are new files
                    var files = control.files;
                            for (var i = 0; i < files.length; i++) {
                        console.log("Filename: " + files[i].name);
                        console.log("Type: " + files[i].type);
                        console.log("Size: " + files[i].size + " bytes");

//                        if(!files[i].type.match('application/vnd.*')) {
//                            alert("only Excel files");
//                            //$("#depositFile").reset(); //the tricky part is to "empty" the input file here I reset the form.
//                            return;
//                        }

                        handleFile(event);

                    }

                }, false);

            });

        </script>

    </body>
</html>
