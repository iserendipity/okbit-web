<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <meta name="format-detection" content="telephone=no">
        <meta charset="UTF-8">

        <meta name="description" content="OK-BIT">
        <meta name="keywords" content="OK-BIT, Bitcoin, Ehterium, Monero, Litecoin, Bitfinex, Poloniex">

        <title>OK-BIT</title>

        <!-- CSS -->
        <link href="https://ok-bit.com/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://ok-bit.com/css/animate.min.css" rel="stylesheet">
        <link href="https://ok-bit.com/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://ok-bit.com/css/form.css" rel="stylesheet">
        <link href="https://ok-bit.com/css/calendar.css" rel="stylesheet">
        <link href="https://ok-bit.com/css/style.css" rel="stylesheet">
        <link href="https://ok-bit.com/css/icons.css" rel="stylesheet">
        <link href="https://ok-bit.com/css/generics.css" rel="stylesheet">
        <link href="https://ok-bit.com/css/custom.css" rel="stylesheet">
        <link href="https://ok-bit.com/css/jquery.loading.min.css" rel="stylesheet">
    </head>
    <body id="skin-blur-sunset">

        <header id="header" class="media">
            <a href="" id="menu-toggle"></a>
            <a class="logo pull-left" href="/console"><img src="/img/logo/logo_main.png" class="img-responsive"></a>

            <div class="media-body">
                <div class="media" id="top-menu">
                    <div class="pull-left tm-icon">
                        <a data-drawer="messages" class="drawer-toggle" href="#" onclick="javascript:getQuestion('1');">
                            <i class="sa-top-message"></i>
                            <i class="n-count animated"></i>
                            <span>Questions</span>
                        </a>
                    </div>
                <#--<div class="pull-left tm-icon">-->
                <#--<a data-drawer="notifications" class="drawer-toggle" href="">-->
                <#--<i class="sa-top-updates"></i>-->
                <#--<i class="n-count animated">9</i>-->
                <#--<span>Updates</span>-->
                <#--</a>-->
                <#--</div>-->

                    <div id="time" class="pull-right">
                        <span id="hours"></span>
                        :
                        <span id="min"></span>
                        :
                        <span id="sec"></span>
                    </div>

                    <div class="media-body">
                    <#--<input type="text" class="main-search">-->
                    </div>
                </div>
            </div>
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            <script type="text/javascript">var nowCoin = '${coin}';</script>
            <#--<!-- Sidebar &ndash;&gt;-->
            <#--<aside id="sidebar">-->

                <#--<!-- Side Menu &ndash;&gt;-->
                <#--<ul class="list-unstyled side-menu">-->
                    <#--<#include "common/sidemenu.ftl">-->
                <#--</ul>-->

            <#--</aside>-->
        
            <!-- Content -->
            <section id="content" class="container">
                <#--<#include "common/messages.ftl">-->
                
                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li class="active">Home</li>
                    <#--<li><a href="#">Library</a></li>-->
                    <#--<li class="active">Data</li>-->
                </ol>
                
                <h4 class="page-title">(주)오케이비트 일일보고서</h4>
                                
                <!-- Shortcuts -->
                <#--<div class="block-area shortcut-area">-->
                    <#--&lt;#&ndash;<#include "common/shortcuts.ftl"/>&ndash;&gt;-->
                    <#--&lt;#&ndash;<a class="shortcut tile" href="">&ndash;&gt;-->
                        <#--&lt;#&ndash;<img src="/img/shortcuts/reports.png" alt="">&ndash;&gt;-->
                        <#--&lt;#&ndash;<small class="t-overflow">Reports</small>&ndash;&gt;-->
                    <#--&lt;#&ndash;</a>&ndash;&gt;-->
                <#--</div>-->

                <hr class="whiter" />
                
                <!-- Quick Stats -->
                <#--<div class="block-area">-->
                    <#--<div class="row">-->
                        <#--<div class="col-md-3 col-xs-6">-->
                            <#--<div class="tile quick-stats">-->
                                <#--<div id="stats-line-2" class="pull-left"></div>-->
                                <#--<div class="data">-->
                                    <#--<h2 data-value="98">0</h2>-->
                                    <#--<small>Tickets Today</small>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->

                        <#--<div class="col-md-3 col-xs-6">-->
                            <#--<div class="tile quick-stats media">-->
                                <#--<div id="stats-line-3" class="pull-left"></div>-->
                                <#--<div class="media-body">-->
                                    <#--<h2 data-value="1452">0</h2>-->
                                    <#--<small>Shipments today</small>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->

                        <#--<div class="col-md-3 col-xs-6">-->
                            <#--<div class="tile quick-stats media">-->

                                <#--<div id="stats-line-4" class="pull-left"></div>-->

                                <#--<div class="media-body">-->
                                    <#--<h2 data-value="4896">0</h2>-->
                                    <#--<small>Orders today</small>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->

                        <#--<div class="col-md-3 col-xs-6">-->
                            <#--<div class="tile quick-stats media">-->
                                <#--<div id="stats-line" class="pull-left"></div>-->
                                <#--<div class="media-body">-->
                                    <#--<h2 data-value="29356">0</h2>-->
                                    <#--<small>Site visits today</small>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</div>-->

                    <#--</div>-->

                <#--</div>-->

                <#--<hr class="whiter" />-->
                
                <!-- Main Widgets -->
                    <div class="block-area">
                        <div class="row">
                            <div class="col-md-1 col-xs-6">
                                <div class="tile quick-stats media">
                                    <div id="stats-line-1" class="pull-left"></div>
                                    <div class="media-body">
                                        <h2 id="tsCountAll" data-value="0">0</h2>
                                        <small>전체 회원수</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1 col-xs-6">
                                <div class="tile quick-stats media">
                                    <div id="stats-line-7" class="pull-left"></div>
                                    <div class="media-body">
                                        <h2 id="tsCountCoin" data-value="0">0</h2>
                                        <small>취급 코인 수</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <div class="tile quick-stats">
                                    <div id="stats-line-2" class="pull-left"></div>
                                    <div class="data">
                                        <h2 id="tsCount" data-value="0">0</h2>
                                        <small>오늘 가입자</small>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-xs-6">
                                <div class="tile quick-stats media">
                                    <div id="stats-line-3" class="pull-left"></div>
                                    <div class="media-body">
                                        <h2 id="tsCountBank" data-value="0">0</h2>
                                        <small>통장사본 인증 수</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <div class="tile quick-stats media">
                                    <div id="stats-line-4" class="pull-left"></div>
                                    <div class="media-body">
                                        <h2 id="tsCountId" data-value="0">0</h2>
                                        <small>신분증 인증 수</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <div class="tile quick-stats media">
                                    <div id="stats-line-5" class="pull-left"></div>
                                    <div class="media-body">
                                        <h2 id="tsCountOrder" data-value="0">0</h2>
                                        <small>주문량</small>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 col-xs-6">
                                <div class="tile quick-stats media">
                                    <div id="stats-line-6" class="pull-left"></div>
                                    <div class="media-body">
                                        <h2 id="tsCountTrade" data-value="0">0</h2>
                                        <small>거래량</small>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <hr class="whiter" />
                <div class="block-area">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="tile">
                                        <h2 class="tile-title">${coin} 보유량</h2>
                                        <#--<div class="tile-config dropdown">-->
                                            <#--<a data-toggle="dropdown" href="" class="tooltips tile-menu" title="Options"></a>-->
                                            <#--<ul class="dropdown-menu pull-right text-right">-->
                                                <#--<li><a href="">Refresh</a></li>-->
                                                <#--<li><a href="">Settings</a></li>-->
                                            <#--</ul>-->
                                        <#--</div>-->
                                        <div class="p-10">
                                            <div id="pie-chart" class="main-chart" style="height: 250px"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="tile">
                                        <h2 class="tile-title">${coin} 날짜별 볼륨량</h2>
                                        <div class="p-10">
                                            <div id="line-chart" class="main-chart" style="height: 250px"></div>

                                            <div class="chart-info">
                                                <ul class="list-unstyled">
                                                    <li class="m-b-10">
                                                        Total Sales 1200
                                                        <span class="pull-right text-muted t-s-0">
                                                    <i class="fa fa-chevron-up"></i>
                                                    +12%
                                                </span>
                                                    </li>
                                                    <li>
                                                        <small>
                                                            Local 640
                                                            <span class="pull-right text-muted t-s-0"><i class="fa m-l-15 fa-chevron-down"></i> -8%</span>
                                                        </small>
                                                        <div class="progress progress-small">
                                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"></div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <small>
                                                            Foreign 560
                                                            <span class="pull-right text-muted t-s-0">
                                                        <i class="fa m-l-15 fa-chevron-up"></i>
                                                        -3%
                                                    </span>
                                                        </small>
                                                        <div class="progress progress-small">
                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 60%"></div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Pies -->
                            <div class="tile text-center">
                                <div class="tile-dark p-10">
                                    <div class="pie-chart-tiny" data-percent="86">
                                        <span class="percent"></span>
                                        <span class="pie-title">New Visitors <i class="m-l-5 fa fa-retweet"></i></span>
                                    </div>
                                    <div class="pie-chart-tiny" data-percent="23">
                                        <span class="percent"></span>
                                        <span class="pie-title">Bounce Rate <i class="m-l-5 fa fa-retweet"></i></span>
                                    </div>
                                    <div class="pie-chart-tiny" data-percent="57">
                                        <span class="percent"></span>
                                        <span class="pie-title">Emails Sent <i class="m-l-5 fa fa-retweet"></i></span>
                                    </div>
                                    <div class="pie-chart-tiny" data-percent="34">
                                        <span class="percent"></span>
                                        <span class="pie-title">Sales Rate <i class="m-l-5 fa fa-retweet"></i></span>
                                    </div>
                                    <div class="pie-chart-tiny" data-percent="81">
                                        <span class="percent"></span>
                                        <span class="pie-title">New Signups <i class="m-l-5 fa fa-retweet"></i></span>
                                    </div>
                                </div>
                            </div>
                            <!--  ORDERS -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="tab-container tile">
                                        <h2 class="tile-title">${coin} 입금내역
                                        </h2>
                                        <div class="clearfix"></div>
                                        <div class="tab-content no-padding overflow">
                                            <!-- Market tab -->
                                            <div class="tab-pane active">
                                                <div class="overflow col-xs-12">
                                                    <table class="table table-condensed table-hover">
                                                        <thead style="height:0px;">
                                                        <tr>
                                                            <th class="col-xs-2">
                                                                날짜
                                                            </th>
                                                            <th class="col-xs-1">
                                                                이름
                                                            </th>
                                                            <th class="col-xs-2">
                                                                이메일
                                                            </th>
                                                            <th class="col-xs-2">
                                                                갯수
                                                            </th>
                                                            <th class="col-xs-4">
                                                                tx
                                                            </th>
                                                        </tr>

                                                        </thead>
                                                        <tbody style="height:366px; overflow:scroll">
                                                        <#list 1..25 as i>
                                                        <tr>
                                                            <td>2017-07-11 16:02:45</td>
                                                            <td>강정우</td>
                                                            <td>kjwme1116@gmail.com</td>
                                                            <td>1</td>
                                                            <td>jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj</td>
                                                        </tr>
                                                        </#list>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <a href="" class="nb pull-right" style="font-size:8px;">전체내역 EXCEL 다운로드</a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <!-- TRADES -->
                                <div class="col-md-6">
                                    <div class="tab-container tile">
                                        <h2 class="tile-title">${coin} 출금내역
                                        </h2>
                                        <div class="clearfix"></div>
                                        <div class="tab-content no-padding overflow">
                                            <!-- Market tab -->
                                            <div class="tab-pane active">
                                                <div class="overflow col-xs-12">
                                                    <table class="table table-condensed table-hover">
                                                        <thead style="height:0px;">
                                                        <tr>
                                                            <th class="col-xs-2">
                                                                날짜
                                                            </th>
                                                            <th class="col-xs-1">
                                                                이름
                                                            </th>
                                                            <th class="col-xs-2">
                                                                이메일
                                                            </th>
                                                            <th class="col-xs-2">
                                                                갯수
                                                            </th>
                                                            <th class="col-xs-4">
                                                                tx
                                                            </th>
                                                        </tr>

                                                        </thead>
                                                        <tbody style="height:366px; overflow:scroll">
                                                            <#list 1..25 as i>
                                                            <tr>
                                                                <td>2017-07-11 16:02:45</td>
                                                                <td>강정우</td>
                                                                <td>kjwme1116@gmail.com</td>
                                                                <td>1</td>
                                                                <td>jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj</td>
                                                            </tr>
                                                            </#list>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <a href="" class="nb pull-right" style="font-size:8px;">전체내역 EXCEL 다운로드</a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="col-md-3">

                            <!-- Balance -->
                                <script id="balanceTemplate" type="text/x-jquery-tmpl">
                                    <tr>
                                        <td style="width:40px" class="visible-lg balanceIcon">
                                            <img src="/img/shortcuts/${r"${coin.name}"}.png" alt="${r"${coin.name}"}">
                                        </td>
                                        <td style="width:30%">
                                            ${r"${coin.name}"}
                                        </td>
                                        <td>
                                            Real Balance : ${r"${realBalanceTxt}"} ${r"${coin.unit}"}
                                        </td>
                                    </tr>
                                </script>
                                <div id="balance" class="tile">
                                    <h2 class="tile-title">Balance</h2>
                                    <div class="tile-config dropdown">
                                        <a data-toggle="dropdown" href="" class="tile-menu"></a>
                                        <ul class="dropdown-menu pull-right text-right">
                                            <li><a href="">Refresh</a></li>
                                        </ul>
                                    </div>
                                    <div class="overflow">
                                        <table id="balanceTab" class="table table-responsive">
                                            <tbody id="balanceBody">
                                            <tr>
                                                <td>
                                                    지갑이 없습니다.
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            <#--<div class="clearfix"></div>-->
                                <script id="coinTemplate" type="text/x-jquery-tmpl">
                                    <tr>
                                        <td style="width:40px" class="balanceIcon">
                                            <img src="/img/shortcuts/${r"${name}"}.png" alt="${r"${name}"}">
                                        </td>
                                        <td>
                                            <input type="text" value="${r"${minConfirmation}"}" class="form-control input-sm" name="minConfirmation">
                                        </td>
                                        <td>
                                            <input type="text" value="${r"${autoCollectAmount}"}" class="form-control input-sm" name="autoCollectAmount">
                                        </td>
                                        <td>
                                            <input type="text" value="${r"${feePercent}"}" class="form-control input-sm" name="feePercent">
                                        </td>
                                        <td>
                                            <input type="text" value="${r"${withdrawalTxFee}"}" class="form-control input-sm" name="withdrawalTxFee">
                                        </td>
                                        <td><button type="button" onclick="javascript:modifyCoin(this);" class="btn btn-primary btn-sm btn-alt">저장</button></td>
                                    </tr>
                                </script>
                                <div class="tile">
                                    <h2 class="tile-title">COINS</h2>
                                    <#--<div class="tile-config dropdown">-->
                                        <#--<a data-toggle="dropdown" href="" class="tile-menu"></a>-->
                                        <#--<ul class="dropdown-menu pull-right text-right">-->
                                            <#--<li><a href="">Refresh</a></li>-->
                                        <#--</ul>-->
                                    <#--</div>-->
                                    <div class="overflow">
                                        <table class="table table-responsive">
                                            <thead>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>최소 승인
                                                    </td>
                                                    <td>자동수집량
                                                    </td>
                                                    <td>거래수수료(%)
                                                    </td>
                                                    <td>출금수수료
                                                    </td>
                                                    <td><i class="fa fa-cog"></i>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody id="coinBody">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <!-- Chat -->
                <div class="chat">
                    
                    <!-- Chat List -->
                    <div class="pull-left chat-list">
                        <div class="listview narrow">
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/1.jpg" width="30" alt="">
                                <div class="media-body p-t-5">
                                    Alex Bendit
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/2.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">David Volla Watkinson</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/3.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Mitchell Christein</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/4.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Wayne Parnell</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/5.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Melina April</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/6.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Ford Harnson</span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <!-- Chat Area -->
                    <div class="media-body">
                        <div class="chat-header">
                            <a class="btn btn-sm" href="">
                                <i class="fa fa-circle-o status m-r-5"></i> Chat with Friends
                            </a>
                        </div>
                    
                        <div class="chat-body">
                            <div class="media">
                                <img class="pull-right" src="/img/profile-pics/1.jpg" width="30" alt="" />
                                <div class="media-body pull-right">
                                    Hiiii<br/>
                                    How you doing bro?
                                    <small>Me - 10 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-left">
                                <img class="pull-left" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    I'm doing well buddy. <br/>How do you do?
                                    <small>David - 9 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-right">
                                <img class="pull-right" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    I'm Fine bro <br/>Thank you for asking
                                    <small>Me - 8 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-right">
                                <img class="pull-right" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    Any idea for a hangout?
                                    <small>Me - 8 Mins ago</small>
                                </div>
                            </div>
                        
                        </div>
                    
                        <div class="chat-footer media">
                            <i class="chat-list-toggle pull-left fa fa-bars"></i>
                            <i class="pull-right fa fa-picture-o"></i> 
                            <div class="media-body">
                                <textarea class="form-control" placeholder="Type something..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Older IE Message -->
            <!--[if lt IE 9]>
                <div class="ie-block">
                    <h1 class="Ops">Ooops!</h1>
                    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser in order to access the maximum functionality of this website. </p>
                    <ul class="browsers">
                        <li>
                            <a href="https://www.google.com/intl/en/chrome/browser/">
                                <img src="/img/browsers/chrome.png" alt="">
                                <div>Google Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.mozilla.org/en-US/firefox/new/">
                                <img src="/img/browsers/firefox.png" alt="">
                                <div>Mozilla Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com/computer/windows">
                                <img src="/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://safari.en.softonic.com/">
                                <img src="/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-10/worldwide-languages">
                                <img src="/img/browsers/ie.png" alt="">
                                <div>Internet Explorer(New)</div>
                            </a>
                        </li>
                    </ul>
                    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
                </div>   
            <![endif]-->
        </section>
        
        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="https://ok-bit.com/js/jquery.min.js"></script> <!-- jQuery Library -->
        <script src="https://ok-bit.com/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
        <script src="https://ok-bit.com/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

        <!-- Bootstrap -->
        <script src="https://ok-bit.com/js/bootstrap.min.js"></script>

        <!-- Charts -->
        <#--<script src="https://ok-bit.com/js/charts/jquery.flot.js"></script> <!-- Flot Main &ndash;&gt;-->
        <#--<script src="https://ok-bit.com/js/charts/jquery.flot.time.js"></script> <!-- Flot sub &ndash;&gt;-->
        <#--<script src="https://ok-bit.com/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub &ndash;&gt;-->
        <#--<script src="https://ok-bit.com/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen &ndash;&gt;-->

        <!-- Charts -->
        <script src="js/charts/jquery.flot.js"></script> <!-- Flot Main -->
        <script src="js/charts/jquery.flot.time.js"></script> <!-- Flot sub -->
        <script src="js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub -->
        <script src="js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen -->
        <script src="js/charts/jquery.flot.orderBar.js"></script> <!-- Flot Bar chart -->
        <script src="js/charts/jquery.flot.pie.min.js"></script> <!-- Flot Pie chart -->

        <script src="https://ok-bit.com/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts -->
        <script src="https://ok-bit.com/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts -->
        <script src="https://ok-bit.com/js/charts.js"></script> <!-- All the above chart related functions -->

        <!--scroll -->
        <script src="https://ok-bit.com/js/scroll.min.js"></script>

        <!--  Form Related -->
        <script src="https://ok-bit.com/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->

        <!-- UX -->
        <#--<script src="https://ok-bit.com/js/scroll.min.js_bak"></script> <!-- Custom Scrollbar &ndash;&gt;-->

        <!-- Jqutemplateslate -->
        <script src="https://ok-bit.com/js/jquery.tmpl.min.js"></script>

        <!-- Other -->
        <script src="https://ok-bit.com/js/calendar.min.js"></script> <!-- Calendar -->
        <script src="https://ok-bit.com/js/feeds.min.js"></script> <!-- News Feeds -->

        <script src="https://ok-bit.com/js/jquery.loading.min.js"></script> <!-- loading -->

        <!-- All JS functions -->
        <script src="https://ok-bit.com/js/functions.js"></script>
        <script src="https://ok-bit.com/js/api/admin_api.js"></script>
        <script src="https://ok-bit.com/js/view/admin.js"></script>

        <script type="text/javascript">

            $('document').ready(function(){
               init();
            });

        </script>


    </body>
</html>
