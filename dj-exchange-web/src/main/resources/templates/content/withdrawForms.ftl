<!-- wihdraw template -->
<script id="walletTemplateBitcoin" type="text/x-jquery-tmpl">
    <div class="listview narrow">
        <div class="tile-title">
            <@spring.message "WITHDRAW_NEW"/>
            <span class="drawer-close">&times;</span>
        </div>
        <#--<div class="panel-body">-->
            <#---->
        <#--</div>-->
        <div class="well tile" style="margin-bottom:0px;">
            <#--<p>Minimum withdrawal amount : 0.1BTC<br>1 time limitation withdrawal amount : 0000000BTC<br>1 month limitation withdrawal amount : 0000000BTC-->
                <#--<br><@spring.message "WITHDRAW_LIMIT_TIME_EXP"/> : <@spring.message "WITHDRAW_LIMIT_TIME"/>-->
                <span id="indicatorAmount"><span class="h4"><@spring.message "NOT_TEMP_BITCOIN"/></span>
                <#--<br>출금 제한시간 :-->
                <br><h5><@spring.message "WITHDRAW_FEE_EXP"/> : <span id="indicatorFee"></span>${r"${coin.unit}"}</h5>

                <#--<br><@spring.message "WITHDRAW_FEE_EXP"/> : <span name="feePercent">${r"${coin.feePercent}"}</span>%-->
            </p>
            <ul class="lists-caret" style="color:white; text-shadow: none; font-weight:none!important">
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION1"/></span></li>
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION2"/></span></li>
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION3"/></span></li>
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION4"/></span></li>
            </ul>
        </div>
    </div>
</script>
<!-- wihdraw template -->
<script id="walletTemplate" type="text/x-jquery-tmpl">
    <div class="listview narrow">
        <div class="tile-title">
            <@spring.message "WITHDRAW_NEW"/>
            <span class="drawer-close">&times;</span>
        </div>
        <div class="panel-body">
            <form role="form" id="formWithdraw">
                <input type="hidden" name="coinName" value="${r"${coin.name}"}">
                <input type="hidden" name="from" class="form-control address" placeholder="" readonly value="${r"${address}"}">
                <input type="hidden" name="amount" value="">
                <#--<div class="form-group pull-left col-md-4" >-->
                    <#--<label for="to">From</label>-->
                    <#--<input type="text" name="from" class="form-control readonly address" placeholder="" readonly value="${r"${address}"}">-->
                <#--</div>-->
                <div class="form-group pull-left col-md-6" >
                    <label for="fromAddress"><@spring.message "WITHDRAW_ADDRESS"/></label>
                    <input type="text" name="fromAddress" class="form-control" placeholder="Recipient Wallet Address">
                </div>
                <div class="form-group pull-left col-md-6">
                    <label for="amountPre"><@spring.message "WITHDRAW_AMOUNT"/></label>
                    <input type="text" name="amountPre" class="form-control" onkeyup="getRealAmount('${r"${coin.name}"}');">
                </div>
                <#--<div class="form-group pull-right" style="width:50%">-->
                    <#--<label for="">2FA OTP</label>-->
                    <#--<input type="text" name="otp" class="form-control">-->
                <#--</div>-->
                <div class="form-group clearfix col-md-12">
                    <button type="button" class="btn btn-alt m-t-10" onclick="requestWithdrawalCoin('${r"${coin.name}"}');"><@spring.message "WITHDRAW_REQUEST"/></button>
                </div>

            </form>
            <div class="clearfix"></div>
        </div>
        <div class="well tile" style="margin-bottom:0px;">
            <#--<p>Minimum withdrawal amount : 0.1BTC<br>1 time limitation withdrawal amount : 0000000BTC<br>1 month limitation withdrawal amount : 0000000BTC-->
                <#--<br><@spring.message "WITHDRAW_LIMIT_TIME_EXP"/> : <@spring.message "WITHDRAW_LIMIT_TIME"/>-->
                <h5><@spring.message "WITHDRAW_REAL_AMOUNT"/> : <span id="indicatorAmount"></span></h5>
                <#--<br>출금 제한시간 :-->
                <br><h5><@spring.message "WITHDRAW_FEE_EXP"/> : <span id="indicatorFee"></span>${r"${coin.unit}"}</h5>

                <#--<br><@spring.message "WITHDRAW_FEE_EXP"/> : <span name="feePercent">${r"${coin.feePercent}"}</span>%-->
            </p>
            <ul class="lists-caret" style="color:white; text-shadow: none; font-weight:none!important">
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION1"/></span></li>
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION2"/></span></li>
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION3"/></span></li>
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION4"/></span></li>
            </ul>
        </div>
    </div>
</script>
<script id="walletTemplateMonero" type="text/x-jquery-tmpl">
    <div class="listview narrow">
        <div class="tile-title">
            <@spring.message "WITHDRAW_NEW"/>
            <span class="drawer-close">&times;</span>
        </div>
        <div class="panel-body">
            <form role="form" id="formWithdraw">
                <input type="hidden" name="coinName" value="${r"${coin.name}"}">
                <input type="hidden" name="from" class="form-control address" placeholder="" readonly value="${r"${address}"}">
                <input type="hidden" name="amount" value="">

                <#--<div class="form-group pull-left col-md-3" >-->
                    <#--<label for="to">From</label>-->
                    <#--<input type="text" name="from" class="form-control readonly address" placeholder="" readonly value="${r"${address}"}">-->
                <#--</div>-->
                <div class="form-group pull-left col-md-4" >
                    <label for="to"><@spring.message "WITHDRAW_ADDRESS"/></label>
                    <input type="text" name="fromAddress" class="form-control" placeholder="Recipient Wallet Address">
                </div>
                <div class="form-group pull-left col-md-4" >
                    <label for="fromTag"><@spring.message "WITHDRAW_ADDRESS_PAYMENTID"/></label>
                    <input type="text" name="fromTag" class="form-control" placeholder="Recipient Wallet Address Tag">
                </div>
                <div class="form-group pull-left col-md-4">
                    <label for=""><@spring.message "WITHDRAW_AMOUNT"/></label>
                    <input type="text" name="amountPre" class="form-control" onkeyup="getRealAmount('${r"${coin.name}"}');">
                </div>
                <#--<div class="form-group pull-right" style="width:50%">-->
                    <#--<label for="">2FA OTP</label>-->
                    <#--<input type="text" name="otp" class="form-control">-->
                <#--</div>-->
                <div class="form-group clearfix col-md-12">
                    <button type="button" class="btn btn-alt m-t-10" onclick="requestWithdrawalCoin('${r"${coin.name}"}');"><@spring.message "WITHDRAW_REQUEST"/></button>
                </div>

            </form>
            <div class="clearfix"></div>
        </div>
        <div class="well tile" style="margin-bottom:0px;">

            <#--<p>Minimum withdrawal amount : 0.1BTC<br>1 time limitation withdrawal amount : 0000000BTC<br>1 month limitation withdrawal amount : 0000000BTC&ndash;-->
            <p>
                <h5><@spring.message "WITHDRAW_REAL_AMOUNT"/> : <span id="indicatorAmount"></span></h5>
                <#--<br>출금 제한시간 :-->
                <br><h5><@spring.message "WITHDRAW_FEE_EXP"/> : <span id="indicatorFee"></span>${r"${coin.unit}"}</h5>
                <#--<span name="feePercent">${r"${coin.feePercent}"}</span>%-->
            </p>
            <ul class="lists-caret" style="color:white; text-shadow: none">
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION1"/></span></li>
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION2"/></span></li>
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION3"/></span></li>
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION4"/></span></li>
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION5"/></span></li>
                <li><span class="h5"><@spring.message "WITHDRAW_CAUTION6"/></span></li>
            </ul>
        </div>
    </div>
</script>
<script id="walletTemplateKRW" type="text/x-jquery-tmpl">
    <div class="listview narrow">
        <div class="tile-title">
            <@spring.message "WITHDRAW_NEW"/>
            <span class="drawer-close">&times;</span>
        </div>
        <div class="panel-body">
            <#if user.userSetting.level != 'LEV1'>

            <form role="form" id="formWithdraw" class="form-validation-2">
                <input type="hidden" name="coinName" value="${r"${coin.name}"}">
                <input type="hidden" name="amount" value="">

                <div class="form-group pull-left col-md-2">
                    <label for="bankCode"><@spring.message "WITHDRAW_BANK_CODE"/></label>
                    <select name="bankCode" class="select form-control validate[required]">
                        <option value="">은행선택</option>
                        <#list bankList as bank>
                            <option value="${bank.code}">${bank.bankName}</option>
                        </#list>
                    </select>
                </div>
                <#--<div class="form-group pull-left col-md-2" >-->
                    <#--<label for="bankNm"><@spring.message "WITHDRAW_BANK_NAME"/></label>-->
                    <#--<input type="text" name="bankNm" class="form-control" >-->
                <#--</div>-->
                <div class="form-group pull-left col-md-2">
                    <label for="userNm"><@spring.message "WITHDRAW_USER_NM"/></label>
                    <input type="text" name="userNm" class="form-control" >
                 </div>
                <div class="form-group pull-left col-md-3">
                    <label for="account"><@spring.message "WITHDRAW_ACCOUNT"/></label>
                    <input type="text" name="account" class="form-control" placeholder="Account">
                </div>
                <div class="form-group pull-left col-md-3">
                    <label for="amountPre"><@spring.message "WITHDRAW_AMOUNT"/></label>
                    <input type="text" name="amountPre" class="form-control" onkeyup="getRealAmount('${r"${coin.name}"}');">
                </div>
                <#--<div class="form-group pull-right" style="width:50%">-->
                    <#--<label for="otpCode">2FA OTP</label>-->
                    <#--<input type="text" name="otpCode" class="form-control">-->
                <#--</div>-->
                <div class="col-md-12">
                    <button type="button" class="btn btn-alt m-t-10" onclick="requestWithdrawalKRW();"><@spring.message "WITHDRAW_REQUEST"/></button>
                </div>

            </form>

            <#else>
            <div class="well tile col-md-12 t-overflow" >
                <span class="h4"><@spring.message "NOT_YET_USE"/></span>
            </div>
            </#if>
            <div class="clearfix"></div>
        </div>
        <div class="well tile" style="margin-bottom:0px;">
            <p>
                <h5><@spring.message "WITHDRAW_LIMIT_TIME_EXP"/> : <@spring.message "WITHDRAW_LIMIT_TIME"/></h5>
                 <h5><@spring.message "WITHDRAW_REAL_AMOUNT"/> : <span id="indicatorAmount"></span>${r"${coin.unit}"}</h5>
                <#--<br>출금 제한시간 :-->
                <br><h5><@spring.message "WITHDRAW_FEE_EXP"/> : <span id="indicatorFee"></span>${r"${coin.unit}"}</h5>
            </p>
            <ul class="lists-caret" style="color:white; text-shadow: none">
                 <li><@spring.message "WITHDRAW_CAUTION1"/></li>
                <li><@spring.message "WITHDRAW_CAUTION2"/></li>
                <li><@spring.message "WITHDRAW_CAUTION3"/></li>
                <li><@spring.message "WITHDRAW_CAUTION4"/></li>
            </ul>
        </div>
    </div>
</script>
<script id="walletTemplateNoWallet" type="text/x-jquery-tmpl">
    <div class="listview narrow">
        <div class="tile-title">
            <span name="name">${r"${name}"}</span>
            <span class="drawer-close">&times;</span>
        </div>
        <div class="overflow">
            <div class="panel-heading">
                 <h5><@spring.message "WALLET_NEW"/></h5>
            </div>
            <div class="panel-body padding-top:0px;">
                <div class="form-group">
                    <#--<label for="">Get new address</label>-->
                    <#--<div class="well tile col-md-12 t-overflow" >-->
                        <a href="#" onclick="javascript:getAddress('${r"${name}"}');" class="btn btn-primary btn-alt" ><i class="fa fa-plus"></i>&nbsp;${r"${name}"} <@spring.message "BTN_WALLET_NEW"/></a>
                    <#--<span class="h4"><a href="#" class="tooltips" onclick="javascript:copy(this);" data-toggle="tooltip" data-placement="top" data-container="body" title="" data-original-title="Copy to clipboard" name="address">1GueuEupVXfy59coiiycMELNinFobG5LEX</a></span>-->
                    <#--</div>-->
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</script>
<#include "../verification/gotp.ftl">
<!-- Messages Drawer -->
<div id="withdrawForm" class="tile animated custom drawer">

</div>