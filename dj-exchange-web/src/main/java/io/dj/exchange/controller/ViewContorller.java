package io.dj.exchange.controller;

import io.dj.exchange.annotation.TransactionalEx;
import io.dj.exchange.domain.model.VerifyModel;
import io.dj.exchange.domain.primary.*;
import io.dj.exchange.domain.readonly.BankCode;
import io.dj.exchange.enums.CodeEnum;
import io.dj.exchange.exception.OkbitAlertException;
import io.dj.exchange.exception.OkbitException;
import io.dj.exchange.provider.OtpProvider;
import io.dj.exchange.repository.hibernate.primary.*;
import io.dj.exchange.repository.hibernate.readonly.BankCodeRepository;
import io.dj.exchange.service.CommonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.TransactionScoped;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Project dj-exchange-web.
 * Created by jeongwoo on 2017. 4. 6.
 * Description
 */
@Slf4j
@Controller
public class ViewContorller {

    @Value("${verify.sitecode}")
    String siteCode;

    @Value("${verify.sitepassword}")
    String sitePassword;

    @Value("${verify.returnUrl}")
    String returnUrl;

    @Value("${verify.errorUrl}")
    String errorUrl;

    @Autowired
    private OtpProvider otpProvider;

    @Autowired
    private AdminLevelRepository adminLevelRepository;

    @Autowired
    private UserSettingRepository userSettingRepository;

    @Autowired
    private OtpRepository otpRepository;

    @Autowired
    private SupportQnaRepository qnaRepository;

    @Autowired
    private CommonService commonService;

    @Autowired
    private BankCodeRepository bankCodeRepository;

    @Autowired
    private WebNoticesRepository webNoticesRepository;

    @RequestMapping(value="deposit")
    public ModelAndView deposit(){
        ModelAndView mv = new ModelAndView("deposit");
        return mv;
    }

    @RequestMapping(value="withdraw")
    public ModelAndView withdraw(){

        ModelAndView mv = new ModelAndView("withdraw");

        List<BankCode> bankCodeList = bankCodeRepository.findAll();
        mv.addObject("bankList", bankCodeList);

        return mv;
    }
    @RequestMapping(value="support")
    public ModelAndView myInfo(){
        ModelAndView mv = new ModelAndView("support");
        return mv;
    }

//    @RequestMapping(value="/support/write", consumes = "application/json;charset=utf-8")
//    public ModelAndView writeQna(@ModelAttribute("user") User user, @RequestBody SupportQna qna){
//
//        qna.setUserId(user.getId());
//        qna.setRegDt(LocalDateTime.now());
//        qna.setShowYn("Y");
//
//        log.info("----------- : {}", qna);
//
//        qnaRepository.save(qna);
//
//        ModelAndView mv = new ModelAndView("redirect:/support");
//        return mv;
//    }

    @RequestMapping(value="/verify/pop")
    public ModelAndView verifyPop(@ModelAttribute("user") User user, HttpServletRequest request, HttpSession session) throws OkbitException{
        //log.info("--------- session : {}", session.getId());
        ModelAndView mv = new ModelAndView("/verification/verifyPop");

        String sEncData = moduleSetting(session);

        mv.addObject("sEncData", sEncData);

        return mv;
    }

    @RequestMapping(value="/notices/{id}")
    public ModelAndView noticePop(@PathVariable("id") long id) throws OkbitException{

        ModelAndView mv = new ModelAndView("/pop/noticeDetailPop");
        mv.addObject("id", id);

        //mv.addObject("sEncData", sEncData);

        return mv;
    }

    @PostMapping(value="/noticeDetail")
    public ModelAndView noticeDetail(@RequestParam("id") long id) throws OkbitException{

        ModelAndView mv = new ModelAndView("/noticeDetail");
        mv.addObject("notice", webNoticesRepository.findOne(id));

        return mv;
    }

    @RequestMapping(value="/verify/result")
    @TransactionalEx
    public ModelAndView verifyResult(@ModelAttribute("user") User user, HttpServletRequest request, HttpSession session) throws OkbitAlertException {

        ModelAndView mv = new ModelAndView("/verification/verifyResult");
        VerifyModel vm = moduleParsing(request, session);
        if(vm.getRealName() != null){

            Optional<AdminLevel> already = adminLevelRepository.findOneByDi(vm.getDi());
            if(already.isPresent() && !already.get().getUserId().equals(user.getId())){
                throw new OkbitAlertException("AREADY_VERYFIED");
            }

            vm.setUserId(user.getId());

            AdminLevel al = adminLevelRepository.findOneByUserId(user.getId()).orElse(new AdminLevel());

            al.setRealName(vm.getRealName());
            al.setCellphone(vm.getCellphone());
            al.setCellphoneYn("Y");
            al.setUserId(vm.getUserId());
            al.setDi(vm.getDi());

            adminLevelRepository.save(al);

        }

        mv.addObject("result", vm);

        return mv;
    }

    public String requestReplace (String paramValue, String gubun) {

        String result = "";

        if (paramValue != null) {

            paramValue = paramValue.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

            paramValue = paramValue.replaceAll("\\*", "");
            paramValue = paramValue.replaceAll("\\?", "");
            paramValue = paramValue.replaceAll("\\[", "");
            paramValue = paramValue.replaceAll("\\{", "");
            paramValue = paramValue.replaceAll("\\(", "");
            paramValue = paramValue.replaceAll("\\)", "");
            paramValue = paramValue.replaceAll("\\^", "");
            paramValue = paramValue.replaceAll("\\$", "");
            paramValue = paramValue.replaceAll("'", "");
            paramValue = paramValue.replaceAll("@", "");
            paramValue = paramValue.replaceAll("%", "");
            paramValue = paramValue.replaceAll(";", "");
            paramValue = paramValue.replaceAll(":", "");
            paramValue = paramValue.replaceAll("-", "");
            paramValue = paramValue.replaceAll("#", "");
            paramValue = paramValue.replaceAll("--", "");
            paramValue = paramValue.replaceAll("-", "");
            paramValue = paramValue.replaceAll(",", "");

            if(gubun != "encodeData"){
                paramValue = paramValue.replaceAll("\\+", "");
                paramValue = paramValue.replaceAll("/", "");
                paramValue = paramValue.replaceAll("=", "");
            }

            result = paramValue;

        }
        return result;
    }

    private VerifyModel moduleParsing(HttpServletRequest request, HttpSession session){

        NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();

        String sEncodeData = requestReplace(request.getParameter("EncodeData"), "encodeData");

        String sSiteCode = siteCode;				// NICE로부터 부여받은 사이트 코드
        String sSitePassword = sitePassword;			// NICE로부터 부여받은 사이트 패스워드

        String sCipherTime = "";			// 복호화한 시간
        String sRequestNumber = "";			// 요청 번호
        String sResponseNumber = "";		// 인증 고유번호
        String sAuthType = "";				// 인증 수단
        String sName = "";					// 성명
        String sDupInfo = "";				// 중복가입 확인값 (DI_64 byte)
        String sConnInfo = "";				// 연계정보 확인값 (CI_88 byte)
        String sBirthDate = "";				// 생년월일(YYYYMMDD)
        String sGender = "";				// 성별
        String sNationalInfo = "";			// 내/외국인정보 (개발가이드 참조)
        String sMobileNo = "";				// 휴대폰번호
        String sMobileCo = "";				// 통신사
        String sMessage = "";
        String sPlainData = "";

        int iReturn = niceCheck.fnDecode(sSiteCode, sSitePassword, sEncodeData);

        VerifyModel vm = new VerifyModel();

        if( iReturn == 0 )
        {
            sPlainData = niceCheck.getPlainData();
            sCipherTime = niceCheck.getCipherDateTime();

            // 데이타를 추출합니다.
            java.util.HashMap mapresult = niceCheck.fnParse(sPlainData);

            sRequestNumber  = (String)mapresult.get("REQ_SEQ");
            sResponseNumber = (String)mapresult.get("RES_SEQ");
            sAuthType		= (String)mapresult.get("AUTH_TYPE");
            sName			= (String)mapresult.get("NAME");
            //sName			= (String)mapresult.get("UTF8_NAME"); //charset utf8 사용시 주석 해제 후 사용
            sBirthDate		= (String)mapresult.get("BIRTHDATE");
            sGender			= (String)mapresult.get("GENDER");
            sNationalInfo  	= (String)mapresult.get("NATIONALINFO");
            sDupInfo		= (String)mapresult.get("DI");
            sConnInfo		= (String)mapresult.get("CI");
            sMobileNo		= (String)mapresult.get("MOBILE_NO");
            sMobileCo		= (String)mapresult.get("MOBILE_CO");

            vm.setCellphone(sMobileNo);
            vm.setRealName(sName);
            vm.setDi(sDupInfo);

            sMessage = "인증에 성공했습니다.";

            String session_sRequestNumber = (String)session.getAttribute("REQ_SEQ");
            if(!sRequestNumber.equals(session_sRequestNumber))
            {
                sMessage = "세션값이 다릅니다. 올바른 경로로 접근하시기 바랍니다.";
                sResponseNumber = "";
                sAuthType = "";
            }
        }
        else if( iReturn == -1)
        {
            sMessage = "복호화 시스템 에러입니다.";
        }
        else if( iReturn == -4)
        {
            sMessage = "복호화 처리오류입니다.";
        }
        else if( iReturn == -5)
        {
            sMessage = "복호화 해쉬 오류입니다.";
        }
        else if( iReturn == -6)
        {
            sMessage = "복호화 데이터 오류입니다.";
        }
        else if( iReturn == -9)
        {
            sMessage = "입력 데이터 오류입니다.";
        }
        else if( iReturn == -12)
        {
            sMessage = "사이트 패스워드 오류입니다.";
        }
        else
        {
            sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
        }

        vm.setMsg(sMessage);

        return vm;

    }

    public String moduleSetting(HttpSession session) throws OkbitException{

        String result = "";

        NiceID.Check.CPClient niceCheck = new  NiceID.Check.CPClient();

        String sSiteCode = siteCode;			// NICE로부터 부여받은 사이트 코드
        String sSitePassword = sitePassword;		// NICE로부터 부여받은 사이트 패스워드

        String sRequestNumber = "REQ0000000001";        	// 요청 번호, 이는 성공/실패후에 같은 값으로 되돌려주게 되므로
        // 업체에서 적절하게 변경하여 쓰거나, 아래와 같이 생성한다.
        sRequestNumber = niceCheck.getRequestNO(sSiteCode);
        session.setAttribute("REQ_SEQ" , sRequestNumber);	// 해킹등의 방지를 위하여 세션을 쓴다면, 세션에 요청번호를 넣는다.

        String sAuthType = "M";      	// 없으면 기본 선택화면, M: 핸드폰, C: 신용카드, X: 공인인증서

        String popgubun 	= "N";		//Y : 취소버튼 있음 / N : 취소버튼 없음
        String customize 	= "";		//없으면 기본 웹페이지 / Mobile : 모바일페이지

        String sGender = ""; 			//없으면 기본 선택 값, 0 : 여자, 1 : 남자

        // CheckPlus(본인인증) 처리 후, 결과 데이타를 리턴 받기위해 다음예제와 같이 http부터 입력합니다.
        //리턴url은 인증 전 인증페이지를 호출하기 전 url과 동일해야 합니다. ex) 인증 전 url : http://www.~ 리턴 url : http://www.~
        String sReturnUrl = returnUrl;      // 성공시 이동될 URL
        String sErrorUrl = errorUrl;          // 실패시 이동될 URL

        // 입력될 plain 데이타를 만든다.
        String sPlainData = "7:REQ_SEQ" + sRequestNumber.getBytes().length + ":" + sRequestNumber +
                "8:SITECODE" + sSiteCode.getBytes().length + ":" + sSiteCode +
                "9:AUTH_TYPE" + sAuthType.getBytes().length + ":" + sAuthType +
                "7:RTN_URL" + sReturnUrl.getBytes().length + ":" + sReturnUrl +
                "7:ERR_URL" + sErrorUrl.getBytes().length + ":" + sErrorUrl +
                "11:POPUP_GUBUN" + popgubun.getBytes().length + ":" + popgubun +
                "9:CUSTOMIZE" + customize.getBytes().length + ":" + customize +
                "6:GENDER" + sGender.getBytes().length + ":" + sGender;

        String sMessage = "";
        String sEncData = "";

        int iReturn = niceCheck.fnEncode(sSiteCode, sSitePassword, sPlainData);
        if( iReturn == 0 )
        {
            result = niceCheck.getCipherData();
        }
        else if( iReturn == -1)
        {
            sMessage = "암호화 시스템 에러입니다.";
        }
        else if( iReturn == -2)
        {
            sMessage = "암호화 처리오류입니다.";
        }
        else if( iReturn == -3)
        {
            sMessage = "암호화 데이터 오류입니다.";
        }
        else if( iReturn == -9)
        {
            sMessage = "입력 데이터 오류입니다.";
        }
        else
        {
            sMessage = "알수 없는 에러 입니다. iReturn : " + iReturn;
        }

        if(iReturn != 0){

            log.info("----- EncError : {}", sMessage);
            throw new OkbitException(CodeEnum.UNKNOWN_ERROR.name());
        }

        return result;
    }

    @RequestMapping(value="profile")
    public ModelAndView profile(@ModelAttribute("user") User user, HttpServletRequest request, HttpSession session) throws OkbitException{

        ModelAndView mv = new ModelAndView("profile");

        AdminLevel adminLevel = adminLevelRepository.findOne(user.getId());
        OtpSetting otpSetting = otpRepository.findOne(user.getId());
        UserSetting userSetting = userSettingRepository.findOne(user.getId());

        String qrcode = "";

        //log.info("--------session : {}", session.getId());

//        String sEncData = moduleSetting(session);
//
//        mv.addObject("sEncData", sEncData);

        user.setEmail(user.getEmail().substring(0,4));

        if(adminLevel != null && adminLevel.getRealName() != null) {
            adminLevel.setRealName(adminLevel.getRealName().substring(0, 1));
        }

        if ("N".equals(userSetting.getOtpActive())) {
            qrcode = otpProvider.getQRBarcodeURL(user.getUserSetting().getOtpHash());
            mv.addObject("qrcode", qrcode);
            mv.addObject("otpHash", user.getUserSetting().getOtpHash());
        }


        mv.addObject("otpActive", userSetting.getOtpActive());
        mv.addObject("otpSetting", otpSetting);
        mv.addObject("adminLevel", adminLevel);

        return mv;
    }
    @RequestMapping(value="test/apitest")
    public ModelAndView apitest(@RequestParam Map<String, Object> params){
        ModelAndView mv = new ModelAndView("kcpcert_start");

        //log.info("------------- 여기 들어옴 : {}", params);

        return mv;
    }
//
//    @RequestMapping(value="test/apitest/config")
//    public @ResponseBody String config(@RequestParam Map<String, Object> params){
//
//        log.info("-------params : {}", params);
//
//        Gson gson = new Gson();
//        Customization config = new Customization();
//        config.setSupports_search(true);
//        config.setSupports_group_request(false);
//        config.setSupports_marks(true);
//
//        Exchange exchange = new Exchange();
//        exchange.setValue("OK-BIT");
//        exchange.setName("OK-BIT");
//        exchange.setDesc("OK-BIT");
//
//        List<Exchange> exArr = new ArrayList<>();
//        exArr.add(exchange);
//
//        config.setExchanges(exArr);
//
//        SymbolsType sType = new SymbolsType();
//        sType.setName("Stock");
//        sType.setValue("stock");
//
//        List<SymbolsType> typeArr = new ArrayList<>();
//        typeArr.add(sType);
//
//        config.setSymbolsTypes(typeArr);
//
//        log.info("-------------config : {}", gson.toJson(config).toString());
//
//        return gson.toJson(config).toString();
//    }
//
//    @Data
//    public class Symbol{
//
//        private String name;
//        private String description;
//        private String exchange;
//        private String type;
//        private String ticker;
//
//    }
//
//    @Data
//    public class Exchange{
//
//        private String name;
//        private String value;
//        private String desc;
//
//    }
//
//    @Data
//    public class SymbolsType{
//
//        private String name;
//        private String value;
//
//    }
//
//    @Data
//    public class Customization{
//
//        private boolean supports_search;
//        private boolean supports_group_request;
//        private boolean supports_marks;
//        private List<Exchange> exchanges;
//        private List<SymbolsType> symbolsTypes;
//        private String[] supportedResolution;
//
//        public Customization(){
//            this.exchanges = new ArrayList<>();
//            this.symbolsTypes = new ArrayList<>();
//            this.supportedResolution = new String[]{"1", "10", "15", "30", "60", "D", "2D", "3D", "W", "3W", "M", "6M"};
//        }
//
//    }
//
//    @RequestMapping(value="test/apitest/symbols")
//    public @ResponseBody String symbols(@RequestParam Map<String, Object> params){
//
//        log.info("-------params : {}", params);
//
//        Gson gson = new Gson();
//
//        CoinInfo coinInfo = new CoinInfo();
//        coinInfo.setName(params.get("symbol").toString());
//
//        //coinInfo = commonService.coinInfo(coinInfo);
//
//
//        Symbol s = new Symbol();
//        s.setExchange("OK-BIT");
//        s.setDescription("심볼");
//
//
//        return gson.toJson(s).toString();
//    }

    @RequestMapping("/qnaDetail")
    @TransactionalEx
    public ModelAndView qnaDetail(@ModelAttribute("user") User user, @RequestParam @Valid Long id, @PageableDefault Pageable p) throws OkbitException {

        ModelAndView mv = new ModelAndView("/qnaDetail");

        SupportQna question = qnaRepository.findOneById(id).orElseThrow(()->new OkbitException("WRONG_ACTION"));

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        question.setRegDtTxt(question.getRegDt().format(formatter));

        Page<SupportQna> reply = qnaRepository.findAllByParentIdOrderByRegDtAsc(id, p);

        mv.addObject("qna", question);

        if(reply.hasContent()){

            reply.getContent().stream().forEach(ans -> {
                ans.setRegDtTxt(ans.getRegDt().format(formatter));
                ans.setReadYn("Y");
            });

            mv.addObject("reply", reply.getContent());
            mv.addObject("pageNo", p.getPageNumber());
            mv.addObject("pageSize", p.getPageSize());
            mv.addObject("totalPage", reply.getTotalPages());

        }

        return mv;

    }

}
