function DepositApi () {
    this.call = function(uri, callback, resultProcFunc, params) {
        $.ajax({
            url: "/api/deposit" + uri
            , type: "POST"
            , dataType: 'json'
            , contentType:"application/json; charset=UTF-8"
            , data: JSON.stringify(params)
            , success: function (data) {
                resultProcFunc(data);
                return callback(data);
            }
            , error:function(e){
                alert('예외가 발생했습니다.');
            }
        });
        return true;
    };

    this.callPage = function(uri, callback, resultProcFunc, params) {
        $.ajax({
            url: "/api/deposit" + uri
            , type: "POST"
            , dataType: 'json'
            //, contentType:"application/json; charset=UTF-8"
            , data: params
            , success: function (data) {
                resultProcFunc(data);
                return callback(data);
            }
            , error:function(e){
                //showAlert("ERROR : " + JSON.parse(e.responseText).message);
            }
        });
        return true;
    };

    this.wallet = function(callback, params) {
        return this.call("/wallet", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.wallets = function(callback, params) {
        return this.call("/wallets", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.regist = function(callback, params) {
        return this.call("/registWallet", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };

    this.transactions = function(callback, params) {
        return this.callPage("/transactions", callback, function(result) {
            if (result.code == "SUCCESS") {
                //showAlert("success");
            } else {
                //showAlert("failed");
            }
        }, params);
    };
}