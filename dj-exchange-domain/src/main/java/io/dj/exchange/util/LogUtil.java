package io.dj.exchange.util;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;

/**
 * Created by kun7788 on 2016. 12. 11..
 */
@Slf4j
public class LogUtil {
    static final org.slf4j.Logger TRADE_LOG = LoggerFactory.getLogger("TRADE");

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static void info(String color, String msg, Object... args) {
        log.info(color + msg + ANSI_RESET, args);
    }

    public static void info(String color, String msg) {
        log.info(color + msg + ANSI_RESET);
    }

    public static void infoTrade(String color, String msg, Object... args) {
        TRADE_LOG.info(color + msg + ANSI_RESET, args);
    }

    public static void infoTrade(String color, String msg) {
        TRADE_LOG.info(color + msg + ANSI_RESET);
    }
}
