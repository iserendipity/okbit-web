package io.dj.exchange.config;

import freemarker.template.utility.XmlEscape;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 10.
 * Description
 */
@Configuration
public class FreeMarkerConfig {

    @Bean(name = "freemarkerConfig")
    public FreeMarkerConfigurer freemarkerConfig() {
        FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
        configurer.setTemplateLoaderPaths("classpath:/templates");
        Map<String, Object> map = new HashMap<>();
        map.put("xml_escape", new XmlEscape());
        configurer.setFreemarkerVariables(map);
        Properties settings = new Properties();
        settings.setProperty("auto_import", "spring.ftl as spring");
        configurer.setFreemarkerSettings(settings);

        return configurer;
    }

}
