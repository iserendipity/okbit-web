package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.BatchOffset;
import io.dj.exchange.domain.primary.WebNotices;
import io.dj.exchange.enums.BatchKey;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by jeongwoo on 2017. 4. 26..
 */
public interface WebNoticesRepository extends JpaRepository<WebNotices, Long> {

    List<WebNotices> findTop4ByStatusOrderByDtDesc(String use);

    Page<WebNotices> findAllByStatusOrderByDtDesc(String status, Pageable p);
}
