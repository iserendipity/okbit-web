<head>
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="format-detection" content="telephone=no">
    <meta charset="UTF-8">

    <meta name="description" content="오케이비트">
    <meta name="keywords" content="OK-BIT, Bitcoin, Ehterium, Monero, Litecoin, Bitfinex, Poloniex, 비트코인, 비트코인시세, 가상화폐, 가상화폐거래소, 비트코인거래소, 이더리움, 이더리움시세">

    <title><@spring.message "TITLE"/></title>

    <!-- CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/animate.min.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/form.css" rel="stylesheet">
    <link href="/css/calendar.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/icons.css" rel="stylesheet">
    <link href="/css/generics.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/jquery.loading.min.css" rel="stylesheet">

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-102994629-1', 'auto');
        ga('send', 'pageview');

    </script>

</head>
<#include "language.ftl">