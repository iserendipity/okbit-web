package io.dj.exchange.provider;

import feign.*;
import io.dj.exchange.domain.primary.Token;

import java.util.List;
import java.util.Map;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 18.
 * Description
 */
public interface BankServiceProvider {

    @RequestLine("GET /oauth/2.0/authorize?client_id={client_id}&client_secret={client_secret}&redirect_url={redirect_url}&scope={scope}&response_type=code")
    @Headers("Content-Type: application/json")
    Map<String, Object> authorize(@Param("client_id") String client_id,@Param("client_secret") String secret, @Param("redirect_url") String redirect_url, @Param("scope") String scope);

    @RequestLine("POST /1.0/token")
    @Headers("Content-Type: application/x-www-form-urlencoded")
    List<Map<String, Object>> accountList();

//    @RequestLine("POST /oauth/2.0/token")
//    @Headers("Content-Type: application/x-www-form-urlencoded")
//    Map<String, Object> accessToken(@Param("client_id") String client_id, @Param("client_secret") String secret, @Param("redirect_uri") String redirect_uri, @Param("code") String code, @Param("grant_type") String grant_type, @Param("scope") String scope);

    @RequestLine("POST /oauth/2.0/token")
    @Headers("Content-Type: application/x-www-form-urlencoded")
    Token accessToken(@Param("client_id") String client_id, @Param("client_secret") String secret, @Param("redirect_uri") String redirect_uri, @Param("code") String code, @Param("grant_type") String grant_type, @Param("scope") String scope);

    @RequestLine("POST /oauth/2.0/token")
    @Headers("Content-Type: application/x-www-form-urlencoded")
    Token accessToken(@Param("client_id") String client_id, @Param("client_secret") String secret, @Param("redirect_uri") String redirect_uri, @Param("grant_type") String grant_type, @Param("scope") String scope);

    @RequestLine("GET /v1.0/account/transaction_list")
    @Headers({"Content-Type: application/x-www-form-urlencoded"})
    Map<String,Object> transactionList(@Param("fintech_use_num") String fintech_use_no, @Param("tran_dtime") String tran_dtime, @Param("inquiry_type") String inquiry_type, @Param("from_date") String from_date, @Param("to_date") String to_date, @Param("sort_order") String sort_order, @Param("page_index") String page_index);
}
