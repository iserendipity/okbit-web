package io.dj.exchange.security;

import lombok.Getter;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 18.
 * Description
 */
@Getter
public class GotpWebAuthenticationDetails extends WebAuthenticationDetails {

    public static final String TOTP_VERIFICATION_CODE = "totp-verification-code";
    private Integer totpKey;

    /**
     * Records the remote address and will also set the session Id if a session
     * already exists (it won't create one).
     *
     * @param request that the authentication request was received from
     */
    public GotpWebAuthenticationDetails(HttpServletRequest request) {
        super(request);
        String totpKeyString = request.getParameter(TOTP_VERIFICATION_CODE);
        if (StringUtils.hasText(totpKeyString)) {
            try {
                this.totpKey = Integer.valueOf(totpKeyString);
            } catch (NumberFormatException e) {
                this.totpKey = null;
            }
        }
    }

}
