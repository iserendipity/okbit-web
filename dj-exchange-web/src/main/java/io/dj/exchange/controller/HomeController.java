package io.dj.exchange.controller;


import io.dj.exchange.annotation.TransactionalEx;
import io.dj.exchange.domain.primary.EmailConfirm;
import io.dj.exchange.domain.primary.WebNotices;
import io.dj.exchange.exception.OkbitViewException;
import io.dj.exchange.repository.hibernate.primary.WebNoticesRepository;
import io.dj.exchange.service.UsersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * Project okbit.
 * Created by jeongwoo on 2017. 4. 4.
 * Description
 */
@Slf4j
@Controller
public class HomeController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private WebNoticesRepository webNoticesRepository;

    @RequestMapping("/")
    public ModelAndView index(@RequestParam(name="regist", defaultValue = "") String regist
            , @RequestParam(name="error", defaultValue = "") String error
            , @RequestParam(name="find", defaultValue = "") String find) {

        ModelAndView mvn = new ModelAndView("index");
        mvn.addObject("error", error);
        mvn.addObject("regist", regist);
        mvn.addObject("find", find);

        List<WebNotices> noticesList = webNoticesRepository.findTop4ByStatusOrderByDtDesc("USE");

        mvn.addObject("notices", noticesList);

        return mvn;
    }

    @RequestMapping(value = "/doSignup", method= RequestMethod.POST)
    public String doSignup(@RequestParam("loginId") String loginId
            , @RequestParam("password") String password
            , HttpServletRequest request) {

        log.info("doSignup : {}, {}", loginId, password);
        return usersService.doSignup(loginId, password, request);
    }

    @RequestMapping(value="/findPassword", method=RequestMethod.POST)
    public String findPassword(@RequestParam("loginId") String loginId
            , HttpServletRequest request) throws OkbitViewException {

        return usersService.findPassword(loginId, request);
    }

    @RequestMapping(value="/confirm")
    public String confirmEmail(@RequestParam("hash") String hash, @RequestParam("confirmCode") String confirmCode) throws OkbitViewException{

        return usersService.confirmEmail(hash, confirmCode);

    }

    @RequestMapping(value="/confirm/result")
    public ModelAndView confirmResult(@RequestParam Map<String, Object> result) {

        log.info("--------requestparam : {}", result);
        ModelAndView mv = new ModelAndView("mailConfirmResult");
        mv.addObject("data", result);

        return mv;

    }

    @PostMapping(value="/resetPassword")
    @TransactionalEx
    public ModelAndView resetPassword(@RequestParam("hash") String hash, @RequestParam("confirmCode") String confirmCode) throws OkbitViewException{

        ModelAndView mv = new ModelAndView();

        EmailConfirm confirm = usersService.getConfirmObjForPasswordReset(hash, confirmCode);

        if(usersService.dateCheck(confirm)){

            mv.setViewName("resetPassword");
            mv.addObject("data", confirm);

        } else {

            throw new OkbitViewException("EMAIL_EXPIRED");

        }

        return mv;

    }

}
