package io.dj.exchange.controller;

import io.dj.exchange.domain.primary.User;
import io.dj.exchange.exception.OkbitException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Project dj-exchange-web.
 * Created by jeongwoo on 2017. 4. 6.
 * Description
 */
@Controller
@RequestMapping
public class ConsoleController {

    @RequestMapping(value="console")
    public ModelAndView console(@ModelAttribute("user") User user, HttpServletRequest req) throws OkbitException{
        ModelAndView mvn = new ModelAndView("console");
        return mvn;
    }

}
