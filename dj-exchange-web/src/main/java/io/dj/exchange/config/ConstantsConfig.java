package io.dj.exchange.config;

import org.springframework.context.annotation.Configuration;

/**
 * Created by kun7788 on 2016. 10. 28..
 */
@Configuration
public class ConstantsConfig {
    public static String DOWNLOAD_TMP_FOLDER = "/data/tmp/";

//    @Value("$(admin.wallet.user_id}")
//    public static Long ADMIN_WALLET_USER_ID;
//
//    @Value("${recaptcha.validation.secretKey}")
//    public static String GOOGLE_RECAPTCHA_SECRET;
//
//    public static boolean IS_TRADING_BEGIN_BATCH = false;

//    @Bean
//    public Cache<Long, Ad> cache() {
//        Cache<Long, Ad> cache = CacheBuilder.newBuilder()
//                .maximumSize(10000)
//                .expireAfterWrite(1, TimeUnit.MINUTES)
//                .build();
//        return cache;
//    }
}
