package io.dj.exchange.domain.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 5.
 * Description
 */
public class ExcelDTO {

    @Data
    public class uploadRequest{

        List<Cells> sheet1;


    }

    @Data
    public class Cells{

        @NotNull
        String date;

        @NotNull
        String amount;

        @NotNull
        String depositDvcd;

        @NotNull
        String bank;

    }

}
