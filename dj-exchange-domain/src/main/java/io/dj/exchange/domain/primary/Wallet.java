package io.dj.exchange.domain.primary;

import io.dj.exchange.util.DataUtil;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 4. 28.
 * Description
 */
@Entity
@Data
@IdClass(WalletsPK.class)
public class Wallet implements Serializable{

    @Id
    @GeneratedValue
    private Long id;

    @Id
    private Long userId;

    @Column(name = "address")
    private String address;

    @Digits(integer=24, fraction=8)
    @Column(name = "total_balance")
    private BigDecimal totalBalance;

    @Digits(integer=24, fraction=8)
    @Column(name = "using_balance")
    private BigDecimal usingBalance;

    @Digits(integer=24, fraction=8)
    @Column(name = "available_balance")
    private BigDecimal availableBalance;

    @Transient
    private String totalBalanceTxt;
    @Transient
    private String usingBalanceTxt;
    @Transient
    private String availableBalanceTxt;


    @Column(name = "coin_name")
    private String coinName;

    @OneToOne
    @JoinColumn(name = "name")
    private Coin coin;

    @Column(name = "tag")
    private String tag;

    @Column(name = "deposit_dvcd")
    private String depositDvcd;

    @Digits(integer=24, fraction=8)
    @Transient
    private BigDecimal realBalance;

    @Transient
    private String realBalanceTxt;

    public void initRealBalance(){
        this.realBalanceTxt = DataUtil.decimal(realBalance, getFormatter());
    }

    public void initTotalBalance() {

        this.totalBalanceTxt = DataUtil.decimal(totalBalance, getFormatter());
    }

    public String getFormatter(){

        String formatter = "";

        switch (this.coin.getName()){
            case "KRW" : formatter = "#,##0";
                break;
            case "BITCOIN" : formatter = "#,##0.##########";
                break;
//            case "DASH" : formatter = "#,##0.##";
//                break;
//            case "MONERO" : formatter = "#,##0.##";
//                break;
//            case "ETHEREUM" : formatter = "#,##0.##";
//                break;
            default : formatter = "#,##0.##########";
                break;

        }

        return formatter;
    }

    public void initAvailableBalance() {
        this.availableBalanceTxt = DataUtil.decimal(availableBalance, getFormatter());
    }

    public void initUsingBalance() {
        this.usingBalanceTxt =DataUtil.decimal(usingBalance, getFormatter());
    }
}
