package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.Token;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by jeongwoo on 2017. 4. 4..
 */
public interface TokenRepository extends JpaRepository<Token, Long> {

    public Token findByScope(String scope);

}
