<script id="myOrderFullTemplate" type="text/x-jquery-tmpl">
    <tr class="text-center">
        <td class="col-xs-2">${r"${orderType}"}</td>
        <td class="col-xs-4">${r"${regDtTxt}"}</td>
        <td class="col-xs-2">${r"${price}"}</td>
        <td class="col-xs-3">${r"${amountRemaining}"}/${r"${amount}"}</td>
        <td class="col-xs-1"><a href="javascript:void(0);" onclick="javascript:cancel('${r"${id}"}', this);"><i class="fa fa-times"></i></a></td>
    </tr>
</script>
<div id="myOrderModal">
    <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->
    <div id="closebt-container" class="close-myOrderModal m-b-20">
        <img class="closebt" src="/img/closebt.svg">
    </div>

    <div class="modal-content container" style="width:calc(100% - 52px); min-height:100vh;">
        <div class="fixed-thead-wrap col-xs-12 text-center">
            <div class="fixed-thead col-xs-2"><@spring.message "TITLE_TYPE"/>
            </div>
            <div class="fixed-thead col-xs-4"><@spring.message "TITLE_DATE"/>
            </div>
            <div class="fixed-thead col-xs-2"><@spring.message "TITLE_PRICE"/>
            </div>
            <div class="fixed-thead col-xs-3"><@spring.message "TITLE_AMOUNT"/>
            </div>
            <div class="fixed-thead col-xs-1"><i class="fa fa-cog"></i>
            </div>
        </div>
        <div style="width:100%" class="overflow">
            <table class="table table-condensed table-hover table-fixed">
                <thead class="text-center">
                <tr>
                    <th class="col-xs-2">
                    </th>
                    <th class="col-xs-4">
                    </th>
                    <th class="col-xs-2">
                    </th>
                    <th class="col-xs-3">
                    </th>
                    <th class="col-xs-1">
                    </th>
                </tr>

                </thead>
                <tbody id="myOrdersFull">
                <tr class="text-center">
                    <td colspan="5">
                        <@spring.message "EMPTY_DATA"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>