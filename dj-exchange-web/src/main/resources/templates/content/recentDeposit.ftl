<script id="recentDepositTemplate" type="text/x-jquery-tmpl">
<tr>
    <td class="col-xs-2">${r"${dtTxt}"}
    </td>
    <td class="col-xs-1">${r"${status}"}
    </td>
    <td class="col-xs-1">${r"${coin.name}"}
    </td>
    <td class="col-xs-1">${r"${amount}"}
    </td>
    <td class="col-xs-1">${r"${confirmation}"} / ${r"${coin.minConfirmation}"}
    </td>
    <td class="col-xs-6">{{if (coin.name == "KRW")}}${r"${txId}"}{{else}}<a target="_blank" href="${r"${blockExplorer.explorerUrl}"}${r"${txId}"}">${r"${txId}"}</a>{{/if}}
    </td>
</tr>
</script>
<div class="overflow" style="max-height:400px;">
<table class="table table-condensed table-fixed">
    <thead style="height:0px;">
    <tr>
        <th class="col-xs-2">
        </th>
        <th class="col-xs-1">
        </th>
        <th class="col-xs-1">
        </th>
        <th class="col-xs-1">
        </th>
        <th class="col-xs-1">
        </th>
        <th class="col-xs-6">
        </th>
    </tr>
    </thead>
    <tbody style="height:366px; overflow:scroll" id="recentDepositTableBody">
    <tr>
        <td colspan="6" class="text-center"><@spring.message "NO_RECENT_DEPOSIT"/></td>
    </tr>
    </tbody>
</table>
</div>