package io.dj.exchange.domain.chart;

import lombok.Data;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 22.
 * Description
 */
@Data
public class Symbol {

    private String name;
    private String ticker;
    private String description;
    private String type;
    private String session;
    private String exchange;

    private int minmov;
    private int minmov2;
    private int pointvalue;
    private boolean has_intraday;
    private boolean has_no_vo;

    private int pricescale;

    private boolean fractional;

    private String currency_code;

}
