package io.dj.exchange.domain.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 23.
 * Description
 */
@Data
public class SumHistory {

    private BigDecimal h;
    private BigDecimal l;
    private BigDecimal v;
    private BigDecimal o;
    private BigDecimal c;
    private BigDecimal p;
    private long id;
    private long t;
    private String ticker;
    private String coinName;
    private LocalDateTime dt;

    public SumHistory(BigDecimal h, BigDecimal l, BigDecimal v, long t, BigDecimal p) {
        this.h = h;
        this.l = l;
        this.v = v;
        this.t = t;
        this.p = p;
    }

    public SumHistory(BigDecimal h, BigDecimal l, BigDecimal v, BigDecimal o, BigDecimal c, long t) {
        this.h = h;
        this.l = l;
        this.v = v;
        this.o = o;
        this.c = c;
        this.t = t;
    }

    public SumHistory(BigDecimal h, BigDecimal l, BigDecimal v, long t) {
        this.h = h;
        this.l = l;
        this.v = v;
        this.t = t;
    }

    public SumHistory(long id, long t, BigDecimal p, BigDecimal v){

        this.v = v;
        this.p = p;
        this.id = id;
        this.t = t;

    }

    public SumHistory(long id, long t, String ticker, String coinName, BigDecimal p, BigDecimal v){

        this.v = v;
        this.p = p;
        this.id = id;
        this.t = t;
        this.ticker = ticker;
        this.coinName = coinName;

    }

    public SumHistory(long id, LocalDateTime dt, String coinName, BigDecimal p, BigDecimal v){

        this.v = v;
        this.p = p;
        this.id = id;
        this.dt = dt;
        this.coinName = coinName;

    }

    public SumHistory(long id, long t, String ticker, BigDecimal p, BigDecimal v){

        this.v = v;
        this.p = p;
        this.id = id;
        this.t = t;
        this.ticker = ticker;

    }

    public SumHistory(long id, LocalDateTime dt, BigDecimal p, BigDecimal v){

        this.v = v;
        this.p = p;
        this.id = id;
        this.dt = dt;

    }

    public Double getVol(){

        return this.p.doubleValue();

    }

}
