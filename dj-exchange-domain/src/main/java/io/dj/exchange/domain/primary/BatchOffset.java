package io.dj.exchange.domain.primary;

import io.dj.exchange.enums.BatchKey;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 8.
 * Description
 */
@Data
@Entity
public class BatchOffset implements Serializable{

    @Id
    @Column(name="offset_name")
    @Enumerated(EnumType.STRING)
    private BatchKey offsetName;

    @Column(name="offset_value")
    private long offsetValue;

}
