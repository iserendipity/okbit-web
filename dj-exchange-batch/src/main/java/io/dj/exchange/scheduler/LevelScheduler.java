package io.dj.exchange.scheduler;

import io.dj.exchange.domain.primary.AdminLevel;
import io.dj.exchange.domain.primary.ManualTransactions;
import io.dj.exchange.domain.primary.User;
import io.dj.exchange.domain.primary.UserSetting;
import io.dj.exchange.enums.UserLevels;
import io.dj.exchange.repository.hibernate.primary.AdminLevelRepository;
import io.dj.exchange.repository.hibernate.primary.ManualTransactionsRepository;
import io.dj.exchange.repository.hibernate.primary.UserRepository;
import io.dj.exchange.repository.hibernate.primary.UserSettingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 6. 22.
 * Description
 */
@Slf4j
@Component
public class LevelScheduler {

    @Autowired
    private AdminLevelRepository adminLevelRepository;

    @Autowired
    private UserSettingRepository userSettingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ManualTransactionsRepository manualTransactionsRepository;

    @Scheduled(fixedRate=300000)
    public void upgradeLevel(){
        Page<AdminLevel> ad = adminLevelRepository.findAllByLevelOrderByLevelModifyDtAsc(UserLevels.LEV2.name(), new PageRequest(0, 100));
        if(ad.hasContent()){

            ad.getContent().forEach(level -> {

                Optional<ManualTransactions> mt = manualTransactionsRepository.findTopByUserIdAndCategoryAndCoinNameOrderByRegDtAsc(level.getUserId(), "receive", "KRW");

                if(mt.isPresent() && mt.get().getRegDt().isBefore(LocalDateTime.now().minusHours(72))){

                    level.setLevel(UserLevels.LEV3.name());
                    level.setLevelModifyDt(LocalDateTime.now());

                    adminLevelRepository.save(level);

                    UserSetting us = userSettingRepository.findOneByUserId(level.getUserId());
                    us.setLevel(UserLevels.LEV3.name());

                    userSettingRepository.save(us);

                }

            });
        }
    }

}
