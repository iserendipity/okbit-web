<script id="tradeTemplate" type="text/x-jquery-tmpl">
    <tr class="text-center">
        <td class="col-xs-3">${r"${dt}"}</td>
        <td class="col-xs-3">${r"${orderType}"}</td>
        <td class="col-xs-3">{{if (orderType == "SELL")}}<span style="color:green; font-weight:bolder; text-shadow: none !important;">${r"${priceTxt}"}</span>{{else}}<span style="color:red; font-weight:bolder; text-shadow: none !important;">${r"${priceTxt}"}</span>{{/if}}</td>
        <td class="col-xs-3">${r"${amountTxt}"}</td>
    </tr>
</script>
<table class="table table-condensed table-hover table-fixed">
    <thead>
    <tr>
        <th class="col-xs-3">
        </th>
        <th class="col-xs-3">
        </th>
        <th class="col-xs-3">
        </th>
        <th class="col-xs-3">
        </th>
    </tr>

    </thead>
    <tbody id="tradeBody">
        <tr class="text-center">
            <td colspan="4">
                <@spring.message "EMPTY_DATA"/>
            </td>
        </tr>
    </tbody>
</table>