package io.dj.exchange.dto;

import lombok.Data;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 7. 17.
 * Description
 */
@Data
public class ReportDTO {

    private String email;
    private String coin;

}
