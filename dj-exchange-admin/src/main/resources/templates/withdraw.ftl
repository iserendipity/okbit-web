<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <#include "common/head.ftl">
    </head>
    <body id="skin-blur-violate">

        <header id="header" class="media">
            <#include "common/top.ftl">
        </header>
        
        <div class="clearfix"></div>
        
        <section id="main" class="p-relative" role="main">
            
            <!-- Sidebar -->
            <aside id="sidebar">

                <!-- Side Menu -->
                <ul class="list-unstyled side-menu">
                    <#include "common/sidemenu.ftl">
                </ul>

            </aside>
        
            <!-- Content -->
            <section id="content" class="container">
            <#include "common/messages.ftl">

                <div class="clearfix"></div>

                <!-- Breadcrumb -->
                <ol class="breadcrumb hidden-xs">
                    <li><a href="/console">Home</a></li>
                    <li class="active">Withdraw</li>
                    <#--<li class="active">Data</li>-->
                </ol>
                
                <h4 class="page-title">&nbsp;</h4>
                                
                <div class="block-area">
                    <div class="row">
                        <div class="col-md-12">
                            <!--  ORDERS -->
                            <div class="row">
                                <div class="col-md-6">
                                    <script id="withdrawTemplate" type="text/x-jquery-tmpl">
                                    <tr>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${regDtTxt}"}</td>
                                        <#--<td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${coinName}"}</td>-->
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${amount}"}</td>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${bankNm}"}</td>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${userNm}"}</td>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${account}"}</td>
                                        <#--<td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${reason}"}</td>-->
                                        <td class="text-center">
                                            <button onclick="javascript:approveSendTransaction('${r"${id}"}', this);" class="btn btn-alt btn-xs" type="button">출금완료</button>
                                            <button href="#" onclick="javascript:reasonTransaction('${r"${id}"}', this);" class="btn btn-alt btn-xs" type="button">출금반려</button>
                                        </td>
                                    </tr>
                                    </script>
                                    <script id="withdrawCoinTemplate" type="text/x-jquery-tmpl">
                                    <tr>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${regDtTxt}"}</td>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${coinName}"}</td>
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${amount}"}</td>
                                        <#--<td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${bankNm}"}</td>-->
                                        <#--<td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${userNm}"}</td>-->
                                        <td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${address}"}</td>
                                        <#--<td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${account}"}</td>-->
                                        <#--<td onclick="javascript:getPreview('${r"${userId}"}');getIdPreview('${r"${userId}"}');">${r"${reason}"}</td>-->
                                        <td class="text-center"><button onclick="javascript:approveSendTransaction('${r"${id}"}', this);" class="btn btn-alt btn-xs" type="button">출금승인</button><button href="#" onclick="javascript:reasonTransaction('${r"${id}"}', this);" class="btn btn-alt btn-xs" type="button">출금반려</button></td>
                                    </tr>
                                    </script>
                                    <div class="tile">
                                        <h2 class="tile-title">원화 출금
                                        </h2>
                                        <div class="overflow">
                                            <table class="table table-hover table-condensed text-center">
                                                <thead>
                                                    <tr>
                                                        <td class="col-md-2">등록일</td>
                                                        <td class="col-md-2">출금요청금액</td>
                                                        <td class="col-md-1">은행명</td>
                                                        <td class="col-md-2">예금주</td>
                                                        <td class="col-md-3">계좌번호</td>
                                                        <td class="text-center col-md-2"><i class="fa fa-cog"></i></td>
                                                    </tr>
                                                </thead>
                                                <tbody id="withReqBody">
                                                    <tr class="text-center">
                                                        <td colspan="5">데이터가 없습니다.</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="panel-footer text-center">

                                            <ul id="withdrawPaging" class="pagination pagination-sm">
                                                <li class="active"><a href="javascript:getReqWithdraw('1');">1</a></li>
                                            </ul>

                                            <#--<a href="" class="nb pull-right" style="font-size:8px;">Full Book</a>-->
                                            <#--<div class="clearfix"></div>-->
                                        </div>
                                    </div>
                                    <div class="tile">
                                        <h2 class="tile-title">코인 출금
                                        </h2>
                                        <div class="overflow">
                                            <table class="table table-hover table-condensed text-center">
                                                <thead>
                                                <tr>
                                                    <td class="col-md-2">등록일</td>
                                                    <td class="col-md-1">코인명</td>
                                                    <td class="col-md-2">출금요청금액</td>
                                                    <#--<td class="col-md-1">요청자</td>-->
                                                    <td class="col-md-5">주소</td>
                                                    <#--<td class="col-md-2">반려사유</td>-->
                                                    <td class="text-center col-md-2"><i class="fa fa-cog"></i></td>
                                                </tr>
                                                </thead>
                                                <tbody id="withReqCoinBody">
                                                <tr class="text-center">
                                                    <td colspan="6">데이터가 없습니다.</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="panel-footer text-center">

                                            <ul id="withdrawCoinPaging" class="pagination pagination-sm">
                                                <li class="active"><a href="javascript:getReqWithdrawCoin('1');">1</a></li>
                                            </ul>

                                        <#--<a href="" class="nb pull-right" style="font-size:8px;">Full Book</a>-->
                                        <#--<div class="clearfix"></div>-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="tile">
                                        <h2 class="tile-title">ID CARD Preview
                                        </h2>
                                        <div class="overflow">
                                            <div class="preview" style="height:605px;" id="idCardPreview">



                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            &nbsp;
                                            <#--<a href="" class="nb pull-right" style="font-size:8px;">출금 승인</a>-->
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="tile">
                                        <h2 class="tile-title">Preview
                                        </h2>
                                        <div class="overflow">
                                            <div class="preview" style="height:605px;" id="preview">



                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            &nbsp;
                                            <#--<a href="" class="nb pull-right" style="font-size:8px;">출금 승인</a>-->
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
                <!-- Chat -->
                <div class="chat">
                    
                    <!-- Chat List -->
                    <div class="pull-left chat-list">
                        <div class="listview narrow">
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/1.jpg" width="30" alt="">
                                <div class="media-body p-t-5">
                                    Alex Bendit
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/2.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">David Volla Watkinson</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/3.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Mitchell Christein</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/4.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Wayne Parnell</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/5.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Melina April</span>
                                </div>
                            </div>
                            <div class="media">
                                <img class="pull-left" src="/img/profile-pics/6.jpg" width="30" alt="">
                                <div class="media-body">
                                    <span class="t-overflow p-t-5">Ford Harnson</span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    <!-- Chat Area -->
                    <div class="media-body">
                        <div class="chat-header">
                            <a class="btn btn-sm" href="">
                                <i class="fa fa-circle-o status m-r-5"></i> Chat with Friends
                            </a>
                        </div>
                    
                        <div class="chat-body">
                            <div class="media">
                                <img class="pull-right" src="/img/profile-pics/1.jpg" width="30" alt="" />
                                <div class="media-body pull-right">
                                    Hiiii<br/>
                                    How you doing bro?
                                    <small>Me - 10 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-left">
                                <img class="pull-left" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    I'm doing well buddy. <br/>How do you do?
                                    <small>David - 9 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-right">
                                <img class="pull-right" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    I'm Fine bro <br/>Thank you for asking
                                    <small>Me - 8 Mins ago</small>
                                </div>
                            </div>
                            
                            <div class="media pull-right">
                                <img class="pull-right" src="/img/profile-pics/2.jpg" width="30" alt="" />
                                <div class="media-body">
                                    Any idea for a hangout?
                                    <small>Me - 8 Mins ago</small>
                                </div>
                            </div>
                        
                        </div>
                    
                        <div class="chat-footer media">
                            <i class="chat-list-toggle pull-left fa fa-bars"></i>
                            <i class="pull-right fa fa-picture-o"></i> 
                            <div class="media-body">
                                <textarea class="form-control" placeholder="Type something..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Older IE Message -->
            <!--[if lt IE 9]>
                <div class="ie-block">
                    <h1 class="Ops">Ooops!</h1>
                    <p>You are using an outdated version of Internet Explorer, upgrade to any of the following web browser in order to access the maximum functionality of this website. </p>
                    <ul class="browsers">
                        <li>
                            <a href="https://www.google.com/intl/en/chrome/browser/">
                                <img src="/img/browsers/chrome.png" alt="">
                                <div>Google Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.mozilla.org/en-US/firefox/new/">
                                <img src="/img/browsers/firefox.png" alt="">
                                <div>Mozilla Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com/computer/windows">
                                <img src="/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://safari.en.softonic.com/">
                                <img src="/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/downloads/ie-10/worldwide-languages">
                                <img src="/img/browsers/ie.png" alt="">
                                <div>Internet Explorer(New)</div>
                            </a>
                        </li>
                    </ul>
                    <p>Upgrade your browser for a Safer and Faster web experience. <br/>Thank you for your patience...</p>
                </div>   
            <![endif]-->
        </section>

        <form id="loadingForm"></form>

        <!-- Javascript Libraries -->
        <!-- jQuery -->
        <script src="/js/jquery.min.js"></script> <!-- jQuery Library -->
        <script src="/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
        <script src="/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

        <!-- Bootstrap -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Charts -->
        <#--<script src="/js/charts/jquery.flot.js"></script> <!-- Flot Main &ndash;&gt;-->
        <#--<script src="/js/charts/jquery.flot.time.js"></script> <!-- Flot sub &ndash;&gt;-->
        <#--<script src="/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub &ndash;&gt;-->
        <#--<script src="/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen &ndash;&gt;-->

        <#--<script src="/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts &ndash;&gt;-->
        <#--<script src="/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts &ndash;&gt;-->
        <#--<script src="/js/charts.js"></script> <!-- All the above chart related functions &ndash;&gt;-->

        <!--scroll -->
        <script src="/js/scroll.min.js"></script>

        <!--  Form Related -->
        <#--<script src="/js/icheck.js"></script> <!-- Custom Checkbox + Radio &ndash;&gt;-->

        <!-- UX -->
        <#--<script src="/js/scroll.min.js_bak"></script> <!-- Custom Scrollbar &ndash;&gt;-->

        <!-- Jqutemplateslate -->
        <script src="/js/jquery.tmpl.min.js"></script>

        <!-- Other -->
        <script src="/js/calendar.min.js"></script> <!-- Calendar -->
        <script src="/js/feeds.min.js"></script> <!-- News Feeds -->

        <script src="/js/jquery.loading.min.js"></script> <!-- loading -->

        <!-- All JS functions -->
        <script src="/js/functions.js"></script>
        <script src="/js/api/admin_api.js"></script>
        <script src="/js/view/admin.js"></script>

        <script type="text/javascript">

            $('document').ready(function(){

                getReqWithdraw('1');
                getReqWithdrawCoin('1');

            });

        </script>

    </body>
</html>
