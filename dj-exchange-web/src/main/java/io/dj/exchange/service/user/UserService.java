package io.dj.exchange.service.user;

import io.dj.exchange.domain.primary.User;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by tommy on 2016. 2. 29..
 */
public interface UserService {
    Optional<User> getUserByLoginId(String loginId);
    public Optional<User> getUserById(long id);
    Collection<User> getAllUsers();
}
