<script type="text/javascript">

    var no_data_msg = '<@spring.message "EMPTY_DATA"/>';
    var no_deposit_msg = '<@spring.message "NO_RECENT_DEPOSIT"/>';
    var no_withdraw_msg = '<@spring.message "NO_RECENT_WITHDRAW"/>';
    var not_exist_wallet_msg = '<@spring.message "NOT_EXIST_WALLET"/>';
    var COPY_DONE = '<@spring.message "COPY_DONE"/>';
    var submitConfirm = '<@spring.message "SUBMIT_CONFIRM"/>';
    var btc_not_now = '<@spring.message "BTC_NOT_NOW"/>';
</script>