<div id="animatedModal">
    <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->
    <div id="closebt-container" class="close-animatedModal m-b-20">
        <img class="closebt" src="/img/closebt.svg">
    </div>

    <div class="modal-content container" style="width:calc(100% - 52px); min-height:100vh;">
        <div class="fixed-thead-wrap col-xs-12 text-center" >
            <#--<div class="col-xs-6">BUY</div>-->
            <#--<div class="col-xs-6">SELL</div>-->
            <#--<div class="col-xs-1">COUNT</div>-->
            <#--<div class="col-xs-2">TOTAL PRICE</div>-->
            <#--<div class="col-xs-1">TOTAL COIN</div>-->
            <#--<div class="col-xs-1">AMOUNT COIN</div>-->
            <#--<div class="col-xs-1">PRICE</div>-->
            <#--<div class="col-xs-1">PRICE</div>-->
            <#--<div class="col-xs-1">AMOUNT COIN</div>-->
            <#--<div class="col-xs-1">TOTAL COIN</div>-->
            <#--<div class="col-xs-2">TOTAL PRICE</div>-->
            <#--<div class="col-xs-1">COUNT</div>-->
            <div class="col-xs-6"><span class="h4"><@spring.message "TITLE_BUY"/></span></div>
            <div class="col-xs-6"><span class="h4"><@spring.message "TITLE_SELL"/></span></div>
            <div class="col-xs-1"><@spring.message "TITLE_COUNT"/></div>
            <div class="col-xs-2"><@spring.message "TITLE_TOT_PRICE"/></div>
            <div class="col-xs-1"><@spring.message "TITLE_TOT_COIN"/></div>
            <div class="col-xs-1"><@spring.message "TITLE_AMOUNT"/></div>
            <div class="col-xs-1"><@spring.message "TITLE_PRICE"/></div>
            <div class="col-xs-1"><@spring.message "TITLE_PRICE"/></div>
            <div class="col-xs-1"><@spring.message "TITLE_AMOUNT"/></div>
            <div class="col-xs-1"><@spring.message "TITLE_TOT_COIN"/></div>
            <div class="col-xs-2"><@spring.message "TITLE_TOT_PRICE"/></div>
            <div class="col-xs-1"><@spring.message "TITLE_COUNT"/></div>
        </div>
        <div style="width:50%;" class="pull-left overflow">
            <table class="table table-condensed table-fixed pull-left table-responsive text-center">
                <thead>
                <tr>
                    <th class="col-xs-1"></th>
                    <th class="col-xs-2"></th>
                    <th class="col-xs-1"></th>
                    <th class="col-xs-1"></th>
                    <th class="col-xs-1"></th>
                </tr>
                </thead>
                <tbody id="buyOrdersFull">
                <tr>
                    <td colspan="5" class="text-center">
                    <@spring.message "EMPTY_DATA"/>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
        <div style="width:50%; " class="pull-right overflow">
            <table class="table table-condensed table-hover table-fixed table-responsive text-center">
                <thead>
                <tr>
                    <th class="col-xs-1"></th>
                    <th class="col-xs-1"></th>
                    <th class="col-xs-1"></th>
                    <th class="col-xs-2"></th>
                    <th class="col-xs-1"></th>
                </tr>
                </thead>
                <tbody id="sellOrdersFull">
                <tr>
                    <td colspan="5" class="text-center">
                    <@spring.message "EMPTY_DATA"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>