package io.dj.exchange.domain.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 17.
 * Description
 */
public class IpfsDTO {

    @Data
    @Builder
    public static class IpfsPubInfo {
        String contentType; //0,1,2,3
        String orgFilename;
        String hashFileName;
        String ipfsHash;
    }

}
