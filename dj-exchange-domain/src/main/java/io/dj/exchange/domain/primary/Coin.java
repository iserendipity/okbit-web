package io.dj.exchange.domain.primary;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Data
public class Coin {

    @Id
    @Size(max = 30)
    @Column(name = "name")
    private String name;

    private String mark;

    @Digits(integer=24, fraction=8)
    @DecimalMin("0.00000001")
    @Column(name = "fee_percent")
    private BigDecimal feePercent;

    private int priority;

    @Column(name = "min_confirmation")
    private int minConfirmation;

    @Digits(integer=24, fraction=8)
    @DecimalMin("0.00000001")
    @Column(name = "auto_collect_amount")
    private BigDecimal autoCollectAmount;

    @Digits(integer=24, fraction=8)
    @DecimalMin("0.00000001")
    @Column(name = "auto_withdrawal_amount")
    private BigDecimal autoWithdrawalAmount;

    @Digits(integer=24, fraction=8)
    @DecimalMin("0.00000001")
    @Column(name = "withdrawal_tx_fee")
    private BigDecimal withdrawalTxFee;

    @Column(name = "once")
    private BigDecimal once;

    @Column(name = "one_day")
    private BigDecimal oneDay;

    @Column(name = "low_limit")
    private BigDecimal lowLimit;

    @Column(name = "min_amount")
    private BigDecimal minAmount;

    private String unit;

    public Coin() {

    }

    public Coin(String name) {
        this.name = name;
    }
}
