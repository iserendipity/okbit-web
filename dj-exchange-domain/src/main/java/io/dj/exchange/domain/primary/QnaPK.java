package io.dj.exchange.domain.primary;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;

@Data
public class QnaPK implements Serializable {
    @Id
    Long id;

    @Id
    Long userId;
}
