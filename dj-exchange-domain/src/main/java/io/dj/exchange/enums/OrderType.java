package io.dj.exchange.enums;

/**
 * Created by kun7788 on 2016. 11. 20..
 */
public enum OrderType {
    BUY, SELL
}
