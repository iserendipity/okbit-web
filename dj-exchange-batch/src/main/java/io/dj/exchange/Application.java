package io.dj.exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ScheduledExecutorFactoryBean;

@SpringBootApplication
@EnableScheduling
//@EntityScan("io.dj.exchange.domain")
//@EnableJpaRepositories(basePackages = "io.dj.exchange.repository.hibernate")
@EnableRedisRepositories(basePackages = {"io.dj.exchange.repository.cache"})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public ScheduledExecutorFactoryBean scheduledExecutorService() { ScheduledExecutorFactoryBean bean = new ScheduledExecutorFactoryBean(); bean.setPoolSize(5); return bean; }



}
