package io.dj.exchange.repository.hibernate.primary;

import io.dj.exchange.domain.primary.MyHistoryOrders;
import io.dj.exchange.domain.primary.MyHistoryOrdersPK;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Project dj-exchange-okbit.
 * Created by jeongwoo on 2017. 5. 12.
 * Description
 */
public interface MyOrdersRepository extends JpaRepository<MyHistoryOrders, MyHistoryOrdersPK> {

    //@Query(value="select p from MyHistoryOrders p where (p.userId = :userId or p.toUserId = :userId) order by p.id desc")
    Page<MyHistoryOrders> findAllByUserIdOrderByIdDesc(Long userId, Pageable p);

    @Query(value="select p from MyHistoryOrders p where p.userId = :userId and (p.fromCoin.name = :fromCoinName or p.toCoin.name = :toCoinName) order by id desc")
    Page<MyHistoryOrders> findAllByUserIdAndFromCoinNameOrToCoinNameOrderByIdDesc(@Param("userId") Long userId, @Param("fromCoinName") String fromCoinName, @Param("toCoinName") String toCoinName, Pageable p);

    @Query(value="select p from MyHistoryOrders p where p.userId = :userId and (p.fromCoin.name = :fromCoinName or p.toCoin.name = :toCoinName) order by id desc")
    List<MyHistoryOrders> findAllByUserIdAndFromCoinNameOrToCoinNameOrderByIdDesc(@Param("userId") Long userId, @Param("fromCoinName") String fromCoinName, @Param("toCoinName") String toCoinName);
}
